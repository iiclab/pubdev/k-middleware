// =========================================================================
/**
 *  @filename	Versioned_Namespace.h
 *	@Revised
 *  @Revision
 *  @Author		SeongHoon Kim
 *  
 *  @Description
 *	for supporting versioned namespace.
 *	This is useful for preventing conflicts when using a third party library.
 *	@NOTE
 *	OSB Operating System Service Broker
 */	
// =========================================================================
#ifndef _OSB_VERSIONED_NAMESPACE_H_
#define _OSB_VERSIONED_NAMESPACE_H_


# ifndef OSB_VERSIONED_NAMESPACE
//#  include "ace/Version.h"
// 
#define OSB_MAKE_VERSIONED_NAMESPACE_IMPL(MAJOR,MINOR,BETA) MAJOR ## _ ## MINOR ## _ ## BETA
#define OSB_MAKE_VERSIONED_NAMESPACE(MAJOR,MINOR,BETA) OSB_MAKE_VERSIONED_NAMESPACE_IMPL(MAJOR,MINOR,BETA)
#define OSB_VERSIONED_NAMESPACE OSB_MAKE_VERSIONED_NAMESPACE(MAJOR_VERSION,MINOR_VERSION,BETA_VERSION)

#define OSB_BEGIN_VERSIONED_NAMESPACE_DECL namespace OSB_VERSIONED_NAMESPACE {
#define OSB_END_VERSIONED_NAMESPACE_DECL } \
  using namespace OSB_VERSIONED_NAMESPACE;

#else

#define OSB_VERSIONED_NAMESPACE

#define OSB_BEGIN_VERSIONED_NAMESPACE_DECL
#define OSB_END_VERSIONED_NAMESPACE_DECL

#endif  /* ACE_HAS_VERSIONED_NAMESPACE */




#endif//_OSB_VERSIONED_NAMESPACE_H_


