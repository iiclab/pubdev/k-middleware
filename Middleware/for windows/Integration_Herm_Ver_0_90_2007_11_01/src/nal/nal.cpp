#include "nal.h"
#include "NetworkManager.h"
#include "Debug.h"

class BroadcastTimer : public EventHandler
{
public:

	void handleEvent()
	{	
		MW_DEBUG((LM_DEBUG,"[NAL] Now,Broadcast Timer is expired !!\n"));

		//purge id in broadcast table
		pBCAST_->purge();

		//request again
		pVPL_->BroadcastTimerRequest(BCAST_ID_SAVE);
	}

	BroadcastTimer(NAL *pVPL, BroadcastID *pBT): pVPL_(pVPL), pBCAST_(pBT)
	{
	}

private:
	NAL *pVPL_;
	BroadcastID *pBCAST_;
};

class NALDataConfirm : public OutputCallbackHandler 
{
public:    
	NALDataConfirm(NAL *pNAL): pLayer_(pNAL) {}
	virtual ~NALDataConfirm(){}

	void handleOutput(SentStatus_t *pSentStatus)
	{
		MW_DEBUG((LM_DEBUG,"NALDataConfirm::handleOutput status: %04x BytesTransfered %d\n", pSentStatus->status_, pSentStatus->BytesTransfered_));
		pLayer_->dataConfirm(pSentStatus);
	}	
private:
	NAL *pLayer_;
};

class NALDataIndication : public InputCallbackHandler 
{
public:    
	NALDataIndication(NAL *pNAL): pLayer_(pNAL)	{}
	virtual ~NALDataIndication(){}

	void handleInput(void *arg, Msg_t *msg)
	{	
		pLayer_->dataIndication(arg, msg);	
	}
	NAL * pLayer_;
};

NAL::NAL(NIL *pLowerLayer): pLowerLayer_(pLowerLayer)
{
	//Init Routing Table
	pRoutingTable_ = RoutingTable::instance();

	//Init Attributes Network Manager
	pNetworkManager_ = NetworkManager::instance();
	pNetworkManager_->setLayer(this);

	//Init Message Broker
	pMessageBroker_ = MessageBroker::instance();

	//Init Timer
	pTimerManager_ = TimerManager::instance(_DEFAULT_TIMER_KEY_);

	//Init Broadcast ID
	pBroadcastID_ = BroadcastID::instance();

	pNALDataIndicationCB_ = new NALDataIndication(this);
	pNALDataConfirmCB_ = new NALDataConfirm(this);

	pUpperLayerDataConfirm_ = NULL;
	pUpperLayerDataIndication_ = NULL;

	state_ = NAL_INITIAL;
}

NAL::~NAL()
{
	OSAL_DELETE(pNALDataIndicationCB_);
	OSAL_DELETE(pNALDataConfirmCB_);
}

void 
NAL::initialize(byte mwAddr)
{
	int status;
	DEBUGTRACE("NAL::initialize");

	//Set Callback
	pLowerLayer_->setDataConfirmCB(pNALDataConfirmCB_);
	pLowerLayer_->setDataIndicationCB(pNALDataIndicationCB_);

	//Network Formation 
	if (state_ == NAL_INITIAL)
	{
		status = NetworkFormation(mwAddr);
	}

	//Send Routing Information Request (RIR) to neighbor middleware
	if (status == SUCCESS)
	{
		byte Buf[BytesOfRoutingInformationRequest];
		Msg_t outMsg = { BytesOfRoutingInformationRequest, Buf};

		NIB NAL_NIB;
		getRequest(NIBAll, &NAL_NIB);

		//Construct Routing Information Request message
		byte frmCtrl = NAL_MAKE_MSGFrmCtrl(MSG_TYPE_CMD, NAL_NIB.ProtocolVer_, 0x00);	// CMD
		byte* pData = outMsg.pData_;
		*pData++ = frmCtrl;					//frame control
		*pData++ = NAL_BROADCAST_ALL;		//destination
		*pData++ = NAL_NIB.ModuleID_;		//source
		*pData++ = NAL_NIB.SequenceNum_++;	//sequence no	
		*pData = NAL_CMD_ROUTING_INFO_REQ;
		setRequest(NIBSeqNo, &NAL_NIB);		

		//Set Timer for Neighbor Response message
		//RoutingInformationRequestHandler *SendDistributeMsghandler = new RoutingInformationRequestHandler(pLowerLayer_);
		//pTimerManager_->setTimer(10000,SendDistributeMsghandler);

		//Send MSG to lower layer
		pLowerLayer_->dataRequest(RIOL_BROADCAST, &outMsg);
	} else
	{
		//network formation result equals to fail
		//so, require process to solve corresponding fail
	}

	//Join Request
	status = joinRequest();

	if (status == SUCCESS)
	{
		//this module is initiated
		//change state
		state_ = NAL_ACTIVE;

		//Set Timer for Broadcast Table
		BroadcastTimer *btTimerHandler = new BroadcastTimer(this, pBroadcastID_);
		pTimerManager_->setTimer(BCAST_ID_SAVE,btTimerHandler);

	}else
	{
		//stay state
		state_ = NAL_FAIL;

		//re-try to initialize 

	}
}

int
NAL::dataRequest(void* arg, Msg_t* inMsg)
{
	DEBUGTRACE("NAL::dataRequest");
	RoutingEntry *rt = NULL;

	//check NAL state to process
	if (state_ & NAL_NON_OPERATIONAL)
	{
		//
		//MW_DEBUG((LM_DEBUG,"This message is dropped due to NAL state"));
		//return -1;
	}

	DataRequestParam *pReqParam = (DataRequestParam *)arg;
	byte dstAddrMode  = pReqParam->dstAddrMode_;
	byte dstAddr	  = pReqParam->dstAddr_;
	byte msgPrior	  = pReqParam->msgPriority_;
	byte nalMsg       = pReqParam->nalMsg_;
	byte reqChannelID = pReqParam->reqChID_;

	if (nalMsg)
	{
		MW_DEBUG((LM_DEBUG,"[NAL:SEND] DataRequest [NAL-CMD] SIZE <%04x>\n", inMsg->len_));
	}
	else                 
	{
		MW_DEBUG((LM_DEBUG,"[NAL:SEND] DataRequest [SL-DATA] SIZE <%04x>\n", inMsg->len_));
	}

	int msgLen = defHdrLength + inMsg->len_;
	byte Buf[maxNALMessageLength];

	Msg_t outMsg;
	outMsg.len_   = msgLen;
	outMsg.pData_ = Buf;

	//construct message hdr 
	NIB NAL_NIB;
	getRequest(NIBAll, &NAL_NIB);

	//construct Frame control field
	byte frmCtrl = NAL_MAKE_MSGFrmCtrl(MSG_TYPE_DATA, NAL_NIB.ProtocolVer_, 0x00);	// DATA

	if (nalMsg)
		frmCtrl = NAL_MAKE_MSGFrmCtrl(MSG_TYPE_CMD, NAL_NIB.ProtocolVer_, 0x00);		// CMD

	//set msg header
	byte* pData = outMsg.pData_;
	*pData++ = frmCtrl;					//frame control
	*pData++ = dstAddr;					//destination
	*pData++ = NAL_NIB.ModuleID_;		//source
	*pData++ = NAL_NIB.SequenceNum_++;	//sequence no	
	setRequest(NIBSeqNo, &NAL_NIB);

	// copy message from higher layer
	memcpy(pData, inMsg->pData_, inMsg->len_);

	//TODO:
	//Consider nsduHandle and DataType

	//send message directly
	if (dstAddrMode == 0x04)
	{
		return (pLowerLayer_->dataRequest(reqChannelID, &outMsg));
	}    

	//Broadcast message (ALL OR GROUP)
	if (dstAddrMode == 0x01 || ((dstAddr & 0x0F) == 0x0F) )
	{
		return (pLowerLayer_->dataRequest(RIOL_BROADCAST, &outMsg));
	} 

	// lookup corresponding route entry in routing table
	if((rt = pRoutingTable_->lookup(dstAddr, SHORTEST_PATH)) == NULL)
	{	
		//if not
		//add route entry for destination

		//queuing packet for discovering route 

		//route discovery
		//Set Timer to wait time for discovery

		//return (pLowerLayer_->dataRequest(rt->rtNextAddr_, &outMsg));
		return 0;
	}

	//if so
	if(rt->rtStatus_ == RT_ACTIVE)
	{
		MW_ASSERT(rt->rtHops_ != INFINITY);
		return (pLowerLayer_->dataRequest(rt->rtNextAddr_, &outMsg));
	}

	return DATA_REQUEST_FAIL;
}

void 
NAL::dataConfirm(SentStatus_t* pSendStatus)
{
	DEBUGTRACE("NAL::dataConfirm");

	if (pUpperLayerDataConfirm_ != NULL)
	{
		pUpperLayerDataConfirm_->handleOutput(pSendStatus);	
		MW_DEBUG((LM_DEBUG,"NAL::dataConfirm\n"));
	}
}

void 
NAL::dataIndication(void *arg, Msg_t* msg)
{
	DEBUGTRACE("NAL::dataIndication");

	NILDEIncomingData_t *pIncoming = (NILDEIncomingData_t *)arg;
	int chID = pIncoming->channelID_;
	MW_DEBUG((LM_DEBUG,"[NAL:RECV] Data Indication ChID: [%d] DATA SIZE <%d>\n", chID, msg->len_));

	//check NAL state to process
	if (state_ != NAL_INACTIVE)
	{
		//put corresponding message in queue
		//	MW_DEBUG((LM_DEBUG,"This message is dropped due to NAL state"));
		//	return;
	}

	byte *pData = msg->pData_;
	byte fc =  *pData++;	// NAL header
	byte dst = *pData++;	// dst
	byte src = *pData++;	// src
	byte seq = *pData;		// sequence no.

	NIB myNIB;
	this->getRequest(NIBMuduleID, &myNIB); 

	//I am source (blocking loop)
	if (src == myNIB.ModuleID_)
	{
		//MW_DEBUG((LM_DEBUG,"*********************************************\n"));
		//MW_DEBUG((LM_DEBUG,"[NAL:LoopBack MSG] Src ID: [%02x]\n",src, myNIB.ModuleID_));
		MW_DEBUG((LM_DEBUG,"[NAL] The packet is dropped!!\n"));
		//MW_DEBUG((LM_DEBUG,"*********************************************\n"));
		return;
	}
	//문제****Source ID 가 A로 전달되어 클라이언트에는 0a로 전달됨..****//
	//check broadcast packet 
	bool ThisIsBroadcastPkt = FALSE; 	
	if (dst == NAL_BROADCAST_ALL || ( (dst & 0x0F) == 0x0F ) )
		ThisIsBroadcastPkt = TRUE;

	if (ThisIsBroadcastPkt)
	{
		//here is destination, check duplicate message
		if (pBroadcastID_->lookup(src, seq))
		{
			//Drop the message
			return;
		}

		//Insert Broadcast Information 
		pBroadcastID_->insert(src, seq);
	}

	//Check whether this message is Control message
	if (fc & FC_MSG_TYPE_CMD)
	{
		//eliminate header from message
		int msgLen = msg->len_ - defHdrLength;
		Msg_t payload = {msgLen, &msg->pData_[4]};

		//control message does not consider broadcast type
		pMessageBroker_->recvNAL(arg, src, &payload);
		return;
	}

	//	String str = pIncoming->pIOChannel_->getOwnDevice()->getName();		
	//	MW_DEBUG((LM_DEBUG,"Data Indication From %s (%d)\n", &str[0] , chID));

	//information for the higher layer
	NALIncomingData_t NALIncoming;
	NALIncoming.srcIOChannelID_ = chID;
	NALIncoming.srcAddr = src;

	//this is DATA pkt
	if (ThisIsBroadcastPkt || dst == myNIB.ModuleID_)
	{	
		//forward for broadcast packet
		if (ThisIsBroadcastPkt)
		{	
			//Forward the message, again
			pLowerLayer_->dataRequest(RIOL_BROADCAST, msg);
		}

		//check group broadcast packet
		if ( ThisIsBroadcastPkt && !(dst & myNIB.ModuleID_ & 0xF0) )
		{
			//Drop the message
			return;
		}

		//eliminate header from message
		int msgLen = msg->len_ - defHdrLength;

		//send data to upper layer
		Msg_t data = {msgLen, &msg->pData_[4]};
		if (pUpperLayerDataIndication_ != NULL)
			pUpperLayerDataIndication_->handleInput(&NALIncoming, &data);
		else //this is just for test
		{
			//return data back to source
			DataRequestParam DataReqParam;
			DataReqParam.dstAddr_ = src;	
			DataReqParam.msgPriority_ = 30;	
			DataReqParam.nalMsg_ = FALSE;
			DataReqParam.reqChID_ = SHORTEST_PATH;
			this->dataRequest(&DataReqParam , &data);
		}

		return;
	}

	//This module is not destination, so, multi-hop transmission
	//lookup table
	RoutingEntry *rt = NULL;
	if( (rt = pRoutingTable_->lookup(dst, SHORTEST_PATH)) == NULL)
	{	
		//if not
		//add route entry for destination

		//queuing packet for discovering route 

		//route discovery
		//Set Timer to wait time for discovery

		//return (pLowerLayer_->dataRequest(rt->rtNextAddr_, &outMsg));
		return;
	}

	//if so
	if(rt->rtStatus_ == RT_ACTIVE)
	{
		MW_ASSERT(rt->rtHops_ != INFINITY);
		pLowerLayer_->dataRequest(rt->rtNextAddr_, msg);
	}
}


int 
NAL::joinRequest()
{
	DEBUGTRACE("NAL::joinRequest");
	int status = pNetworkManager_->processJoinRequest();

	if (status == SUCCESS)
	{

	}else
	{

	}

	return SUCCESS;
}

void 
NAL::resetRequest()
{
	DEBUGTRACE("NAL::resetRequest");

	//change state
	state_ = NAL_INITIAL;

	//remove callback for lower layer and upper layer

	//request reset to network manager
	pNetworkManager_->processResetRequest();

	//restart NAL
	//this->initialize();
}

void 
NAL::getRequest(NIBAenum NIBAttribute, NIB * NIBAttributeValue)
{
	DEBUGTRACE("NAL::datgetRequestaConfirm");
	pNetworkManager_->getAttribute(NIBAttribute, NIBAttributeValue);
}

void 
NAL::setRequest(NIBAenum NIBAttribute, NIB * NIBAttributeValue)
{	
	DEBUGTRACE("NAL::setRequest");
	pNetworkManager_->setAttribute(NIBAttribute, NIBAttributeValue);
}

int 
NAL::NetworkFormation(byte mwAddr)
{
	int status = pNetworkManager_->processNetworkFormation(mwAddr);
	return status;
}

void 
NAL::setDataIndicationCB(InputCallbackHandler *pDataIndication)
{
	pUpperLayerDataIndication_ = pDataIndication;
}

void 
NAL::setDataConfirmCB(OutputCallbackHandler *pDataConfirm)
{
	pUpperLayerDataConfirm_ = pDataConfirm;
}

NAL::NALState_t 
NAL::getState()
{
	return state_;
}

void 
NAL::setState(NAL::NALState_t state)
{
	state_ = state;
}

void 
NAL::BroadcastTimerRequest(int delay)
{
	BroadcastTimer *btTimerHandler = new BroadcastTimer(this, pBroadcastID_);
#ifdef OSAL_WIN32
	pTimerManager_->setTimer(delay,btTimerHandler);
#endif
}