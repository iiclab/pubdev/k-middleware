#ifndef _BROADCASTTABLE_H_
#define	_BROADCASTTABLE_H_

#include <vector>
#define BCAST_ID_SAVE 100000

typedef unsigned char byte;

class BroadcastEntry;

// Broadcast ID Cache
class BroadcastID
{
public:

	/** singleton access point */
	static BroadcastID * instance();
	virtual ~BroadcastID();

	void insert(byte src, byte seqNo);
	bool lookup(byte src, byte seqNo);
	void purge(void);	// flush broadcast table

protected:
	BroadcastID();

private:
	static BroadcastID * pInstance_;
	typedef std::vector<BroadcastEntry *> Broadcast_Vector;
	Broadcast_Vector v_broadcast_vector;

};

class BroadcastEntry 
{
	friend class BroadcastID;
	//	friend class NAL;
public:
	BroadcastEntry(byte srcAddr, byte seqNo)
	{
		src_ = srcAddr;
		seq_ = seqNo;
	}

protected:
	byte    src_;
	byte	seq_;
	//    double  expire_;   // now + BCAST_ID_SAVE s
};

#endif //_BROADCASTTABLE_H_
