#ifndef  RoutingEntry_H
#define  RoutingEntry_H

#define INFINITY		0xff	

#define RT_INACTIVE		0
#define RT_ACTIVE		1
#define RT_IN_REPAIR	2

typedef unsigned char byte;

class RoutingEntry 
{
	friend class RoutingTable;
	friend class NAL;
	friend class NetworkManager;
	// TimerClass will be added as Friend Class
public:
	RoutingEntry(int);
	~RoutingEntry();

	int getIOChannelIndex();

protected:	
	byte	rtDst_;			// Dst. Module Address
	byte	rtHops_;		// Hop Count
	unsigned int rtBW_;		// BW
	int		rtNextAddr_;	// Channel Address for next module
	int		rtStatus_;		// Entry Status
	int	rtInterfaceType_; // Interface List	

};
#endif // RoutingEntry_H
