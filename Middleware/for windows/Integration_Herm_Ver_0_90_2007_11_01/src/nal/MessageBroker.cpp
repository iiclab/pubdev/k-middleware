#include "MessageBroker.h"
#include "NetworkManager.h"
#include "Debug.h"
#include "NALDefine.h"
#include "NIB.h"

//** Static Member variables*/
MessageBroker *
MessageBroker::pInstance_ = NULL;

MessageBroker::MessageBroker() 
{ 
	lnkNetworkManager_= NetworkManager::instance();
}

MessageBroker * 
MessageBroker::instance()
{
	if(pInstance_ == NULL)
		pInstance_ = new MessageBroker;
	return pInstance_;
}

void
MessageBroker::recvNAL(void* arg, byte srcAddr, Msg_t* InputMsg)
{
	DEBUGTRACE("NAL::recvNAL");
	byte cmdType = InputMsg->pData_[0];

	//Incoming Command Message
	switch(cmdType)
	{
	case NAL_CMD_ROUTING_INFO_REQ:
		DEBUGTRACE("This message is Routing Information Request!\n");
		lnkNetworkManager_->processRoutingInformationRequest(arg, srcAddr);
		break;
	case NAL_CMD_ROUTING_INFO_REP:
		DEBUGTRACE("This message is Routing Information Response!\n");
		lnkNetworkManager_->processRoutingInformationResponse(arg, srcAddr, InputMsg);
		break;
	default:
		DEBUGTRACE("Invalid NAL type Message\n");
		exit(1);
	}
}
