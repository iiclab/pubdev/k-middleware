#ifndef NALDEFINE_H
#define NALDEFINE_H

#include "Types.h"
//#include "NetworkManager.h"
//#include "Utility/Msg_t.h"


#define NOT_DEFINED 0xFF

///////////////////////////////////
// NAL enum
///////////////////////////////////
typedef enum
{
	SUCCESS = 0,
	INVALID_PARAMETER,
	NET_FORM_FAIL,
	UNSUPPORTED_ATTRIBUTE,
	DATA_REQUEST_FAIL
} NALenum;

/** TYPEDEFS */
typedef enum 
{
	NAL_CHANNEL_LINK_USB11, 
	NAL_CHANNEL_LINK_USB20, 
	NAL_CHANNEL_LINK_IEEE1394, 
	NAL_CHANNEL_LINK_ETHERNET

} NAL_CHANNEL_LINK;

typedef enum 
{
	MODULE_INITIALIZE, 
	MODULE_READY,
	MODULE_START,
	MODULE_STOP,
	MODULE_FINALIZE

} MODULE_STATE;

#define MSG_TYPE_DATA	0
#define MSG_TYPE_CMD	1

#define BytesOfRoutingInformationRequest  5
#define BytesOfRoutingInformationResponse 4 + 2 + 10 * 2	//hdr + id + num + ..

#define maxNALMessageLength		200
#define defHdrLength			4  // 1 + 1 + 1 + 1
#define NumOfRouteDistribute    50*2

/* =====================================================================
NAL CONTROL PACKET IDENTIFIERS
===================================================================== */
#define NAL_CMD_ROUTING_INFO_REQ		0x01
#define NAL_CMD_ROUTING_INFO_REP		0x02
//#define MSG_TYPE_RESERVED		0x00, 0x03-0xFF


/* =====================================================================
NAL HEADER DEFAULT FIELD NAME
===================================================================== */
#define NAL_DEF_HDR_FRAME_CONTROL	0
#define NAL_DEF_HDR_DST_ID			1
#define NAL_DEF_HDR_SRC_ID			2
#define NAL_DEF_HDR_SEQ_NO			3
#define NAL_DEF_CMD_TYPE			4
//For route information response
#define NAL_ROUTE_NUMBER			5

/* =====================================================================
FRAME CONTROL FIELDS
===================================================================== */
//MESSAGE TYPE(Bits:0-1)
#define FC_MSG_TYPE_DATA		0x00
#define FC_MSG_TYPE_CMD			0x40
//#define MSG_TYPE_RESERVED		

//MODULE PROTOCOL VERSION(Bits:2-3)
#define FC_KNU_MW_DEVELOP_VER	0x00		// Develop Ver.
#define FC_KNU_MW_RELEASE_VER	0x10		// Release Ver.1.0
//RESERVED FIELD(Bit:4-7)

#define _C_ETHERNET_	10	//10.00 bps
#define _C_RS232C_		47	//47.17
#define _C_IEEE1394a_	 3	//3.98
#define _C_IEEE1394b_	 1	//0.97
#define _C_CAN2_0_		30	//30.00
#define _C_USB1_1_		19	//19.21
#define _C_USB2_0_		 3	//3.19

struct frmCtrl 
{
	byte MsgType_ : 2;
	byte ProtocolVer_ : 2;
	byte reserved_ : 4;
};

struct hdrNAL
{
	frmCtrl	FrmCtrl_;
	byte	src_;
	byte	dst_;
	byte	seqNo_;
};

#define NAL_MAKE_MSGFrmCtrl(msgType, ver, rev) (uint8) \
	(											\
	(((msgType) & 0x03) << 6) |					\
	(((ver) & 0x03) << 5) |						\
	((rev) & 0x00)								\
	)

#endif //NALDEFINE_H
