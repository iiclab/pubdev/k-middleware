#include "BroadcastTable.h"

//** Static Member variables*/
BroadcastID *
BroadcastID::pInstance_ = NULL;

BroadcastID::BroadcastID() 
{ 
}

BroadcastID *
BroadcastID::instance()
{
	if(pInstance_ == NULL)
		pInstance_ = new BroadcastID;
	return pInstance_;
}

BroadcastID::~BroadcastID()
{
}

void 
BroadcastID::insert(byte src, byte seqNo)
{
	BroadcastEntry *bt = new BroadcastEntry(src, seqNo);

	bool ThisEntryExists;
	ThisEntryExists = lookup(src, seqNo);

	if (!ThisEntryExists)
	{
		v_broadcast_vector.push_back(bt);	
	}
//	bt->expire_ = 0;	// Current time + Entry save time
}

bool 
BroadcastID::lookup(byte src, byte seqNo)
{
	Broadcast_Vector::iterator it;

	// First check if the entry already exists
	for (it=v_broadcast_vector.begin(); it != v_broadcast_vector.end(); it++)
	{
		if ( ((*it)->src_ == src) && ((*it)->seq_ == seqNo))
		{
			return true;
		}
	}
	return false;
}

void 
BroadcastID::purge()
{
	Broadcast_Vector::iterator pos;

	// First check if the entry already exists
	for (pos=v_broadcast_vector.begin(); pos != v_broadcast_vector.end(); pos++)
	{
		if (*pos)
		{	
			delete *pos;
			*pos = NULL;
		}
	}
	v_broadcast_vector.clear();
}