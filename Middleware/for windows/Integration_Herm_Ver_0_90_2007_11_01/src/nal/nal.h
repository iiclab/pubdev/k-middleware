#ifndef NAL_H
#define NAL_H

#include "RoutingTable.h"
#include "RoutingEntry.h"
#include "Utility/Msg_t.h"
#include "nil/nil.h"
#include "MessageBroker.h"
#include "NetworkManager.h"
#include "QuantumFramework/TimerManager.h"

#include "NALDefine.h"
#include "BroadcastTable.h"

#define SHORTEST_PATH			0x000000FE
#define RIOL_BROADCAST			0xFFFFFFFF

#define NAL_BROADCAST_ALL       0xFF
#define UNDIFEND_MODULE_ID		0xFE		//un-difined module ID

#define NAL_OPERATIONAL			0x03
#define NAL_NON_OPERATIONAL		0x0C

#define HELLO_MSG_INTERVAL		20000		// interval of periodical hello message

typedef struct  
{
	int srcIOChannelID_;
	byte srcAddr;	//source Middle-ware address

}NALIncomingData_t;

struct DataRequestParam
{
	byte dstAddr_;			//destination address
	byte dstAddrMode_;		//unicast/broadcast or multicast
	byte msgPriority_;		//message priority
	byte nalMsg_;			//frame type from NAL
	int	 reqChID_;			//request channel ID
};

class NALDataIndication;
class NALDataConfirm;
class RoutingTable;
class BroadcastID;
class MessageBroker;

class NAL
{	
public:
	//Enumeration of NAL state
	typedef enum NALState_t
	{
		NAL_CREATED	= 0x00,
		NAL_INITIAL	= 0x01,
		NAL_ACTIVE	= 0x02,
		NAL_INACTIVE= 0x04,
		NAL_FAIL	= 0x08
	};

	NAL(NIL *);
	virtual ~NAL();

	void initialize(byte );

	//*************************** Data Entity ******************************

	int dataRequest(void * , Msg_t* );
	void dataConfirm(SentStatus_t*);    
	void dataIndication(void *, Msg_t *);

	//*************************** Management Entity ******************************


	int joinRequest();
	void resetRequest();

	void getRequest(NIBAenum , NIB *);
	void setRequest(NIBAenum , NIB *);

	virtual int NetworkFormation(byte);
	void BroadcastTimerRequest(int);

	void setDataIndicationCB(InputCallbackHandler * pDataIndication);
	void setDataConfirmCB(OutputCallbackHandler * pDataConfirm);

	NAL::NALState_t getState();
	void setState(NAL::NALState_t );

private:    

	NetworkManager * pNetworkManager_;
	MessageBroker * pMessageBroker_;
	RoutingTable * pRoutingTable_;
	TimerManager * pTimerManager_;

	NALDataIndication * pNALDataIndicationCB_;	
	NALDataConfirm * pNALDataConfirmCB_;

	InputCallbackHandler *pUpperLayerDataIndication_;
	OutputCallbackHandler *pUpperLayerDataConfirm_;

	BroadcastID * pBroadcastID_;

	// Lower Layer
	NIL * pLowerLayer_;
	NALState_t state_;
};
#endif //NAL_H
