#include "RoutingTable.h"

//** Static Member variables*/
RoutingTable *
RoutingTable::pInstance_ = NULL;

RoutingTable::RoutingTable()
{ 
}

RoutingTable::~RoutingTable()
{
}

RoutingTable * 
RoutingTable::instance()
{
	if(pInstance_ == NULL)
		pInstance_ = new RoutingTable;
	return pInstance_;
}

RoutingEntry *
RoutingTable::add(byte dstAddr, int chID)
{

	RoutingEntry *rt = new RoutingEntry(chID);

	RoutingEntry *ThisEntryExists;
	ThisEntryExists = lookup(dstAddr, chID);

	if (!ThisEntryExists)
	{		
		m_routing_multimap.insert(std::make_pair(dstAddr, rt));
	}

	return rt;
}

void
RoutingTable::del(byte dstAddr, int chID)
{
	Routing_Multimap::iterator pos;

	// First check if the entry already exists
	for (pos=m_routing_multimap.begin(); pos != m_routing_multimap.end(); pos++)
	{
		if((pos->first == dstAddr) && ((pos->second)->rtNextAddr_ == chID))
		{
			delete pos->second;
			pos->second = NULL;
			break;
		}
	}
	m_routing_multimap.erase(pos);
}

RoutingEntry *
RoutingTable::lookup(byte dstAddr, int chID)
{
	Routing_Multimap::iterator it;

	if (chID == SHORTEST_PATH)
	{
		for (it = m_routing_multimap.begin(); it != m_routing_multimap.end(); it++)
		{
			//TODO:Consider a shortest path in multiple routes
			if (it->first == dstAddr)
				return it->second;
		}
	}
	// First check if the entry already exists
	for (it = m_routing_multimap.begin(); it != m_routing_multimap.end(); it++)
	{
		if ((it->first == dstAddr) && ((it->second)->rtNextAddr_ == chID))
			return (it->second);
	}

	return NULL;
}

int
RoutingTable::getSize()
{
	return m_routing_multimap.size();
}
