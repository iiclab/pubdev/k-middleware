#ifndef NetworkManager_H
#define NetworkManager_H

#include "NIB.h"
#include "NALDefine.h"
#include "Utility/Msg_t.h"
#include "RoutingTable.h"
#include "nal.h"
//#include "BroadcastTable.h"

class MessageBroker;
class BroadcastID;
class RoutingTable;
class NAL;

class NetworkManager 
{
public:

	/** singleton access point */
	static NetworkManager * instance();
	virtual ~NetworkManager();

	virtual int processNetworkFormation(byte);
	virtual int processResetRequest();
	virtual int processJoinRequest(); 
	//    virtual int processRouteDiscoveryReponse();

	virtual NALenum setAttribute(NIBAenum NIBAddribute, NIB * NIBAttributeValue);
	virtual NALenum getAttribute(NIBAenum NIBAddribute, NIB * NIBAttributeValue);

	virtual int processRoutingInformationRequest(void* arg, byte srcAddr);
	virtual void processRoutingInformationResponse(void* arg, byte srcAddr, Msg_t* InputMsg);

	virtual void setLayer(NAL* pNAL); 

protected :
	NetworkManager();

private:
	NIB lnkNIB;
	RoutingTable * lnkRoutingTable_;
	NAL * pLayer;
	static NetworkManager * pInstance_;
};

#endif //NetworkManager_H
