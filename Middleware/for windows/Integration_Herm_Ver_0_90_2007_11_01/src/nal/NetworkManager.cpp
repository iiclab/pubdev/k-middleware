#include "NetworkManager.h"
#include "nal.h"
#include "MessageBroker.h"
#include "Debug.h"
#include "configAll.h"
#include <fstream>
#include <iostream>
#include <iomanip>

static int iofc1 = 0;
static int iofc2 = 0;

//** Static Member variables*/
NetworkManager *
NetworkManager::pInstance_ = NULL;

static NIB VNIB = 
{
	DEF_MODULE_ID, 
	DEF_SEQ,
	DEF_VER
};

NetworkManager::NetworkManager()
{
	//Create NIB 
	lnkNIB = VNIB;

	lnkRoutingTable_ = RoutingTable::instance();
}

NetworkManager::~NetworkManager()
{

}

NetworkManager * 
NetworkManager::instance()
{
	if(pInstance_ == NULL)
		pInstance_ = new NetworkManager;
	return pInstance_;
}

//int NetworkManager::processRouteDiscoveryReponse(){ }

NALenum 
NetworkManager::setAttribute(NIBAenum NIBAttribute, NIB * NIBAttributeValue)
{
	NALenum status = SUCCESS;

	switch(NIBAttribute)
	{		
	case NIBMuduleID:
		lnkNIB.ModuleID_ = NIBAttributeValue->ModuleID_;
		break;
	case NIBSeqNo:
		lnkNIB.SequenceNum_  = NIBAttributeValue->SequenceNum_;
		break;
	case NIBProtoclVersion:
		lnkNIB.ProtocolVer_ = NIBAttributeValue->ProtocolVer_;
		break;
	default:
		status = UNSUPPORTED_ATTRIBUTE;
	}

	return status;
}

NALenum 
NetworkManager::getAttribute(NIBAenum NIBAttribute, NIB * NIBAttributeValue)
{
	NALenum status = SUCCESS;

	switch(NIBAttribute)		
	{		
	case NIBMuduleID:
		NIBAttributeValue->ModuleID_ = lnkNIB.ModuleID_;
		break;
	case NIBSeqNo:
		NIBAttributeValue->SequenceNum_ = lnkNIB.SequenceNum_;
		break;
	case NIBProtoclVersion:
		NIBAttributeValue->ProtocolVer_ = lnkNIB.ProtocolVer_;
		break;
	case NIBAll:
		NIBAttributeValue->ModuleID_ = lnkNIB.ModuleID_;
		NIBAttributeValue->SequenceNum_ = lnkNIB.SequenceNum_;
		NIBAttributeValue->ProtocolVer_ = lnkNIB.ProtocolVer_;
		break;
	default:
		status = UNSUPPORTED_ATTRIBUTE;
	}

	return status;
}

int
NetworkManager::processNetworkFormation(byte mwAddr)
{
	NIB NAL_NIB;
	RoutingEntry * rt;
	int status = SUCCESS;

	//Must assign Source Address
	if (status == SUCCESS) 
	{
		NAL_NIB.ModuleID_ = mwAddr;		
		status = this->setAttribute(NIBMuduleID, &NAL_NIB);
	}

	//add own information in routing table
	rt = lnkRoutingTable_->add(mwAddr, 0);
	rt->rtHops_ = 0;
	rt->rtStatus_ = RT_ACTIVE;
	rt->rtDst_ = mwAddr;

	if (status != SUCCESS)
	{
		//Report Error 
		status = NET_FORM_FAIL;
	}

	return status;		
}

int
NetworkManager::processJoinRequest() 
{
	//this function is not supported from current middleware version
	//if middleware is in dynamic environment, this function will be implemented

	return SUCCESS;
}

int
NetworkManager::processRoutingInformationRequest(void *arg, byte srcAddr)
{
	RoutingEntry * rt;
	int status = SUCCESS;

	NILDEIncomingData_t *pIncoming = (NILDEIncomingData_t *)arg;
	int chID = pIncoming->channelID_;

	//************ Process Routing Information Request ****************************

	//Lookup route entry
	rt = lnkRoutingTable_->lookup(srcAddr, chID);

	if (!rt)
	{
		//Add route entry
		rt = lnkRoutingTable_->add(srcAddr, chID);
		rt->rtHops_ = 1;			//request message from neighbor middleware
		rt->rtStatus_ = RT_ACTIVE;	
		rt->rtDst_ = srcAddr;
		rt->rtInterfaceType_ = pIncoming->interfaceType_;

		//cost
		switch(rt->rtInterfaceType_){
		case _INTERFACE_ETHERNET_:
			rt->rtBW_ =_C_ETHERNET_;
			break;
		case _INTERFACE_RS232C_:
			rt->rtBW_ = _C_RS232C_;
			break;
		case _INTERFACE_IEEE1394a_:
			rt->rtBW_ =_C_IEEE1394a_;
			break;
		case _INTERFACE_IEEE1394b_:
			rt->rtBW_ = _C_IEEE1394b_;
			break;
		case _INTERFACE_CAN2_0_:
			rt->rtBW_ = _C_CAN2_0_;
			break;
		case _INTERFACE_USB1_1_:
			rt->rtBW_ =_C_USB1_1_;
			break;
		case _INTERFACE_USB2_0_:
			rt->rtBW_ = _C_USB2_0_;
			break;
		default:
			rt->rtBW_ = 0; 
			break;
		}

		MW_DEBUG((LM_DEBUG,"******************************************\n"));
		MW_DEBUG((LM_DEBUG," Middleware %02x is added in Routing Table\n", srcAddr));
		MW_DEBUG((LM_DEBUG,"RT: DstAddr(%02x) NextChID(%d) hop(%d) CostBW(%d) Status(%d) \n", rt->rtDst_, chID, rt->rtHops_, rt->rtBW_, rt->rtStatus_));
		MW_DEBUG((LM_DEBUG,"******************************************\n"));
	}

	//************ Reply to Routing Information Request ****************************

	//bring routing size  
	const int tmpRtSize = lnkRoutingTable_->getSize();
	byte rtData[NumOfRouteDistribute];
	int rtSize = 0;

	//bring routing table entries into an array
	RoutingTable::Routing_Multimap::iterator pos;
	for (pos=lnkRoutingTable_->m_routing_multimap.begin(); pos != lnkRoutingTable_->m_routing_multimap.end(); pos++)
	{
		if ((pos->second)->rtStatus_ != RT_ACTIVE) continue;
		rtData[rtSize*4]	 = (pos->second)->rtDst_;		//destination
		rtData[rtSize*4 + 1] = (pos->second)->rtHops_;		//hops
		rtData[rtSize*4 + 2] = (pos->second)->rtBW_;		//bandwidth	
		rtData[rtSize*4 + 3] = (pos->second)->rtInterfaceType_;//interface
		rtSize++;
	}

	//determine packet size including routing entries
	int pktSize = 1 + 1 + (rtSize * 4);

	//declare message
	byte Buf[NumOfRouteDistribute];
	Msg_t RoutingInformationResponseMsg = {pktSize, Buf};

	//make packet payload
	byte* pData = RoutingInformationResponseMsg.pData_;
	*pData++ = NAL_CMD_ROUTING_INFO_REP;	//control identifier
	*pData++ = rtSize;						//route numbers
	memcpy(pData, rtData, rtSize*4);		//routing table information

	//************ Data Request to NAL ****************************

	DataRequestParam DataReqParam;
	DataReqParam.dstAddr_ = srcAddr;
	DataReqParam.dstAddrMode_ = 0x00;		//unicast
	DataReqParam.msgPriority_ = 0x01;		//command message
	DataReqParam.nalMsg_ = 0x01;			//NAL CMD
	pLayer->dataRequest(&DataReqParam, &RoutingInformationResponseMsg);

	return status;
}
void
NetworkManager::processRoutingInformationResponse(void *arg, byte srcAddr, Msg_t* InputMsg)
{
	RoutingEntry * rt; 
	int status = SUCCESS;
	bool ChangedRT = FALSE;

	NILDEIncomingData_t *pIncoming = (NILDEIncomingData_t *)arg;
	int currentChannelID = pIncoming->channelID_;
	//	byte srcAddr = InputMsg->pData_[NAL_DEF_HDR_SRC_ID];

	//Check the number of middleware
	int rtSize = InputMsg->pData_[1];

	// bring information of current middleware
	NIB NAL_NIB;
	getAttribute(NIBAll, &NAL_NIB);
	for(int i=1, j=0 ; i<=rtSize ; i++, j++)
	{
		//Check corresponding Address
		byte *dstAddr	= &InputMsg->pData_[2*(i+j)];
		byte *pathCost	= &InputMsg->pData_[(2*(i+j))+1];
		byte *pathBWCost = &InputMsg->pData_[(2*(i+j))+2];
		byte *InterfaceType = &InputMsg->pData_[(2*(i+j))+3];

		//add BandWidth to pathcost
		byte relativePathBWCost;
		byte relativePathCost;

		//Check my module ID 
		if (*dstAddr == NAL_NIB.ModuleID_) 
		{
			(*pathBWCost) = 0;
			(*pathCost) = 0;
			continue;
		} else
		{	

			if(srcAddr == *dstAddr)
				*InterfaceType = pIncoming->interfaceType_;

			//add BandWidth to pathcost
			switch(*InterfaceType){
			case _INTERFACE_ETHERNET_:	
				relativePathBWCost = *pathBWCost + _C_ETHERNET_;
				break;
			case _INTERFACE_RS232C_:
				relativePathBWCost = *pathBWCost + _C_RS232C_;
				break;
			case _INTERFACE_IEEE1394a_:
				relativePathBWCost = *pathBWCost + _C_IEEE1394a_;
				break;
			case _INTERFACE_IEEE1394b_:
				relativePathBWCost = *pathBWCost + _C_IEEE1394b_;
				break;
			case _INTERFACE_CAN2_0_:
				relativePathBWCost = *pathBWCost + _C_CAN2_0_;
				break;
			case _INTERFACE_USB1_1_:
				relativePathBWCost = *pathBWCost + _C_USB1_1_;
				break;
			case _INTERFACE_USB2_0_:
				relativePathBWCost = *pathBWCost + _C_USB2_0_;
				break;
			default:
				relativePathBWCost = *pathBWCost;
				break;
			}
			relativePathCost = *pathCost + 1;
		}

		//Check routing table 
		rt = lnkRoutingTable_->lookup(*dstAddr, currentChannelID);

		if (!rt)
		{
			//Add routing entry
			rt = lnkRoutingTable_->add(*dstAddr, currentChannelID);
			rt->rtHops_ = relativePathCost;
			rt->rtBW_ = relativePathBWCost;
			rt->rtStatus_ = RT_ACTIVE;
			rt->rtDst_ =  *dstAddr;
			rt->rtInterfaceType_ = *InterfaceType;

			MW_DEBUG((LM_DEBUG,"*************************************************\n"));
			MW_DEBUG((LM_DEBUG,"Another Middleware %02x is added in Routing Table\n", *dstAddr));
			MW_DEBUG((LM_DEBUG,"RT: DstAddr(%02x) NextChID(%d) hop(%d) CostBW(%d) Status(%d) \n", rt->rtDst_, currentChannelID,  rt->rtHops_, rt->rtBW_, rt->rtStatus_));
			MW_DEBUG((LM_DEBUG,"*************************************************\n"));

			ChangedRT = TRUE;
		}else
		{
			//Compared with current routing entry
			if ( relativePathCost < rt->rtHops_)
			{
				//if better, Update routing entry
				rt->rtHops_ = relativePathCost;
				ChangedRT = TRUE;
			}

			//Add 1 hop to each routing entry for routing cost
			*pathCost = relativePathCost;

			if ( relativePathBWCost < rt->rtBW_)
			{
				rt->rtBW_ = relativePathBWCost;
				ChangedRT = TRUE;
			}

			*pathBWCost = relativePathBWCost;
		}
	}

	//************ Data Request to NAL ****************************

	if (ChangedRT)
	{
		DataRequestParam DataReqParam;
		DataReqParam.dstAddr_ = NAL_BROADCAST_ALL;
		DataReqParam.dstAddrMode_ = 0x01;		//broadcast
		DataReqParam.msgPriority_ = 0x01;		//command message
		DataReqParam.nalMsg_ = 0x01;			//NAL CMD
		pLayer->dataRequest(&DataReqParam, InputMsg);
	}
}

int
NetworkManager::processResetRequest() 
{ 

	return SUCCESS;
}

void
NetworkManager::setLayer(NAL* pNAL)
{
	pLayer = pNAL;
}

