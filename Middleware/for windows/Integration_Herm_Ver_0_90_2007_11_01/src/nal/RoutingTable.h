#ifndef RoutingTable_H
#define RoutingTable_H

#include <map>
#include "RoutingEntry.h"
#include "nal.h"

class RoutingEntry;

class RoutingTable {
	friend class NetworkManager;
	friend class NAL;
public:

	/** singleton access point */
	static RoutingTable * instance();
	~RoutingTable();

	RoutingEntry * add(byte dstAddr, int chID);
	void del(byte dstAddr, int chID);	
	RoutingEntry * lookup(byte dstAddr, int	chID);

	int getSize();

protected:
	RoutingTable();

private:
	static RoutingTable * pInstance_;
	typedef std::multimap<byte, RoutingEntry *> Routing_Multimap;
	Routing_Multimap m_routing_multimap;
};
#endif //RoutingTable_H
