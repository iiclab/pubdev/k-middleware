#ifndef MessageBroker_H
#define MessageBroker_H

#include "NALDefine.h"
#include "Utility/Msg_t.h"

class NetworkManager;

class MessageBroker 
{

public:
    ~MessageBroker();
    static MessageBroker * instance();
    
	void recvNAL(void *, byte, Msg_t* );

protected:
	MessageBroker();

private:
	static MessageBroker * pInstance_;
    NetworkManager * lnkNetworkManager_;
};
#endif //MessageBroker_H
