#ifndef NIB_H
#define NIB_H

#include "NALDefine.h"
#include "Types.h"

typedef enum
{
	NIBMuduleID = 0,
	NIBSeqNo,
	NIBProtoclVersion,
	NIBAll
} NIBAenum;

#define DEF_MODULE_ID		0x00	//default module ID
#define DEF_SEQ				0x00	//default Sequence number
#define DEF_VER				0x00	//default protocol version

struct NIB 
{
	byte	ModuleID_;		// Short Module ID
	byte	SequenceNum_;	// Sequence number
	byte	ProtocolVer_;
};

#endif //NIB_H
