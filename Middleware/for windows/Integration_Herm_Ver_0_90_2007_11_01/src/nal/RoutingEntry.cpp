#include "RoutingEntry.h"
#include "NALDefine.h"
#include "nil/nil.h"

RoutingEntry::RoutingEntry(int chID)
{
	rtDst_	= 0;
	rtHops_ = INFINITY;
	rtBW_	= 0;
	rtNextAddr_ = chID;
	rtStatus_ = RT_INACTIVE;
	rtInterfaceType_ = 0;

	//rt_expire = 0.0;
}
RoutingEntry::~RoutingEntry()
{

}

int 
RoutingEntry::getIOChannelIndex()
{
	return 0;
}
