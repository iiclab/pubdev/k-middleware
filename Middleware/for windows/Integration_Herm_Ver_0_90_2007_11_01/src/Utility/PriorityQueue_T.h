// =========================================================================
/**
 *  @filename	.h
 *	@Revised
 *  @Revision
 *  @Author		SeongHoon Kim
 *  
 *  @Description
 *		
 *	@NOTE
 *	
 */	
// =========================================================================
#ifndef PRIORITYQUEUE_H
#define PRIORITYQUEUE_H
#include <queue>
#include "PriorityEventHandler.h"

/**
 * Wrapper Class
 */
template < class T >
class PriorityQueue {
public:
	
	/**
	 * 	 @Returns the actual number of elements.
	 * 	 To check whether the priority queue is empty (contains no elements), 
	 * 	 use empty() because it might be faster. 
	 */
    int size();
	
	/**
	 * 	@Returns whether the priority queue is empty (contains no elements).
	 * 	It is equivalent to priority _queue::size()==0, but it might be faster.
	*/
	bool empty();

	/**
	 * 	 Inserts a copy of elem into the priority queue.
	 */
	void push(T &);
	
	/**
	 * 	 @Returns the next element of the priority queue. The next element is the element that, of
	 * 	all elements in the priority queue, has the maximum value. If more than one element has
	 * 	the maximum value, which element it returns is undefined.
	 * 	 @The caller must ensure that the queue contains an element (size()>0); otherwise, the 
	 * 	behavior is undefined.
	 */
	T & top() const;
	
/**
 * 	 @Removes the next element from the queue. The next element is the element that, of all 
 * 	elements in the priority queue, has the maximum value. If more than one element has 
 * 	the maximum value, which element it removes is undefined. 
 * 	 @Note that this function has no return value. To process the next element, you must call
 * 	top() first. 
 * 	 @The caller must ensure that the queue contains an element (size()>0); otherwise, the behavior is undefined.
 */
	void pop();

private:	
	std::priority_queue< T > pq_;
};


#include "PriorityQueue_T.cpp"


#endif //PRIORITYQUEUE_H
