/************************************************************************/
/*Implementer : Seong hoon Kim                                          */
/************************************************************************/
/**
 * Usage:
 //invoke init_timer
 init_timer();

 //Record current time
 start_timer(0);
	.... do something
 printf("first Time: %f\n",get_timer(0));

 start_timer(1);
 .. do something
 printf("second Time: %f\n",get_timer(1));
 */

#ifndef _TIMEINGPROC_H_
#define _TIMEINGPROC_H_

#ifdef __cplusplus 
extern "C" { 
#endif 

#define NUMTOMEASURE	10

#define USE_QUERYPERFORMANCECOUNTER


extern void init_timer();
//Record current time
extern void start_timer(int idx);

//Get number of seconds since last call to start_timer
extern double get_timer(int idx);

#ifdef  __cplusplus 
} 
#endif 

#endif	//_TIMEINGPROC_H_


