

#ifndef QUEUE_T_CPP
#define QUEUE_T_CPP
#include "Queue_T.h"


template < class T >
inline int 
Queue< T >::size()
{
	q_.size();
}

template < class T >
inline bool 
Queue< T >::empty()
{
	return q_.empty();
}

template < class T >
inline void 
Queue< T >::push(T &x)
{
	q_.push(x);
}

template < class T >
inline T & 
Queue< T >::front()
{
	return q_.front();
}

template < class T >
inline T & 
Queue< T >::back()
{
	return q_.back();
}

template < class T >
inline void 
Queue< T >::pop()
{
	q_.pop();
}

#endif//QUEUE_T_CPP