/***********************************************************************
Implementer : Seong hoon Kim
This if to measure timing procedures on both WIN32 and LINUX KERNEL
This code is very portable, but its accuracy depends on how the clock 
is implemented.
***********************************************************************/
#ifdef WIN32
#include <winsock2.h>
#include <time.h>

#else

#include <sys/time.h>
#include <unistd.h>

#endif

#include <stdio.h>
#include "timingproc.h"




#ifdef WIN32

#	if defined(USE_QUERYPERFORMANCECOUNTER)
LARGE_INTEGER freq;

static struct measureTime
{
	LARGE_INTEGER tstart;
	LARGE_INTEGER tfin;
}elapsedTime[NUMTOMEASURE];

int Resolution;

#else

static struct measureTime
{
	struct timeval tstart;
	struct timeval tfin;
}elapsedTime[NUMTOMEASURE];

#	endif//USE_QUERYPERFORMANCECOUNTER




#endif//WIN32



void init_timer()
{
#ifdef USE_QUERYPERFORMANCECOUNTER
	SetThreadAffinityMask(GetCurrentThread(), 1);
	QueryPerformanceFrequency(&freq);
#endif
}
#ifdef WIN32
/*
	Make windows compatible with linux
	@vptr : which is not used currently. it is just for compatibility
*/
void gettimeofday(struct timeval *tv, void* vptr)
{	
	clock_t clk;
	clk=clock();
	tv->tv_sec	=clk / CLOCKS_PER_SEC;
	time(&tv->tv_usec);
	tv->tv_usec	=( clk % CLOCKS_PER_SEC ) * 1000 / CLOCKS_PER_SEC ;
}
#endif

//Record current time
void start_timer(int idx)
{

#ifdef USE_QUERYPERFORMANCECOUNTER
	QueryPerformanceCounter(&elapsedTime[idx].tstart);
#else
	
	gettimeofday(&elapsedTime[idx].tstart, NULL);
#endif//USE_QUERYPERFORMANCECOUNTER

}

//Get number of seconds since last call to start_timer
double get_timer(int idx)
{
	
#ifdef USE_QUERYPERFORMANCECOUNTER
	LARGE_INTEGER *ptstart = &elapsedTime[idx].tstart;
	LARGE_INTEGER *ptfin = &elapsedTime[idx].tfin;
	QueryPerformanceCounter(&elapsedTime[idx].tfin);
	return (((double)ptfin->QuadPart - (double)ptstart->QuadPart)/(double)(freq.QuadPart));
#else
	long sec, usec;
	struct timeval *ptfin = &elapsedTime[idx].tfin;
	struct timeval *ptstart = &elapsedTime[idx].tstart;


	gettimeofday(ptfin, NULL);

	sec		= ptfin->tv_sec - ptstart->tv_sec;
	usec	= ptfin->tv_usec - ptstart->tv_usec;
	return sec+1e-3*usec;
#endif//USE_QUERYPERFORMANCECOUNTER

	

}

