

#ifndef PRIORITYQUEUE_T_CPP
#define PRIORITYQUEUE_T_CPP
#include "PriorityQueue_T.h"
template < class T >
inline int
PriorityQueue< T >::size()
{
	return pq_.size();
}

template < class T >
inline bool 
PriorityQueue< T >::empty()
{
	return pq_.size();
}

template < class T >
inline void 
PriorityQueue< T >::push(T &x)
{
	pq_.push(x);
}

template < class T >
inline T & 
PriorityQueue< T >::top() const
{
	return pq_.top;
}

template < class T >
inline void 
PriorityQueue< T >::pop()
{
	pq_.pop();
}

#endif //PRIORITYQUEUE_T_CPP