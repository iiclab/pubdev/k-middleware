// =========================================================================
/**
 *  @filename	.h
 *	@Revised
 *  @Revision
 *  @Author		SeongHoon Kim
 *  
 *  @Description
 *		
 *	@NOTE
 *	
 */	
// =========================================================================
#ifndef QUEUE_H
#define QUEUE_H

#include "ConfigAll.h"
#include <queue>

template < class T >
class Queue {
public:
	/**
	*/
	int size();
	
	/**
	*/
	bool empty();

	/**
	*/
	void push(T &);
	
	/**
	*/
	T& front();

	/**
	*/
	T& back();

	/**
	*/
	void pop();
private:
	std::queue < T > q_;
};


#include "Queue_T.cpp"


#endif //QUEUE_H
