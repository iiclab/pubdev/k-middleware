#ifndef COMPLETION_HANDLER_ACT_H
#define COMPLETION_HANDLER_ACT_H
class Completion_Handler_ACT {
public:    

    virtual void handle_Event() = 0;
};
#endif //COMPLETION_HANDLER_ACT_H
