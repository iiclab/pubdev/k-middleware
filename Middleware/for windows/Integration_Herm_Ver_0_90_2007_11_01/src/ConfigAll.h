// =========================================================================
/**
 *  @filename	ConfigAll.h
 *	@Revised
 *  @Revision
 *  @Author		SeongHoon Kim
 *  
 *  @Description
 *		
 *	@NOTE
 *	
 */	
// =========================================================================
#ifndef CONFIGALL_H
#define CONFIGALL_H

#include "ConfigMacros.h"

#define THIS_IS_CLIENT FALSE
//TODO: following configuration macros have to be separated on different file.
#define MW_EXPORT

#pragma warning(disable: 4786)

#endif//_CONFIG_H_
