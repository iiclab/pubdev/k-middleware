// ============================================================================
/**
 *  @filename	ConfigMacros.h
 *	@Revised
 *  @Revision
 *  @Author		SeongHoon Kim
 *  
 *  @Description

 *	@NOTE
 *	Currently, the name of middle-ware is not determined. So, I name it 
 *	QoS-Enabled Low-weight Robot Middle-ware(QLR) temperarily. And this file
 *	is used to configure general macros. 
 */
// ============================================================================
#ifndef CONFIGMACROS_H
#define CONFIGMACROS_H

/***************************************************************************
 * INCLUDES
 */
#include "Versioned_Namespace.h"

//we have to differentiate between .NET and VC++
#ifdef WIN32
//FIXME:version management
#define OSAL_WIN32

//For Waitable Timer
#define _WIN32_WINNT 0x0500

#include <WinSock2.h>
#include <windows.h>


#else/* WIN32 */

#define OSAL_LINUX

#endif/* WIN32 */

#include "Types.h"

#ifndef NULL
#define NULL (0)
#endif

#ifndef false
#define false 0
#endif

#ifndef true
#define true 1
#endif

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif


#if defined (WIN32)
	typedef HANDLE OSAL_HANDLE;
	#define INVALID_HANDLE INVALID_HANDLE_VALUE
#else /* ! WIN32 */
	typedef int OSAL_HANDLE;
	#define INVALID_HANDLE -1
#endif /* WIN32 */

#define _IP_BASED_DEVICE_			TRUE
#define _NON_IP_BASED_DEVICE_		FALSE

// ============================================================================
// INLINE macros
//
// These macros handle all the inlining of code via the .i or .inl files
// ============================================================================
#if defined(_INLINE)
#define __MW_INLINE__
#endif
//OSAL inlining has been explicitly disabled.  Implement
//internally within MW by undefining __MW_INLINE__.
#if defined (__MW_INLINE__)

#	define OSAL_INLINE inline

#else

#	define OSAL_INLINE

#endif /* __OSAL_INLINE__ */


#define MW_INLINE OSAL_INLINE
// ============================================================================
// Compiler Silencing macros
//
// Some compilers complain about parameters that are not used.  This macro
// should keep them quiet.
// ============================================================================

#if defined (__GNUC__)
// Some compilers complain about "statement with no effect" with (a).
// This eliminates the warnings, and no code is generated for the null
// conditional statement.  @note that may only be true if -O(optimization flag) is enabled,

#	define OSAL_UNUSED_ARG(a) do {/* null */} while (&a == 0)
#	define OSAL_NOTSUP_RETURN(a) return -1;
#else // __GNUC__ 

#	define OSAL_UNUSED_ARG(a) (a)

#endif // __GNUC__


#define OSAL_NEW_RETURN(POINTER,CONSTRUCTOR,RET_VAL) \
	do { POINTER = new CONSTRUCTOR; \
	if (POINTER == 0) { errno = ENOMEM; return RET_VAL; } \
	} while (0)
#define OSAL_NEW(POINTER,CONSTRUCTOR) \
	do { POINTER = new CONSTRUCTOR; \
	if (POINTER == 0) { errno = ENOMEM; return; } \
	} while (0)
#define OSAL_NEW_NORETURN(POINTER,CONSTRUCTOR) \
	do { POINTER = new CONSTRUCTOR; \
	if (POINTER == 0) { errno = ENOMEM; } \
	} while (0)

#define OSAL_DELETE(POINTER) \
	do { if(POINTER){		\
	delete POINTER; \
	POINTER = NULL;}\
	} while (0)

// ============================================================================
// Fundamental types
// ============================================================================

#if defined (OSAL_WIN32)

typedef HANDLE OSAL_HANDLE;
typedef SOCKET OSAL_SOCKET;
#define OSAL_INVALID_HANDLE INVALID_HANDLE_VALUE

#else /* ! MW_WIN32 */

typedef int OSAL_HANDLE;
typedef int OSAL_SOCKET;
#define OSAL_INVALID_HANDLE -1

#endif /* MW_WIN32 */

//Adaptive, evolvable, real-time
//QoS-enabled and Low-weight Middleware for Robot



// Define the type that's returned from the platform's native thread
// functions. THR_FUNC_RETURN is the type defined as the thread
// function's return type.

# if defined (OSAL_WIN32)
typedef DWORD THR_FUNC_RETURN;
# else
typedef void* THR_FUNC_RETURN;
# endif /* OSAL_WIN32 */


# ifdef __cplusplus
extern "C"
{
# endif  /* __cplusplus */

typedef THR_FUNC_RETURN (*pfTHR_FUNC_t)(void *);
typedef void (*pfAIOCallBack_t)(int ,int , void *);

# ifdef __cplusplus
}
# endif  /* __cplusplus */

//FIXME: if the number of configuration related to compilation,
//			I have to create another file to move below configuration macros.
//to remove STL debug warnings
#if defined (OSAL_WIN32)
#pragma warning(disable: 4786)
#endif

#endif//_CONFIGMACROS_H_
