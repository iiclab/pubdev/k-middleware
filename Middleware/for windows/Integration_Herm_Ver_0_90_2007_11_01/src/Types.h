// =========================================================================
/**
 *  @filename	Type.h
 *	@Revised
 *  @Revision
 *  @Author		SeongHoon Kim
 *  
 *  @Description

 *	@NOTE
 */
// =========================================================================
#ifndef TYPES_H
#define TYPES_H



/*********************************************************************
 * INCLUDES
 */


/*********************************************************************
 * MACROS
 */

/*********************************************************************
 * CONSTANTS
 */

/*********************************************************************
 * TYPEDEFS
 */
typedef unsigned char		byte;
typedef unsigned char		uint8;
typedef char				int8;
typedef unsigned short		uint16;
typedef short				int16;
typedef unsigned long		uint32;
typedef long				int32;	

#endif //TYPES_H