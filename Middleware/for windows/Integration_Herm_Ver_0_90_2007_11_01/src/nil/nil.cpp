#include "nil.h"
#include "Debug.h"
#include "errorno.h"
#include "configAll.h"

class NILDE_DataConfirm : public OutputCallbackHandler 
{
public:
	NILDE_DataConfirm(NIL *pNIL):pNIL_(pNIL){}
	virtual ~NILDE_DataConfirm(){}
    
	void handleOutput(SentStatus_t * pSentStatus)
	{
		pNIL_->dataConfirm(pSentStatus);
	}
private:
	NIL *pNIL_;
};

class NILDE_DataIndication : public InputCallbackHandler 
{
public:
	NILDE_DataIndication(NIL *pNIL):pNIL_(pNIL){}
	virtual ~NILDE_DataIndication(){}
	
    void handleInput(void *arg, Msg_t *msg)
	{
        pNIL_->dataIndication(arg, msg);
	}
private:
	NIL *pNIL_;
};

class NILME_Callback: public IOChannelMgntCallback
{
public:
    NILME_Callback(NIL *pNIL):pNIL_(pNIL) {}
    virtual ~NILME_Callback() {}
    
    virtual void handleSetChannelDataIndication(IOChannel*pIOC)
    {
        pNIL_->setChannelDataIndication(pIOC);
    }
    virtual void handleSetChannelDataConfirm(IOChannel*pIOC)
    {
        pNIL_->setChannelDataConfirm(pIOC);
    }
    
private:
    NIL *pNIL_;
};

NIL::NIL(void *configure)
{
    pConAddr = (configureAddr*)configure;
    
    pIODevice_Manager_ = IODeviceManager::instance();
    pIODevice_Manager_->initialize(configure);

    pIOChannelManager_ = IOChannelManager::instance();
    pIOChannelManager_->initialize();

	pDataConfirm_ = new NILDE_DataConfirm(this);
	pDataIndication_ = new NILDE_DataIndication(this);
}


NIL::~NIL()
{
    //close available devices
    finalize();
    
    OSAL_DELETE(pIOChannelManager_);
    OSAL_DELETE(pIODevice_Manager_);
       
    //delete data callback
    OSAL_DELETE(pDataConfirm_);
 	OSAL_DELETE(pDataIndication_);
}

void 
NIL::initialize()
{
    //register callback for management
	pNILME_ = new NILME_Callback(this);
    pIOChannelManager_->subscribeIOChannelMgntEvent(pNILME_);
    
    //TODO: from configure file 
    //start TCP_IP Device 
	String devName = "_TCP_DEVICE_NAME_";
	IODevice *pIODevice = pIODevice_Manager_->findIODevice(devName);
	
	if(pIODevice != NULL)
	{
        //start device 
		pIODevice->start();	
	}
    
    //start UDP_IP Device and open broadcast channel
    devName = "_UDP_DEVICE_NAME_";
    pIODevice = pIODevice_Manager_->findIODevice(devName);
    
    if(pIODevice != NULL)
	{
        //start device 
		pIODevice->start();	
        
		os_sockaddr_in toAddr;
        toAddr.sin_family = AF_INET;
        toAddr.sin_addr.s_addr = inet_addr(pConAddr->_SERVER_IP_ADDR_);
        //toAddr.sin_addr.s_addr = htonl(INADDR_BROADCAST);
        toAddr.sin_port = htons(pConAddr->_SERVER_PORT_UDP_NO_);
        
        //os_sockaddr_in *dstAddr;
        //dstAddr = &toAddr;
        
        //create defualt broadcast channel
        //pIOChannelManager_->channelJoinNotification( pIODevice ,  dstAddr ); 
		pIOChannelManager_->channelJoinNotification( pIODevice ,  &toAddr ); 
	}
/*
	//start RS232C Device and open default channel
	devName = "RS232:COM1";
	pIODevice = pIODevice_Manager_->findIODevice(devName);

	if (pIODevice != NULL)
	{
		//start device 
		pIODevice->start();
		static char *peerAddr_RS232C = "COM1";

		//create channel for peer rs232c 
		pIOChannelManager_->channelJoinNotification( pIODevice, peerAddr_RS232C );
	}
	*/
}

void 
NIL::finalize()
{
    //delete management callback
    OSAL_DELETE(pNILME_);
    
    //close all devices
    pIODevice_Manager_->finalize();
}

int 
NIL::dataRequest(int chID, Msg_t *msg)
{
    //broadcasting to existing channel
	if(chID == NIL_BROADCASTADDR)
	{
		//bring channel list from channel manager
        typedef std::map<int, IOChannel*> ChannelMapTable;
        ChannelMapTable chList= pIOChannelManager_->IOChannelTable_.ctChannelList_;
        
        ChannelMapTable::iterator pos;
        IOChannel* pIOC;
                
		for (pos = chList.begin(); pos != chList.end(); ++pos)
        {
            pIOC = pos->second;
            
			//check channel broadcasting
            if (!pIOC->ThisIsBroadcastingChannel())
                continue;
            
            //data request for broadcast
			MW_DEBUG((LM_DEBUG,"[NIL] BROADCAST DATA REQUEST To CHANNEL(%d), DATA SIZE(%04x)\n", pos->first, msg->len_));
            pIOC->dataRequest(NULL,msg);
		}
        
		return MW_SUCCESS;
	}
	
    //find corresponding channel
    MW_DEBUG((LM_DEBUG,"[NIL] UNICAST DATA REQUEST To CHANNEL(%d), DATA SIZE(%04x)\n", chID, msg->len_));
    IOChannel* pIOC = pIOChannelManager_->IOChannelTable_.find(chID);
	
    //request for data 
    return pIOC->dataRequest(NULL,msg);
}

int 
NIL::dataIndication(void *arg, Msg_t *msg)
{
	NILDEIncomingData_t incomingData;
	int channelID = (int)arg;
	IOChannel* pIOC = pIOChannelManager_->IOChannelTable_.find(channelID);
	MW_DEBUG((LM_DEBUG,"[NIL] DataIndication From ChID: [%d] len: %04x\n", channelID, msg->len_));
	
    incomingData.channelID_ = channelID;
	incomingData.pIOChannel_ = pIOChannelManager_->IOChannelTable_.find(channelID);

	//Interface ����
	switch(pIOC->getInterfaceInformation()){
	case _INTERFACE_UDP_:
		incomingData.interfaceType_ = _INTERFACE_ETHERNET_;
		break;
	case _INTERFACE_TCP_:
		incomingData.interfaceType_ = _INTERFACE_ETHERNET_;
		break;
	default:
		incomingData.interfaceType_ = pIOC->getInterfaceInformation(); 
		break;
	}

    //send data to upper layer
    pUpperLayerDataIndication_->handleInput(&incomingData, msg);
	return MW_SUCCESS;
}

int 
NIL::dataConfirm(SentStatus_t *pSendStatus)
{
//    MW_DEBUG((LM_DEBUG,"NIL::dataConfirm cID %d len: %04x\n", channelID, msg->len_));
	pUpperLayerDataConfirm_->handleOutput(pSendStatus);
	return MW_SUCCESS;
}

/*
IOChannel* 
NIL::GetIOChannelRequest(int channelIndex)
{
	return IOChannelTable_[channelIndex];
}

IODevice* 
NIL::GetIODeviceRequest(const String &str)i
{
	return pIODevice_Manager_->findIODevice(str);
}
*/

int 
NIL::createChannelRequest(int portAddr, void* ipAddr)
{
    //for TCP/IP
    String devName = "_TCP_DEVICE_NAME_";
	IODevice *pIODevice = pIODevice_Manager_->findIODevice(devName);
	    
	struct sockaddr_in toAddr;
	memset(&toAddr, 0, sizeof(toAddr));
    toAddr.sin_family = AF_INET;
    toAddr.sin_addr.s_addr = *(unsigned int *)ipAddr;
    toAddr.sin_port = htons(portAddr);
	 
    //create defualt broadcast channel
    pIOChannelManager_->channelJoinNotification( pIODevice ,  &toAddr );
    int retChID = pIOChannelManager_->genID_ - 1;    
    
    return retChID;
}

int
NIL::removeChannelRequest(int)
{
	return 0;
}

void 
NIL::setDataIndicationCB(InputCallbackHandler *pDataIndication)
{
	pUpperLayerDataIndication_ = pDataIndication;
}

void 
NIL::setDataConfirmCB(OutputCallbackHandler * pDataConfirm)
{
	pUpperLayerDataConfirm_ = pDataConfirm;
}

int 
NIL::setChannelDataIndication(IOChannel * pIOC)
{
	pIOC->setInputHandler(pDataIndication_);
	return MW_SUCCESS;
}

int 
NIL::setChannelDataConfirm(IOChannel * pIOC)
{
    pIOC->setOutputHandler(pDataConfirm_);  
	return MW_SUCCESS;
}


