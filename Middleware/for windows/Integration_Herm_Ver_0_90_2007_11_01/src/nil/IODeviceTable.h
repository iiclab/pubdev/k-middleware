// =========================================================================
/**
 *  @filename	IODeviceTable.h
 *	@Revised
 *  @Revision
 *  @Author		SeongHoon Kim
 *  
 *  @Description
 *	Device Manager
 *	@NOTE 
 */
// =========================================================================

#ifndef IODEVICETABLE_H
#define IODEVICETABLE_H

#include <map>
//#include "ConfigAll.h"
#include "IODevice.h"
#include "Utility/String.h"
#include "Debug.h"

//this constant value should be set by a specified configuration file.
const int sizeOfRepository = 10;

class IODeviceTable  
{
public:
	IODeviceTable();	
	virtual ~IODeviceTable();

	int insert(IODevice *);

	IODevice * find(const String &szDeviceName);	
	IODevice * find(IODevice * szDeviceName);

	int remove(const String & szDeviceName);
	int remove(const IODevice * szDeviceName);	
	void removeAll();
	
	uint32 getSize();	
	void setSize(uint32 size);

protected:
private:
    uint32  nDevices_;
    uint32	nMaxCapacity_;
    
    typedef std::map< String , IODevice* > DevList_t; 
    DevList_t ctDeviceList_;	//ct => container.

    IODevice * lnkIODevice;
};

#endif //DEVICEIO_H

