#ifndef INPUTCALLBACKHANDLER_H
#define INPUTCALLBACKHANDLER_H

#include "Types.h"
#include "Utility/Msg_t.h"

class InputCallbackHandler {
public:    
	virtual ~InputCallbackHandler(){}
    virtual void handleInput(void *arg, Msg_t *msg) = 0;
};
#endif //INPUTCALLBACKHANDLER_H
