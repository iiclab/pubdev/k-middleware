#ifndef IOCHANNELMANAGER_H
#define IOCHANNELMANAGER_H

#include "Types.h"
#include "IODevice.h"
#include "IOChannelTable.h"

class NIL;
class IOChannelMgntCallback;

class IOChannelManager 
{
    friend class NIL;
public:

    static IOChannelManager * instance();
    virtual ~IOChannelManager();

    int initialize();
    int finalize();
    int reset();
    
    //handle channel operations
    void subscribeIOChannelMgntEvent(IOChannelMgntCallback *);
//     IOChannel *createChannel(int, const String &, void *);
// 	IOChannel *findChannel(const String &, void *);
//     int removeChannel(const String &, void *);
    
    void channelJoinNotification(IODevice *iodev, DevAddr_t devaddr);
    void channelLeaveNotification(IODevice *iodev, DevAddr_t devaddr); 

protected:
    IOChannelManager();

private:    
    static int genID_;
    static IOChannelManager * pInstance_;

    IOChannelMgntCallback *pIOChannelMgntCallback_;
    IOChannelTable IOChannelTable_;
};

/**IOChannelManager Management Callback interface*/
class IOChannelMgntCallback
{
public:
    virtual ~IOChannelMgntCallback(){}
    
    virtual void handleSetChannelDataIndication(IOChannel*) = 0;
    virtual void handleSetChannelDataConfirm(IOChannel*) = 0;
};


#endif //IOCHANNELMANAGER_H
