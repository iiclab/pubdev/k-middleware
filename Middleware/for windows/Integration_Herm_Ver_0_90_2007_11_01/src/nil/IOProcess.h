#ifndef IOPROCESS_H
#define IOPROCESS_H

//#include "ConfigAll.h"

#include "osal/SysEventProcessor.h"
#include "osal/IAsyncOperation.h"
#include <vector>
#include "osal/Thread.h"
#include "ConfigMacros.h"
#include "Utility/Msg_t.h"
#include "Utility/String.h"

class IRunnable;
class IAsyncOperation;

class IOProcess
{
public:   	
	IOProcess();
	virtual ~IOProcess();
	
    //asynchronous operations
	int registerAsyncOperation(bool, OSAL_HANDLE, OSAL::IAsyncOperation *, OSAL_SOCKET);	
    int unregisterAsyncOperation(OSAL_HANDLE);
    
    //asynchronous operations (i.e, using thread blocking I/O)
	int registerSyncOperation(OSAL::IRunnable *);
    int unregisterSyncOperation(OSAL::IRunnable *);   

    int getOpMode();
    void setOpMode(OSAL::OpMode_t );
	
protected:
	OSAL::SysEventProcessor *pSysEventProcessor_;

private:
	int opMode_;
     
    typedef std::vector<int> ThreadIDVector;
    ThreadIDVector v_threadId_vector;
};

/**
 * @class AIO_Result
 *
 * @brief Asynchronous Completion Token(ACT)
 *	When an asyncRead() or asyncWrite() method is invoked on an IODevice, 
 *	which may uses AsyncOperation, they create a new Async_XXX_Read_Result or
 *	Async_XXX_Write_Result ACT, respectively and pass it to the
 *	corresponding Win32 asynchronous operation. When this asynchronous
 *	operation finishes, the Windows NT kernel queues the completion event
 *	on the completion port designated by the handle that was passed during
 *	the original asynchronous operation invocation. The ACT is used by the
 *	completion routine provided by Window Kernel to demultiplex the completion 
 *	event to the completion handler designated in the original call.
 */
class AIO_Result
{
public:
	virtual ~AIO_Result(){}

	virtual void complete() = 0;
	
	virtual void setCompletionState(int) = 0;	
	virtual void setReadOrTransferred(int) = 0;
	
#define ACT_READEVENT	0x01
#define ACT_WRITEEVENT	0x00	
#define ACT_CLOSEEVENT	0x02

protected:
	int state_;
	int readOrTransferred_;	
private:
	
};

#endif //IOPROCESS_H
