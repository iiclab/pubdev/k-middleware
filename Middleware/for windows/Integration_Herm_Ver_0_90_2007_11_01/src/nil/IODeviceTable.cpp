// =========================================================================
/**
 *  @filename	IODeviceManager.cpp
 *	@Revised
 *  @Revision
 *  @Author		SeongHoon Kim
 *  
 *  @Description
 *				  Device manager class
 *	@NOTE
 */
// =========================================================================

//#include "ConfigAll.h"
#include "Utility/String.h"
#include "IODeviceManager.h"
#include "IODevice.h"
#include "errorno.h"

// ============================================================================
// Macros
// 
// ============================================================================

//#define MOD_DEVICEMANAGER "[DEVICE_Repository] "

#define	PRINT(X)	MW_DEBUG(X)
#define	ERR(X)		MW_DEBUG(X) 

#ifndef _DBG_DEVICEMANAGER_

#undef MW_DEBUG(X)

#else

#endif//_DBG_DEVICEMANAGER_

IODeviceTable::IODeviceTable()
{
	nDevices_ = 0;
}

IODeviceTable::~IODeviceTable()
{
	this->removeAll();
}

int 
IODeviceTable::insert(IODevice * pIODev)
{
	ctDeviceList_.insert(std::make_pair(pIODev->getName(), pIODev));
	return MW_SUCCESS;
}

int 
IODeviceTable::remove(const String& szDeviceName)
{
	MW_DEBUG((LM_DEBUG,"IODeviceTable::remove\n"));
    DevList_t::iterator it;

	it = ctDeviceList_.find(szDeviceName);
    if(it == ctDeviceList_.end())
		return MW_FAILED;

    OSAL_DELETE(it->second);
	ctDeviceList_.erase(it);

	return MW_SUCCESS;
}

int 
IODeviceTable::remove(const IODevice * pIODevice)
{
	MW_DEBUG((LM_DEBUG,"IODeviceTable::remove\n"));
    DevList_t::iterator it;
    
// 	it = ctDeviceList_.find(szDeviceName);
//     if(it == ctDeviceList_.end())
// 		return MW_FAILED;
    
	//ctDevices_.erase(reinterpret_cast< DevList::iterator >(this->find(szDeviceName)));
	return 0;
}

void 
IODeviceTable::removeAll()
{
    MW_DEBUG((LM_DEBUG,"IODeviceTable::remove all devices in table\n"));
    DevList_t::iterator it;
    
    for (it=ctDeviceList_.begin();it!=ctDeviceList_.end();it++)
    {
        OSAL_DELETE(it->second);
    }
	ctDeviceList_.clear();
}

IODevice * 
IODeviceTable::find(const String& szDeviceName)
{
	DevList_t::iterator it;
	//time complexity : O(n)
	it = ctDeviceList_.find(szDeviceName);
	if(it == ctDeviceList_.end())
		return NULL;
	else
		return (*it).second;
}

IODevice * 
IODeviceTable::find(IODevice * pIODev)
{
	return find(pIODev->getName());
}

void 
IODeviceTable::setSize(uint32 size)
{

}

uint32 
IODeviceTable::getSize()
{
	return ctDeviceList_.size();
}
