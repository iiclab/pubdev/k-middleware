#ifndef IOCHANNEL_H
#define IOCHANNEL_H

#include "Types.h"
#include "ConfigMacros.h"
#include "Utility/Msg_t.h"
#include <vector>

#include "InputCallbackHandler.h"
#include "OutputCallbackHandler.h"

#define IOCHANNEL_BROADCASTADDR 0xFFFFFFFF
#define IOCHANNEL_UNUSED	(unsigned)-1
//= IOChannel type 
// IOCHANNEL_OPEN_ACTIVE shall be set when the channel is created by local
#define IOCHANNEL_OPEN_ACTIVE 0x01
// IOCHANNEL_OPEN_PASIVE shall be set when the channel is created by remote
#define IOCHANNEL_OPEN_PASIVE 0x02

typedef enum 
{
    IO_INIT = 0x00,
    IO_RUN  = 0xF0,
    IO_RUN_WAIT = 0xF2,
    IO_RUN_RECV = 0xF4,
    IO_FIN = 0x01
} IO_State_t;

typedef void* DevAddr_t;

class IODevice;
class IOChannel_Scheduler;
/**
 * @class IOChannel
 *
 * @brief An IO channel class that provides clients/user(in our case, it 
 *	is upper layer) with transparency of device types.
 * This class shall be implemented with independence of IO mode.
 * IODevice shall configure mode of IO channel according to it IO mode.
 * Channel Manager shall use IOChannel class depending on pre-configured mode.
 *  In detail, in case of asynchronous mode, Channel Manager invokes write/read
 *	member method directly. in case synchronous mode, it invokes write/read using
 * Thread_Impl class provided by Micro-OS Service Broker.
 */
class IOChannel 
{
	//Normalized error codes. The error result of 
	//the last file I/O operation in this object. 
	enum Errors {
		NonError,		
		NotFound,		
		AccessDenied,
		DeviceInUse,
		BadParameter,
		NoMemory,
		NotOpen,
		BufferTooSmall,
		Musscellaneous,
		ProtocolFailure,
		NumNormalizedErrors
	};

public: 
	//create channel and close down it
	IOChannel();
    IOChannel(DevAddr_t);
	virtual ~IOChannel();

	virtual void start() = 0;
	virtual int close() = 0;
    
	bool isInUse();
	
	//data entity
    virtual int dataRequest(void * arg, Msg_t *msg) = 0;	
	virtual void dataIndication(void * arg, Msg_t *msg) = 0;
	virtual void dataConfirm(SentStatus_t *);

	void setID(int ID);	
    int getID();
	
	virtual IODevice * getOwnDevice() = 0;
    
	void setPriority(int priority);
    int getPriority();
	
	/**
	 * Channel's clients receive data from a remote peer 
	 * through InputCallbackHandler registered by the client itself.
	 */
    void setInputHandler(InputCallbackHandler *);
    InputCallbackHandler * getInputHandler();
	
	/**
	 * Channel's clients confirm its data request transaction 
	 * through OutputCallbackHandler registered by the client itself.
	 */
	void setOutputHandler(OutputCallbackHandler *);	
    OutputCallbackHandler * getOutputHandler();
	
	void setDstAddr(DevAddr_t);
	virtual DevAddr_t getDstAddr();

	bool setBufferSize(int newSize);
	virtual int getSizeofBuffer();	
	//virtual byte* getBuffer(int bufferIndex);
	virtual byte* getBuffer();
	
	void setMaxPayload(int );
	int getMaxPayload();
    
	//2007.12.20
	void setInterfaceInformation(int interInfo);
	int getInterfaceInformation();

	//Function for broadcasting 
    bool ThisIsBroadcastingChannel();

	//Functions handling errors
	//Errors GetLastErrorNumber();

protected:

	//send buffer
	struct NPDU {
		byte *pBuffer_;
		int bufferLen_;
	};
	typedef std::vector< NPDU > NetworkInterfacePayloadDataUnit;
	NetworkInterfacePayloadDataUnit v_chBuffer_;	//channel buffer

	//recv buffer
	int bufferLen_;	
	byte *pBuffer_;

	Msg_t m_RecvBuf;    
    bool broadcast_;
    
private:    
    int channelID_;   	
	int priority_;    	
	int maxPayload_;
	int interfaceInformation_;

	/** Destination Address*/
	DevAddr_t DstAddr_;

    InputCallbackHandler * pInputCallbackHandler_;
    OutputCallbackHandler * pOutputCallbackHandler_;
};
#endif //IOCHANNEL_H
