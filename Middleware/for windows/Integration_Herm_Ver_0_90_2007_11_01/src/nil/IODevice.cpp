//#include "osal/SysEventProcessor.h"
#include "IODeviceManager.h"
#include "IODevice.h"
#include "errorno.h"

IODevice::IODevice()
{
	handle_ = 0;
//	opMode_ = 0;
//	pIOThread_ = NULL;
	deviceID_ = 0;
//	pIODeviceProfile_ = NULL;
//	pSysEventProcessor_ = OSAL::SysEventProcessor::instance();
	state_ = IODEV_CREATED;
}

IODevice::~IODevice()
{
//	if(pIOThread_ != NULL)
//		delete pIOThread_;
	
}

int 
IODevice::registerIODevice()
{
	IODeviceManager::instance()->registerIODevice(this);
	return 0;
}

int 
IODevice::open(void *configure)
{
    if(doConfigIOParam(configure) != MW_SUCCESS)
		return MW_FAILED;

	if(doOpen(NULL) != 0)
		return MW_FAILED;

	state_ = IODEV_READY;

	return MW_SUCCESS;
}

int
IODevice::start()
{
	state_ = IODEV_ACTIVE;

	if(doStart() == MW_SUCCESS)
	{	
		return MW_SUCCESS;
	}
	else
		return MW_FAILED;
}

int 
IODevice::stop()
{
    state_ = IODEV_STOP;
    return doStop();
}

int 
IODevice::close()
{	
	state_ = IODEV_FINALIZED;
	return doClose();
}

void 
IODevice::channelJoinIndication(IODevice *iodev, DevAddr_t devID)
{
	IODeviceManager::instance()->channelJoinNotification(iodev, devID);
}

void 
IODevice::channelLeaveIndication(IODevice *iodev, DevAddr_t devID)
{
	IODeviceManager::instance()->channelLeaveNotification(iodev, devID);
}

String &
IODevice::getName()
{
	return name_;
}

void 
IODevice::setName(const String &name)
{
	name_ = name;
}

OSAL_HANDLE 
IODevice::getHandle()
{
	return handle_;
}

void 
IODevice::setHandle(OSAL_HANDLE h)
{
	handle_ = h;
}

uint8 
IODevice::getID()
{ 
	return deviceID_; 
}

void 
IODevice::setID(uint8 ID)
{
	this->deviceID_ = ID; 
}

IODevice::IODevState_t 
IODevice::getState()
{
	return state_;
}

void 
IODevice::setState(IODevice::IODevState_t state)
{
	state_ = state;
}

bool 
IODevice::getDevType()
{
	return DevType_;
}

void
IODevice::setDevType(bool devType)
{
	DevType_ = devType;
}
