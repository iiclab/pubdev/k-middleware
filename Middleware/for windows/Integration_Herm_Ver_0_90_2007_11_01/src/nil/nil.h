#ifndef NIL_H
#define NIL_H

#include "ConfigMacros.h"
#include "osal/OSAL_Socket.h"
#include "IODeviceManager.h"
#include "IOChannelManager.h"
#include "IODevice.h"
#include "IOChannel.h"
#include "InputCallbackHandler.h"
#include "OutputCallbackHandler.h"
#include "Utility/Msg_t.h"
#include "Utility/String.h"

#define NIL_BROADCASTADDR 0xFFFFFFFF
#define NIL_NUMOFIOCHANNEL 10

#define _INTERFACE_UDP_	0
#define _INTERFACE_TCP_	1

#define _INTERFACE_ETHERNET_	1 
#define _INTERFACE_RS232C_		2
#define _INTERFACE_IEEE1394a_	3
#define _INTERFACE_IEEE1394b_	4
#define _INTERFACE_CAN2_0_		5 
#define _INTERFACE_USB1_1_		6 
#define _INTERFACE_USB2_0_		7 

/**
 * incoming data structure
 */
typedef struct 
{
    int channelID_;
    IOChannel *pIOChannel_;
	int	interfaceType_;
} NILDEIncomingData_t;

struct configureAddr
{
    bool _THIS_IS_SERVER_;
    int _PORT_TCP_NO_;
    int _PORT_UDP_NO_;
    int _SERVER_PORT_UDP_NO_;
    int _SERVER_PORT_TCP_NO_;
    const char _IP_ADDR_[16];
    const char _SERVER_IP_ADDR_[16];
};

class NILDE_DataIndication;
class NILDE_DataConfirm;
class NILME_Callback;

// Network Interface Layer 
class NIL 
{
    friend class NILME_Callback;
   
public:    

    NIL(void *);
    virtual ~NIL();

    //NIL Data Entity
    int dataRequest(int , Msg_t *);
    int dataIndication(void *, Msg_t *);
    int dataConfirm(SentStatus_t*);

    //NIL Management Entity
    int setChannelDataIndication(IOChannel *);
    int setChannelDataConfirm(IOChannel *);
            
    void initialize();
    void finalize();
    
    //handle channel
    //IODevice* GetIODeviceRequest(const String &devName);
    //IOChannel* GetIOChannelRequest(int channelIndex);
    int createChannelRequest(int, void*);
    int removeChannelRequest(int );

    void setDataIndicationCB(InputCallbackHandler *);
    void setDataConfirmCB(OutputCallbackHandler *);

private:       
    NILME_Callback *pNILME_;
		
    IODeviceManager *pIODevice_Manager_;
    IOChannelManager *pIOChannelManager_;

    NILDE_DataIndication *pDataIndication_;
    NILDE_DataConfirm *pDataConfirm_;
  
    InputCallbackHandler *pUpperLayerDataIndication_;
    OutputCallbackHandler *pUpperLayerDataConfirm_;
    
    struct configureAddr* pConAddr;
};

#endif //NIL_H
