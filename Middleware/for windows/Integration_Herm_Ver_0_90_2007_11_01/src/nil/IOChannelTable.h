#ifndef IOCHANNELTABLE_H
#define IOCHANNELTABLE_H

#include <map>
//#include "ConfigAll.h"
#include "IOChannel.h"
#include "Debug.h"

class NIL;

class IOChannelTable  
{
    friend class NIL;
    
  public:
    IOChannelTable();
	
    virtual ~IOChannelTable();

    int insert(int, IOChannel *);

    IOChannel *find(int );

    int remove(int );
	
    void removeAll();
	
    uint32 getSize();

  protected:
    
  private:
    
    typedef std::map< int , IOChannel* > chList_t;
    
    chList_t ctChannelList_;	//ct => container.
};

#endif //IOCHANNELTABLE_H


