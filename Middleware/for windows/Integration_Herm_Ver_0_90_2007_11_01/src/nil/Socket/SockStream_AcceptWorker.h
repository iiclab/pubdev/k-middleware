// =========================================================================
/**
 *  @filename	SockStream_SyncOpeartion.h
 *	@Revised
 *  @Revision
 *  @Author		SeongHoon Kim
 *  
 *  @Description
 *				  Socket Stream class
 *	@NOTE
 */
// =========================================================================
#ifndef SOCKSTREAM_ACCEPTWORKER_H
#define SOCKSTREAM_ACCEPTWORKER_H

#include "osal/IRunnable.h"
#include "osal/AsyncOperation.h"
#include "osal/IAsyncOperation.h"

class SockStream;

class SockStream_AcceptWorker : public OSAL::AsyncOperation//public OSAL::IRunnable
{
public:
    SockStream_AcceptWorker(SockStream *pSockStream);
	virtual ~SockStream_AcceptWorker();

	void asyncOperation(void);
    void handleUserDefinedSystemEvent(OSAL_SOCKET);

private:
	SockStream *pOwnDevice_;
	void run();
};


#endif//SOCKSTREAM_ACCEPTWORKER_H




