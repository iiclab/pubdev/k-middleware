#include "ConfigMacros.h"
#include "SocketBase.h"
#include "SockStream.h"
#include "SockStream_IOChannel.h"
#include "Debug.h"
#include "errorno.h"
//#include "configure.h"

//#define BUFFER_SIZE 4096

SockStream_IOChannel::SockStream_IOChannel(DevAddr_t pDestAddr,SockStream *pSockStream) : 
IOChannel(pDestAddr), pOwnDevice_(pSockStream)
{
	bufferLen_ = BUFFER_SIZE;	
	pBuffer_ = new byte[bufferLen_];
	memset(pBuffer_, 0, BUFFER_SIZE);
	this->setInterfaceInformation(_INTERFACE_TCP_);
	
	memset(&remote, 0, sizeof(remote));
	remote = *(sockaddr_in *)pDestAddr;
    
    //set socket descriptor
    if (pOwnDevice_->currentAcceptSock_)
        sd_ = pOwnDevice_->currentAcceptSock_;
    else
        sd_ = pOwnDevice_->getSD(_CONNECT_USE_);
    
	m_RecvBuf.len_ = bufferLen_;
	m_RecvBuf.pData_ = pBuffer_;

	runnable_ = NULL;
	this->setMaxPayload(BUFFER_SIZE);	
}

SockStream_IOChannel::SockStream_IOChannel(const SockStream_IOChannel &init)
{
	bufferLen_ = init.bufferLen_;
	
	OSAL_DELETE(pBuffer_);
	pBuffer_ = new byte[bufferLen_];

/*
	setDstAddr(init.getDstAddr());	
	this->setMaxPayload(init.getMaxPayload());	
	setInputHandler(init.getInputHandler());
	this->setOutputHandler(init.getOutputHandler());	
	this->setPriority(init.getPriority());
	this->setID(init.getID());
*/

	pOwnDevice_ = init.pOwnDevice_;	
	runnable_ = init.runnable_;
}

SockStream_IOChannel::~SockStream_IOChannel()
{
	pOwnDevice_ = NULL;
	OSAL_DELETE(pBuffer_);

#ifndef OSAL_WIN32    
    ::close(sd_);
#endif
    
//	OSAL_DELETE(pOlReadACT_);
//	OSAL_DELETE(pOlWriteACT_);
}

void 
SockStream_IOChannel::start()
{
    if (pOwnDevice_->getOpMode() == OSAL::OPMODE_SYNC)
    {
        if(runnable_ != NULL)
            return;
        
        runnable_ = new SockStream_IOChannel_Thread(this);
        pOwnDevice_->registerSyncOperation(runnable_);
    }
    else
    {
#ifdef OSAL_WIN32
		/*
		if(read(&m_RecvBuf) == MW_ERROR)
		{
			close();
		}*/
#else
		pOwnDevice_->registerAsyncOperation(sd_, pOwnDevice_->pSockAsyncOperation_);
#endif

    }
    
}

int 
SockStream_IOChannel::close()
{
    if (pOwnDevice_->getOpMode() == OSAL::OPMODE_SYNC)
    {
        //	pOwnDevice_->unregisterSyncOperation(this->getRunnable());
        return MW_SUCCESS;
    }
    else
    {
#ifdef OSAL_WIN32
//		pOwnDevice_->channelLeaveIndication(pOwnDevice_, this->getDstAddr());
//		pOwnDevice_->finalizeChannel(this->getDstAddr());        
#else
		pOwnDevice_->unregisterAsyncOperation(sd_);
#endif
        return MW_SUCCESS;
    }
}

IODevice * 
SockStream_IOChannel::getOwnDevice()
{
	return pOwnDevice_;
}

OSAL::IRunnable *
SockStream_IOChannel::getRunnable()
{
	return runnable_;
}

/*
OSAL::AsyncOperation* 
SockStream_IOChannel::getAsyncOperation()
{
	return 
}
*/

void
SockStream_IOChannel::setSockDesc(OSAL_SOCKET sd)
{
    sd_ = sd;
}

OSAL_SOCKET
SockStream_IOChannel::getSockDesc()
{
    return sd_;
}

void
SockStream_IOChannel::setHandle(OSAL_HANDLE hd)
{
	hd_ = hd;
}

OSAL_HANDLE
SockStream_IOChannel::getHandle()
{
	return hd_;
}

//Blocking Read operation
int 
SockStream_IOChannel::read(Msg_t *msg)
{
    return pOwnDevice_->read(sd_, msg);
}

//Interface for sending data to physical network.
int 
SockStream_IOChannel::dataRequest(void * arg, Msg_t *msg)
{
    return pOwnDevice_->write(sd_, msg);
}

//Interface for indicating incoming data to user(subscriber).
void 
SockStream_IOChannel::dataIndication(void * arg, Msg_t *msg)
{
	DEBUGTRACE("SockStream_IOChannel::dataIndication");
	MW_ASSERT(getInputHandler() != NULL);
	getInputHandler()->handleInput((void *)this->getID(), msg);
}
/*
int 
SockStream_IOChannel::control (ControlMode cmd, void *arg)
{
	return pOwnDevice_->control(cmd, arg);
}
*/

SockStream_IOChannel_Thread::SockStream_IOChannel_Thread(SockStream_IOChannel *pSockStream_IOChannel):pSockStream_IOChannel_(pSockStream_IOChannel)
{

}

SockStream_IOChannel_Thread::~SockStream_IOChannel_Thread()
{

}

void 
SockStream_IOChannel_Thread::run()
{	
	Msg_t incomingBuf, ReadBuf;
	incomingBuf.len_ = pSockStream_IOChannel_->getSizeofBuffer();
	incomingBuf.pData_ = ReadBuf.pData_ = pSockStream_IOChannel_->getBuffer();
	
	int nRead = 0;
	DEBUGTRACE("SockStream_IOChannel::run");
	//TODO: state handling
	while((pSockStream_IOChannel_ != NULL) ) 
	{
		MW_DEBUG((LM_DEBUG,"SockStream_IOChannel_Thread::run: Ready to ready data!\n"));
		nRead=pSockStream_IOChannel_->read(&incomingBuf);
		if(nRead > 0)
		{
			ReadBuf.len_= nRead;
			
			pSockStream_IOChannel_->dataIndication(NULL, &ReadBuf);
			MW_DEBUG((LM_DEBUG,"SockStream_IOChannel_Thread::Read data is succeeded nRead %d!\n",nRead));
		}
		else
		{
			MW_DEBUG((LM_DEBUG,"SockStream_IOChannel_Thread:: Error on reading data"));
			pSockStream_IOChannel_->close();
			break;
		}
	}
}


