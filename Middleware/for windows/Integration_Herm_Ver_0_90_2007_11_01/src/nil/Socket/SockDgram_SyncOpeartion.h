#ifndef SOCKSTREAM_SYNCOPERATION_H
#define SOCKSTREAM_SYNCOPERATION_H

#include "osal/IRunnable.h"
#include "SockDgram.h"

class SockDgram;

class SockDgram_SyncOpeartion : public OSAL::IRunnable
{
public:
	SockDgram_SyncOpeartion(SockDgram *);
	virtual ~SockDgram_SyncOpeartion();

	void run();
private:
	SockDgram *pOwnDevice_;
	Msg_t incomingMsg_;
};

#endif//SOCKSTREAM_SYNCOPERATION_H

