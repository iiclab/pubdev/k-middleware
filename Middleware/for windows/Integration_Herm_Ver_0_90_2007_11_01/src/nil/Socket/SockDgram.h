#ifndef SockDgram_H
#define SockDgram_H

#include <map>
#include "SocketBase.h"
#include "nil/IOChannel.h"
#include "nil/IODevice.h"
#include "nil/nil.h"
#include "SockDgram_IOChannel.h"
#include "SockDgram_SyncOpeartion.h"
#include "SockDgram_AsyncOpeartion.h"

class SockDgram_AIO_Read_Result;
class SockDgram_AIO_Write_Result;

static os_sockaddr_in PeerAddr_[MAXCONNECTIONS];

class SockDgram : public SocketBase
{
	friend class SockDgram_IOChannel;
	friend class SockDgram_AsyncOpeartion;
	friend class SockDgram_SyncOpeartion;

public:
	
	struct SockDgramParam
	{
		SockDgramParam();
		uint32 ipAddr_;
        uint16 port_;
	};

	SockDgram();
	virtual ~SockDgram();
	
	IOChannel * createChannel(int IOCHANNEL_TYPE, void * peerAddr);
	IOChannel * findIOChannel(void * dstAddr);
	int changeChannel(IOChannel *pOldChannel, IOChannel *pNewChannel);
	int finalizeChannel(void * peerAddr);

#ifdef OSAL_WIN32	
	int read(Msg_t *msg, void *arg, os_sockaddr *fromAddr);
	int write(Msg_t *msg, void *arg, os_sockaddr *toAddr);
#else
	int read(Msg_t *msg, void *arg, struct sockaddr_in *fromAddr);
	int write(Msg_t *msg, void *arg, struct sockaddr_in *toAddr);
#endif
	void dispatch(DevAddr_t ,int len, int readOrWrite);
	byte* getBuf();
	
private:
	int doOpen(void *);
	int doStop();
	int doStart();
    int doClose();
	int doConfigIOParam(void *);		
	
private:
	
	typedef int (*pfRWFunc_t)(os_socket, Msg_t *, void *, struct sockaddr *, int *);

	static pfRWFunc_t pfWrite_;
	static pfRWFunc_t pfRead_;
	
	//Native API wrapper functions.
	static int write(os_socket, Msg_t *, void *, struct sockaddr *, int *);	
	static int read(os_socket, Msg_t *, void *, struct sockaddr *, int *);
	
	static int asyncWrite(os_socket, Msg_t *, void *, struct sockaddr *, int *);	
	static int asyncRead(os_socket, Msg_t *, void *, struct sockaddr *, int *);

	//The size of buffers should be set by IODevice because
	//its characteristics are dependent on each device.

	int bufferLen_;	
	byte *pBuffer_;

	struct SockDgramParam ConfigParam_;
	os_sockaddr fromAddr_;
    int sockAddrLen_;

	//static os_sockaddr_in PeerAddr_[MAXCONNECTIONS];
	int connectedPeer_;

	SockDgram_AsyncOpeartion *pSockAsyncOperation_;
	SockDgram_SyncOpeartion *pSockSyncOperation_;

	typedef std::map< os_sockaddr_in *, IOChannel * > sockDgram_IOChannels;
    sockDgram_IOChannels ctChannelList;
};

#endif//SockDgram_H
