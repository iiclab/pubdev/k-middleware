#ifndef SOCKSTREAM_SYNCOPERATION_H
#define SOCKSTREAM_SYNCOPERATION_H

#include "osal/IRunnable.h"

class SockStream;

class SockStream_SyncOpeartion : public OSAL::IRunnable
{
public:
	SockStream_SyncOpeartion(SockStream *pSockStream);
	~SockStream_SyncOpeartion();

	void run();

private:
	SockStream *pOwnDevice_;
};

#endif//SOCKSTREAM_SYNCOPERATION_H




