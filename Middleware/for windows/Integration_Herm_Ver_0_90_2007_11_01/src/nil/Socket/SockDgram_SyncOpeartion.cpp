// =========================================================================
/**
 *  @filename	SockDgram_SyncOpeartion.cpp
 *	@Revised
 *  @Revision
 *  @Author		SeongHoon Kim
 *  
 *  @Description
 *				  SocketBase class
 *	@NOTE
 */
// =========================================================================
#include "SockDgram.h"
#include "SockDgram_SyncOpeartion.h"
#include "Debug.h"

SockDgram_SyncOpeartion::SockDgram_SyncOpeartion(SockDgram *pSockStream):pOwnDevice_(pSockStream)
{
	incomingMsg_.len_ = pOwnDevice_->bufferLen_;
	incomingMsg_.pData_ = pOwnDevice_->pBuffer_;
}

SockDgram_SyncOpeartion::~SockDgram_SyncOpeartion()
{
	
}
void 
SockDgram_SyncOpeartion::run()
{
    sockaddr_in fromAddr;
	DEBUGTRACE("[UDP Server] SockDgram_SyncOpeartion: run!");
    
    Msg_t ReadBuf;
    ReadBuf.pData_ = incomingMsg_.pData_;
	
	while(pOwnDevice_->getState() == IODevice::IODEV_ACTIVE)
	{
		MW_DEBUG((LM_DEBUG,"[UDP Server] SockDgram_SyncOpeartion: Ready to receive data!\n\n"));	
#ifdef OSAL_WIN32		
		int nRead = pOwnDevice_->read(&incomingMsg_, NULL, (os_sockaddr*)&fromAddr);
#else
		int nRead = pOwnDevice_->read(&incomingMsg_, NULL, &fromAddr);
#endif

		MW_DEBUG((LM_DEBUG,"[UDP Server] From IP address=%s, Port number=%d\n", 
		inet_ntoa(fromAddr.sin_addr), fromAddr.sin_port));

		if(nRead <= 0)
		{
			MW_DEBUG((LM_DEBUG,"[UDP Server] SockDgram_SyncOpeartion: Error!\n"));
            perror("SockDgram Read Error:");
		}
		else
        {
            MW_DEBUG((LM_DEBUG,"[UDP Server] SockDgram_SyncOpeartion: dispatch!\n"));
            ReadBuf.len_= nRead;
#ifdef OSAL_WIN32		
			pOwnDevice_->dispatch(&fromAddr, nRead, ACT_READEVENT);
#else
			pOwnDevice_->dispatch(&fromAddr, &ReadBuf, ACT_READEVENT);	
#endif
		}
		//Thread synchronization scheme is necessary.
	}
	pOwnDevice_->close();
}



