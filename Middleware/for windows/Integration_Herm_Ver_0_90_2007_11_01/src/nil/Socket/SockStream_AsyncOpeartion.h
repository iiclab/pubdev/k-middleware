#ifndef SOCKSTREAM_ASYNCOPERATION_H
#define SOCKSTREAM_ASYNCOPERATION_H

#include "osal/AsyncOperation.h"
#include "osal/IAsyncOperation.h"

class SockStream;

class SockStream_AsyncOpeartion : public OSAL::AsyncOperation
{
public:
	SockStream_AsyncOpeartion(SockStream * pSockStream);
	virtual ~SockStream_AsyncOpeartion();
	
	void asyncOperation();
    void handleUserDefinedSystemEvent(OSAL_SOCKET);

private:
	SockStream *pOwnDevice_;
    Msg_t incomingMsg_;
};

#endif//SOCKSTREAM_ASYNCOPERATION_H





