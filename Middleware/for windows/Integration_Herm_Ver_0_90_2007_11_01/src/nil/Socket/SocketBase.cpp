#include "SocketBase.h"
#include "Debug.h"
#include "errorno.h"

#ifdef OSAL_WIN32

SocketBase::SocketBase():connectedNumbers_(0)
{
	memset ( &os_InitialSock_, 0, sizeof ( os_InitialSock_ ) );
	//memset ( os_ConnectSock_, 0, sizeof ( os_ConnectSock_ ) * MAXCONNECTIONS );

	WSADATA wsa;

	if (WSAStartup(MAKEWORD(2,2), &wsa))
	{
		//print error message
	}
}

SocketBase::~SocketBase()
{
	//finalize async socket
	if (WSACleanup())
	{
		//get error 
		WSAGetLastError();
	}

	::closesocket(os_InitialSock_);
}

int
SocketBase::openSyncSocket(int af, int type, int protocol)
{
	return (os_InitialSock_ = ::socket(af, type, protocol));
}

int
SocketBase::openAsyncSocket(int af, int type, int protocol, os_sockspec lpProtocolInfo, os_sockgroup g, os_sockflag dwFlags, bool AcceptOrConnect)
{
	if (AcceptOrConnect == _ACCEPT_USE_)
	{
		return (os_InitialSock_ = ::WSASocket(af, type, protocol, lpProtocolInfo, g, dwFlags));
	} else
	{
		return (os_ConnectSock_[connectedNumbers_] = ::WSASocket(af, type, protocol, lpProtocolInfo, g, dwFlags));
	}

	
}

int
SocketBase::connect(sockaddr *pServAddr, int addrlen)
{
	return ::connect (os_ConnectSock_[connectedNumbers_], pServAddr, addrlen);
}

os_socket
SocketBase::accept(sockaddr *pSockAddr, int *pAddrLen)
{
	return ::accept(os_InitialSock_, pSockAddr, pAddrLen);
}

int 
SocketBase::listen()
{
	return ::listen(os_InitialSock_, MAXCONNECTIONS);
}

int
SocketBase::control(ControlMode cmd, void *arg)
{
	SocketControlParam_t * pCtrl = (SocketControlParam_t *)arg;
	int ret;
	switch(cmd) {
	case SETPARAMS:
		ret = ::setsockopt(pCtrl->sd_,pCtrl->level_, pCtrl->optname_, pCtrl->optval_, (*pCtrl->optlen_));
		break;
	case GETPARAMS:
		ret = ::getsockopt(pCtrl->sd_,pCtrl->level_, pCtrl->optname_, pCtrl->optval_, pCtrl->optlen_);
		break;
	default:
		ret = MW_FAILED;
		break;
	}

	if(ret != MW_SUCCESS)
	{
		MW_ERRMSG((LM_ERROR, "SocketBase::control Error!\n"));
	}
	return ret;
}

int
SocketBase::bind(uint32 ipAddr, uint16 port)
{
	os_sockaddr_in addr;

	memset(&addr, 0,sizeof(os_sockaddr_in));
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = ipAddr;
	addr.sin_port = port; 
	//addr.sin_port = htons(port); 

	if(::bind(os_InitialSock_, (sockaddr *)&addr, sizeof(addr)) == SOCKET_ERROR)
		return SOCKET_ERROR;

	return MW_SUCCESS;
}

#else //linux


#endif 

os_socket
SocketBase::getSD(bool AcceptOrConnet)
{
	if (AcceptOrConnet == _ACCEPT_USE_)
		return os_InitialSock_;
	else
		return os_ConnectSock_[connectedNumbers_];
}

void 
SocketBase::setSD(os_socket sd)
{
	os_InitialSock_ = sd;
}
