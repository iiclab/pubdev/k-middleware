#ifndef SOCKETSTREAMIMPLEMENTATION_H
#define SOCKETSTREAMIMPLEMENTATION_H

#include "ConfigAll.h"



class sockStreamImplementation : public SockBaseImplementation
{

public:


	sockStreamImplementation();
	virtual ~sockStreamImplementation();



	//data entity functions
	IOChannel * createChannel(int IOCHANNEL_TYPE, void * peerAddr);	
	IOChannel * findIOChannel(void * dstAddr);
	int changeChannel(IOChannel *pOldChannel, IOChannel *pNewChannel);
	int finalizeChannel(void * peerAddr);

	//read and write operation
	virtual int read(os_socket sd, Msg_t *msg) = 0;
	virtual int write(os_socket sd, Msg_t *msg) = 0;
	virtual void dispatch(os_socket, Msg_t *, int ) = 0;

	//int connect2Server(uint32 ipAddr, uint16 port);	
	//os_socket acceptSocket();

	void setRole(bool);
	bool getRole();	

protected:
	virtual int doOpen(void *);
	virtual int doStop();
	virtual int doStart();
	virtual int doClose();
	virtual int doConfigIOParam(void *);

	//The size of buffers should be set by IODevice because
	//its characteristics are dependent on each device.


};

#endif //SOCKETSTREAMIMPLEMENTATION_H
