// =========================================================================
/**
 *  @filename	SockDgram_IOChannel.cpp
 *	@Revised
 *  @Revision
 *  @Author		SeongHoon Kim
 *  
 *  @Description
 * 
 *	@NOTE
 */
// =========================================================================
#ifndef SOCKDGRAM_IOCHANNEL_H
#define SOCKDGRAM_IOCHANNEL_H

#include "ConfigMacros.h"
#include "nil/IOChannel.h"

class SockDgram;
class SockDgram_IOChannel_Thread;

class SockDgram_IOChannel : public IOChannel
{
public:

	SockDgram_IOChannel( DevAddr_t , SockDgram* );	
	virtual ~SockDgram_IOChannel();
	
	void start();
	int close();
        
    //int read(Msg_t *msg, sockaddr_in *fromAddr );	
	int dataRequest(void * arg, Msg_t *msg);
	void dataIndication(void * arg, Msg_t *msg);
	
	IODevice * getOwnDevice();    
    OSAL::IRunnable* getRunnable();

private:	
	OSAL_SOCKET sd_;	
	SockDgram *pOwnDevice_;
	
	struct sockaddr_in remote;        
	SockDgram_IOChannel_Thread *runnable_;
};

class SockDgram_IOChannel_Thread : public OSAL::IRunnable
{
public:
	SockDgram_IOChannel_Thread(SockDgram_IOChannel *);	
	virtual ~SockDgram_IOChannel_Thread();
	
	void run();
protected:
    
private:
	SockDgram_IOChannel *pSockDgram_IOChannel_;
};


#endif//SOCKDGRAM_IOCHANNEL_H
