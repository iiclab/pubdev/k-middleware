// =========================================================================
/**
 *  @filename	SockStream_AsyncOpeartion.h
 *	@Revised
 *  @Revision
 *  @Author		SeongHoon Kim
 *  
 *  @Description
 *				  Socket Stream class
 *	@NOTE
 */
// =========================================================================

#include "osal/AsyncOperation.h"
#include "SockStream.h"
#include "SockStream_AsyncOpeartion.h"
#include "SockStream_IOChannel.h"
#include "Debug.h"

SockStream_AsyncOpeartion::SockStream_AsyncOpeartion(SockStream * pSockStream): pOwnDevice_(pSockStream)
{
	setAOPType(OSAL::AOP_TYPE_USER_CALLBACK);
	setHandle(pOwnDevice_->getHandle());
    
    incomingMsg_.len_ = pOwnDevice_->bufferLen_;
	incomingMsg_.pData_ = pOwnDevice_->pBuffer_;
}

SockStream_AsyncOpeartion::~SockStream_AsyncOpeartion()
{
	pOwnDevice_ = NULL;
}

void 
SockStream_AsyncOpeartion::asyncOperation()
{
	DEBUGTRACE("SockStream_AsyncOpeartion::asyncOperation");
	//make all Stream channels perform asyncOperation 
//	pOwnDevice_->SockStream_IOChannel_DemuxTable_.start();
	//pOwnDevice_->read(pOwnDevice_->bufferLen_, pOwnDevice_->pBuffer_);
}

void 
SockStream_AsyncOpeartion::handleUserDefinedSystemEvent(os_socket sd)
{
    DEBUGTRACE("[TCP Server] SockStrean_AsyncOpeartion: run!");       
    Msg_t ReadBuf;
    ReadBuf.pData_ = incomingMsg_.pData_;
    
    MW_DEBUG((LM_DEBUG,"[TCP Server] SockStrean_AsyncOpeartion: Ready to receive data!\n\n"));	
    int nRead = pOwnDevice_->read(sd, &incomingMsg_);
        
    if(nRead < 0)
    {
        MW_DEBUG((LM_DEBUG,"[TCP Server] SockStrean_AsyncOpeartion: Error!\n"));
        perror("SockStream Read Error:");
		pOwnDevice_->dispatch(sd, 0, ACT_CLOSEEVENT);
    }
    else
    {
        MW_DEBUG((LM_DEBUG,"[TCP Server] SockStrean_AsyncOpeartion: dispatch!\n"));
        ReadBuf.len_= nRead;
        pOwnDevice_->dispatch(sd, ReadBuf.len_, ACT_READEVENT);
    }       
}






