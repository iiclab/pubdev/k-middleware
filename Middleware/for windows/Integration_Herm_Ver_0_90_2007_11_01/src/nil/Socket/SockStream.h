#ifndef SOCKETSTREAM_H
#define SOCKETSTREAM_H

#include "SocketBase.h"
#include "SockStream_AsyncOpeartion.h"
#include "SockStream_AcceptWorker.h"
#include "SockStream_IOChannel.h"
#include "nil/nil.h"
#include "Utility/Msg_t.h"
#include <map>

#define SOCKSTREAM_BUFSIZE	BUFFER_SIZE
static os_sockaddr_in PeerTcpAddr_[MAXCONNECTIONS];

class SockStream_IOChannel;

class SockStream : public SocketBase
{
	friend class SockStream_IOChannel;
	friend class SockStream_AsyncOpeartion;
	friend class SockStream_AcceptWorker;
public:
	enum 
	{
		ROLE_SERVER = 0x01,
		ROLE_CLIENT = 0x02
	};

	SockStream();
	~SockStream();

	struct SockStreamParam
	{
		SockStreamParam();	
		uint32 ipAddr_;
		uint16 port_;
	};

	//data entity functions
	IOChannel * createChannel(int IOCHANNEL_TYPE, void * peerAddr);	
	IOChannel * findIOChannel(void * dstAddr);
	int changeChannel(IOChannel *pOldChannel, IOChannel *pNewChannel);
	int finalizeChannel(void * peerAddr);
	
	//read and write operation
	int read(os_socket sd, Msg_t *msg);
	int write(os_socket sd, Msg_t *msg);
    void dispatch(os_socket, int , int );

	//int connect2Server(uint32 ipAddr, uint16 port);	
	os_socket acceptSocket();
    void setRole(bool);
    bool getRole();	

private:	
	int doOpen(void *);
	int doStop();
	int doStart();
    int doClose();
	int doConfigIOParam(void *);	
	
private:

	typedef int (*pfRWFunc_t)(os_socket sd, Msg_t *msg, int32 *pFlag, void *arg);

	static pfRWFunc_t pfWrite_;	
	static pfRWFunc_t pfRead_;

	//Native API wrapper functions.
	static int syncWrite(os_socket sd, Msg_t *msg, int32 *pFlag, void *arg);	
	static int syncRead(os_socket sd, Msg_t *msg, int32 *pFlag, void *arg);

	static int asyncWrite(os_socket sd, Msg_t *msg, int32 *pFlag, void *arg);	
	static int asyncRead(os_socket sd, Msg_t *msg, int32 *pFlag, void *arg);

	int bufferLen_;	
	byte* pBuffer_;
	bool role_;
	int TCPstate_;
	os_socket currentAcceptSock_;
	int connectedPeer_;
	bool finalize_;

	SockStream_AsyncOpeartion *pSockAsyncOperation_;	
	SockStream_AcceptWorker *pSockAcceptWorker_;

	SockStreamParam ConfigParam_;

	typedef std::map<os_socket, IOChannel*> TCP_CHANNEL_MAP_TABLE;
	TCP_CHANNEL_MAP_TABLE m_tcpChannel_t;
};

#endif //SockStream_H

