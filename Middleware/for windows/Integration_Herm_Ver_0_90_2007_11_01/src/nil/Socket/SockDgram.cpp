#include "SockDgram.h"
#include "configAll.h"
#include "Debug.h"
#include "errorno.h"
#include <string.h>
#include <fcntl.h>

SockDgram::SockDgramParam::SockDgramParam()
{
	memset(this, 0, sizeof(SockDgram::SockDgramParam));
}

SockDgram::SockDgram() :connectedPeer_(0)
{
	//this buffer is set for default channel
	bufferLen_ = BUFFER_SIZE;
	pBuffer_ = new byte[BUFFER_SIZE];
	memset(pBuffer_, 0, BUFFER_SIZE);

	memset(&fromAddr_, 0, sizeof(fromAddr_));
	sockAddrLen_ = sizeof(struct sockaddr_in);

	DevType_ = _IP_BASED_DEVICE_;

	pSockAsyncOperation_ = NULL;
	pSockSyncOperation_ = NULL;
}

SockDgram::~SockDgram()
{
	doClose();
	OSAL_DELETE(pBuffer_);
}

IOChannel * 
SockDgram::createChannel(int IOCHANNEL_TYPE, void * pAddr)
{
	if(pAddr == NULL)
		return NULL;

	//In Socket Datagram, whether its connection is active or not is not important.
	if(IOCHANNEL_TYPE == IOCHANNEL_OPEN_ACTIVE)
	{

	}
	else if(IOCHANNEL_TYPE == IOCHANNEL_OPEN_PASIVE)
	{

	}

	PeerAddr_[connectedPeer_] = *(os_sockaddr_in *)pAddr;
	os_sockaddr_in *fromAddr = &PeerAddr_[connectedPeer_++];

	MW_DEBUG((LM_DEBUG,"*************************************************\n"));
	MW_DEBUG((LM_DEBUG,"[UDP] NEW CHANNEL IS CREATED FOR ADDR!\n"));
	MW_DEBUG((LM_DEBUG,"[UDP] <Remote Peer - IP:%s Port:%d>\n", 
		//    inet_ntoa(PeerAddr.sin_addr), ntohs(PeerAddr.sin_port)));
		inet_ntoa(fromAddr->sin_addr), ntohs(fromAddr->sin_port)));
	MW_DEBUG((LM_DEBUG,"*************************************************\n"));

	IOChannel *pIOC = new SockDgram_IOChannel(fromAddr, this);
	ctChannelList.insert(std::make_pair(fromAddr, pIOC));

	return pIOC;
}

IOChannel *
SockDgram::findIOChannel(void * peerAddr)
{
	sockDgram_IOChannels::iterator it;
	it = ctChannelList.find((sockaddr_in *)peerAddr);
	return it->second;
}

int 
SockDgram::changeChannel(IOChannel *pOldChannel, IOChannel *pNewChannel)
{
	//	SockDgram_IOChannel_DemuxTable_.assign((OSAL::SockAddr_in_t *)pOldChannel->getDstAddr(), pNewChannel);
	return MW_FAILED;
}

int 
SockDgram::finalizeChannel(void * peerAddr)
{
	sockDgram_IOChannels::iterator it;
	it = ctChannelList.find((sockaddr_in *)peerAddr);

	if(it == ctChannelList.end())
		return MW_FAILED;

	ctChannelList.erase(it);

	return MW_SUCCESS;
}

byte*
SockDgram::getBuf()
{
	return pBuffer_;
}

/* Because dispatch routine is invoked by the OS-callback function in asynchronous mode, 
* it should be completed as soon as possible.
*/
void
SockDgram::dispatch(DevAddr_t peerAddr, int len, int eventType)
{
	IOChannel *pIOC = NULL;
	os_sockaddr_in *storedAddr = NULL;
	os_sockaddr_in *comingAddr = NULL;
	byte *pBuf = NULL;

	//when dispatch is invoked by ACT, Address shall be NULL
	//In this case information about who sends data is stored in fromAddr_
	if(peerAddr != NULL)
	{
		comingAddr = (os_sockaddr_in *)peerAddr;
	}
	else
	{
		comingAddr = (os_sockaddr_in *)&fromAddr_;
	}

	sockDgram_IOChannels::iterator it;        
	for (it = ctChannelList.begin(); it != ctChannelList.end() ; it++)
	{
		storedAddr = it->first;
		if (!memcmp(storedAddr, comingAddr, sizeof(os_sockaddr_in)))
		{
			//peer address equals to existing address
			pIOC = it->second;
			break;
		}
		storedAddr = NULL;
	}

	if(eventType == ACT_READEVENT)
	{	
		//Data is received from whom but there is no channel.
		if(pIOC == NULL)
		{
			//if there is no channel, we should create channel, 
			this->channelJoinIndication(this, comingAddr);

			//find again
			for (it = ctChannelList.begin(); it != ctChannelList.end() ; it++)
			{
				if (!memcmp(it->first, comingAddr, sizeof(os_sockaddr_in)))
				{
					//peer address equals to existing address
					pIOC = it->second;
					break;
				}
			}

			//Should not anyone want to create a new channel, pSD_IOC will be NULL.
			if(pIOC == NULL)
				return;

			//pBuf = this->getBuf();
		}

		//check buffer
		//		if (pBuf == NULL)
		pBuf = pIOC->getBuffer();

		//If found the channel, indicate it.
		Msg_t msg = { len, pBuf };
		pIOC->dataIndication(NULL, &msg);

	}else
	{
		SentStatus_t sentStatus;
		sentStatus.BytesTransfered_ = len;
		sentStatus.status_ = 0;
		pIOC->dataConfirm(&sentStatus);
	}
}

int 
SockDgram::doOpen(void *fileDescriptor)
{
	DEBUGTRACE("SockDgram::doOpen");

	//read configuration file
	OSAL_UNUSED_ARG(fileDescriptor);

	//set operation mode
	this->setOpMode(pSysEventProcessor_->getOpMode());

	if(this->getOpMode() == OSAL::OPMODE_ASYNC)
	{
		//pfRead_ = SockDgram::asyncRead;
		//pfWrite_ = SockDgram::asyncWrite;
		pfRead_ = SockDgram::read;
		pfWrite_ = SockDgram::write;

#ifdef OSAL_WIN32

		if(this->openAsyncSocket(AF_INET, SOCK_DGRAM, IPPROTO_UDP, NULL, 0, WSA_FLAG_OVERLAPPED, _ACCEPT_USE_) == INVALID_SOCKET)
		{
			return INVALID_SOCKET;
		}
#else
		pOlReadACT_ = NULL;
		pOlWriteACT_= NULL; 

		if(this->socket(AF_INET, SOCK_DGRAM, 0) == -1)
		{
			perror("Failed to open socket");
			return -1;
		}

		int m_sockfd = this->getSD();
		int flag = fcntl(m_sockfd, F_GETFL, 0);            // socket status flags
		fcntl(m_sockfd, F_SETFL, flag | O_NONBLOCK);
#endif
	}
	else	/*	Synchronous mode*/
	{
		pfRead_ = SockDgram::read;
		pfWrite_ = SockDgram::write;

		//create socket
		if(this->openSyncSocket(AF_INET, SOCK_DGRAM, 0) == -1)
		{
			return -1;
		}

	}

	//set socket options
#ifdef OSAL_WIN32
	BOOL bOptVal = TRUE;
	int bOptLen = sizeof(BOOL);

	SocketControlParam_t scp[] = {
		{this->getSD(_ACCEPT_USE_), SOL_SOCKET, MW_SO_BROADCAST, (char*)&bOptVal, &bOptLen},				
	};
#else
	int one = 1;
	SocketControlParam_t scp[] = {
		{this->getSD(), SOL_SOCKET, SO_BROADCAST, &one},				
	};    
#endif

	if(control(SETPARAMS, scp) == -1 )
	{
		MW_ERRMSG((LM_DEBUG,"SockDgram::Broadcast is not enabled\n"));
		return -1;
	}

	//bind socket
	if(this->bind(ConfigParam_.ipAddr_, ConfigParam_.port_) != MW_SUCCESS)
		return MW_ERROR;

	MW_DEBUG((LM_DEBUG,"[Dev:Socket Dgram] UDP Service is ready to start!\n"));
	return MW_SUCCESS;
}

int 
SockDgram::doStart()
{
	int ret = MW_SUCCESS;

	if(getOpMode() == OSAL::OPMODE_ASYNC)
	{
#ifdef OSAL_WIN32      
		OSAL_SOCKET os_sock_desc = this->getSD(_ACCEPT_USE_);
		setHandle(pSysEventProcessor_->createHandleRequest());
		OSAL_HANDLE os_handle = this->getHandle();
		WSAEventSelect(os_sock_desc, os_handle, FD_READ);
#else
		setHandle(this->getSD());
#endif
		//TODO: Who will deallocate this allocated memory?
		pSockAsyncOperation_ = new SockDgram_AsyncOpeartion(this);
		ret = registerAsyncOperation(DevType_, os_handle, pSockAsyncOperation_, os_sock_desc);
	}
	else
	{	
		//TODO: Who will deallocate this allocated memory?
		pSockSyncOperation_ = new SockDgram_SyncOpeartion(this);
		//TODO: When it comes to sync operation, its return handle shall be managed by concrete IO devices.
		//		so, I have to implement it.
		ret=registerSyncOperation(pSockSyncOperation_);

	}
	return ret;
}

int 
SockDgram::doStop()
{
	return 0;
}

int 
SockDgram::doClose()
{
	if(this->getOpMode() == OSAL::OPMODE_ASYNC)
	{
		unregisterAsyncOperation(this->getHandle());
		OSAL_DELETE(pSockAsyncOperation_);
	}
	else
	{
		unregisterSyncOperation(pSockSyncOperation_);
		OSAL_DELETE(pSockSyncOperation_);
	}

	//return closesocket(m_sock_);
	return 0;
}

int 
SockDgram::doConfigIOParam(void *configure)
{
	struct configureAddr* pCa = (configureAddr *)configure;

	ConfigParam_.ipAddr_ = inet_addr(pCa->_IP_ADDR_);
	ConfigParam_.port_ = htons(pCa->_PORT_UDP_NO_);

	this->setName("_UDP_DEVICE_NAME_");
	return MW_SUCCESS;
}

SockDgram::pfRWFunc_t 
SockDgram::pfWrite_ = (0);

SockDgram::pfRWFunc_t 
SockDgram::pfRead_ = (0);

int 
SockDgram::read(Msg_t *msg, void *arg, os_sockaddr *fromAddr)
{	
	return (*pfRead_)(os_InitialSock_, msg, NULL, fromAddr, &sockAddrLen_);
}

int
SockDgram::write(Msg_t *msg, void *arg, os_sockaddr *toAddr)
{
	return (*pfWrite_)(os_InitialSock_, msg, NULL, toAddr,&sockAddrLen_);
}

int 
SockDgram::write(os_socket sd, Msg_t *msg, void *arg, os_sockaddr *toAddr , int *toLen)
{
	return sendto(sd, (char *)msg->pData_, msg->len_, 0, toAddr, *toLen);
}

int 
SockDgram::read(os_socket sd, Msg_t *msg, void *arg, os_sockaddr *fromAddr , int *fromLen)
{
	return recvfrom(sd,(char *)msg->pData_, msg->len_, 0, fromAddr, fromLen);
}

int 
SockDgram::asyncWrite(os_socket sd, Msg_t *msg, void *arg, struct sockaddr *toAddr , int *toLen)
{
	uint32 nWrite = 0;
	int ret;
	int flag = 0;

#ifdef OSAL_WIN32
/*	WSABUF DataBuf;
	DataBuf.len = msg->len_;
	DataBuf.buf = (char*)msg->pData_;
	//When writing data in Socket Datagram asynchronously, each channel must have its own ACT.
	SockDgram_AIO_Write_Result *pACT = (SockDgram_AIO_Write_Result *)arg;

	pACT->setEventAddr((DevAddr_t)toAddr);
	pACT->setTransaction(msg);

	ret = ::WSASendTo( sd, &DataBuf, 1, &nWrite, flag, toAddr, *toLen,
		LPWSAOVERLAPPED(pACT), SockDgram::WriteCompletionROUTINE);

	if(ret == SOCKET_ERROR)
	{
		int err = ::WSAGetLastError();
		if( err != WSA_IO_PENDING)
		{
			MW_ERRMSG((LM_ERROR,"SockDgram::asyncWrite() : Error occured #(%d)\n",err));
			return MW_ERROR;
		}
	}
	*/
#else
	char* tmp;                               // void* ??歃濌?毳??勴暔

	size_t leftN;                            // ?办暭???? ?办澊????
	ssize_t sendN;                           // ?瓣赴???标车???办澊????
	tmp = (char*) msg->pData_; 
	nWrite = leftN = msg->len_;

	while (leftN > 0) // ???办澊?瓣? ?晞?堧姅?欖晥
	{                      
		if ((sendN = sendto(sd, tmp, leftN, 0, toAddr,*toLen) ) <= 0) 
		{
			if(errno==EWOULDBLOCK)
			{
				continue;
			}
			return(-1);
		}
		leftN -= sendN;             // ?办暭???办澊?半焿 臧愳唽
		tmp += sendN;               // ?瓣赴 ?标车???办澊?瓣箤歆� ?澑?办澊??
	} 

#endif//OSAL_WIN32

	return nWrite;
}

int 
SockDgram::asyncRead(os_socket sd, Msg_t *msg, void *arg, os_sockaddr *fromAddr, int *fromLen)
{
	uint32 nRead = 0;
	int ret;
	int flag = 0;

#ifdef OSAL_WIN32
/*	WSABUF DataBuf;
	SockDgram_AIO_Read_Result *pACT = (SockDgram_AIO_Read_Result *)arg;
	//	pACT->setEventAddr((DevAddr_t)fromAddr);

	DataBuf.len = msg->len_;
	DataBuf.buf = (char*)msg->pData_;

	//memset(fromAddr, 0, sizeof(os_sockaddr));

	ret = WSARecvFrom( sd, &DataBuf, 1, &nRead, (LPDWORD)&flag, fromAddr , fromLen,
		LPWSAOVERLAPPED(pACT), SockDgram::ReadCompletionROUTINE);

	if(ret == SOCKET_ERROR)
	{
		int err = ::WSAGetLastError();
		if( err != WSA_IO_PENDING)
		{
			MW_ERRMSG((LM_ERROR,"SockDgram::asyncRead() : Error occured #(%d)\n",err));
			return MW_ERROR;
		}
	}
	*/
#else
	if ((nRead = recvfrom(sd, (char *)msg->pData_, msg->len_, 0, fromAddr, (socklen_t *)fromLen ) ) < 0) 
	{
		MW_ERRMSG((LM_ERROR,"SockDgram::asyncRead() Failed\n"));
	}

#endif//OSAL_WIN32

	return nRead;
}

