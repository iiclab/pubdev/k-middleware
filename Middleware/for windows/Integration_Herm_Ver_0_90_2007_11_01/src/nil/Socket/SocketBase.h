#ifndef SOCKETBASE_H
#define SOCKETBASE_H

#include "nil/IODevice.h"
#include "nil/IOProcess.h"
#include "osal/OSAL_Socket.h"

#define BUFFER_SIZE 4086
#define _ACCEPT_USE_	TRUE
#define _CONNECT_USE_	FALSE
#define MAXCONNECTIONS	60

const int MAXHOSTNAME = 200;
const int MAXRECV = 500;

enum{
	STATE_LISTEN = 0x01,
	STATE_CONNECT = 0x02,
	STATE_CONNECTED = 0x04
};

class SocketBase : public IODevice, public IOProcess
{
public:
    SocketBase();
    virtual ~SocketBase();
    
	//open socket 
    int openSyncSocket(int af, int type, int protocol);
	int openAsyncSocket(int af, int type, int protocol, os_sockspec lpProtocolInfo, os_sockgroup g, os_sockflag dwFlags, bool);
    
	//device function 
    int control(ControlMode cmd, void *arg);
	int connect(sockaddr *pServAddr, int addrlen);
	int bind(uint32 ip_address, uint16 port_address);    
	os_socket accept(sockaddr *pSockAddr, int *pAddrLen);
	int listen();

	//virtual bool 
	//DWORD host2net(DWORD v);		//Convert from host to network byte order. 
	//DWORD net2host(DWORD v);		//Convert from network to host byte order. 
    
	//socket set/get function
    os_socket getSD(bool);    
    void setSD(os_socket sd);
         
protected:
	//socket descriptor
	os_socket os_InitialSock_;
	os_socket os_ConnectSock_[MAXCONNECTIONS];
	os_sockaddr_in os_sockaddr_in_;
	int connectedNumbers_;
                
};

#endif //SOCKETBASE_H

