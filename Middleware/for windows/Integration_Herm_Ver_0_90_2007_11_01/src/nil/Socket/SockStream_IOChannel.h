#ifndef SOCKSTREAM_IOCHANNEL_H
#define SOCKSTREAM_IOCHANNEL_H

#include "nil/IOChannel.h"
#include "osal/IRunnable.h"

class SockStream;
class SockStream_IOChannel_Thread;
class SockStream_AIO_Read_Result;
class SockStream_AIO_Write_Result;

class SockStream_IOChannel : public IOChannel 
{
public:

	SockStream_IOChannel(DevAddr_t DestAddr, SockStream *pSockStream);
	SockStream_IOChannel(const SockStream_IOChannel &init);
	virtual ~SockStream_IOChannel();
	
	void start();
    int close();
    //int control (ControlMode cmd, void *arg);
	int read(Msg_t *msg);
	
    //IOChannel mandatory interface
    int dataRequest(void * arg, Msg_t *msg);
	void dataIndication(void * arg, Msg_t *msg);	
	
	IODevice * getOwnDevice();
    
	OSAL::IRunnable* getRunnable();
	//OSAL::AsyncOperation* getAsyncOperation();
    
    void setSockDesc(OSAL_SOCKET);
    OSAL_SOCKET getSockDesc();

	void setHandle(OSAL_HANDLE);
	OSAL_HANDLE getHandle();
      
private:
	SockStream_IOChannel_Thread *runnable_;
//	SockStream_IOChannel_AsyncOp *runnable_;
    
	OSAL_SOCKET sd_;
	OSAL_HANDLE hd_;
    struct sockaddr_in remote; 
    
    SockStream *pOwnDevice_;
};

class SockStream_IOChannel_Thread : public OSAL::IRunnable
{
public:
	SockStream_IOChannel_Thread(SockStream_IOChannel *);	
	virtual ~SockStream_IOChannel_Thread();
	
	void run();
protected:
    
private:
	SockStream_IOChannel *pSockStream_IOChannel_;
};

#endif//SOCKSTREAM_IOCHANNEL_H

