#include "SockDgram.h"
#include "SockDgram_IOChannel.h"
#include "configAll.h"
#include "Debug.h"

SockDgram_IOChannel::SockDgram_IOChannel(DevAddr_t pDestAddr,SockDgram * pSockDgram): 
	IOChannel(pDestAddr), pOwnDevice_(pSockDgram)
{
	bufferLen_ = BUFFER_SIZE;
	pBuffer_ = pOwnDevice_->getBuf();
	runnable_ = NULL;
    this->setInterfaceInformation(_INTERFACE_UDP_);

    memset(&remote, 0, sizeof(remote));
	remote = *(sockaddr_in *)pDestAddr;
                 
	//distinguish broadcast channel
    char* dstAddr = inet_ntoa(remote.sin_addr);    
    if (strcmp(dstAddr, "255.255.255.255") == 0)
	{
		broadcast_ = true;
	} 
	/*
	else
	{
		pBuffer_ = new byte[BUFFER_SIZE];
		memset(pBuffer_, 0, BUFFER_SIZE);
	}
*/
    this->setMaxPayload(BUFFER_SIZE);
}

SockDgram_IOChannel::~SockDgram_IOChannel()
{
	pOwnDevice_ = NULL;
    OSAL_DELETE(runnable_);
}

void 
SockDgram_IOChannel::start()
{
#ifdef OSAL_WIN32
	if(pOwnDevice_->getOpMode() == OSAL::OPMODE_ASYNC)
	{
		Msg_t msg;
		msg.len_ = bufferLen_;
		msg.pData_ = pBuffer_;
		//pOwnDevice_->read(&msg, NULL, NULL);
		//pOwnDevice_->write(&msg, NULL, (struct sockaddr *)this->getDstAddr());
	}
#else
	if(runnable_ != NULL)
		return;
	
	//runnable_ = new SockDgram_IOChannel_Thread(this);
	//pOwnDevice_->registerSyncOperation(runnable_);

#endif
}

/*
//Blocking Read operation
int 
SockDgram_IOChannel::read(Msg_t *msg, sockaddr_in *fromAddr )
{
    return pOwnDevice_->read(msg, NULL, fromAddr);
}
*/
int
SockDgram_IOChannel::dataRequest(void * arg, Msg_t *msg)
{    
#ifdef OSAL_WIN32
	return pOwnDevice_->write(msg, NULL, (SOCKADDR*)getDstAddr());
#else
	MW_DEBUG((LM_DEBUG,"[UDP] Send message to IP address=%s, Port number=%d\n", 
		inet_ntoa(remote.sin_addr), ntohs(remote.sin_port)));
    
    return pOwnDevice_->write(msg, NULL, &remote);
#endif
}

void
SockDgram_IOChannel::dataIndication(void * arg, Msg_t *msg)
{
	getInputHandler()->handleInput((void *)this->getID(), msg);
}

IODevice * 
SockDgram_IOChannel::getOwnDevice()
{
	return pOwnDevice_;
}

OSAL::IRunnable *
SockDgram_IOChannel::getRunnable()
{
	return runnable_;
}

int 
SockDgram_IOChannel::close()
{
	pOwnDevice_->channelLeaveIndication(pOwnDevice_, this->getDstAddr());
	pOwnDevice_->finalizeChannel(this);
    
    //close thread 
	pOwnDevice_->unregisterSyncOperation(this->getRunnable());
	
	return 0;
}


SockDgram_IOChannel_Thread::SockDgram_IOChannel_Thread(SockDgram_IOChannel *pSockDgram_IOChannel)
:pSockDgram_IOChannel_(pSockDgram_IOChannel)
{

}

SockDgram_IOChannel_Thread::~SockDgram_IOChannel_Thread()
{

}

void 
SockDgram_IOChannel_Thread::run()
{	
    sockaddr_in fromAddr;
    Msg_t incomingBuf, ReadBuf;

	memset(&fromAddr,0, sizeof(sockaddr_in));
    
	incomingBuf.len_ = pSockDgram_IOChannel_->getSizeofBuffer();
	incomingBuf.pData_ = ReadBuf.pData_ = pSockDgram_IOChannel_->getBuffer();
	
	int nRead = 0;
	DEBUGTRACE("SockDgram_SyncOpeartion: run!");
	
	while((pSockDgram_IOChannel_ != NULL) ) 
	{
		MW_DEBUG((LM_DEBUG,"SockDgram_SyncOpeartion: Ready to receive data!\n\n"));	
//		int nRead = pSockDgram_IOChannel_->read(&incomingBuf, &fromAddr);
		  		
		MW_DEBUG((LM_DEBUG,"[UDP] From IP address=%s, Port number=%d\n", 
		inet_ntoa(fromAddr.sin_addr), ntohs(fromAddr.sin_port)));

        if(nRead > 0)
		{
			ReadBuf.len_= nRead;
			
			pSockDgram_IOChannel_->dataIndication(NULL, &ReadBuf);
			MW_DEBUG((LM_DEBUG,"SockDgram_IOChannel_Thread::Read data is succeeded nRead %d!\n",nRead));
		}
		else
		{
			MW_DEBUG((LM_DEBUG,"SockDgram_IOChannel_Thread:: Error on reading data"));
			pSockDgram_IOChannel_->close();
			break;
		}
	}  
}
