#include "SockDgram_AsyncOpeartion.h"
#include "Debug.h"

SockDgram_AsyncOpeartion::SockDgram_AsyncOpeartion(SockDgram * pSockDgram): pOwnDevice_(pSockDgram)
{
	setAOPType(OSAL::AOP_TYPE_USER_CALLBACK);
	setHandle(pOwnDevice_->getHandle());
    
	incomingMsg_.len_ = pOwnDevice_->bufferLen_;
	incomingMsg_.pData_ = pOwnDevice_->pBuffer_;
}

SockDgram_AsyncOpeartion::~SockDgram_AsyncOpeartion()
{
	pOwnDevice_ = NULL;
}

void 
SockDgram_AsyncOpeartion::asyncOperation()
{
	//byte buf[3] = {0x01, 0x02, 0x03};	
	//pOwnDevice_->SockDgram_IOChannel_DemuxTable_.start();	
	//pOwnDevice_->read( &incomingMsg_, NULL, NULL);
}

void 
SockDgram_AsyncOpeartion::handleUserDefinedSystemEvent(OSAL_SOCKET sd)
{      
    os_sockaddr_in fromAddr;
    DEBUGTRACE("[UDP Server] SockDgram_AsyncOpeartion: run!");
    
    Msg_t ReadBuf;
    ReadBuf.pData_ = incomingMsg_.pData_;
    
    MW_DEBUG((LM_DEBUG,"[UDP Server] SockDgram_AsyncOpeartion: Ready to receive data!\n\n"));	
#ifdef OSAL_WIN32		
	int nRead = pOwnDevice_->read(&incomingMsg_, NULL, (os_sockaddr*)&fromAddr);
#else
	int nRead = pOwnDevice_->read(&incomingMsg_, NULL, &fromAddr);
#endif
    
    MW_DEBUG((LM_DEBUG,"[UDP Server] From IP address=%s, Port number=%d\n", 
    inet_ntoa(fromAddr.sin_addr), ntohs(fromAddr.sin_port)));

    if(nRead <= 0)
    {
        MW_DEBUG((LM_DEBUG,"[UDP Server] SockDgram_AsyncOpeartion: Error!\n"));
        perror("SockDgram Read Error:");
    }
    else
    {
        MW_DEBUG((LM_DEBUG,"[UDP Server] SockDgram_AsyncOpeartion: dispatch!\n"));
        ReadBuf.len_= nRead;
#ifdef OSAL_WIN32		
		pOwnDevice_->dispatch(&fromAddr, nRead, ACT_READEVENT);
#else
		pOwnDevice_->dispatch(&fromAddr, &ReadBuf, ACT_READEVENT);	
#endif
    }   
}
