#include "SockStream.h"
#include "errorno.h"
#include "ConfigMacros.h"

//////////////////////////////////////////////
// implementation of socket stream class 
//////////////////////////////////////////////

SockStream::SockStreamParam::SockStreamParam()
{
	memset(this, 0, sizeof(SockStream::SockStreamParam));
}

SockStream::SockStream():connectedPeer_(0), finalize_(false)
{
	TCPstate_ = STATE_LISTEN;
	//role_ = ROLE_SERVER;
	//role_ = ROLE_CLIENT;	//Default Client
	bufferLen_ = 0;
	pBuffer_ = new byte[BUFFER_SIZE];
	currentAcceptSock_ = 0;

	DevType_ = _IP_BASED_DEVICE_;

	pSockAsyncOperation_ = NULL;
	pSockAcceptWorker_ = NULL;
}

SockStream::~SockStream()
{
	OSAL_DELETE(pSockAcceptWorker_);
	OSAL_DELETE(pSockAsyncOperation_);
}

IOChannel * 
SockStream::createChannel(int IOCHANNEL_TYPE, void * peerAddr)
{
	DEBUGTRACE("SockStream::createChannel");
	os_sockaddr_in *pServerAddr = (os_sockaddr_in *)peerAddr;

	if (!currentAcceptSock_)
	{
		//connect socket
		if(this->openAsyncSocket(PF_INET, SOCK_STREAM, IPPROTO_TCP,NULL, 0, WSA_FLAG_OVERLAPPED, _CONNECT_USE_) == INVALID_SOCKET)
		{
			return NULL;
		}

		if(this->connect( (sockaddr *)pServerAddr, sizeof(sockaddr_in) ) != MW_SUCCESS)
		{
#ifdef OSAL_WIN32
			int err = ::WSAGetLastError();
			if( err != WSA_IO_PENDING)
			{
				MW_ERRMSG((LM_ERROR,"SockStream::asyncWrite::Error occured #(%d)\n",err));
				return NULL;
			}
#else
			perror("error:");
			return NULL;
#endif
		}
	}

	//only if its role is client, we process rest parts
	//This is for a client but already connected.
	SockStream_IOChannel *pIOC = new SockStream_IOChannel(peerAddr, this);

	//register async operation
	OSAL_HANDLE os_handle = pSysEventProcessor_->createHandleRequest();
	pIOC->setHandle(os_handle);
	
	int os_socket;
	if (currentAcceptSock_)			
		os_socket = currentAcceptSock_;
	else		
		os_socket = this->getSD(_CONNECT_USE_);
	
	WSAEventSelect(os_socket, os_handle, FD_READ | FD_CLOSE);
	registerAsyncOperation(DevType_, os_handle, pSockAsyncOperation_, os_socket);
	
	if (os_socket && currentAcceptSock_)	//server
	{
		m_tcpChannel_t.insert(std::make_pair(currentAcceptSock_, pIOC));
		currentAcceptSock_ = 0;
	} 
	else	//client
	{
		m_tcpChannel_t.insert(std::make_pair(os_socket, pIOC));
	}

	this->connectedNumbers_++;

	return pIOC;
}

IOChannel *
SockStream::findIOChannel(void * dstHandle)
{
	os_socket *pSd = (os_socket *)dstHandle;
	TCP_CHANNEL_MAP_TABLE::iterator it;    
	it = m_tcpChannel_t.find(*pSd);
	IOChannel *pIOC = it->second;
	return pIOC;
}

int 
SockStream::changeChannel(IOChannel *pOldChannel, IOChannel *pNewChannel)
{
	//SockStream_IOChannel_DemuxTable_.assign((OSAL_SOCKET)pOldChannel->getDstAddr(), pNewChannel);
	return MW_FAILED;
}

int 
SockStream::finalizeChannel(void *dstHandle)
{
	os_socket connectedSocket = *(os_socket *)dstHandle;
	TCP_CHANNEL_MAP_TABLE::iterator it;
	it = m_tcpChannel_t.find(connectedSocket);
	IOChannel *pIOC = it->second;

	if(pIOC == NULL)
	{
		return MW_ERROR;
	}
	else
	{
#ifdef OSAL_WIN32
		/*
		if(this->getOpMode() == OSAL::OPMODE_ASYNC && !finalize_)
		{
			finalize_ = true;
			this->channelLeaveIndication(this, &connectedSocket);
			return MW_SUCCESS;
		}*/
		SockStream_IOChannel* pSockChannel = (SockStream_IOChannel*) pIOC;
		OSAL_HANDLE sock_handle = pSockChannel->getHandle();
		this->unregisterAsyncOperation(sock_handle);
#else

#endif
		//close channel in channel table
		m_tcpChannel_t.erase(it);        
		//close socket
		OSAL_DELETE(pIOC);
		finalize_ = false;

		return MW_SUCCESS;
	}
}

void 
SockStream::dispatch(OSAL_SOCKET sd, int len, int readOrWrite)
{
	IOChannel *pIOC = NULL;
	TCP_CHANNEL_MAP_TABLE::iterator pos;
	for ( pos=m_tcpChannel_t.begin() ; pos!=m_tcpChannel_t.end() ; pos++ )
	{
		if ( pos->first == sd )
		{
			pIOC = pos->second;
			break;
		}
	}	

	//process close socket 
	if (len == 0)
	{
		//notify leave channel 
		//os_sockaddr_in *peerAddr = (os_sockaddr_in*)pIOC->getDstAddr();
		//this->channelLeaveIndication(this, peerAddr);
		this->channelLeaveIndication(this, &sd);
		return;
	}

	if(readOrWrite == ACT_READEVENT)
	{	
		//data indication
		Msg_t msg= {len, this->pBuffer_};
		pIOC->dataIndication(NULL, &msg);
	}
	else
	{
		SentStatus_t sentStatus;
		sentStatus.BytesTransfered_ = len;
		sentStatus.status_ = 0;
		pIOC->dataConfirm(&sentStatus);
	}
}

int
SockStream::doOpen(void *configure)
{
	DEBUGTRACE("SockStream::doOpen");
	int retval = MW_SUCCESS;
	this->setOpMode(pSysEventProcessor_->getOpMode());     //set operation mode

	if(this->getOpMode() == OSAL::OPMODE_ASYNC)
	{
		/*
		pfRead_ = SockStream::asyncRead;
		pfWrite_ = SockStream::asyncWrite;  */
		pfRead_ = SockStream::syncRead;
		pfWrite_ = SockStream::syncWrite;

		//accept socket
		if(this->openAsyncSocket(PF_INET, SOCK_STREAM, IPPROTO_TCP,NULL, 0, WSA_FLAG_OVERLAPPED, _ACCEPT_USE_) == INVALID_SOCKET)
		{
			return INVALID_SOCKET;
		}
	}
	/*	Synchronous mode*/
	else
	{
		pfRead_ = SockStream::syncRead;
		pfWrite_ = SockStream::syncWrite;

		//All device shall have OpHandle
		if(this->openSyncSocket(PF_INET, SOCK_STREAM, IPPROTO_TCP) == INVALID_SOCKET)
		{
			return INVALID_SOCKET;
		}
	}

	if(!role_)	//Server
		return retval;

	//codes below are for server.
	this->bind(ConfigParam_.ipAddr_, ConfigParam_.port_);	

	//set socket options
	BOOL sdReuse = TRUE;
	int optlen_ = sizeof(BOOL);
	SocketControlParam_t scp[] = {
		{this->getSD(_ACCEPT_USE_), SOL_SOCKET, MW_SO_REUSEADDR, (char*)&sdReuse, &optlen_},
	};
	/*
	int one = 1;
	SocketControlParam_t scp[] = {
	{m_sock_, SOL_SOCKET, SO_REUSEADDR, &one},				
	};    
	*/	
	if(control(SETPARAMS, &scp[0]) != MW_SUCCESS)
		return MW_FAILED;

	MW_DEBUG((LM_DEBUG,"[Dev:Socket Stream] TCP Service is ready to start!\n"));

	return 0;
}

int 
SockStream::doStart()
{
	int ret = MW_SUCCESS;
	DEBUGTRACE("SockStream::doStart");
	pSockAsyncOperation_ = new SockStream_AsyncOpeartion(this);

	if (!role_)
		return ret;

 	if(pSockAcceptWorker_ == NULL)
 	{
		/*
 		pSockAcceptWorker_ = new SockStream_AcceptWorker(this);
 		//Accept Routine is executed independent of it operating modes.
 		ret=registerSyncOperation(pSockAcceptWorker_);
 		
 		if(ret != MW_SUCCESS)
 			return MW_FAILED;
			*/
	}

	if (getOpMode() == OSAL::OPMODE_ASYNC) 
	{
#ifdef OSAL_WIN32      
		//handle for accept
		OSAL_SOCKET os_socket_desc = this->getSD(_ACCEPT_USE_);

		setHandle(pSysEventProcessor_->createHandleRequest());
		//pSockAsyncOperation_ = new SockStream_AsyncOpeartion(this);
		//ret = registerAsyncOperation(this->getHandle(), pSockAsyncOperation_, os_socket_desc);

		HANDLE hSockStream = pSysEventProcessor_->createHandleRequest();
		WSAEventSelect(os_socket_desc, hSockStream, FD_ACCEPT);

		//Accept Routine is executed independent of it operating modes.
		pSockAcceptWorker_ = new SockStream_AcceptWorker(this);		
		registerAsyncOperation(DevType_, hSockStream, pSockAcceptWorker_, os_socket_desc);

		ret = this->listen();		
#else
		setHandle(this->getSD());
#endif
	}

	return ret;
}

int
SockStream::doStop()
{
    return 0;
}

int 
SockStream::doClose()
{
	//	SockStream_IOChannel_DemuxTable_.clear();
	//	closesocket(this->getSD());
	return MW_SUCCESS;
}

int 
SockStream::doConfigIOParam(void *configure)
{
	struct configureAddr* pCa = (configureAddr *)configure;
	this->setRole(!pCa->_THIS_IS_SERVER_);

	int ret = MW_SUCCESS; 
	ConfigParam_.ipAddr_ = inet_addr(pCa->_IP_ADDR_);
	ConfigParam_.port_ = htons(pCa->_PORT_TCP_NO_);

	this->setName("_TCP_DEVICE_NAME_");	
	bufferLen_ = SOCKSTREAM_BUFSIZE;

	return ret;
}

SockStream::pfRWFunc_t 
SockStream::pfWrite_ = NULL;

SockStream::pfRWFunc_t 
SockStream::pfRead_ = NULL;

//read or write operations
int 
SockStream::read(os_socket sd, Msg_t *msg)
{
	return (*pfRead_)(sd, msg, NULL, NULL);
}

int 
SockStream::write(os_socket sd, Msg_t *msg)
{
	return (*pfWrite_)(sd, msg, NULL, NULL);
}

int 
SockStream::syncWrite(OSAL_SOCKET sd, Msg_t *msg, int32 *pFlag, void *arg)
{
	return send(sd, (char *)msg->pData_, msg->len_, 0);
}

int 
SockStream::syncRead(OSAL_SOCKET sd, Msg_t *msg, int32 *pFlag, void *arg)
{
	return recv(sd, (char *)msg->pData_, msg->len_, 0);
}

int 
SockStream::asyncWrite(OSAL_SOCKET sd, Msg_t *msg, int32 *pFlag, void *arg)
{
	uint32 nWrite = 0;
	int flag = 0;
	int ret; 
#ifdef OSAL_WIN32
/*	WSABUF DataBuf;
	DataBuf.len = msg->len_;
	DataBuf.buf = (char*)msg->pData_;

	ret = WSASend( sd, &DataBuf, 1, &nWrite, flag, 
		LPWSAOVERLAPPED(pACT), SockStream::WriteCompletionROUTINE);

	if(ret == SOCKET_ERROR)
	{
		int err = ::WSAGetLastError();
		if( err != WSA_IO_PENDING)
		{
			MW_ERRMSG((LM_ERROR,"SockStream::asyncWrite::Error occured #(%d)\n",err));
			return MW_ERROR;
		}
	}
*/
#endif//OSAL_WIN32
	return nWrite;
}

int 
SockStream::asyncRead(OSAL_SOCKET sd, Msg_t *msg, int32 *pFlag, void *arg)
{
	uint32 nRead = 0;
	int ret;
	int flag = 0;
#ifdef OSAL_WIN32
/*	WSABUF DataBuf;
	SockStream_AIO_Read_Result *pACT = (SockStream_AIO_Read_Result *)arg;
	pACT->setEventSocket(sd);

	DataBuf.len = msg->len_;
	DataBuf.buf = (char*)msg->pData_;

	ret = WSARecv( sd, &DataBuf, 1, &nRead, (LPDWORD)&flag, 
		LPWSAOVERLAPPED(pACT), SockStream::ReadCompletionROUTINE);
	
	MW_DEBUG((LM_DEBUG,"SockStream::asyncRead sd: %x\n",sd));

	if(ret == SOCKET_ERROR)
	{
		int err = ::WSAGetLastError();
		if( err != WSA_IO_PENDING)
		{
			MW_ERRMSG((LM_ERROR,"SockStream::asyncRead::Error occured #(%d)\n",err));
			return MW_ERROR;
		}
	}
	*/
#else

#endif//OSAL_WIN32
	return nRead;
}

os_socket
SockStream::acceptSocket()
{
	os_socket clientSock;
	os_sockaddr_in *pAddr =  &PeerTcpAddr_[connectedPeer_];
	int addrlen = sizeof(os_sockaddr_in);		

	clientSock = this->accept((os_sockaddr *)pAddr, &addrlen);
	MW_DEBUG((LM_DEBUG,"***********************************************************\n"));
	MW_DEBUG((LM_DEBUG,"[SockStream Accept] <Remote Peer - IP:%s Port:%d>\n", 
		inet_ntoa(pAddr->sin_addr), ntohs(pAddr->sin_port)));
	MW_DEBUG((LM_DEBUG,"***********************************************************\n"));
	
	if(clientSock == -1)
	{
		MW_DEBUG((LM_DEBUG,"SockStrean_Accept: Error!\n"));
		return -1;
	}

	//channel join indication
	currentAcceptSock_ = clientSock;
	this->channelJoinIndication(this , pAddr);
	connectedPeer_++;

	return clientSock;
}
void 
SockStream::setRole(bool role)
{
	role_ = role;
}

bool 
SockStream::getRole()
{
	return role_;
}

