#ifndef SOCKBASEIMPLEMENTATION_H
#define SOCKBASEIMPLEMENTATION_H


#include "nil/IODevice.h"



class SockBaseImplementation 
{
public:
	SockBaseImplementation();
	virtual ~SockBaseImplementation();

	//open sync/async socket 
	virtual int openSyncSocket(int af, int type, int protocol) = 0;
	virtual int openAsyncSocket(int af, int type, int protocol, 
							os_sockspec lpProtocolInfo, os_sockgroup g, os_sockflag dwFlags) = 0;

	//device function 
	virtual int control(ControlMode cmd, void *arg) = 0;
	virtual int connect(sockaddr *pServAddr, int addrlen) = 0;
	virtual int bind(uint32 ip_address, uint16 port_address) = 0;   
	virtual os_socket accept(sockaddr *pSockAddr, int *pAddrLen) = 0;
	virtual int listen() = 0;

	//virtual bool 
	//DWORD host2net(DWORD v);		//Convert from host to network byte order. 
	//DWORD net2host(DWORD v);		//Convert from network to host byte order. 

	//socket set/get function
	os_socket getSD();    
	void setSD(os_socket sd);

protected:

};

//win32 version
class Win32SockBaseImplementation : public SockBaseImplementation
{
public:
	Win32SockBaseImplementation();
	virtual ~Win32SockBaseImplementation();

	//open sync/async socket 
	int openSyncSocket(int af, int type, int protocol);
	int openAsyncSocket(int af, int type, int protocol, os_sockspec lpProtocolInfo, os_sockgroup g, os_sockflag dwFlags);

	//device function 
	int control(ControlMode cmd, void *arg);
	int connect(sockaddr *pServAddr, int addrlen);
	int bind(uint32 ip_address, uint16 port_address);    
	os_socket accept(sockaddr *pSockAddr, int *pAddrLen);
	int listen();
};

//linux version
class LinuxSockBaseImplementation : public SockBaseImplementation
{
public:
	LinuxSockBaseImplementation();
	virtual ~LinuxSockBaseImplementation();

	//open sync/async socket 
	int openSyncSocket(int af, int type, int protocol);
	int openAsyncSocket(int af, int type, int protocol, os_sockspec lpProtocolInfo, os_sockgroup g, os_sockflag dwFlags);

	//device function 
	int control(ControlMode cmd, void *arg);
	int connect(sockaddr *pServAddr, int addrlen);
	int bind(uint32 ip_address, uint16 port_address);    
	os_socket accept(sockaddr *pSockAddr, int *pAddrLen);
	int listen();
};

#endif //SOCKBASEIMPLEMENTATION_H