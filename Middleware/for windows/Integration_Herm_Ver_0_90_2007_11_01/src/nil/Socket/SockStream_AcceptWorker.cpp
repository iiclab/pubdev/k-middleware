// =========================================================================
/**
 *  @filename	SockStream_AcceptWork.h
 *	@Revised
 *  @Revision
 *  @Author		SeongHoon Kim
 *  
 *  @Description
 *				  Socket Stream Synchronous Opeartion class
 *	@NOTE
 */
// =========================================================================

#include "SockStream_AcceptWorker.h"
#include "SockStream.h"
#include "Debug.h"

SockStream_AcceptWorker::SockStream_AcceptWorker(SockStream *pSockStream):pOwnDevice_(pSockStream)
{

}

SockStream_AcceptWorker::~SockStream_AcceptWorker()
{

}

void
SockStream_AcceptWorker::asyncOperation()
{

}

void 
SockStream_AcceptWorker::handleUserDefinedSystemEvent(OSAL_SOCKET sock)
{
	DEBUGTRACE("[TCP Server] _SOCKET_ACCEPT_");
	pOwnDevice_->acceptSocket();  		
}

void 
SockStream_AcceptWorker::run()
{
	DEBUGTRACE("SockStream_AcceptWork::run");
        
	while(pOwnDevice_->getState() == IODevice::IODEV_ACTIVE)
	{
		MW_DEBUG((LM_DEBUG,"[Server] Wait for requested socket!\n"));			
		int newChannelSocket = pOwnDevice_->acceptSocket();  
              
        //pOwnDevice_->setSD(newChannelSocket);		
		if(newChannelSocket <= 0)
		{
			//TODO: perform some error procedures.
			MW_DEBUG((LM_DEBUG,"SockStream_AcceptWork: Error!\n"));
			continue;
		}
        //pOwnDevice_->channelJoinIndication(pOwnDevice_ , &newChannelSocket);
			
		//Thread synchronization scheme is necessary.
	}
	
	pOwnDevice_->close();
}

