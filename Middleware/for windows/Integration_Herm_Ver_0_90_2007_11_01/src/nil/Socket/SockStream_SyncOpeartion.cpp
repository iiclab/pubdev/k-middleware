#include "SockStream.h"
#include "SockStream_IOChannel.h"
#include "SockStream_SyncOpeartion.h"
#include "Debug.h"

SockStream_SyncOpeartion::SockStream_SyncOpeartion(SockStream *pSockStream):pOwnDevice_(pSockStream)
{

}

SockStream_SyncOpeartion::~SockStream_SyncOpeartion()
{

}

void 
SockStream_SyncOpeartion::run()
{
	DEBUGTRACE("SockStream_SyncOpeartion::run");
	
	while(pOwnDevice_->getState() == IODevice::IODEV_ACTIVE)
	{
		MW_DEBUG((LM_DEBUG,"SockStream_SyncOpeartion: Ready to receive data!\n"));	
	}
	
	pOwnDevice_->close();
}
