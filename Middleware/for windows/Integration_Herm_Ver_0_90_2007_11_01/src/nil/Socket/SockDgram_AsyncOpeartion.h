#ifndef SOCKDGRAM_ASYNCOPERATION_H
#define SOCKDGRAM_ASYNCOPERATION_H

#include "SockDgram.h"
#include "SockDgram_IOChannel.h"
#include "osal/AsyncOperation.h"
#include "osal/IAsyncOperation.h"

class SockDgram_AsyncOpeartion : public OSAL::AsyncOperation
{
public:
	SockDgram_AsyncOpeartion(SockDgram * pSockDgram);	
	virtual ~SockDgram_AsyncOpeartion();
	
	void asyncOperation();
    void handleUserDefinedSystemEvent(OSAL_SOCKET);
	
private:
	SockDgram *pOwnDevice_;
	Msg_t incomingMsg_;
};


//TODO: I want to add client's callback function.


#endif//SOCKDGRAM_ASYNCOPERATION_H
