#include "IOChannelManager.h"
#include "Debug.h"
#include "errorno.h"

//** Static Member variables*/
IOChannelManager* 
IOChannelManager::pInstance_ = NULL;

int
IOChannelManager::genID_ = 0;

IOChannelManager::IOChannelManager()
{
}

IOChannelManager::~IOChannelManager()
{
    OSAL_DELETE(pInstance_);
    OSAL_DELETE(pIOChannelMgntCallback_);
}

IOChannelManager* 
IOChannelManager::instance()
{
    if(pInstance_ == NULL)
        pInstance_ = new IOChannelManager;
    return pInstance_;
}

// IOChannel * 
// IOChannelManager::createChannel(int IOCHANNEL_TYPE, const String & devName, void * peerAddr)
// {
//     //first check whether this device already exists
// 	IODevice *pIODev = this->findIODevice(devName);
//     
// 	if(pIODev == NULL)
// 		return NULL;
// 	
// 	return pIODev->createChannel(IOCHANNEL_TYPE, peerAddr);
// }
// 
// IOChannel * 
// IOChannelManager::findChannel(const String & devName, void * peerAddr)
// {
//     //first check whether this device already exists
// 	IODevice *pIODev = this->findIODevice(devName);
// 	
//     if(pIODev == NULL)
// 		return NULL;
// 
// 	return pIODev->findIOChannel(peerAddr);
// }
// 
// int
// IOChannelManager::removeChannel(const String & devName, void * peerAddr)
// {
//     //first check whether this device already exists
// 	IODevice *pIODev = this->findIODevice(devName);
// 	
//     if(pIODev == NULL)
// 		return NULL;
// 
// 	return pIODev->finalizeChannel(peerAddr);
// }


void
IOChannelManager::channelJoinNotification(IODevice *iodev, DevAddr_t devaddr)
{
    MW_DEBUG((LM_DEBUG,"[IOChannelManager] ChannelJoinIndication\n"));
        
    //create IO channel
    IOChannel *pIOC = iodev->createChannel(IOCHANNEL_OPEN_PASIVE, devaddr);
    MW_ASSERT(pIOC != NULL);
    
    // set callback  
    if(pIOChannelMgntCallback_)
    {
        pIOChannelMgntCallback_->handleSetChannelDataConfirm(pIOC);
        pIOChannelMgntCallback_->handleSetChannelDataIndication(pIOC);
    }
        
    //set IOChannel ID. 
    pIOC->setID(genID_);
	MW_DEBUG((LM_DEBUG,"[IOChannelManager:Assign] ChID: [%d] \n", genID_));
    
    //insert new channel in IOchannel table
    IOChannelTable_.insert(genID_++, pIOC);
            
    //start IOChannel
	pIOC->start();
}


void 
IOChannelManager::channelLeaveNotification(IODevice *iodev, DevAddr_t devaddr)
{
    MW_DEBUG((LM_DEBUG,"[IOChannelManager] ChannelLeaveIndication\n"));
    
    //find IO channel
    IOChannel *pIOC = iodev->findIOChannel(devaddr);
    MW_ASSERT(pIOC != NULL);
        
    //close IOChannel
    pIOC->close();
    int LeaveChannel = pIOC->getID();
    genID_--;
    
    IOChannelTable_.remove(LeaveChannel);
    iodev->finalizeChannel(devaddr);    
}

void
IOChannelManager::subscribeIOChannelMgntEvent(IOChannelMgntCallback *pMgntCB)
{
    pIOChannelMgntCallback_ = pMgntCB;
}

int 
IOChannelManager::initialize()
{ 
    MW_DEBUG((LM_DEBUG,"[IOChannelManager] Initialize\n"));
    return MW_SUCCESS;
}

int 
IOChannelManager::finalize()
{
    DEBUGTRACE("[IOChannelManager] Finalize");
    return MW_SUCCESS;
}

int 
IOChannelManager::reset()
{
    DEBUGTRACE("[IOChannelManager] Reset");
    return MW_SUCCESS;
}

