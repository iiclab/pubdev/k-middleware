#include "IOProcess.h"
#include "Errno.h"

IOProcess::IOProcess()
{
	pSysEventProcessor_ = OSAL::SysEventProcessor::instance();
}

IOProcess::~IOProcess()
{
    ThreadIDVector::iterator pos;
    for (pos = v_threadId_vector.begin(); pos != v_threadId_vector.end(); pos++)
    {
//        pSysEventProcessor_->unregisterThread(v_threadId_vector[pos]);
    }
    
    //flush all elements in vector 
    v_threadId_vector.clear();
}

int 
IOProcess::registerAsyncOperation(bool devType, OSAL_HANDLE handle, OSAL::IAsyncOperation *pAsycOp, OSAL_SOCKET sock)
{
//	pAsycOp->asyncOperation();
	return pSysEventProcessor_->registerAsyncOperation(devType, handle, pAsycOp, sock);
}

int 
IOProcess::unregisterAsyncOperation(OSAL_HANDLE handle)
{
	return pSysEventProcessor_->unregisterAsyncOperation(handle);
}

int 
IOProcess::registerSyncOperation(OSAL::IRunnable *pRunnable)
{
    int retThreadId;
    retThreadId = pSysEventProcessor_->registerSyncOperation(pRunnable);
    
    //add thread Id in vector table
    v_threadId_vector.push_back(retThreadId);
    return retThreadId;
}


int 
IOProcess::unregisterSyncOperation(OSAL::IRunnable *pRunnable)
{
    //erase thread ID in vector table
    //v_threadId_vector.erase(pRunnable->);
    return pSysEventProcessor_->unregisterSyncOperation(pRunnable);
}

void 
IOProcess::setOpMode(OSAL::OpMode_t opMode)
{
	//opMode_ = opMode;
    pSysEventProcessor_->setOpMode( opMode );
}

int 
IOProcess::getOpMode()
{
	return pSysEventProcessor_->getOpMode();
}

