#ifndef IODEVICE_H
#define IODEVICE_H

//#include "ConfigAll.h"

//#include "osal/SysEventProcessor.h"
//#include "osal/IAsyncOperation.h"
//#include "osal/Thread.h"
//#include "IODeviceProfile.h"
#include "IOChannel.h"
#include "ConfigMacros.h"
//#include "Utility/Msg_t.h"
#include "Utility/String.h"

enum ControlMode
{
	SETPARAMS,              ///< Set control parameters.
	GETPARAMS               ///< Get control parameters.
};

class IOChannel;

class IODevice 
{
public:    
	//Enumeration of I/O state
	typedef enum IODevState_t
	{
		IODEV_CREATED		= 0x00,
		IODEV_ACTIVE		= 0xF0,
		IODEV_READY		    = 0xF2,
		IODEV_ERROR		    = 0xF4,
        IODEV_STOP          = 0xF8,
		IODEV_FINALIZED		= 0x01
	};
	
	IODevice();
	virtual ~IODevice();
	
	int registerIODevice();

	virtual int open(void *);
	virtual int start();
    virtual int stop();
    virtual int close();
	virtual int control(ControlMode cmd, void *arg) = 0;

    //handle channel operations
    virtual IOChannel * createChannel(int IOCHANNEL_TYPE, void * peerAddr) = 0;
    virtual IOChannel * findIOChannel(void * peerAddr) = 0;
    virtual int finalizeChannel(void * peerAddr) = 0;
    
    void channelJoinIndication(IODevice *iodev, DevAddr_t devaddr);	
    void channelLeaveIndication(IODevice *iodev, DevAddr_t devaddr);

    //property list
    String &getName();
    void setName(const String &);
    
    OSAL_HANDLE getHandle();    
    void setHandle(OSAL_HANDLE );
	
    uint8 getID();
    void setID(uint8 ID);
	
//    void setDeviceProfile(int IODevAttribute, void * IODevAttributeValue);    
//    IODeviceProfile * getDeviceProfile();
	
    IODevice::IODevState_t getState();
    void setState(IODevice::IODevState_t );

	bool getDevType();
	void setDevType(bool);	
		
protected:
	//Following interfaces are shall be implemented by inherited class.
	virtual int doOpen(void *) = 0;
	virtual int doConfigIOParam(void *) = 0;
	virtual int doStart() = 0;
    virtual int doStop() = 0;
	virtual int doClose() = 0;

protected:
//	OSAL::SysEventProcessor *pSysEventProcessor_;
	bool DevType_;

private:
	
	String name_;
	//The operating system file handle return by standard open() function. 
	OSAL_HANDLE handle_;	    
	int deviceID_;
	IODevState_t state_;
};

#endif //IODEVICE_H
