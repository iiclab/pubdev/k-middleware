//#include "ConfigAll.h"
#include "IOChannelTable.h"
#include "IOChannel.h"
#include "errorno.h"

IOChannelTable::IOChannelTable()
{
}

IOChannelTable::~IOChannelTable()
{
	removeAll();
}

int 
IOChannelTable::insert(int chID, IOChannel * pIOC)
{
    //insert element     
	if (ctChannelList_.insert(std::make_pair(chID, pIOC)).second)
    {
        return MW_SUCCESS;
    }
	else
    {
        return MW_FAILED;
    }
}

int 
IOChannelTable::remove(int chID)
{
    std::map< int , IOChannel* >::iterator it;
	it = ctChannelList_.find(chID);
    
    //remove element at corresponding channel ID
	ctChannelList_.erase(chID);
    
    //release memory of IOchannel 
//     delete it->second;
//     it->second = NULL;
    
	return MW_SUCCESS;
}

void 
IOChannelTable::removeAll()
{
    std::map< int , IOChannel* >::iterator pos;
    
    //remove all elements
    for (pos = ctChannelList_.begin(); pos != ctChannelList_.end(); ++pos)
    {
        //remove element at iterator position 
        ctChannelList_.erase(pos);
        
        //release memory of IOchannel 
        delete pos->second;
        pos->second = NULL;
    }
}

IOChannel * 
IOChannelTable::find(int chID)
{
    //find corresponding channel
    std::map< int , IOChannel* >::iterator fi;
	fi = ctChannelList_.find(chID);
    
    return fi->second;
}

uint32 
IOChannelTable::getSize()
{
	return ctChannelList_.size();
}

