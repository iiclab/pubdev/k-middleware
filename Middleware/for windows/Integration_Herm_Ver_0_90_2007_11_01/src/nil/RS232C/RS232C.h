#ifndef RS232C_H
#define RS232C_H

#include "nil/nil.h"
#include "nil/IODevice.h"
#include "nil/IOProcess.h"
#include "RS232C_IOChannel.h"
#include "RS232C_AsyncOperation.h"
#include "RS232C_SyncOperation.h"
#include "RS232C_Configure.h"

/**
Wrapper Facade class
*/
class RS232_AIO_Read_Result;
class RS232_AIO_Write_Result;

class RS232C : public IODevice, public IOProcess
{
	friend class RS232C_IOChannel;
	friend class RS232C_SyncOpeartion;
	friend class RS232C_AsyncOpeartion;

public:
	RS232C();
	virtual ~RS232C();

	struct SerialParams
	{
		SerialParams();

		/** Specifies the baudrate at which the communication port operates. */
		int baudrate;
		/** Specifies the minimum number of bytes in input buffer before XON char
		is sent. Negative value indicates that default value should
		be used (Win32). */
		int xonlim;
		/** Specifies the maximum number of bytes in input buffer before XOFF char
		is sent. Negative value indicates that default value should
		be used (Win32). */
		int xofflim;
		/** Specifies the minimum number of characters for non-canonical
		read (POSIX). */
		unsigned int readmincharacters;
		/** Specifies the time to wait before returning from read. Negative value
		means infinite timeout. */
		int readtimeoutmsec;
		/** Enable/disable parity checking. */
		bool parityEnable;
		/** Specifies the parity mode. POSIX supports "even" and "odd" parity.
		Additionally Win32 supports "mark" and "space" parity modes. */
		const char *paritymode;
		/** Enable & set CTS mode. Note that RTS & CTS are enabled/disabled
		together on some systems (RTS/CTS is enabled if either
		<code>ctsenb</code> or <code>rtsenb</code> is set). */
		bool ctsEnable;
		/** Enable & set RTS mode. Note that RTS & CTS are enabled/disabled
		together on some systems (RTS/CTS is enabled if either
		<code>ctsenb</code> or <code>rtsenb</code> is set).
		- 0 = Disable RTS.
		- 1 = Enable RTS.
		- 2 = Enable RTS flow-control handshaking (Win32).
		- 3 = Specifies that RTS line will be high if bytes are available
		for transmission. After transmission RTS will be low (Win32). */
		int rtsEnable;
		/** Enable/disable software flow control on input. */
		bool xinEnable;
		/** Enable/disable software flow control on output. */
		bool xoutEnable;
		/** Specifies if device is a modem (POSIX). If not set modem status
		lines are ignored. */
		bool modem;
		/** Enable/disable receiver (POSIX). */
		bool rcvenb;
		/** Controls whether DSR is disabled or enabled (Win32). */
		bool dsrenb;
		/** Controls whether DTR is disabled or enabled (Win32). */
		int dtrEnable;
		/** Data bits. Valid values 5, 6, 7 and 8 data bits.
		Additionally Win32 supports 4 data bits. */
		unsigned char databits;
		/** Stop bits. Valid values are 1 and 2. */
		unsigned char stopbits;
	};

	/**IOChannel related interface*/
	IOChannel * createChannel(int IOCHANNEL_TYPE, void * peerAddr);
	IOChannel * findIOChannel(void * );
	int changeChannel(IOChannel *pOldChannel, IOChannel *pNewChannel);
	int finalizeChannel(void * peerAddr);

	/** Interface for reading/writing serial device parameters. */
	int control (ControlMode cmd, void *arg);
	int read(void * arg, Msg_t *msg);
	int write(void * arg, Msg_t *msg);

	void dispatch(int eventType, Msg_t *pTransactionMsg);

private:

#ifdef OSAL_WIN32
	//Native Windows API wrapper functions.
	static VOID CALLBACK ReadCompletionRoutine(
		DWORD dwErrorCode,
		DWORD dwNumberOfBytesTransfered,
		LPOVERLAPPED lpOverlapped
		);
	static VOID CALLBACK WriteCompletionRoutine(
		DWORD dwErrorCode,
		DWORD dwNumberOfBytesTransfered,
		LPOVERLAPPED lpOverlapped
		);
#endif//#endif

	int doOpen(void *);
	int doClose();
	int doConfigIOParam(void *);	
	int doStart();
	int doStop();

	struct SerialParams serialParam_;
	typedef int (*pfRWFunc_t)(OSAL_HANDLE , Msg_t *, void *);

	static pfRWFunc_t pfWrite_;
	static pfRWFunc_t pfRead_;

	//Native API wrapper functions.	
	static int syncRead(OSAL_HANDLE, Msg_t *, void *);
	static int syncWrite(OSAL_HANDLE, Msg_t *, void *);
	static int asyncRead(OSAL_HANDLE, Msg_t *, void *);
	static int asyncWrite(OSAL_HANDLE, Msg_t *, void *);

	RS232C_AsyncOpeartion* pRS232C_Async;
	RS232C_SyncOpeartion* pRS232C_Sync;
	//The size of buffers should be set by IODevice because
	//its characteristics are dependent on each device.
	int bufferLen_;
	byte *pBuffer_;

	RS232_AIO_Read_Result *m_pOlReadACT;
	RS232_AIO_Write_Result *m_pOlWriteACT;

	/* In case of RS232, 
		demux table is not necessary 
		because there is only one physical connection
	*/
	IOChannel *pRS232_IOChannel_;

};

class RS232_AIO_Write_Result : public AIO_Result
#ifdef OSAL_WIN32
	,		public OVERLAPPED
#endif//OSAL_WIN32
{
public:
	RS232_AIO_Write_Result(RS232C *);
	virtual ~RS232_AIO_Write_Result();

	void complete();
	void setCompletionState(int);
	void setTransaction(Msg_t *msg);
	void setReadOrTransferred(int);
protected:
private:
	//Completetion Handler are located in CRS232 class
	Msg_t TransactionMsg_;
	RS232C *pRS232_;
};

class RS232_AIO_Read_Result : public AIO_Result
#ifdef OSAL_WIN32
	, public OVERLAPPED
#endif//OSAL_WIN32
{
public:
	RS232_AIO_Read_Result(RS232C *);
	virtual ~RS232_AIO_Read_Result();

	void complete();
	void setCompletionState(int);
	void setReadOrTransferred(int);
protected:
private:
	//Completetion Handler are located in CRS232 class
	RS232C *pRS232_;
};

#endif // RS232C_H