#ifndef RS232C_SYNCOPERATION_H
#define RS232C_SYNCOPERATION_H

#include "OSAL/IRunnable.h"
#include "Utility/Msg_t.h"

class RS232C;
class RS232C_SyncOpeartion : public OSAL::IRunnable
{
public:
	RS232C_SyncOpeartion(RS232C *);
	virtual ~RS232C_SyncOpeartion();
	void run();
private:
	RS232C *pRS232_;
	Msg_t incomingMsg_;
};

#endif //RS232C_SYNCOPERATION_H