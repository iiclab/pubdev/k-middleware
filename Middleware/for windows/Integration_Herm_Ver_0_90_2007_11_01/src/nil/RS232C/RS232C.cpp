#include "RS232C.h"
#include "Debug.h"
#include "ConfigAll.h"
#include "osal/OSAL_Errno.h"
#include "errorno.h"

const char SERIAL_IO_ODD[]   = "odd";
const char SERIAL_IO_EVEN[]  = "even";

#if defined (OSAL_WIN32)

const char SERIAL_IO_MARK[]  = "mark";
const char SERIAL_IO_SPACE[] = "space";

#endif /* OSAL_WIN32 */


RS232_AIO_Write_Result::RS232_AIO_Write_Result(RS232C *pRS232) : pRS232_(pRS232)
{
	this->Internal = 0;
	this->InternalHigh = 0;
	this->Offset = 0;
	this->OffsetHigh = 0;
	this->hEvent = NULL;
}

RS232_AIO_Write_Result::~RS232_AIO_Write_Result()
{
	pRS232_ = NULL;
}

void 
RS232_AIO_Write_Result::complete()
{
	pRS232_->dispatch(ACT_WRITEEVENT, &TransactionMsg_);
}

void 
RS232_AIO_Write_Result::setCompletionState(int state)
{
	state_ = state;
}

void 
RS232_AIO_Write_Result::setReadOrTransferred(int readOrTransferred)
{
	readOrTransferred_ = readOrTransferred;
}

void 
RS232_AIO_Write_Result::setTransaction(Msg_t *msg)
{
	TransactionMsg_ = *msg;
}


RS232_AIO_Read_Result::RS232_AIO_Read_Result(RS232C *pRS232): pRS232_(pRS232)
{
	this->Internal = 0;
	this->InternalHigh = 0;
	this->Offset = 0;
	this->OffsetHigh = 0;
	this->hEvent = NULL;
}

RS232_AIO_Read_Result::~RS232_AIO_Read_Result()
{
	pRS232_ = NULL;
}

void 
RS232_AIO_Read_Result::complete()
{
	Msg_t msg = {readOrTransferred_, NULL};
	pRS232_->dispatch(ACT_READEVENT, &msg);
}

void 
RS232_AIO_Read_Result::setCompletionState(int state)
{
	state_ = state;
}

void 
RS232_AIO_Read_Result::setReadOrTransferred(int readOrTransferred)
{
	readOrTransferred_ = readOrTransferred;
}

RS232C::pfRWFunc_t 
RS232C::pfWrite_ = NULL;

RS232C::pfRWFunc_t 
RS232C::pfRead_ = NULL;

RS232C::RS232C()
{
	pRS232_IOChannel_ = NULL;
	pBuffer_		= NULL;
	pRS232C_Sync	= NULL;
	pRS232C_Async	= NULL;

	DevType_ = _NON_IP_BASED_DEVICE_;
	//TODO: This procedure shall be proceeded by configuration file.
	setName("RS232:COM1");
}

RS232C::~RS232C()
{
	OSAL_DELETE(pBuffer_);
}

RS232C::SerialParams::SerialParams()
{
	memset (this, 0, sizeof *this);
}

IOChannel * 
RS232C::createChannel(int , void * peerAddr)
{
	//In RS232, peerAddr is not necessary.
	OSAL_UNUSED_ARG(peerAddr);

	if(pRS232_IOChannel_ == NULL)
		pRS232_IOChannel_ = new RS232C_IOChannel(NULL,this); 
	return pRS232_IOChannel_;
}

IOChannel *
RS232C::findIOChannel(void * )
{
	return pRS232_IOChannel_;
}

int 
RS232C::changeChannel(IOChannel *pOldChannel, IOChannel *pNewChannel)
{
	//pRS232_IOChannel_ shall be the same pointer as pOlsChannel
	if(pRS232_IOChannel_ == pOldChannel)
	{
		pRS232_IOChannel_ = pNewChannel;
		return MW_SUCCESS;
	}else
	{
		return MW_FAILED;
	}

}

int 
RS232C::finalizeChannel(void * peerAddr)
{

	OSAL_DELETE(pRS232_IOChannel_);
#ifdef OSAL_WIN32
	CancelIo(getHandle());
#else

#endif
	return 0;
}

//This shall be multiple of 1024 bytes

#define MAX_BUFFER 1024
int 
RS232C::doOpen(void *configurationProfile)
{
	DEBUGTRACE("RS232::doOpen\n");
	OSAL_HANDLE handle = OSAL_INVALID_HANDLE;
	int opMode = getOpMode();

#ifdef OSAL_WIN32
	int ffa;
	//Get configuration parameters and set a configuration profile
	ffa = FILE_ATTRIBUTE_NORMAL;

	if(opMode == OSAL::OPMODE_ASYNC)
	{
		ffa |= FILE_FLAG_OVERLAPPED;
		pfRead_ = RS232C::asyncRead;
		pfWrite_ = RS232C::asyncWrite;
		//Register a handle and a Call Back function to AsynchronousEventArbiter
		m_pOlReadACT  = new RS232_AIO_Read_Result(this);
		m_pOlWriteACT = new RS232_AIO_Write_Result(this);
	}
	/*	Synchronous mode*/
	else if(opMode == OSAL::OPMODE_SYNC)
	{
		pfRead_ = RS232C::syncRead;
		pfWrite_ = RS232C::syncWrite;
		m_pOlWriteACT = NULL;
		m_pOlReadACT = NULL;
	}
	else
	{
		return MW_ERROR;
	}

	//Open a device
	handle = CreateFile("COM1",			\
		GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING,
		ffa, NULL);

	if(handle == OSAL_INVALID_HANDLE)	
		return NULL;

	this->setHandle(handle);

	//Set buffer size and allocate memory for buffer.
	bufferLen_ = MAX_BUFFER;

	pBuffer_ = new byte[bufferLen_];

	//set EV_RXCHAR event	
	SetCommMask(this->getHandle(), EV_RXCHAR);	

	//set Size of InQueue and OutQueue in Serial Port
	SetupComm(this->getHandle(), bufferLen_, bufferLen_);
	PurgeComm(this->getHandle(), PURGE_RXCLEAR | PURGE_TXCLEAR | PURGE_RXABORT | PURGE_TXABORT);

	if(control(SETPARAMS, &serialParam_) != 0)
		return -1;
#else

#endif// OSAL_WIN32

	return 0;
}

int 
RS232C::doClose()
{
	pRS232_IOChannel_->close();
	CloseHandle(getHandle());
	return 0;
}

int 
RS232C::doStart()
{
	int ret = MW_SUCCESS;

	if(getOpMode() == OSAL::OPMODE_ASYNC)
	{
		pRS232C_Async = new RS232C_AsyncOpeartion(this);
		OSAL_HANDLE os_handle = this->getHandle();
		//TODO: Who will deallocate this allocated memory?
		ret = registerAsyncOperation(DevType_, os_handle, pRS232C_Async, NULL);
	}
	else
	{	
		//TODO: Who will deallocate this allocated memory?

		//TODO: When it comes to sync operation, its return handle shall be managed by concrete IO devices.
		//		so, I have to implement it.
		ret=registerSyncOperation(new RS232C_SyncOpeartion(this));

	}

	return ret;
}

int
RS232C::doStop()
{
	return 0;
}

//Operation mode independent API
int 
RS232C::read(void * arg, Msg_t *msg)
{
	return (*pfRead_)(this->getHandle(), msg, m_pOlReadACT);
}

int 
RS232C::write(void * arg, Msg_t *msg)
{
	return (*pfWrite_)(this->getHandle(), msg, m_pOlWriteACT);
}

int 
RS232C::syncWrite(OSAL_HANDLE h, Msg_t *msg, void *arg)
{
	DWORD dwWritten;
	int ret;
#ifdef OSAL_WIN32

	ret = WriteFile(h, msg->pData_, msg->len_, &dwWritten, (LPOVERLAPPED)arg);
	int errorNo = OSAL::getLastErrno();
#endif//OSAL_WIN32
	return dwWritten;
}

int 
RS232C::syncRead(OSAL_HANDLE h, Msg_t *msg, void *arg)
{
	DWORD dwRead;
#ifdef OSAL_WIN32
	ReadFile(h, msg->pData_, msg->len_, &dwRead, (LPOVERLAPPED)arg);
#endif//OSAL_WIN32
	return dwRead;
}

int 
RS232C::asyncWrite(OSAL_HANDLE h, Msg_t *msg, void *arg)
{
	int ret = 0;
#ifdef OSAL_WIN32
	
	RS232_AIO_Write_Result *pWriteACT = (RS232_AIO_Write_Result*)arg;
	pWriteACT->setTransaction(msg);

	if(WriteFileEx(h, msg->pData_, msg->len_, (LPOVERLAPPED)(pWriteACT), RS232C::WriteCompletionRoutine) == TRUE)
		return msg->len_;

	ret = OSAL::getLastErrno();

	if(ret != ERROR_SUCCESS)
	{
		if(ret == ERROR_IO_PENDING)
			return MW_SUCCESS;
	}
#elif OSAL_POSIX

#endif//OSAL_WIN32
	return ret;
}

int
RS232C::asyncRead(OSAL_HANDLE h, Msg_t *msg, void *arg)
{

#ifdef OSAL_WIN32
	RS232_AIO_Read_Result * pReadACT = (RS232_AIO_Read_Result*)arg;
	return ReadFileEx(h, msg->pData_, msg->len_, (LPOVERLAPPED)(pReadACT), RS232C::ReadCompletionRoutine);
#elif OSAL_POSIX

#endif//OSAL_WIN32
}


void 
RS232C::dispatch(int eventType, Msg_t *pTransactionMsg)
{
	//A RS232 device has only one channel.
	MW_ASSERT(pRS232_IOChannel_ != NULL);
	if(eventType == ACT_READEVENT)
	{
		//Since the number of data read is already set, the only remaining part is to set 
		//pTransactionMsg->pData_ to Recv Buffer
		pTransactionMsg->pData_ = pBuffer_;

		//data indication to upper user
		pRS232_IOChannel_->dataIndication(NULL, pTransactionMsg);
		/**In case of asynchronous operation, non-blocking read or corresponding methods 
		must be called to receive data.*/

	}else if(eventType == ACT_WRITEEVENT)
	{
		SentStatus_t sentStatus;
		//sentStatus.BytesTransfered_ = *pTransactionMsg;
		sentStatus.status_ = 0;

		pRS232_IOChannel_->dataConfirm(&sentStatus);
	}

	if(this->getOpMode() == OSAL::OPMODE_ASYNC)
	{
		pTransactionMsg->len_ = bufferLen_;
		pTransactionMsg->pData_ = pBuffer_;
		this->read(NULL, pTransactionMsg);

	}

}

int 
RS232C::doConfigIOParam(void *NVList)
{	
#ifdef OSAL_WIN32
	serialParam_.baudrate = B38400;
	serialParam_.databits = 8;
	serialParam_.stopbits = STOPBIT_ONE;
	serialParam_.parityEnable = FALSE;
	//serialParam_.paritymode = "";
	serialParam_.xinEnable = 0;
	serialParam_.xoutEnable = 0;
	serialParam_.xonlim = 0;
	serialParam_.xofflim = 0;
	serialParam_.dtrEnable = DTR_CTRL_ENABLE;
	serialParam_.rtsEnable = RTS_CTRL_ENABLE;
	serialParam_.readtimeoutmsec = 20;
#else // POSIX

#endif
	return MW_SUCCESS;
}

int 
RS232C::control(ControlMode cmd, void *param)
{		
	int baudrate;
	SerialParams *arg = static_cast< SerialParams *> (param);
	switch (arg->baudrate)
	{
#if defined (B110)
	case 110:     baudrate = B110;     break;
#endif /* B110 */
#if defined (B300)
	case 300:     baudrate = B300;     break;
#endif /* B300 */
#if defined (B600)
	case 600:     baudrate = B600;     break;
#endif /* B600 */
#if defined (B1200)
	case 1200:    baudrate = B1200;    break;
#endif /* B1200 */
#if defined (B2400)
	case 2400:    baudrate = B2400;    break;
#endif /* B2400 */
#if defined (B4800)
	case 4800:    baudrate = B4800;    break;
#endif /* B4800 */
#if defined (B9600)
	case 9600:    baudrate = B9600;    break;
#endif /* B9600 */
#if defined (B19200)
	case 19200:   baudrate = B19200;   break;
#endif /* B19200 */
#if defined (B38400)
	case 38400:   baudrate = B38400;   break;
#endif /* B38400 */
#if defined (B56000)
	case 56000:   baudrate = B56000;   break;
#endif /* B56000 */
#if defined (B57600)
	case 57600:   baudrate = B57600;   break;
#endif /* B57600 */
#if defined (B115200)
	case 115200:  baudrate = B115200;  break;
#endif /* B115200 */
#if defined (B128000)
	case 128000:  baudrate = B128000;  break;
#endif /* B128000 */
#if defined (B256000)
	case 256000:  baudrate = B256000;  break;
#endif /* B256000 */
	default:
		return INVALID_PARAM;
	}

#ifdef OSAL_WIN32
	DCB dcb;
	dcb.DCBlength = sizeof dcb;
	if (!::GetCommState (this->getHandle (), &dcb))
	{
		return OSAL::getLastErrno();
	}

	COMMTIMEOUTS timeouts;
	if (!::GetCommTimeouts (this->getHandle(), &timeouts))
	{
		return OSAL::getLastErrno();
	}

	if(cmd == GETPARAMS)
	{
		return 0;	
	}else if(cmd != SETPARAMS)
	{
		return INVALID_PARAM;
	}

	//From here, procedure for SETPARAM .

	dcb.BaudRate = arg->baudrate;

	switch (arg->databits)
	{
	case 4:
	case 5:
	case 6:
	case 7:
	case 8:
		dcb.ByteSize = arg->databits;
		break;
	default:
		return INVALID_PARAM;
	}

	switch (arg->stopbits)
	{
	case STOPBIT_ONE:
		dcb.StopBits = ONESTOPBIT;
		break;
	case STOPBITS_TWO:
		dcb.StopBits = TWOSTOPBITS;
		break;
	case STOPBITS_ONE5:
		dcb.StopBits = ONE5STOPBITS;
		break;
	default:
		return INVALID_PARAM;
	}

	if (arg->parityEnable && arg->paritymode)
	{
		dcb.fParity = TRUE;
		if (::strcmp (arg->paritymode, SERIAL_IO_ODD) == 0)
			dcb.Parity = ODDPARITY;
		else if (::strcmp (arg->paritymode, SERIAL_IO_EVEN) == 0)
			dcb.Parity = EVENPARITY;
		else if (::strcmp (arg->paritymode, SERIAL_IO_MARK) == 0)
			dcb.Parity = MARKPARITY;
		else if (::strcmp (arg->paritymode, SERIAL_IO_SPACE) == 0)
			dcb.Parity = SPACEPARITY;
		else
			dcb.Parity = NOPARITY;
	}
	else
	{
		dcb.fParity = FALSE;
		dcb.Parity = NOPARITY;
	}

	// Enable/disable RTS protocol.
	switch (arg->rtsEnable)
	{
	case 1:
		dcb.fRtsControl = RTS_CONTROL_ENABLE;
		break;
	case 2:
		dcb.fRtsControl = RTS_CONTROL_HANDSHAKE;
		break;
	case 3:
		dcb.fRtsControl = RTS_CONTROL_TOGGLE;
		break;
	default:
		dcb.fRtsControl = RTS_CONTROL_DISABLE;
	}

	// Enable/disable CTS protocol.
	if (arg->ctsEnable)
		dcb.fOutxCtsFlow = TRUE;
	else
		dcb.fOutxCtsFlow = FALSE;

	// Enable/disable DSR protocol.
	if (arg->dsrenb)
		dcb.fOutxDsrFlow = TRUE;
	else
		dcb.fOutxDsrFlow = FALSE;

	// Disable/enable DTR protocol
	if (arg->dtrEnable)
		dcb.fDtrControl = DTR_CONTROL_DISABLE;
	else
		dcb.fDtrControl = DTR_CONTROL_ENABLE;

	// Enable/disable software flow control on input
	if (arg->xinEnable)
		dcb.fInX = TRUE;
	else
		dcb.fInX = FALSE;

	// Enable/disable software flow control on output
	if (arg->xoutEnable)
		dcb.fOutX = TRUE;
	else
		dcb.fOutX = FALSE;

	// Always set limits unless set to negative to use default.
	if (arg->xonlim >= 0)
		dcb.XonLim  = arg->xonlim;
	if (arg->xofflim >= 0)
		dcb.XoffLim = arg->xofflim;

	dcb.fAbortOnError = FALSE;
	dcb.fErrorChar = FALSE;
	dcb.fNull = FALSE;
	dcb.fBinary = TRUE;

	if (!::SetCommState(this->getHandle(), &dcb))
	{
		return OSAL::getLastErrno();
	}


	//set configuration of the device dependent on the configuration profile.

	timeouts.ReadIntervalTimeout = arg->readtimeoutmsec;
	timeouts.ReadTotalTimeoutMultiplier = 0;
	timeouts.ReadTotalTimeoutConstant = 0;
	timeouts.WriteTotalTimeoutMultiplier = 0;
	timeouts.WriteTotalTimeoutConstant = 0;

	/*
	if (arg->readtimeoutmsec < 0)
	{
	// Settings for infinite timeout.
	timeouts.ReadIntervalTimeout        = 0;
	timeouts.ReadTotalTimeoutMultiplier = 0;
	timeouts.ReadTotalTimeoutConstant   = 0;
	}
	else if (arg->readtimeoutmsec == 0)
	{
	// Return immediately if no data in the input buffer.
	timeouts.ReadIntervalTimeout        = MAXDWORD;
	timeouts.ReadTotalTimeoutMultiplier = 0;
	timeouts.ReadTotalTimeoutConstant   = 0;
	}
	else
	{
	// Wait for specified timeout for char to arrive before returning.
	timeouts.ReadIntervalTimeout        = MAXDWORD;
	timeouts.ReadTotalTimeoutMultiplier = MAXDWORD;
	timeouts.ReadTotalTimeoutConstant   = arg->readtimeoutmsec;
	}
	*/	
	if (!::SetCommTimeouts (this->getHandle (), &timeouts))
	{
		return OSAL::getLastErrno();
	}

#elif defined(OSAL_POSIX)


#else
	OSAL_UNUSED_ARG (cmd);
	OSAL_UNUSED_ARG (arg);
	OSAL_NOTSUP_RETURN (-1);

#endif// OSAL_WIN32

	return 0;
}


#ifdef OSAL_WIN32
VOID CALLBACK 
RS232C::WriteCompletionRoutine(
							  DWORD dwErrorCode,
							  DWORD dwNumberOfBytesTransfered,
							  LPOVERLAPPED lpOverlapped
							  )
{
	DEBUGTRACE("RS232::WriteCompletionRoutine\n");
	RS232_AIO_Write_Result *pRS232_AIO_Write_Result = static_cast< RS232_AIO_Write_Result* > (lpOverlapped);

	printf("RS232::Write Callback dwErrorCode %d nBytesTransfered : %d\n",dwErrorCode, dwNumberOfBytesTransfered);
	pRS232_AIO_Write_Result->setCompletionState(dwErrorCode);
	pRS232_AIO_Write_Result->setReadOrTransferred(dwNumberOfBytesTransfered);
	pRS232_AIO_Write_Result->complete();

}

VOID CALLBACK 
RS232C::ReadCompletionRoutine(
							 DWORD dwErrorCode,
							 DWORD dwNumberOfBytesReceived,
							 LPOVERLAPPED lpOverlapped
							 )
{	
	DEBUGTRACE("RS232::ReadCompletionRoutine\n");
	RS232_AIO_Read_Result *pRS232_AIO_Read_Result = static_cast< RS232_AIO_Read_Result* > (lpOverlapped);
	//여기서 잘리는 데이터를 붙여서 위로 보내야 함
	printf("RS232::ReadCompletionRoutine : Read Callback ErrorCode %d nBytesReceived : %d\n",dwErrorCode, dwNumberOfBytesReceived);

	pRS232_AIO_Read_Result->setCompletionState(dwErrorCode);
	pRS232_AIO_Read_Result->setReadOrTransferred(dwNumberOfBytesReceived);
	pRS232_AIO_Read_Result->complete();

}

#endif//OSAL_WIN32
