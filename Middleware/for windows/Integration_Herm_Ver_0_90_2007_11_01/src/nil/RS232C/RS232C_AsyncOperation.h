#ifndef RS232C_ASYNCOPERATION_H
#define RS232C_ASYNCOPERATION_H

#include "osal/AsyncOperation.h"
#include "osal/IAsyncOperation.h"
#include "Utility/Msg_t.h"

class RS232C;
class RS232C_AsyncOpeartion : public OSAL::AsyncOperation
{
public:

	RS232C_AsyncOpeartion(RS232C * pRS232);
	virtual ~RS232C_AsyncOpeartion();

	void asyncOperation();
	void handleUserDefinedSystemEvent(OSAL_SOCKET);

private:
	RS232C *pOwnDevice_;
	Msg_t incomingMsg_;
};

#endif //RS232C_ASYNCOPERATION_H