#include "RS232C_AsyncOperation.h"
#include "RS232C.h"
#include "Debug.h"

RS232C_AsyncOpeartion::RS232C_AsyncOpeartion(RS232C * pRS232): pOwnDevice_(pRS232)
{
	//Even if RS232 uses system callback, it should use AOP_TYPE_USER_CALLBACK. 
	//For the purpose of using handleUserDefinedSystemEvent(), which is to make RS232
	// readable asynchronously.
	setAOPType(OSAL::AOP_TYPE_USER_CALLBACK);
	setHandle(pOwnDevice_->getHandle());
	incomingMsg_.len_ = pOwnDevice_->bufferLen_;
	incomingMsg_.pData_ = pOwnDevice_->pBuffer_;
}

RS232C_AsyncOpeartion::~RS232C_AsyncOpeartion()
{
	pOwnDevice_ = NULL;
}

void
RS232C_AsyncOpeartion::asyncOperation()
{
	DEBUGTRACE("RS232_AsyncOpeartion::asyncOperation\n");
	//pOwnDevice_->read(NULL, &incomingMsg_);
}

void
RS232C_AsyncOpeartion::handleUserDefinedSystemEvent(OSAL_SOCKET non_process)
{
	pOwnDevice_->read(NULL, &incomingMsg_);
}
