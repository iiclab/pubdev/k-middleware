#ifndef RS232C_IOCHANNEL_H
#define RS232C_IOCHANNEL_H

#include "nil/IOChannel.h"

class RS232C;
class RS232C_IOChannel : public IOChannel 
{
public:
	//In RS232 Peer Address is not necessary.
	RS232C_IOChannel(DevAddr_t pDestAddr,RS232C *pRS232);
	virtual ~RS232C_IOChannel();

	void start();
	int close();

	//IOChannel mandatory interface
	int dataRequest(void * arg, Msg_t *msg);
	void dataIndication(void * arg, Msg_t *msg);
	void dataConfirm(SentStatus_t *sent);

	IODevice * getOwnDevice();

private:
	RS232C *pOwnDevice_;
};

#endif //RS232C_IOCHANNEL_H