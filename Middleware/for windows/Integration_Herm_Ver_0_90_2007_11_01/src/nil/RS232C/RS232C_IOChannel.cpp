#include "RS232C_IOChannel.h"
#include "configAll.h"
#include "Debug.h"
#include "RS232C.h"

RS232C_IOChannel::RS232C_IOChannel(DevAddr_t pDestAddr,RS232C *pRS232):
IOChannel(pDestAddr), pOwnDevice_(pRS232)
{
	//Mandatory
	bufferLen_ = pOwnDevice_->bufferLen_;
	pBuffer_ = pOwnDevice_->pBuffer_;
	this->setInterfaceInformation(_INTERFACE_RS232C_);

	//For Test
	setMaxPayload(200);
	m_RecvBuf.len_ = bufferLen_;
	m_RecvBuf.pData_ = pBuffer_;
}

RS232C_IOChannel::~RS232C_IOChannel()
{
	pOwnDevice_ = NULL;
}

void 
RS232C_IOChannel::start()
{
	MW_DEBUG((LM_DEBUG, "RS232_IOChannel::start()\n"));
	
	if(pOwnDevice_->getOpMode() == OSAL::OPMODE_ASYNC)
	{
		pOwnDevice_->read(NULL, &m_RecvBuf);
	}
}

int 
RS232C_IOChannel::dataRequest(void * arg, Msg_t *msg)
{
	return pOwnDevice_->write(arg, msg);
}

void 
RS232C_IOChannel::dataIndication(void * arg, Msg_t *msg)
{
	MW_ASSERT(getInputHandler() != NULL);
	getInputHandler()->handleInput((void*)this->getID(), msg);
}

void 
RS232C_IOChannel::dataConfirm(SentStatus_t *sent)
{
	MW_ASSERT(getOutputHandler() != NULL);

	getOutputHandler()->handleOutput(sent);
}

IODevice * 
RS232C_IOChannel::getOwnDevice()
{
	return pOwnDevice_;
}

int 
RS232C_IOChannel::close()
{
	pOwnDevice_->channelLeaveIndication(pOwnDevice_, this->getDstAddr());
	pOwnDevice_->finalizeChannel(this);
	return 0;
}
