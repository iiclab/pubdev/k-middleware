#include "RS232C_SyncOperation.h"
#include "RS232C.h"
#include "Debug.h"

RS232C_SyncOpeartion::RS232C_SyncOpeartion(RS232C *pRS232):pRS232_(pRS232)
{
	incomingMsg_.len_ = pRS232_->bufferLen_;
	incomingMsg_.pData_ = pRS232_->pBuffer_;
}

RS232C_SyncOpeartion::~RS232C_SyncOpeartion()
{

}

void
RS232C_SyncOpeartion::run()
{

	DEBUGTRACE("RS232_SyncOpeartion::run\n");

	while(pRS232_->getState() == IODevice::IODEV_ACTIVE)
	{
		MW_DEBUG((LM_DEBUG,"RS232_SyncOpeartion:  Ready to receive data!\n"));
		//TODO: Along with state of IO channel, I have to control its behavior.
		int nRead = pRS232_->read(NULL, &incomingMsg_);

		if(nRead <= 0)
		{
			MW_DEBUG((LM_DEBUG,"RS232_SyncOpeartion: read error!\n"));
		}
		MW_DEBUG((LM_DEBUG,"RS232_SyncOpeartion:  dispatch!\n"));
		pRS232_->dispatch(ACT_READEVENT,&incomingMsg_);

		//Thread synchronization scheme is necessary.
	}

	pRS232_->close();
}