/** Baud rates at which the communication device operates */
#ifdef OSAL_WIN32

#define B110	CBR_110             
#define B300	CBR_300             
#define	B600	CBR_600             
#define B1200	CBR_1200            
#define B2400	CBR_2400            
#define B4800	CBR_4800            
#define	B9600	CBR_9600
#define B14400	CBR_14400
#define B19200	CBR_19200
#define B38400	CBR_38400
#define	B56000	CBR_56000
#define	B57600	CBR_57600
#define B115200 CBR_115200
#define B128000 CBR_128000
#define B256000 CBR_256000

#define PARITYMODE_NONE		NOPARITY
#define PARITYMODE_ODD		ODDPARITY       
#define PARITYMODE_EVEN		EVENPARITY
#define PARITYMODE_MARK     MARKPARITY 
#define PARITYMODE_SPACE	SPACEPARITY


/**Stopbit*/
#define STOPBIT_ONE		ONESTOPBIT
#define STOPBITS_ONE5	ONE5STOPBITS
#define STOPBITS_TWO	TWOSTOPBITS

//
// DTR Control Flow Values.
//

#define DTR_CTRL_DISABLE	DTR_CONTROL_DISABLE
#define DTR_CTRL_ENABLE		DTR_CONTROL_ENABLE
#define DTR_CTRL_HANDSHAKE	DTR_CONTROL_HANDSHAKE

//
// RTS Control Flow Values
//
#define RTS_CTRL_DISABLE    RTS_CONTROL_DISABLE
#define RTS_CTRL_ENABLE     RTS_CONTROL_ENABLE
#define RTS_CTRL_HANDSHAKE  RTS_CONTROL_HANDSHAKE
#define RTS_CTRL_TOGGLE     RTS_CONTROL_TOGGLE

#elif defined(OSAL_POSIX)

#define PARITYMODE_NONE		NOPARITY
#define PARITYMODE_ODD		ODDPARITY       
#define PARITYMODE_EVEN		EVENPARITY


#define STOPBIT_ONE		PARODD
#define STOPBITS_ONE5	ONE5STOPBITS
#define STOPBITS_TWO	TWOSTOPBITS

//
// DTR Control Flow Values.
//

#define DTR_CTRL_DISABLE	
#define DTR_CTRL_ENABLE		
#define DTR_CTRL_HANDSHAKE	

//
// RTS Control Flow Values
//
#define RTS_CTRL_DISABLE    
#define RTS_CTRL_ENABLE     
#define RTS_CTRL_HANDSHAKE  
#define RTS_CTRL_TOGGLE     

#endif//OSAL_WIN32