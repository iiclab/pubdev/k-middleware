#ifndef OUTPUTCALLBACKHANDLER_H
#define OUTPUTCALLBACKHANDLER_H

#include "Utility/Msg_t.h"

typedef struct _sent_status_t
{	
  int sequence_;
  int status_;
  int BytesTransfered_;
  byte moduleAddr_;			// module ID or group ID
  //Msg_t MsgTransfered_;

}SentStatus_t;

class OutputCallbackHandler {
public:    
    virtual void handleOutput(SentStatus_t *) = 0;
};
#endif //OUTPUTCALLBACKHANDLER_H
