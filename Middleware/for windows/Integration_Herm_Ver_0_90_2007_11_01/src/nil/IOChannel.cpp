#include "IOChannel.h"
#include "InputCallbackHandler.h"
#include "OutputCallbackHandler.h"

IOChannel::IOChannel()
{
    channelID_ = IOCHANNEL_UNUSED;
        
    //pChannel_Scheduler_ = NULL;
    
    priority_ = 0; 
    broadcast_ = false;
        
    maxPayload_ = -1;
    //bufferLen_ = 0;
    pInputCallbackHandler_ = NULL;
    pOutputCallbackHandler_ = NULL;

}

IOChannel:: IOChannel(DevAddr_t PeerAddr):DstAddr_(PeerAddr)
{
    channelID_ = IOCHANNEL_UNUSED;
	
//    pChannel_Scheduler_ = NULL;
    
	priority_ = 0; 
//	bufferLen_ = 0;
    pInputCallbackHandler_ = NULL;
	pOutputCallbackHandler_ = NULL;
}

IOChannel::~IOChannel()
{

}

void 
IOChannel::dataConfirm(SentStatus_t *status)
{
	pOutputCallbackHandler_->handleOutput(status);
}
//dummy operation
void 
IOChannel::start()
{

}

bool 
IOChannel::isInUse()
{
	return (channelID_ == IOCHANNEL_UNUSED) ? FALSE : TRUE;
}

void 
IOChannel::setID(int ID)
{
	channelID_ = ID;
}

int 
IOChannel::getID()
{
	return channelID_;
}

void 
IOChannel::setPriority(int priority)
{
	priority_ = priority;
}

int 
IOChannel::getPriority()
{
	return priority_;
}

void 
IOChannel::setInputHandler(InputCallbackHandler * pOCbH)
{
	pInputCallbackHandler_ = pOCbH;
}

InputCallbackHandler * 
IOChannel::getInputHandler()
{
	return pInputCallbackHandler_;
}

void 
IOChannel::setOutputHandler(OutputCallbackHandler * pOCbH)
{
	pOutputCallbackHandler_ = pOCbH;
}

OutputCallbackHandler * 
IOChannel::getOutputHandler()
{
	return pOutputCallbackHandler_;
}

int 
IOChannel::getSizeofBuffer()
{
	return v_chBuffer_.size();
}


byte* 
IOChannel::getBuffer()
{
	return pBuffer_;
}

void 
IOChannel::setDstAddr(DevAddr_t dstAddr)
{
	DstAddr_ = dstAddr;
}

DevAddr_t 
IOChannel::getDstAddr()
{
	return DstAddr_;
}

void 
IOChannel::setMaxPayload(int maxPayload)
{
	maxPayload_ = maxPayload;
}

int 
IOChannel::getMaxPayload()
{
	return maxPayload_;
}

bool
IOChannel::ThisIsBroadcastingChannel()
{
    return broadcast_;
}

void 
IOChannel::setInterfaceInformation(int interInfo)
{
	interfaceInformation_ = interInfo;
}

int 
IOChannel::getInterfaceInformation()
{
	return interfaceInformation_;
}
