#include "IODeviceManager.h"
#include "IODevice.h"
#include "Utility/String.h"
#include "IODeviceTable.h"
#include "errorno.h"

//Following headers shall be intialized through Abstract Factory or device repository
#include "nil/Socket/SockStream.h"
#include "nil/Socket/SockDgram.h"
#include "nil/RS232C/RS232C.h"

//** Static Member variables*/
IODeviceManager *
IODeviceManager::pInstance_ = NULL;

IODeviceManager::IODeviceManager()
{
    //bring the instance of channel manager
	pIOChannelManager_ = IOChannelManager::instance();
}

IODeviceManager::~IODeviceManager()
{
    OSAL_DELETE(pInstance_);
}

uint8
IODeviceManager::deviceCount_ = 0;

IODeviceManager * 
IODeviceManager::instance()
{
	if(pInstance_ == NULL)
		pInstance_ = new IODeviceManager;
	return pInstance_;
}

int 
IODeviceManager::registerIODevice(IODevice *iodev)
{
    if(IODeviceTable_.find(iodev->getName()) != NULL)
		return MW_FAILED;

    //insert io device into table
	iodev->setID(++deviceCount_);	
	return IODeviceTable_.insert(iodev);
}

int
IODeviceManager::unregisterIODevice(IODevice *iodev)
{
	if(IODeviceTable_.find(iodev->getName()) != NULL)
		return MW_FAILED;
    
    //remove io device into table
    return IODeviceTable_.remove(iodev);
}

IODevice * 
IODeviceManager::findIODevice(const String &devName)
{
	return IODeviceTable_.find(devName);
}

void 
IODeviceManager::channelJoinNotification(IODevice *iodev, DevAddr_t devaddr)
{
    pIOChannelManager_->channelJoinNotification(iodev,devaddr);
}

void 
IODeviceManager::channelLeaveNotification(IODevice *iodev, DevAddr_t devaddr)
{
    pIOChannelManager_->channelLeaveNotification(iodev,devaddr);
}

int 
IODeviceManager::initialize(void *configure)
{ 
	DEBUGTRACE("IODeviceManager::initialize");	
        
    //TODO: Herein, procedure for reading directives of IODevices and configuring them.
	//		from pre-configured directory
  
    //TCP/IP
	IODevice * ioDev_SockStreamServer = new SockStream;
    int ret = ioDev_SockStreamServer->open(configure);
        
	if(ret == MW_SUCCESS)
	{
        IODeviceTable_.insert(ioDev_SockStreamServer);
	}

    //UDP/IP
	IODevice * ioDev_SockDgram = new SockDgram;
	ret = ioDev_SockDgram->open(configure);
    
	if(ret == MW_SUCCESS)
	{
		IODeviceTable_.insert(ioDev_SockDgram);
	}

	//RS232C
	IODevice * ioDev_RS232C = new RS232C;
	ret = ioDev_RS232C->open(NULL);

	if(ret == MW_SUCCESS)
	{
		IODeviceTable_.insert(ioDev_RS232C);
	}

	return MW_SUCCESS;
}

void 
IODeviceManager::finalize()
{
	DEBUGTRACE("IODeviceManager::finalize");
    IODeviceTable_.removeAll();
}

int 
IODeviceManager::reset()
{
	DEBUGTRACE("IODeviceManager::reset");
    deviceCount_ = 0;
    IODeviceTable_.removeAll();
    return MW_SUCCESS;
}

