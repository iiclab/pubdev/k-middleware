#ifndef IODEVICEMANAGER_H
#define IODEVICEMANAGER_H

#include "IOChannel.h"
#include "IODevice.h"
#include "IODeviceTable.h"
#include "IOChannelManager.h"
#include "Utility/String.h"

//class IODeviceMgntCallback;
class IODeviceManager 
{
public:
	static IODeviceManager * instance();
	virtual ~IODeviceManager();

	int registerIODevice(IODevice *);
    int unregisterIODevice(IODevice *);
	IODevice *findIODevice(const String &);

// 	IOChannel *createChannel(int, const String &, void *);
// 	IOChannel *findChannel(const String &, void *);
//     int removeChannel(const String &, void *);

	void channelJoinNotification(IODevice *, DevAddr_t );
	void channelLeaveNotification(IODevice *, DevAddr_t );

	int initialize(void *);
	int reset();
	void finalize();

protected:
	IODeviceManager();

private:    
	static IODeviceManager *pInstance_;
    IOChannelManager *pIOChannelManager_;
    
    IODeviceTable IODeviceTable_;
	static uint8 deviceCount_;
};

#endif //IODEVICE_MANAGER_H
