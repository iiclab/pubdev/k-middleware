// =========================================================================
/**
 *  @filename	DebugTrace.h
 *	@Revised
 *  @Revision
 *  @Author		SeongHoon Kim
 *  
 *  @Description
 *		
 *	@NOTE
 *	
 */	
// =========================================================================
#include <cstdio>
#include <cstring>
#include "DebugTrace.h"

OSB_BEGIN_VERSIONED_NAMESPACE_DECL


int DebugTrace::enableTracing_ = DEFAULT_TRACING;

int DebugTrace::nestingIndent_ = 0;

//TODO: I wonder if the path of source can be ignored.
//		If it can be, I want to find it. Otherwise, I will implement it.

DebugTrace::DebugTrace( const char *modname, 
//						const int line, 
						const char *funcname)
{
	moduleName_ = new char[strlen(modname)+1];
	funcName_ = new char[strlen(funcname)+1];

	strcpy(moduleName_,modname);
	strcpy(funcName_,funcname);

	printDBGMessage("@@@Enter ", funcName_,moduleName_);
	printf("\n");
	nestingIndent_++;

//	printf("nesting Indent %d\n", nestingIndent_);
}

DebugTrace::~DebugTrace()
{
//	if(DebugTrace::enableTracing_ && DebugTrace::nestingIndent_ < DEFAULT_INDENT)
//	{	
//		fprintf(stderr,"###Exit  [%s]-[%s]-[%d]\n",  getModuleName(moduleName_), funcName_, nestingIndent_);
//		//fprintf(stderr,"Enter [%s]-[%d]\n", funcName_, nestingIndent_);

//	}
	
	printDBGMessage("###Exit ", funcName_,moduleName_);
	printf("\n");
	nestingIndent_--;
	
	if(moduleName_)
		delete [] moduleName_;
	
	if(funcName_)
		delete [] funcName_;
}

// Enable the tracing facility.
void 
DebugTrace::startTracing (void)
{
	enableTracing_= 1;
}

// Disable the tracing facility.
void 
DebugTrace::stopTracing (void)
{
	enableTracing_= 0;
}

// Change the nesting indentation level.
void 
DebugTrace::setNestingIndent (int indent)
{
	nestingIndent_ = indent;
}

// Get the nesting indentation level.
int 
DebugTrace::getNestingIndent (void)
{
	return nestingIndent_;
}

inline
void 
DebugTrace::printDBGMessage(const char *szDebugMessage, 
							const char *funcName,
							char *modname
							)
{
	if(DebugTrace::enableTracing_ && DebugTrace::nestingIndent_ < DEFAULT_INDENT)
	{	
		//fprintf(stderr,"[%20s]-%s%s-[%d]\n", getModuleName(modname), szDebugMessage, funcName,  nestingIndent_);
		fprintf(stderr,"[%-20s]-%s%s", getModuleName(modname), szDebugMessage, funcName);
	}	
}

void 
DebugTrace::dump (void) const
{

}

char * 
DebugTrace::getModuleName(char *modName)
{
	int moduleNameLen = strlen(modName);
	for(int i=1; i < moduleNameLen; i++)
	{
		int strIndex = moduleNameLen - i;
		if (modName[strIndex] == '\\') {
			return &modName[++strIndex];
		}	
	}
	return NULL;
}

OSB_END_VERSIONED_NAMESPACE_DECL

