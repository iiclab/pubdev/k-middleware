#include "SysEventImplementation.h"
#include "Debug.h"
#include "errorno.h"

namespace OSAL
{

	SysEventImplementation::SysEventImplementation()
	{
		//resize event-handle map
//		m_handle_.resize(10);
	}

	SysEventImplementation::~SysEventImplementation()
	{
		//remove all handles from event-handle map 
		//and all threads from thread vector
		ThreadVector::iterator pos;
		for (pos = v_thread_.begin(); pos != v_thread_.end(); pos++)
		{
			OSAL_DELETE(*pos);
		}

		//flush all elements in vector 
		v_thread_.clear();
	}

	int
	SysEventImplementation::registerSyncOperation( IRunnable *pRunnable )
	{
		//create a thread 
		Thread *pIOThread = new Thread(pRunnable);
		ThreadVector::iterator pos;

		//first check whether already exists
		for (pos = v_thread_.begin(); pos != v_thread_.end(); pos++)
		{
			if ( (*pos)->pTarget_ == pRunnable )
			{
				//TODO:check thread state, if thread is running, it's ok
				//if not, it will start thread again
				return (*pos)->getThreadID();
			}
		}

		//if not, push in thread vector
		v_thread_.push_back(pIOThread);

		//start corresponding thread
		pIOThread->start();

		//return thread ID
		return pIOThread->getThreadID();
	}

	int
	SysEventImplementation::unregisterSyncOperation(IRunnable *pRunnable)
	{
		ThreadVector::iterator pos;    

		//first check whether already exists
		for (pos = v_thread_.begin(); pos != v_thread_.end(); pos++)
		{
			if ( (*pos)->pTarget_ == pRunnable )
			{
				OSAL_DELETE(*pos);		
				break;
			}
		}

		//delete corresponding thread in memory
		v_thread_.erase(pos);

		return MW_SUCCESS;
	} 


#ifdef WIN32
	
	Win32SysEventImplementation::Win32SysEventImplementation():nEventHandle_(0)
	{
		apHandles_ = new OSAL_HANDLE[HERM_MAXIMUM_EVENTS]; 
		pSockArray_ = new OSAL_SOCKET[HERM_MAXIMUM_EVENTS];
		pDeviceType_ = new bool[HERM_MAXIMUM_EVENTS];
		v_handle_.resize(HERM_MAXIMUM_EVENTS);
	}

	Win32SysEventImplementation::~Win32SysEventImplementation()
	{
		delete[] apHandles_;
		apHandles_ = NULL;
	}

	int 
	Win32SysEventImplementation::registerAsyncOperation(bool DeviceType, OSAL_HANDLE handle, IAsyncOperation *aop, OSAL_SOCKET sock)
	{
		pDeviceType_[nEventHandle_] = DeviceType;
		apHandles_[nEventHandle_] = handle;
		v_handle_[nEventHandle_] = aop;
		pSockArray_[nEventHandle_++] = sock;				
		return MW_SUCCESS;
	}

	int
	Win32SysEventImplementation::unregisterAsyncOperation(OSAL_HANDLE handle)
	{
		int omitIndex;
		//find a handle and then remove handler
		for(int pos=0; pos<nEventHandle_ ; pos++)
		{
			if ( apHandles_[pos] == handle )
			{
//				OSAL_DELETE(v_handle_[pos]);
				nEventHandle_--;
				omitIndex = pos;
				break;
			}
		}
		
		//compress socket and handle array
		for (int i=omitIndex ; i<nEventHandle_ ; i++)
		{
			apHandles_[i] = apHandles_[i+1];
			pSockArray_[i] = pSockArray_[i+1];
			v_handle_[i] = v_handle_[i+1];
			pDeviceType_[i] = pDeviceType_[i+1];
		}

		return MW_SUCCESS;
	}

	void
	Win32SysEventImplementation::handle_event(timeValue timeout)
	{
		int i, index;
		WSANETWORKEVENTS netEvents;
		register int iEventVector = 0;
		register IAsyncOperation * pAsyncOP;
		
		//FIXME: This routine is quite overhead. 
//		EventVectorTable::iterator pos; 
		MW_DEBUG((LM_DEBUG,"[_REACTOR_] EVENT_PROCEDURE: REGISTERED_STSTEM_EVENT: %d\n", nEventHandle_));
/*		for(int pos=0; pos<nEventHandle_ ; pos++)
		{
			v_handle_[pos]->asyncOperation();
		}*/
		//event loop
		for(;;)
		{
			MW_DEBUG((LM_DEBUG,"[_REACTOR_] _WAIT_SYSTEM_EVENT_ \n"));
			//NOTE:The order of handling events is dependent on the position of handle in array.
			iEventVector = WaitForMultipleObjectsEx(nEventHandle_,apHandles_,FALSE, WSA_INFINITE,TRUE);
			iEventVector = iEventVector - WSA_WAIT_EVENT_0;

			for (i = iEventVector ; i<nEventHandle_ ; i++)
			{
				if (iEventVector == WAIT_TIMEOUT ||
					iEventVector == WAIT_FAILED) continue;
				//IP-based devices
				if (pDeviceType_[iEventVector])
				{
					iEventVector = WaitForMultipleObjectsEx(1,&apHandles_[i],TRUE, 0,TRUE);
					if (iEventVector == WSA_WAIT_FAILED ||
						iEventVector == WSA_WAIT_TIMEOUT) continue;
						
					else
					{
						iEventVector = i;
						WSAEnumNetworkEvents(pSockArray_[iEventVector], apHandles_[iEventVector], &netEvents);
						if ( netEvents.lNetworkEvents & FD_ACCEPT ) 
						{
							if (netEvents.iErrorCode[FD_ACCEPT_BIT] != 0 )
							{
								puts("ACCEPT ERROR");
								break;
							}
							//find handler of corresponding acync operation
							pAsyncOP = v_handle_[iEventVector];			
							pAsyncOP->handleUserDefinedSystemEvent(pSockArray_[iEventVector]);
						}

						if ( netEvents.lNetworkEvents & FD_READ ) 
						{
							if (netEvents.iErrorCode[FD_READ_BIT] != 0 )
							{
								puts("READ ERROR");
								break;
							}
							//find handler of corresponding acync operation
							pAsyncOP = v_handle_[iEventVector];			
							pAsyncOP->handleUserDefinedSystemEvent(pSockArray_[iEventVector-WSA_WAIT_EVENT_0]);
						}	

						if ( netEvents.lNetworkEvents & FD_CLOSE ) 
						{
							if (netEvents.iErrorCode[FD_CLOSE_BIT] != 0 )
							{
								//process abnormal termination
								fprintf(stderr, "FD_CLOSE error %d\n", netEvents.iErrorCode[FD_CLOSE_BIT]); 
								//break;
							}
							//find handler of corresponding acync operation
							pAsyncOP = v_handle_[iEventVector];			
							pAsyncOP->handleUserDefinedSystemEvent(pSockArray_[iEventVector]);
						}

					}
				}
				//non-IP-based devices
				else
				{
					pAsyncOP = v_handle_[iEventVector];			
					pAsyncOP->handleUserDefinedSystemEvent(pSockArray_[iEventVector]);
				}

			}
			MW_DEBUG((LM_DEBUG,"[_REACTOR_] EVENT_PROCEDURE: REGISTERED_STSTEM_EVENT: %d\n", nEventHandle_));
		}

	}
#else // Linux 
	
	int 
	LinuxSysEventImplementation::registerAsyncOperation(bool DeviceType, OSAL_HANDLE handle, IAsyncOperation *aop)
	{
		//insert particular handle in event-handle map
		m_handle_.insert(std::make_pair(handle, aop));

		return MW_SUCCESS;
	}

	int
	LinuxSysEventImplementation::unregisterAsyncOperation(OSAL_HANDLE handle)
	{
		//find handle and then remove handler
		EventMapTable::iterator pos;    
		for (pos=m_handle_.begin(); pos!=m_handle_.end(); pos++)
		{
			if (pos->first == handle)
			{
				OSAL_DELETE(pos->second);
				break;
			}
		}
		//delete corresponding handle from event-handle map 
		m_handle_.erase(pos);

		return MW_SUCCESS;
	}

	void 
	LinuxSysEventImplementation::handle_event(timeValue timeout)
	{
		register int iEventVector = 0;
		register IAsyncOperation * pAsyncOP;

		for(;;)
		{
			MW_DEBUG((LM_DEBUG,"********Running Main********\n"));		
			int retFd, maxFd = 0;
			fd_set read_fds;
			EventMapTable::iterator pos; 
			MW_DEBUG((LM_DEBUG,"connected file descriptos: %d\n", m_handle_.size()));

			if (m_handle_.size())
			{
				FD_ZERO(&read_fds);

				for (pos=m_handle_.begin(); pos!=m_handle_.end(); pos++)
				{
					retFd = pos->first;
					FD_SET(retFd, &read_fds);

					if (retFd > maxFd)
						maxFd = retFd;
				}

				//asynchronous operation by using select            
				retFd=select(maxFd+1, &read_fds, NULL, NULL, NULL);                
			}
			else
			{
				printf("ERROR: No file descriptos left in AsyncNotificationLoop\n");
				continue;
			}	

			if (retFd <= 0)
			{
				perror("Select in AsyncNotifications()");
				continue;
			}

			if (retFd > 0)
			{   
				for (pos=m_handle_.begin(); pos!=m_handle_.end(); pos++)
				{
					OSAL_HANDLE handle = pos->first;
					if (FD_ISSET(handle, &read_fds))
					{
						pAsyncOP = pos->second;              

						//Handle asynchronous callback for reading data
						if(pAsyncOP->getAOPType() & AOP_TYPE_USER_CALLBACK)
							pAsyncOP->handleUserDefinedSystemEvent(handle);

					}
				}
			}
		}
	}
#endif
}