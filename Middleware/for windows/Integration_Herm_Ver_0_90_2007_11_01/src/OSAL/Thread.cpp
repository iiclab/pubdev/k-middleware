// =========================================================================
/**
 *  @filename	Thread.cpp
 *	@Revised
 *  @Revision
 *  @Author		SeongHoon Kim
 *  
 *  @Description
 *				  Thread base class
 *	@NOTE
 */
// =========================================================================

#include "Thread.h"
#include "ThreadManager.h"
#include "IRunnable.h"
#if !defined(__MW_INLINE__)

#include "Thread.inl"
#endif

namespace OSAL{


/** If this thread was constructed using a separate Runnable run object, 
then that Runnable object's run method is called; otherwise, this method does nothing and returns. */

void 
Thread::run()
{
	pTarget_->run();
}


void 
Thread::start()
{
	hThread_ = pThreadManager_->startThread(*pTarget_);
}

void 
Thread::stop()
{
	suspend();
}

/**Waits for this thread to die. */
void 
Thread::join()
{
	
}

/**Waits at most millis milliseconds for this thread to die. */
void 
Thread::join(long millis)
{

}

/**Deprecated. This method exists solely for use with suspend(), 
which has been deprecated because it is deadlock-prone. For more 
information, see Why are Thread.stop, Thread.suspend and Thread.resume. */ 
void 
Thread::resume()
{
	OSAL::resumeThread( hThread_ );
}

void 
Thread::suspend()
{
	OSAL::suspendThread( hThread_ );
}

void
Thread::sleep(long millis)
{
	
}

}//namespace OSAL