// =========================================================================
/**
 *  @filename	AsyncOperation.h
 *	@Revised
 *  @Revision
 *  @Author		SeongHoon Kim
 *  
 *  @Description
 *		
 *	@NOTE
 *	
 */
// =========================================================================
#include <cstdio>
#include "ConfigAll.h"
#include "osal/AsyncOperation.h"

namespace OSAL{

AsyncOperation::AsyncOperation()
{

}

AsyncOperation::~AsyncOperation()
{

}

OSAL_HANDLE 
AsyncOperation::getHandle()
{
	return handle_;
}

void 
AsyncOperation::setHandle(OSAL_HANDLE h)
{
	handle_ = h;
}

AOP_Type_t
AsyncOperation::getAOPType()
{
	return AOPType_;
}

void 
AsyncOperation::setAOPType(AOP_Type_t AOPType)
{
	AOPType_ = AOPType;
}

}//namespace OSAL
