#include "SysEventProcessor.h"
#include "Debug.h"
#include "errorno.h"

#ifdef OSAL_WIN32

#define MAX_SYS_EVENT MAXIMUM_WAIT_OBJECTS 

#else

#define MAX_SYS_EVENT

#endif//OSAL_WIN32

namespace OSAL{

SysEventProcessor * 
SysEventProcessor::instance_ = NULL;

SysEventProcessor * 
SysEventProcessor::instance()
{
	if(instance_ == NULL)	
		instance_ = new SysEventProcessor;
	return instance_;
}

SysEventProcessor::SysEventProcessor()
{
#ifdef OSAL_WIN32
	pSysEventImpl_ = new Win32SysEventImplementation;
#else //linux 
	pSysEventImpl_ = new LinuxSysEventImplementation;
#endif
}


SysEventProcessor::~SysEventProcessor()
{

}

int 
SysEventProcessor::initialize()
{
	printConfiguration();
	return 0;
}

OSAL_HANDLE 
SysEventProcessor::createHandleRequest()
{
#ifdef OSAL_WIN32
	if(opMode_ == OPMODE_ASYNC)
	{
		return WSACreateEvent();
	}
	else
	{
		return CreateEvent(NULL, FALSE, FALSE, NULL);
	}
#else  // linux
	return 0;
#endif
}

int 
SysEventProcessor::setSysEvent(OSAL_HANDLE h)
{
#ifdef OSAL_WIN32
	return SetEvent(h);
#else
	return 0;
#endif
}

OpMode_t 
SysEventProcessor::getOpMode()
{
	return opMode_;
}

void 
SysEventProcessor::setOpMode(OpMode_t om)
{
	opMode_ = om;
}

void 
SysEventProcessor::closeHandleRequest(OSAL_HANDLE h)
{
#ifdef OSAL_WIN32
	CloseHandle(h);
#else

#endif	
}

int 
SysEventProcessor::registerAsyncOperation(bool devType, OSAL_HANDLE h, IAsyncOperation *aop, OSAL_SOCKET sock)
{
	return pSysEventImpl_->registerAsyncOperation(devType, h, aop, sock);
}

int 
SysEventProcessor::unregisterAsyncOperation(OSAL_HANDLE h)
{
	return pSysEventImpl_->unregisterAsyncOperation(h);
}

int
SysEventProcessor::registerSyncOperation(IRunnable *pRun)
{
	return pSysEventImpl_->registerSyncOperation(pRun);
}

int
SysEventProcessor::unregisterSyncOperation(IRunnable *pRun)
{
	return pSysEventImpl_->unregisterSyncOperation(pRun);
}

void
SysEventProcessor::handle_event(timeValue timeout)
{
	pSysEventImpl_->handle_event(timeout);
}

void 
SysEventProcessor::printConfiguration()
{
	char mode[10];
	if(opMode_ == OPMODE_ASYNC)
	{
		strcpy(mode,"ASYNC");
	}
	else if(opMode_ == OPMODE_SYNC)
	{	
		strcpy(mode,"SYNC");
	}else
	{
		strcpy(mode,"UNKNOW");
	}
	
	MW_DEBUG((LM_INFO, "Middleware Opeartion Mode : %s\n", mode));
	
}

}//namespace OSAL