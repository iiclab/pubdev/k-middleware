// =========================================================================
/**
 *  @filename	OSAL_Thread.h
 *	@Revised
 *  @Revision
 *  @Author		SeongHoon Kim
 *  
 *  @Description

 *	@NOTE 
 */
// =========================================================================
#ifndef _OSAL_THREAD_H_
#define _OSAL_THREAD_H_

#include "ConfigAll.h"

#ifdef OSAL_WIN32
	
typedef OSAL_HANDLE THR_Handle_t;

#else

typedef pthread_t THR_Handle_t;

#endif



namespace OSAL
{
    THR_Handle_t createThread(void *, uint32, pfTHR_FUNC_t,
							  void *, uint32, uint32 *);
	
	bool TerminateThread(THR_Handle_t, int);

	THR_FUNC_RETURN suspendThread(THR_Handle_t);

	THR_FUNC_RETURN resumeThread(THR_Handle_t h);
}



#endif//_OSAL_THREAD_H_
