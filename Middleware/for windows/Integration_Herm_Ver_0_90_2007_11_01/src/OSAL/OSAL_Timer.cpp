// =========================================================================
/**
 *  @filename	OSAL_Timer.cpp
 *	@Revised
 *  @Revision
 *  @Author		SeongHoon Kim
 *  
 *  @Description

 *	@NOTE
 */
// =========================================================================

#include "OSAL_Timer.h"
#include <stdio.h>

////Global 


#ifdef OSAL_WIN32

OSAL_INLINE
VOID CALLBACK 
TimerAPCProc( LPVOID lpArgToCompletionRoutine, DWORD dwTimerLowValue, DWORD dwTimerHighValue)
{
	(*(OSAL::pfSystemTimerHandler_t)lpArgToCompletionRoutine)();
}

#else


#endif

OSAL::pfSystemTimerHandler_t OSAL::gpfTimerHandler = NULL;

OSAL_INLINE int 
OSAL::setSystemTimer(OSAL_HANDLE h, int timeout)
{
#ifdef OSAL_WIN32
	LARGE_INTEGER liDueTime;
	liDueTime.QuadPart = -timeout*10000;
	
	return !SetWaitableTimer(h, &liDueTime, 0, TimerAPCProc, gpfTimerHandler, FALSE);

#else

#endif//OSAL_WIN32
}

OSAL_INLINE OSAL_HANDLE 
OSAL::createSystemTimer()
{
#ifdef OSAL_WIN32
/*
	char buf[30];
	OSAL_HANDLE h;
	int m = MAXIMUM_WAIT_OBJECTS*MAXIMUM_WAIT_OBJECTS;//*MAXIMUM_WAIT_OBJECTS*MAXIMUM_WAIT_OBJECTS*MAXIMUM_WAIT_OBJECTS*MAXIMUM_WAIT_OBJECTS;
	m=0;
	for(int i = 0; i < m; i++)
	{
		sprintf(buf, "TimerService%d",i);
		h = CreateWaitableTimer(NULL, FALSE, buf);
		printf("%s is created %04x\n",buf, (int)h);
		if(h == NULL)
			printf("Error number %d\n",GetLastError());
	}
*/
	return CreateWaitableTimer(NULL, FALSE, "WaitableTimer1");
#else

#endif// OSAL_WIN32
}

OSAL_INLINE int 
OSAL::closeSystemTimer(OSAL_HANDLE h)
{
#ifdef OSAL_WIN32
	return !CloseHandle(h);
#else
	
#endif// OSAL_WIN32
}


OSAL_INLINE int 
OSAL::cancelSystemTimer(OSAL_HANDLE h)
{
#ifdef OSAL_WIN32
	return !CancelWaitableTimer(h);
#else

#endif//OSAL_WIN32
}

OSAL_INLINE uint32 
OSAL::getTimeofDay()
{
#ifdef OSAL_WIN32
	return GetTickCount();
#else

#endif//OSAL_WIN32
}

OSAL_INLINE void 
OSAL::registerSystemTimerHandler(pfSystemTimerHandler_t pfTimerHandler)
{
	gpfTimerHandler = pfTimerHandler;
}



