#ifndef SYSEVENTPROCESSOR_H
#define SYSEVENTPROCESSOR_H

#include <map>
#include <vector>
#include "IAsyncOperation.h"
#include "IRunnable.h"
#include "Thread.h"
#include "SysEventImplementation.h"

typedef unsigned long timeValue;

namespace OSAL{

	class SysEventProcessor 
	{
	public:        
		static SysEventProcessor * instance();
		virtual ~SysEventProcessor();

		//asynchronous operation
		int registerAsyncOperation(bool, OSAL_HANDLE, IAsyncOperation *, OSAL_SOCKET sock);
		int unregisterAsyncOperation(OSAL_HANDLE);

		//synchronous operation
		int registerSyncOperation(IRunnable *);
		int unregisterSyncOperation(IRunnable *);   

		//handle event 
		void handle_event(timeValue timeout = 0);

		OSAL_HANDLE createHandleRequest();	
		int setSysEvent(OSAL_HANDLE h);
		void closeHandleRequest(OSAL_HANDLE);
		int initialize();

		OpMode_t getOpMode();
		void setOpMode(OpMode_t);

		void printConfiguration();

	protected:
		SysEventProcessor();

	private:	
		static SysEventProcessor * instance_;
		OpMode_t opMode_;
		
		//divide interface into implementation by bridge pattern
		SysEventImplementation *pSysEventImpl_;
	};

}

/*
class MW_EXPORT SysEventProcessor {
public:        

	static SysEventProcessor * instance();

    SysEventProcessor(const SysEventProcessor & X);
	
    virtual ~SysEventProcessor();

    int registerAsyncOperation(IAsyncOperation *);
	int unregisterAsyncOperation(IAsyncOperation *);
	
	int registerSyncOperation(IAsyncOperation *);
	int unregisterSyncOperation(IAsyncOperation *);

	OSAL_HANDLE createHandleRequest();
	
	int setSysEvent(OSAL_HANDLE h);

	void closeHandleRequest(OSAL_HANDLE);

    void runEventLoop();

    int intialize();
	
	OpMode_t getOpMode();

	void setOpMode(OpMode_t);
	
	void printConfiguration();

protected:
	SysEventProcessor();
private:
	
	static SysEventProcessor * instance_;

	///NOTE: This should be changed into list
	typedef std::vector< IAsyncOperation* > EventVectorTable;

	EventVectorTable ctEventVT_;	//ct => container.
	
	OSAL_HANDLE *apHandles_;
	
	int nEventHandle_;
	
	int eventMode_;
	
	OpMode_t opMode_;
};
*/

#endif //SYSEVENTPROCESSOR_H
