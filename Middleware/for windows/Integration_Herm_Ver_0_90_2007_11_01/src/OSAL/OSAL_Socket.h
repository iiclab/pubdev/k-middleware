#ifndef _OSAL_SOCKET_H_
#define _OSAL_SOCKET_H_

/*
 * Option flags per-socket and Additional options.
 */

#include "ConfigAll.h"

#ifdef OSAL_WIN32

#	define MW_SO_DEBUG        SO_DEBUG				/* turn on debugging info recording */
#	define MW_SO_ACCEPTCONN   SO_ACCEPTCONN			/* socket has had listen() */
#	define MW_SO_REUSEADDR    SO_REUSEADDR			/* allow local address reuse */
#	define MW_SO_KEEPALIVE    SO_KEEPALIVE			/* keep connections alive */
#	define MW_SO_DONTROUTE    SO_DONTROUTE			/* just use interface addresses */
#	define MW_SO_BROADCAST    SO_BROADCAST			/* permit sending of broadcast msgs */
#	define MW_SO_USELOOPBACK  SO_USELOOPBACK		/* bypass hardware when possible */
#	define MW_SO_LINGER       SO_LINGER				/* linger on close if data present */
#	define MW_SO_OOBINLINE    SO_OOBINLINE			/* leave received OOB data in line */

#	define MW_SO_DONTLINGER	 SO_DONTLINGER  

#	define MW_SO_SNDBUF       SO_SNDBUF				/* send buffer size */
#	define MW_SO_RCVBUF       SO_RCVBUF				/* receive buffer size */
#	define MW_SO_SNDLOWAT     SO_SNDLOWAT			/* send low-water mark */
#	define MW_SO_RCVLOWAT     SO_RCVLOWAT			/* receive low-water mark */
#	define MW_SO_SNDTIMEO     SO_SNDTIMEO			/* send timeout */
#	define MW_SO_RCVTIMEO     SO_RCVTIMEO			/* receive timeout */
#	define MW_SO_ERROR        SO_ERROR				/* get error status and clear */
#	define MW_SO_TYPE         SO_TYPE				/* get socket type */

#else//

#	define MW_SO_DEBUG        SO_DEBUG				/* turn on debugging info recording */
#	define MW_SO_ACCEPTCONN   SO_ACCEPTCONN			/* socket has had listen() */
#	define MW_SO_REUSEADDR    SO_REUSEADDR			/* allow local address reuse */
#	define MW_SO_KEEPALIVE    SO_KEEPALIVE			/* keep connections alive */
#	define MW_SO_DONTROUTE    SO_DONTROUTE			/* just use interface addresses */
#	define MW_SO_BROADCAST    SO_BROADCAST			/* permit sending of broadcast msgs */
#	define MW_SO_USELOOPBACK  SO_USELOOPBACK		/* bypass hardware when possible */
#	define MW_SO_LINGER       SO_LINGER				/* linger on close if data present */
#	define MW_SO_OOBINLINE    SO_OOBINLINE			/* leave received OOB data in line */

#	define SO_DONTLINGER SO_DONTLINGER  

#	define MW_SO_SNDBUF       SO_SNDBUF				/* send buffer size */
#	define MW_SO_RCVBUF       SO_RCVBUF				/* receive buffer size */
#	define MW_SO_SNDLOWAT     SO_SNDLOWAT			/* send low-water mark */
#	define MW_SO_RCVLOWAT     SO_RCVLOWAT			/* receive low-water mark */
#	define MW_SO_SNDTIMEO     SO_SNDTIMEO			/* send timeout */
#	define MW_SO_RCVTIMEO     SO_RCVTIMEO			/* receive timeout */
#	define MW_SO_ERROR        SO_ERROR				/* get error status and clear */
#	define MW_SO_TYPE         SO_TYPE				/* get socket type */

#endif//OSAL_WIN32


#if defined(OSAL_WIN32)
	typedef SOCKET os_socket;
	typedef struct sockaddr		os_sockaddr;
	typedef struct sockaddr_in  os_sockaddr_in;
	typedef LPWSAPROTOCOL_INFO	os_sockspec;	//socket specification
	typedef GROUP				os_sockgroup;
	typedef DWORD				os_sockflag;
	typedef WSAOVERLAPPED		os_overlapped;

	typedef struct {
		os_socket sd_;
		int level_;
		int optname_;
		char* optval_;
		int* optlen_;
	}SocketControlParam_t;

#else	//linux
	typedef int os_socket;
	typedef struct sockaddr		SockAddr_t;
	typedef void*				os_sockspec;	//socket specification
	typedef unsigned int		os_sockgroup;
	typedef unsigned long		os_sockflag;
	typedef void*				os_overlapped;

	typedef struct {
		os_socket sd_;
		int level_;
		int optname_;
		int* optval_;
	}SocketControlParam_t;

#endif//OSAL_WIN32

#endif//_OSAL_TIMER_H_


