// =========================================================================
/**
 *  @filename	Thread_Impl.h
 *	@Revised
 *  @Revision
 *  @Author		SeongHoon Kim
 *  
 *  @Description
 *				  Thread Implementation base class
 *	@NOTE
 *	Currently, the name of middle-ware is not determined. So, I name it 
 *	QoS-Enabled Low-weight Robot Middle-ware(QLR) temperarily. And this file
 *	is used for configuration of 
 */
// =========================================================================

#ifndef THREAD_IMPL_H
#define THREAD_IMPL_H
#include "ConfigAll.h"
#include "IRunnable.h"
#include "OSAL_Thread.h"

class ThreadManager;
//This class implemented using template method pattern.
namespace OSAL{

class Thread
{
	friend class IOProcess;
	friend class SysEventImplementation;
public:

	/**Allocates a new Thread object. */
	Thread();

	/**Allocates a new Thread object. */          
	Thread(IRunnable *pTarget);
	
	virtual ~Thread();

	/** If this thread was constructed using a separate Runnable run object, 
	then that Runnable object's run method is called; otherwise, this method does nothing and returns. */
	void run();
	
	void start();
	
	void stop();

	/**Waits for this thread to die. */
	void join();

	/**Waits at most millis milliseconds for this thread to die. */
	void join(long millis);
	
	/**Deprecated. This method exists solely for use with suspend(), 
	which has been deprecated because it is deadlock-prone. For more 
	information, see Why are Thread.stop, Thread.suspend and Thread.resume. */ 
	void resume();

	/**Deprecated. This method has been deprecated, as it is inherently deadlock-prone. 
	If the target thread holds a lock on the monitor protecting a critical system resource 
	when it is suspended, no thread can access this resource until the target thread is
	resumed. If the thread that would resume the target thread attempts to lock this 
	monitor prior to calling resume, deadlock results. Such deadlocks typically manifest
	themselves as "frozen" processes 
	*/
	void suspend();          
	
	int	getPriority();
	
	void setPriority(int);

	int getType();

	void setType();

	THR_Handle_t getThreadHandle();

	int getThreadID();

	void setThreadID(int thrID);
	
	static void sleep(long millis);
	

protected:
	
private:
	
	int	priority_;
	int	type_;

	IRunnable *pTarget_;
	
	THR_Handle_t hThread_;
	
	int ThreadID_;

	ThreadManager *pThreadManager_;
};

}//namespace OSAL

#if defined(__MW_INLINE__)

#include "Thread.inl"
#endif


#endif //_THREAD_IMPL_H_

