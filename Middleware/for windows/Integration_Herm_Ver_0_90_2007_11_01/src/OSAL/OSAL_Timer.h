// =========================================================================
/**
 *  @filename	OSAL_Timer.h
 *	@Revised
 *  @Revision
 *  @Author		SeongHoon Kim
 *  
 *  @Description

 *	@NOTE
 */
// =========================================================================
#ifndef _OSAL_TIMER_H_
#define _OSAL_TIMER_H_

#include "ConfigAll.h"

namespace OSAL
{

	typedef	void (*pfSystemTimerHandler_t)();

	extern pfSystemTimerHandler_t gpfTimerHandler;
	
	OSAL_HANDLE createSystemTimer();
	
	int closeSystemTimer(OSAL_HANDLE h);

	int setSystemTimer(OSAL_HANDLE h, int timeout);
	
	int cancelSystemTimer(OSAL_HANDLE);
	
	uint32 getTimeofDay();

	void registerSystemTimerHandler(pfSystemTimerHandler_t);
}


#endif//_OSAL_TIMER_H_
