// =========================================================================
/**
 *  @filename	OSAL_Timer.cpp
 *	@Revised
 *  @Revision
 *  @Author		SeongHoon Kim
 *  
 *  @Description

 *	@NOTE
 */
// =========================================================================

#include "ConfigAll.h"
#include "OSAL_Errno.h"
#ifdef OSAL_POSIX
#include <errno.h>
#endif


namespace OSAL{
	
int getLastErrno()
{
#ifdef OSAL_WIN32
	return errno = ::GetLastError();
#elif OSAL_POSIX
	
#endif
	return errno;
}

}

