// =========================================================================
/**
 *  @filename	OSAL_Thread.h
 *	@Revised
 *  @Revision
 *  @Author		SeongHoon Kim
 *  
 *  @Description

 *	@NOTE
 *	Currently, the name of middle-ware is not determined. So, I name it 
 *	QoS-Enabled Low-weight Robot Middle-ware(QLR) temperarily. And this file
 *	is used for configuration of 
 */
// =========================================================================

#include "OSAL_Thread.h"


namespace OSAL{



OSAL_INLINE
THR_Handle_t 
createThread(void *pStack,
                  uint32 stacksize,
                  pfTHR_FUNC_t pfStartRoutine,
                  void *args,
                  uint32 flags,
                  uint32 *pThrId)
{
#ifdef OSAL_WIN32
	return CreateThread(NULL, stacksize, (LPTHREAD_START_ROUTINE)pfStartRoutine,
						args, flags, pThrId);
#else
	pthread_t thr;
	pthread_create(&thr, NULL, pfStartRoutine,args);
	return thr;
#endif
}

OSAL_INLINE bool
TerminateThread(THR_Handle_t h, int exitCode)
{
//	OSAL_UNUSED_ARG(h);
	
#ifdef OSAL_WIN32
	return ::TerminateThread(h, exitCode);
#else
	
#endif	
}

OSAL_INLINE THR_FUNC_RETURN
suspendThread(THR_Handle_t h)
{
#ifdef OSAL_WIN32
	return SuspendThread(h);
#else
	
#endif	
}

OSAL_INLINE THR_FUNC_RETURN
resumeThread(THR_Handle_t h)
{
#ifdef OSAL_WIN32
	return ResumeThread(h);
#else
	
#endif	
}

};//namespace OSAL
//TODO: all APIs related to thread should be implemented here.


