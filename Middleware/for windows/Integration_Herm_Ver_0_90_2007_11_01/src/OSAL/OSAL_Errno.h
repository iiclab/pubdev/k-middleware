// =========================================================================
/**
 *  @filename	OSAL_Timer.h
 *	@Revised
 *  @Revision
 *  @Author		SeongHoon Kim
 *  
 *  @Description

 *	@NOTE
 */
// =========================================================================
#ifndef _OSAL_TIMER_H_
#define _OSAL_TIMER_H_

#include "ConfigAll.h"

#define INVALID_PARAM	0x05


namespace OSAL
{
	int getLastErrno();
}


#endif//_OSAL_TIMER_H_
