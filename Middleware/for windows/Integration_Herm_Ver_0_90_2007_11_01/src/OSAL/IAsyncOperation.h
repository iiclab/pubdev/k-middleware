// =========================================================================
/**
 *  @filename	IAsyncOperation.h
 *	@Revised
 *  @Revision
 *  @Author		SeongHoon Kim
 *  
 *  @Description
 *		
 *	@NOTE
 *	
 */	
// =========================================================================
#ifndef IASYNCOPERATION_H
#define IASYNCOPERATION_H

#include "ConfigAll.h"
#include "Callback_Handler.h"
#include "EventHandler.h"

//Removing comment of marco below result in ...
//DO NOT REMOVE this Macro.
//#define CALLBACK_BY_FUNCTION_PTR


namespace OSAL{

// internal mode for processing data.
typedef enum OpMode_t
{
OPMODE_SYNC		= 0x0001,              ///< read/write synchronously.
OPMODE_ASYNC    = 0x0002           ///< read/write asynchronously.
};
typedef void (* pfSysEventHandler_t)(void *arg);


/**
 * AOP_TYPE_SYS_CALLBACK shall be used when a handler is not needed.
 * For example, ReadFileEx in Win32 is executed along with registering its callback specified by API or other stuff.
 * So, in this case, the internal system event processor calls dummy handler with no operation instead of a specified user handler.
 * AOP_TYPE_USER_CALLBACK shall be used when an event operation shall be handled by user.
 * In this case, its callback operation shall be registered.
 * If AsyncOpeartion need not to process asyncOperation, you shall set AOPType as AOP_TYPE_NONE.
 * @stereotype enumeration 
 */
typedef enum AOP_Type_t{
	AOP_TYPE_NONE			= 0x00,
	AOP_TYPE_SYS_CALLBACK	= 0x01,
	AOP_TYPE_USER_CALLBACK	= 0x02
};

	/** @interface 
	 *	Interface class for Asynchronous Operations dependent on Operating System
	 */
	class IAsyncOperation 
	{
	public:
		virtual ~IAsyncOperation(){}
		/**
		 *	NOTE: if asynOperation's overhead is high, it is recommended 
		 *		  that virtual function be substituted to function pointer
		 */
		virtual void asyncOperation() = 0;

	#ifdef CALLBACK_BY_FUNCTION_PTR	
		virtual void setSysEvent_Handler(pfSysEventHandler_t pCallbackHandler) = 0;	
		virtual pfSysEventHandler_t getSysEvent_Handler() = 0;	
	#else	
		virtual void handleUserDefinedSystemEvent(OSAL_SOCKET) = 0;
	#endif//CALLBACK_BY_FUNCTION_PTR

		virtual OSAL_HANDLE getHandle() =0;
		virtual void setHandle(OSAL_HANDLE h) =0;

		virtual AOP_Type_t getAOPType() = 0;	
		virtual void setAOPType(AOP_Type_t h) =0;
	};

}
#endif //IASYNCOPERATION_H
