// =========================================================================
/**
 *  @filename	ThreadManager.cpp
 *	@Revised
 *  @Revision
 *  @Author		SeongHoon Kim
 *  
 *  @Description

 *	@NOTE
 *	Currently, the name of middle-ware is not determined. So, I name it 
 *	QoS-Enabled Low-weight Robot Middle-ware(QLR) temperarily. And this file
 *	is used for configuration of 
 */
// =========================================================================

#include "ThreadManager.h"
#include "IRunnable.h"
#include "Debug.h"


ThreadManager *
ThreadManager::instance_ = NULL;

ThreadManager::ThreadManager()
{
	
}

ThreadManager::~ThreadManager()
{
	if(instance_)
		delete instance_;
}


ThreadManager *
ThreadManager::instance()
{
	if(instance_ == NULL)
		instance_ = new ThreadManager;
	return instance_;
}

THR_Handle_t 
ThreadManager::startThread(OSAL::IRunnable &target)
{	
	//TODO:This routine shall be changed into thread pool
	uint32 ThreadId;
	THR_Handle_t hThread;
	
	hThread = OSAL::createThread(NULL, 0,  ThreadRoutine, (void *)&target, 0, &ThreadId);
	static int cnt = 0;
	
	if(hThread == NULL)
	{
		MW_DEBUG((LM_DEBUG,"Starting Thread is failed.\n"));
		OSAL::TerminateThread(hThread, -1);
	}

	MW_DEBUG((LM_DEBUG,"Thread Start #Thread(%d) \n",++cnt));
	return hThread;
}


OSAL_INLINE
THR_FUNC_RETURN 
ThreadManager::ThreadRoutine(void *args)
{
	OSAL::IRunnable *pThr = static_cast<OSAL::IRunnable* >(args);
	
	pThr->run();
	
	return 0;
}



