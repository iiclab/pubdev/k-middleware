// =========================================================================
/**
 *  @filename	OSAL_Socket.cpp
 *	@Revised
 *  @Revision
 *  @Author		SeongHoon Kim
 *  
 *  @Description

 *	@NOTE
 */
// =========================================================================

#ifdef OSAL_WIN32
#include <windows.h>
#else

#endif//OSAL_WIN32

//#include "ConfigAll.h"
#include "OSAL_Socket.h"
#include "Debug.h"
#include "errorno.h"

namespace OSAL {

#ifdef OSAL_WIN32


	OSAL_INLINE os_socket
	OSAL::openAsyncSocket(int af, int type, int protocol,  os_sockspec lpProtocolInfo, os_sockgroup g, os_sockflag dwFlags)
	{
		return ;
	}

	OSAL_INLINE os_socket
	OSAL::accept (os_socket h, os_sockaddr *addr, int *addrlen)
	{
		return ::accept (h, addr, addrlen);
	}

	OSAL_INLINE int
	OSAL::connect (os_socket h, const os_sockaddr *addr, int addrlen)
	{
		DEBUGTRACE ("connect");
		return ::connect (h, const_cast <os_sockaddr *> (addr), addrlen);
	}

	OSAL_INLINE int
	OSAL::listen (os_socket handle, int backlog)
	{
		DEBUGTRACE("listen");
		return ::listen ((os_socket) handle, backlog);
	}

	OSAL_INLINE int
	OSAL::close(os_socket sd)
	{
		return ::closesocket(sd);
	}

	int
	OSAL::bind(os_socket sd, unsigned long ipAddr, int port)
	{
		os_sockaddr_in addr;

		memset(&addr, 0,sizeof(os_sockaddr_in));
		addr.sin_family = AF_INET;
		addr.sin_addr.s_addr = ipAddr;
		addr.sin_port = port; 
		//addr.sin_port = htons(port); 

		if(::bind(sd, (sockaddr *)&addr, sizeof(addr)) == SOCKET_ERROR)
			return SOCKET_ERROR;

		return MW_SUCCESS;
	}

	OSAL_INLINE int 
	OSAL::setsockopt(os_socket sd, int level, 
					 int optname, const char* optval, int optlen)
	{
		return ::setsockopt(sd, level, optname, optval, optlen);
	}

	OSAL_INLINE int 
	OSAL::getsockopt(os_socket sd, int level, 
					 int optname, char* optval, int *optlen)
	{
		return ::getsockopt(sd, level, optname, optval, optlen);
	}

	OSAL_INLINE uint32
	OSAL::inet_addr(const char *c)
	{
		return ::inet_addr(c);
	}

	int 
	OSAL::syncSend(os_socket sd, unsigned char *buf, int bufLen, int *flag, void *arg)
	{
		return send(sd, buf, bufLen, flag);
	}

	int 
	OSAL::syncRecv(os_socket sd, unsigned char *buf, int bufLen, int *flag, void *arg)
	{
		return recv(sd, buf, bufLen, flag);
	}

	int 
	OSAL::asyncSend(os_socket sd, unsigned char *buf, int bufLen, int *flag, void *arg)
	{
		uint32 nWrite = 0;
		int flag = 0;
		WSABUF DataBuf;

		DataBuf.len = bufLen;
		DataBuf.buf = buf;

		//We can use a hEvent member only when pOlWrite is not null.
		WSAOVERLAPPED *pOverlapped = (WSAOVERLAPPED *)arg;
		SockStream_AIO_Write_Result *pACT = (SockStream_AIO_Write_Result *)pOverlapped->hEvent;
		pACT->setEventSocket(sd);

		WSASend(sd, &DataBuf, 1, &nWrite, flag, LPWSAOVERLAPPED(arg), SockStream::WriteCompletionROUTINE);
	}

	int 
	OSAL::asyncRecv(os_socket sd, unsigned char *buf, int bufLen, int *flag, void *arg)
	{

	}

#else //Linux

	int
	OSAL::bind(os_socket sd, uint32 myAddr, int port)
	{
		int error;

		if ( port > 0 )
		{
			os_sockaddr_in_.sin_family = AF_INET;
			os_sockaddr_in_.sin_addr.s_addr = myAddr;
			os_sockaddr_in_.sin_port = (short)port;

			if  (::bind ( sd, ( os_sockaddr * ) &os_sockaddr_in_, sizeof ( os_sockaddr_in_ ) ) == -1)
			{
				error = errno;
				while (error = OSAL::close(sd), error == -1 && errno == EINTR);
				errno = error;
				return -1;
			}
		}
		return MW_SUCCESS;
	}


	error = errno;
	while (error = OSAL::close(os_sock_), error == -1 && errno == EINTR);
	errno = error;
	return -1;

#endif

}