// =========================================================================
/**
 *  @filename	IAsyncOperation.h
 *	@Revised
 *  @Revision
 *  @Author		SeongHoon Kim
 *  
 *  @Description
 *		
 *	@NOTE
 *	
 */	
// =========================================================================
#ifndef ASYNCOPERATION_H
#define ASYNCOPERATION_H

#include "IAsyncOperation.h"

//Removing comment of marco below result in ...
//DO NOT REMOVE this Macro.
//#define CALLBACK_BY_FUNCTION_PTR



namespace OSAL{

class AsyncOperation : public OSAL::IAsyncOperation{
public:
	AsyncOperation();

	virtual ~AsyncOperation();
	
	virtual void handleUserDefinedSystemEvent(OSAL_SOCKET){};

    OSAL_HANDLE getHandle();

    void setHandle(OSAL_HANDLE h);

	AOP_Type_t getAOPType();
	
    void setAOPType(AOP_Type_t h);
private:

	OSAL_HANDLE handle_;
	
	AOP_Type_t AOPType_;
};

}
#endif //IASYNCOPERATION_H
