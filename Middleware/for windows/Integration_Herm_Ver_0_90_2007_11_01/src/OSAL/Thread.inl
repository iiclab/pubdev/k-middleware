// =========================================================================
/**
 *  @filename	Thread.inl
 *	@Revised
 *  @Revision
 *  @Author		SeongHoon Kim
 *  
 *  @Description
 *				  Thread Implementation base class
 *	@NOTE
 *	Currently, the name of middle-ware is not determined. So, I name it 
 *	QoS-Enabled Low-weight Robot Middle-ware(QLR) temperarily.
 */
// =========================================================================

namespace OSAL{

/**Allocates a new Thread object. */
Thread::Thread()
{
//	rTarget_ = this;
	ThreadID_ = 0;
	pTarget_ = NULL;
	pThreadManager_ = ThreadManager::instance();
}

/**Allocates a new Thread object. */          
Thread::Thread(IRunnable *pTarget)// : pTarget_(pTarget)
{
	pTarget_ = pTarget;
	ThreadID_ = 0;
	pThreadManager_ = ThreadManager::instance();
}

Thread::~Thread()
{
	ThreadID_ = 0;
	pTarget_ = NULL;
	pThreadManager_ = NULL;
}

//This class is implemented using template method pattern.

OSAL_INLINE
int	
Thread::getPriority()
{
	return priority_;
}

OSAL_INLINE
int 
Thread::getType()
{
	return type_;
}


OSAL_INLINE
void
Thread::setPriority(int priority)
{
	priority_ = priority;
}

OSAL_INLINE
void 
Thread::setType()
{

}


OSAL_INLINE
THR_Handle_t 
Thread::getThreadHandle()
{
	return hThread_;
}

OSAL_INLINE
int 
Thread::getThreadID()
{
	return ThreadID_;
}

OSAL_INLINE
void 
Thread::setThreadID(int thrID)
{
	ThreadID_ = thrID;
}
}//namespace OSAL