// =========================================================================
/**
 *  @filename	IRunnable.h
 *	@Revised
 *  @Revision
 *  @Author		SeongHoon Kim
 *  
 *  @Description
 *				  Thread Implementation base class
 *	@NOTE
 */
// =========================================================================

#ifndef IRUNNABLE_H
#define IRUNNABLE_H
#include "ConfigAll.h"
/**@interface*/

namespace OSAL {
class IRunnable
{
public:
	virtual ~IRunnable(){};

	virtual void run() = 0;
};
}
#endif //IRUNNABLE_H

