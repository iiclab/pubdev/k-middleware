#ifndef SYSEVENTIMPLEMENTATION_H
#define SYSEVENTIMPLEMENTATION_H

#include <map>
#include <vector>
#include "IAsyncOperation.h"
#include "IRunnable.h"
#include "Thread.h"
#include "ConfigMacros.h"

typedef unsigned long timeValue;
#define HERM_MAXIMUM_EVENTS 128

namespace OSAL{

	class SysEventImplementation 
	{
	public:
		//asynchronous operation
		virtual int registerAsyncOperation(bool, OSAL_HANDLE, IAsyncOperation *, OSAL_SOCKET) = 0;
		virtual int unregisterAsyncOperation(OSAL_HANDLE) = 0;

		//synchronous operation
		int registerSyncOperation(IRunnable *);
		int unregisterSyncOperation(IRunnable *);   

		SysEventImplementation();
		~SysEventImplementation();
	
		virtual void handle_event(timeValue timeout) = 0;

	protected:
		typedef std::vector< Thread* > ThreadVector;
		ThreadVector v_thread_;

	};

	//linux	version
	class LinuxSysEventImplementation : public SysEventImplementation
	{
	public:
		//asynchronous operation
		int registerAsyncOperation(bool, OSAL_HANDLE, IAsyncOperation *, OSAL_SOCKET);
		int unregisterAsyncOperation(OSAL_HANDLE);

		void handle_event(timeValue timeout);
	private:
		typedef std::map<OSAL_HANDLE, IAsyncOperation* > EventMapTable;
		EventMapTable m_handle_;	//ct => container.
	};

	//win32	version
	class Win32SysEventImplementation : public SysEventImplementation
	{
	public:
		Win32SysEventImplementation();
		~Win32SysEventImplementation();

		//asynchronous operation
		int registerAsyncOperation(bool, OSAL_HANDLE, IAsyncOperation *, OSAL_SOCKET);
		int unregisterAsyncOperation(OSAL_HANDLE);

		void handle_event(timeValue timeout);
	private :
		typedef std::vector<IAsyncOperation*> EventVectorTable;
		EventVectorTable v_handle_;	//ct => container.

		OSAL_HANDLE *apHandles_;	
		OSAL_SOCKET *pSockArray_;
		bool *pDeviceType_;
		int nEventHandle_;	
	};
}

#endif //SYSEVENTIMPLEMENTATION_H