// =========================================================================
/**
 *  @filename	ConfigMacros.h
 *	@Revised
 *  @Revision
 *  @Author		SeongHoon Kim
 *  
 *  @Description

 *	@NOTE
 *	Currently, the name of middle-ware is not determined. So, I name it 
 *	QoS-Enabled Low-weight Robot Middle-ware(QLR) temperarily. And this file
 *	is used for configuration of 
 */
// =========================================================================
#ifndef THREADMANAGER_H
#define THREADMANAGER_H
#include "ConfigAll.h"
#include "OSAL_Thread.h"
#include "Thread.h"

class IRunnable;

class ThreadManager
{
public:

	virtual ~ThreadManager();

	THR_Handle_t startThread(OSAL::IRunnable &);

	static ThreadManager *instance();

protected:
	ThreadManager();
private:
	static THR_FUNC_RETURN ThreadRoutine(void *);

	static ThreadManager *instance_;
};


#endif//_THREADMANAGER_H_
