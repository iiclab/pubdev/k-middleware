#include <iostream>
using namespace std;

#include <cstdio>
#include <TIME.H>

#include "osal/SysEventProcessor.h"
#include "osal/Thread.h"
#include "osal/IRunnable.h"

#include "QuantumFramework/TimerManager.h"

#include "nil/IODeviceManager.h"
#include "nil/nil.h"

#include "nal/nal.h"
#include "Utility/Msg_t.h"

unsigned char curMiddlewareAddr = 0x24;     // current module address
unsigned char retMiddlewareAddr = 0x32;     // remote module address

class MyRunnable : public OSAL::IRunnable
{
public:
	void run()
	{
		for(;;)
		{
			printf("My Thread is run\n");
			Sleep(1000);
		}
	}
};

class TestEventHandler : public EventHandler
{
public:
	void handleEvent()
	{
		printf("Handle Timer Event\n");
	}
};

class DataSendHandler : public EventHandler
{
public:
	void handleEvent()
	{	
		MW_DEBUG((LM_DEBUG,"******************************************\n"));
		MW_DEBUG((LM_DEBUG,"Handle for Sending Data to Actuator Module!!\n"));
		MW_DEBUG((LM_DEBUG,"******************************************\n"));

		//fill random data out
		byte Buf[100];
		Msg_t outMsg = {100, Buf};
		for (int i=0;i<outMsg.len_;i++) Buf[i] = i;	

		//Send MSG to VPL
		DataRequestParam DataReqParam;
		DataReqParam.dstAddr_ = retMiddlewareAddr;	// to Vision Module
		DataReqParam.msgPriority_ = 30;	
		DataReqParam.nalMsg_ = FALSE;
		DataReqParam.reqChID_ = SHORTEST_PATH;
		pLowerLayer_->dataRequest(&DataReqParam , &outMsg);
	}

	DataSendHandler(NAL *pNal):pLowerLayer_(pNal)
	{
	}

private:
	NAL *pLowerLayer_;
};

//{server role, tcp_port, udp_port, server udp_port, server_tcp_port, ip_address, server_ip_address}      
struct configureAddr ca = {TRUE, 5002, 9001, 9001, 5002, "210.115.43.171", "210.115.43.171"};
struct configureAddr *pCa = &ca;

int main() 
{
	std::cout << "*****************************************" << endl;
	std::cout << "Communication Middleware is started..." << endl;
	std::cout << "*****************************************" << endl; 

	OSAL::SysEventProcessor * sysEventProcessor = OSAL::SysEventProcessor::instance();	
	sysEventProcessor->setOpMode(OSAL::OPMODE_ASYNC);	
	sysEventProcessor->initialize();
	
	TimerManager *pTimerManager = TimerManager::instance();
	pTimerManager->initialize();

	NIL NetworkInterfaceLayer(pCa);
	NetworkInterfaceLayer.initialize();

	NAL NetworkAdaptationLayer(&NetworkInterfaceLayer);
	NetworkAdaptationLayer.initialize(curMiddlewareAddr);

	//TIMER TEST
	DataSendHandler *SendDatahandler = new DataSendHandler(&NetworkAdaptationLayer);
	pTimerManager->setTimer(9500,SendDatahandler);
		
	sysEventProcessor->handle_event();

	return 0;
}



