

MW_INLINE
StateHandler::StateHandler()
{
	
}

MW_INLINE
StateHandler::~StateHandler()
{

}
    
MW_INLINE
void 
StateHandler::initialize(SystemComponent*)
{	
//	DEBUGTRACE("StateHandler::initialize");
//	pOwner_->onInitialize();
}

MW_INLINE
void 
StateHandler::fatalError(SystemComponent *pAction)
{
	DEBUGTRACE("StateHandler::fatalError");
	pAction->onFatalError(pAction->pExecutionCommand_);
}

MW_INLINE
void 
StateHandler::finalize()
{
	DEBUGTRACE("StateHandler::finalize");
}

MW_INLINE
void 
StateHandler::ready(SystemComponent *pAction)
{
	DEBUGTRACE("StateHandler::ready");
}

MW_INLINE
void
StateHandler::error(SystemComponent *pAction)
{
	DEBUGTRACE("StateHandler::error");
	pAction->onError(pAction->pExecutionCommand_);
}

MW_INLINE
void
StateHandler::resume(SystemComponent *pAction)
{
	DEBUGTRACE("StateHandler::resume");
}

MW_INLINE
void
StateHandler::suspend(SystemComponent *pAction)
{
	DEBUGTRACE("StateHandler::suspend");
}

MW_INLINE
void
StateHandler::reset(SystemComponent *pAction)
{
	DEBUGTRACE("StateHandler::reset");
	pAction->onInitialize();
}

MW_INLINE
void 
StateHandler::setState(CompState_t state)
{
	state_ = state;
}

MW_INLINE
CompState_t 
StateHandler::getState()
{
	return state_;
}


/***********************************************************************
State
***********************************************************************/

CreatedState	gCreatedState;
ReadyState		gReadyState;
ActiveState		gActiveState;
ErrorState		gErrorState;
FatalErrorState gFatalErrorState;
FinalizedState	gFinalizeState;

MW_INLINE
CreatedState::CreatedState()
{
	setState(CSTATE_CREATED);
}

MW_INLINE
CreatedState::~CreatedState()
{

}

//who invokes all state handler shall invoke componentAction->onError() if return value is not MW_SUCCESS
MW_INLINE
void 
CreatedState::initialize(SystemComponent* pAction)
{
	DEBUGTRACE("CreatedState::initialize");
	if(pAction->onInitialize() != MW_SUCCESS)
	{
		pAction->transition(&gFatalErrorState);
		pAction->onError(pAction->pExecutionCommand_);
	}
	else
	{
		pAction->transition(&gReadyState);
		pAction->onReady(pAction->pExecutionCommand_);
	}
}

MW_INLINE
ReadyState::ReadyState()
{
	setState(CSTATE_READY);
}

MW_INLINE
ReadyState::~ReadyState()
{
	
}

MW_INLINE
void 
ReadyState::resume(SystemComponent *pAction) 
{
	DEBUGTRACE("ReadyState::resume");
	pAction->transition(&gActiveState);
	if(pAction->onActive(pAction->pExecutionCommand_) != MW_SUCCESS)
	{
		pAction->transition(&gErrorState);
		pAction->onError(pAction->pExecutionCommand_);
	}
}

MW_INLINE
ActiveState::ActiveState()
{
	setState(CSTATE_ACTIVE);
}

MW_INLINE
ActiveState::~ActiveState()
{
	
}

MW_INLINE
void
ActiveState::suspend(SystemComponent *pAction)
{
	DEBUGTRACE("ActiveState::suspend");
	pAction->transition(&gReadyState);
	if(pAction->onReady(pAction->pExecutionCommand_) != MW_SUCCESS)
	{
		pAction->transition(&gErrorState);
		pAction->onError(pAction->pExecutionCommand_);
	}
}

MW_INLINE
void
ActiveState::resume(SystemComponent *pAction)
{
	if(pAction->onActive(pAction->pExecutionCommand_) != MW_SUCCESS)
	{
		pAction->transition(&gErrorState);
		pAction->onError(pAction->pExecutionCommand_);
	}
}

MW_INLINE
ErrorState::ErrorState()
{
	setState(CSTATE_ERROR);
}

MW_INLINE
ErrorState::~ErrorState()
{

}

MW_INLINE
FatalErrorState::FatalErrorState()
{
	setState(CSTATE_FATALERROR);
}

MW_INLINE
FatalErrorState::~FatalErrorState()
{

}

MW_INLINE
FinalizedState::FinalizedState()
{
	setState(CSTATE_FINALIZED);
}

MW_INLINE
FinalizedState::~FinalizedState()
{

}
