// =========================================================================
/**
 *  @filename	.h
 *	@Revised
 *  @Revision
 *  @Author		SeongHoon Kim
 *  
 *  @Description
 *		
 *	@NOTE
 *	
 */	
// =========================================================================
#ifndef _DEBUGTRACE_H_
#define _DEBUGTRACE_H_

#include "ConfigAll.h"

OSB_BEGIN_VERSIONED_NAMESPACE_DECL

#define DEBUG_STRING_LEN 30


/**
 * @class DebugTrace
 *
 * @brief A C++ trace facility that keeps track of which methods are
 * entered and exited.
 *
 * This class uses C++ constructors and destructors to automate
 * the DebugTrace nesting.  In addition, thread-specific storage
 * is used to enable multiple threads to work correctly.
 */
//FIXME: 
//TODO: add timestamp
//NOTE:
class DebugTrace {
public:
	
	DebugTrace( const char *, 
				const char *);
	//DebugTrace(const int line,const char *funcname);
	virtual ~DebugTrace(); 
	
	// Enable the tracing facility.
	static void startTracing (void);
	
	// Disable the tracing facility.
	static void stopTracing (void);

	// Change the nesting indentation level.
	static void setNestingIndent (int indent);
	
	// Get the nesting indentation level.
	static int getNestingIndent (void);
	

	static void printDBGMessage(const char *szDebugMessage,
								const char *funcName,
									  char *modname);

	/// Dump the state of an object.
	void dump (void) const;
	
	static char * getModuleName(char *);
protected:
private:
	
	// Keeps track of how deeply the call stack is nested (this is
	// maintained in thread-specific storage to ensure correctness in
	// multiple threads of control.
	
	// Name of the method we are in.
	char *funcName_;
	
	//file name we are in.
	char *moduleName_;
	
	//line number we are located.
	int line_;
	
	/// Keeps track of how far to indent per trace call.
	static int nestingIndent_;
	
	/// Is tracing enabled?
	static int enableTracing_;
	
	/// Default values.
	enum
	{
		DEFAULT_INDENT  = 10,
		DEFAULT_TRACING = 1
	};
};

OSB_END_VERSIONED_NAMESPACE_DECL

#endif