/* Generated by Together */

#ifndef PRIORITYEVENTHANDLER_H
#define PRIORITYEVENTHANDLER_H
#include "EventHandler.h"


class PriorityEventHandler : public EventHandler {
public:

    int getPriority();

    void setPriority(int priority);

private:
    int priority_;
};
#endif //PRIORITYEVENTHANDLER_H
