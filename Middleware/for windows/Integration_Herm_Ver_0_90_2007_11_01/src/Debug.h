// =========================================================================
/**
 *  @filename	Debug.h
 *	@Revised
 *  @Revision
 *  @Author		SeongHoon Kim
 *  
 *  @Description

 *	@NOTE 
 */
// =========================================================================
#ifndef _DEBUG_H_
#define _DEBUG_H_

#include <cstdio>
#include <cassert>
#include "ConfigAll.h"
//#include "LogMessage.h"
#include "DebugTrace.h"

#define	LM_DEBUG stderr
#define LM_ERROR stderr
#define	LM_INFO  stdout	//shall be switched to file descirptor

#define OSB_LOGMSG(X) fprintf X;

#define MW_PRINT(X) OSB_LOGMSG(X)
#define MW_ERRMSG(X) OSB_LOGMSG(X)


#ifdef _DEBUG
// blank
#define _MY_DEBUG_

#endif

/*MW_DEBUG((LM_DEBUG,"%d %d %d",a,b,c));*/

#ifdef _MY_DEBUG_

#	define DEBUG_LEVEL 1



#	if DEBUG_LEVEL == 1

#		define MW_DEBUG(X)  
#		define DEBUGTRACE(FUNCNAME)

#	elif DEBUG_LEVEL == 2

#		define MW_DEBUG(X)  OSB_LOGMSG(X)
#		define DEBUGTRACE(FUNCNAME)

#	elif DEBUG_LEVEL == 3
#		define MW_DEBUG(X)  OSB_LOGMSG(X)
#		define DEBUGTRACE(FUNCNAME) DebugTrace __(__FILE__,FUNCNAME) ;
#	endif



#define MW_ASSERT(X) assert(X)



#else	//_MY_DEBUG_

#	define DEBUGTRACE(FUNCNAME)
#	define MW_DEBUG(X)
#	define MW_ASSERT(X)

#endif//_MY_DEBUG_




#define _DBG_DEVICEMANAGER_



//macros for debugging by a module unit.

#define DBG_COMPONENT_SYSCOMP

#define DBG_IODEVICES

#define DBG_OS_SERVICE

#define DBG_LAYER_RIOL
#define DBG_LAYER_VPL
#define DBG_LAYER_SL

#if defined(DBG_IODEVICES)

#	define DBG_IODEVICE_RS232
#	define DBG_IODEVICE_SOCKSTREAM
#	define DBG_IODEVICE_SOCKDGRAM

#	define DBG_IOCHANNEL_RS232
#	define DBG_IOCHANNEL_SOCKSTREAM
#	define DBG_IOCHANNEL_SOCKDGRAM

#endif//defined(DBG_IODEVICES)

#if defined(DBG_LAYER_RIOL)

#	define DBG_DEVICEMNGR
#	define DBG_IOCHANNEL_SCHEDULER

#endif//defined(DBG_LAYER_RIOL)


#if defined(DBG_OS_SERVICE)

#	define DBG_OS_SERVICE_EVENTMNGR
#	define DBG_OS_SERVICE_TIMERMNGR
#	define DBG_OS_SERVICE_THREADMNGR

#endif//defined(DBG_OS_SERVICE)

#if defined(DBG_LAYER_VPL)

#	define DBG_MESSAGE_BROKER
#	define DBG_ROUTING_SERVICE

#endif//defined(DBG_LAYER_VPL)

#if defined(DBG_LAYER_SL)

#endif//defined(DBG_LAYER_SL)


#endif//_DEBUG_H_
