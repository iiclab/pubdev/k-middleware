// =========================================================================
/**
 *  @filename	TimerManager.h
 *	@Revised
 *  @Revision
 *  @Author		SeongHoon Kim
 *  
 *  @Description

 *	@NOTE
 * 
 */
// =========================================================================


#ifndef TIMERLIST_T_CPP
#define TIMERLIST_T_CPP

#include "TimerList_T.h"

/*********************************************************************
 * TimerList
 */


template < class TimerNode >
OSAL_INLINE
TimerList< TimerNode >::TimerList()
{
}


template < class TimerNode >
OSAL_INLINE
TimerList< TimerNode >::~TimerList()
{
	
}

template < class TimerNode >
OSAL_INLINE uint32
TimerList< TimerNode >::handleTimeouts(const uint32 to)
{
	TimerList_t::iterator it = tmlist_.begin();
	while (it != tmlist_.end()) {
		
		if(it->getTimeout() <= to){
			//dispatch timer event to destination task
			it->handleTimeout();
			//Remove signaled timer
			it=tmlist_.erase(it);
			
		}else
		{
			return (it->getTimeout() - to);
		}
	}

	return 0;
}

template < class TimerNode >
OSAL_INLINE int 
TimerList< TimerNode >::remove(const TimerNode &tm)
{
	//FIXME: I have to use tmlist_.find() instead of iteration
	TimerList_t::iterator it;
	
	for(it = tmlist_.begin(); it != tmlist_.end(); it++)
		if(tm == *it)
		{
			tmlist_.erase(it);
			break;
		}
//	it = tmlist_.find(tm);
//	if(it != tmlist_.end())
//		tmlist_.erase(it);	
	
		
	//TimerNode tn = it.value_type;
	//delete tn;
	return tmlist_.size();
}

template < class TimerNode >
OSAL_INLINE int 
TimerList< TimerNode >::remove(const uint32 UID)
{
	TimerNode tm(UID,NULL,0);
	
	return remove(tm);
}

template < class TimerNode >
OSAL_INLINE bool 
TimerList< TimerNode >::empty()
{
	return tmlist_.empty();
}

template < class TimerNode >
OSAL_INLINE uint32 
TimerList< TimerNode >::getMinTimeout()
{
	return tmlist_.begin()->getTimeout();
}

#ifdef _DEBUG_TIMER_
template < class TimerNode >
void 
TimerList< TimerNode >::ShowTimers()
{
	for(it = tmlist_.begin();it != tmlist_.end(); it++){
		
	}
}
#endif//_DEBUG_TIMER_


template < class TimerNode > 
OSAL_INLINE TimerNode *
TimerList< TimerNode >::find(const uint32 UID)
{
	TimerNode tm(UID,NULL,0);
	return tmlist_.find(tm);
}


//Insertion sort
template < class TimerNode >
void 
TimerList< TimerNode >::add(TimerNode &tm)
{
	//TODO: Performance optimization is needed.
	TimerList_t::iterator it;
	if(tmlist_.empty())
	{
		tmlist_.insert(tm);
		return;
	}

	for(it = tmlist_.begin();it != tmlist_.end(); it++){
		if(tm < *it)
			break;
	}
	tmlist_.insert(it,tm);
}

#endif //TIMERLIST_T_CPP