// =========================================================================
/**
 *  @filename	TimerNode.h
 *	@Revised
 *  @Revision
 *  @Author		SeongHoon Kim
 *  
 *  @Description
 *		Timer Manager for all components timeout behavior.
 *	@NOTE
 *
 */
// =========================================================================
#ifndef TIMERNODE_H
#define TIMERNODE_H

#include "ConfigAll.h"
#include "EventHandler.h"



template<typename timeout_t>
class Timer
{
#ifdef _DEBUG_TIMER_
	friend std::ostream &operator<<(std::ostream &, const Timer &);
#endif
	
public:
	Timer(const uint32 UID, EventHandler *h, const timeout_t & to);
	
	Timer(const Timer &init);
	
	Timer();
	
	~Timer();

	void setAttr(const uint32 UID, EventHandler *, const timeout_t &);

	void handleTimeout();
	
	timeout_t getTimeout();
	
	uint32 getUID();
	
	void setUID(uint32 );
	
	const Timer &operator=(const Timer &right);
	
	bool operator < (const Timer &right) const;
	
	bool operator == (const Timer &right) const;
	
private:
		
	EventHandler * handler_;
	
	uint32 UID_;
	
	timeout_t timeout_;

    /**
     * @associates TimerList
     * @link association
     * @supplierCardinality 0..*
     */
    /*# int lnkTimerList; */
};


#include "Timer_T.cpp"


#endif//TIMERNODE_H
