// =========================================================================
/**
 *  @filename	TimerManager_Impl.cpp
 *	@Revised
 *  @Revision
 *  @Author		SeongHoon Kim
 *  
 *  @Description

 *	@NOTE
 * 
 */
// =========================================================================





/*********************************************************************
 * TimerManager_Impl 
 */



OSAL_INLINE
TimerManager_Impl::TimerManager_Impl(void *pfDispatcher)
{
	TimerManager_Impl::pfDispatcher_ = (pfDispatch_t)pfDispatcher;
}

OSAL_INLINE
TimerManager_Impl::~TimerManager_Impl()
{

}
// =========================================================================

#ifdef OSAL_WIN32


OSAL_INLINE
Win32_TimerManager_Impl::Win32_TimerManager_Impl(void * pf) :
						 TimerManager_Impl(pf)
{
	
}

OSAL_INLINE
Win32_TimerManager_Impl::~Win32_TimerManager_Impl()
{

}
	
//Start Timer
OSAL_INLINE int 
Win32_TimerManager_Impl::setTimerEvent(OSAL_HANDLE h, int timeout)
{
	LARGE_INTEGER liDueTime;
	liDueTime.QuadPart = -timeout*10000;
	
	return SetWaitableTimer(h, &liDueTime, 0, TimerAPCProc, NULL, FALSE);
	
}

OSAL_INLINE OSAL_HANDLE 
Win32_TimerManager_Impl::initialize()
{
	return CreateWaitableTimer(NULL, FALSE, "WaitableTimer");
}

OSAL_INLINE int 
Win32_TimerManager_Impl::finalize(OSAL_HANDLE h)
{
	return CancelWaitableTimer(h);
}

OSAL_INLINE uint32 
Win32_TimerManager_Impl::getTimeofDay()
{
	return GetTickCount();
}

OSAL_INLINE
VOID CALLBACK 
Win32_TimerManager_Impl::TimerAPCProc(
	LPVOID lpArgToCompletionRoutine,
	DWORD dwTimerLowValue,
	DWORD dwTimerHighValue
	)
{
	(*TimerManager_Impl::pfDispatcher_)();
}

#else	//POSIX


OSAL_INLINE
POSIX_TimerManager_Impl::POSIX_TimerManager_Impl(void * pf) :
						 TimerManager_Impl(pf)
{
	
}

OSAL_INLINE
POSIX_TimerManager_Impl::~POSIX_TimerManager_Impl()
{

}
	
//Start Timer
OSAL_INLINE int 
POSIX_TimerManager_Impl::setTimerEvent(OSAL_HANDLE h, int timeout)
{
	LARGE_INTEGER liDueTime;
	liDueTime.QuadPart = -timeout*10000;
	
	return SetWaitableTimer(h, &liDueTime, 0, TimerAPCProc, NULL, 0);
	
}

OSAL_INLINE OSAL_HANDLE 
POSIX_TimerManager_Impl::initialize()
{
	return CreateWaitableTimer(NULL, FALSE, "WaitableTimer");
}

OSAL_INLINE int 
POSIX_TimerManager_Impl::finalize(OSAL_HANDLE h)
{
	return CancelWaitableTimer(h);
}

OSAL_INLINE
VOID CALLBACK 
POSIX_TimerManager_Impl::signalHandler(
	LPVOID lpArgToCompletionRoutine,
	DWORD dwTimerLowValue,
	DWORD dwTimerHighValue
	)
{
	(*TimerManager_Impl::pfDispatcher_)();
}


#endif// OSB_WIN32
