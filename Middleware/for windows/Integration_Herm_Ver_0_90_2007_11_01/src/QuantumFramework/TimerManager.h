// =========================================================================
/**
 *  @filename	TimerManager.h
 *	@Revised
 *  @Revision
 *  @Author		SeongHoon Kim
 *  
 *  @Description
 *		Timer Manager for all components timeout behavior.
 *	@NOTE
 *
 */
// =========================================================================
#ifndef TIMERMANAGER_H
#define TIMERMANAGER_H


#include <set>
#include <functional>

#include "ConfigAll.h"
#include "EventHandler.h"

#ifdef OSAL_WIN32
#include "TimerList_T.h"
#include "OSAL/OSAL_Timer.h"
#endif

#include "Errorno.h"
#include "Debug.h"



#define	_DEFAULT_TIMER_KEY_	3400
#define EXPIRED_TIME_EVENT 0x03

#ifdef OSAL_WIN32

	#include "TimerManager_Impl.h"
	#include "PriorityEventHandler.h"

	typedef int			os_timer_key;
	typedef int			os_timer_t;
	typedef int			os_timer_sec;

#else
	
	struct eventMsg
	{
		long msgType; // msgType value have to be > 0
		EventHandler *listener;
		//int cnt;

	};

	typedef key_t		os_timer_key;
	typedef timer_t		os_timer_t;
	typedef double		os_timer_sec;
	
	#include <sys/time.h>
	#include <pthread.h>
	#include <sys/types.h>
	#include <sys/ipc.h>
	#include <sys/msg.h>
	#include <sys/stat.h>
	#include <signal.h>
	
	#define BILLION 1000000000L
	#define MILLION 1000000L
#endif

//namespace OSAL{
//template< class TimerNode > class TimerList;
//template< typename timeout_t > class Timer;

//TODO: overwhelming timer's limitation
class TimerManager
{
public:
	
	virtual ~TimerManager();	
	void initialize();
	int finalize();

	static TimerManager *instance(os_timer_key queueKey);
	
	os_timer_t setTimer(os_timer_sec, EventHandler *);	
	void removeTimer(os_timer_t);	
	uint32 getTimeofDay();
	OSAL_HANDLE getHandle();

protected:
	TimerManager(os_timer_key queueKey);

private:
	//Implementaion
	int setTimerEvent(int);
	static inline void dispatch();
	static inline void timeoutEventHandler();

#ifndef OSAL_WIN32
	static void sigHandler(int signo, siginfo_t *info, void *context); // signal callback fucntion
	static void* sigDispatcherThread(void *); // pthread function	
	int setDefaultSignal();
#endif

	static TimerManager * instance_;
	
#ifdef OSAL_WIN32
	uint32 uuid_;
	OSAL_HANDLE phTimer_;
	TimerList< Timer< int > > timerList_;
#else
	static int _hMsgQueue;
	pthread_t _hThread;	
#endif
    /** @link dependency */
    /*# TimerManager_Impl lnkTimerManager_Impl; */

    /** @link dependency */
    /*# PriorityEventHandler lnkPriorityEventHandler; */
};

//}//namespace OSAL

#if defined (__MW_INLINE__)
#include "TimerManager.inl"
#endif//__MW_INLINE__



#endif//TIMERMANAGER_H
