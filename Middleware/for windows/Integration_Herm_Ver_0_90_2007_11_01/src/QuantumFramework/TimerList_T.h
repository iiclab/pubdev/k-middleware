// =========================================================================
/**
 *  @filename	TimerList.h
 *	@Revised
 *  @Revision
 *  @Author		SeongHoon Kim
 *  
 *  @Description
 *		Timer Manager
 *	@NOTE
 *
 */
// =========================================================================
#ifndef TIMERLIST_H
#define TIMERLIST_H



#include <set>
#include <iterator>

#include "ConfigAll.h"
#include "Timer_T.h"

/**
 * @class TimerList
 *
 * @brief 
 * 
 * @NOTE: Actually, this class can be replaced directly with STL containers.
 *		But I build TimerList class to encapsulate STL function because this
 *		will be ported to some other platforms which does not support STL. 
 *		
 */


template< class TimerNode >
class TimerList
{	
public:
	typedef std::multiset< TimerNode > TimerList_t;

	TimerList();
	
	virtual ~TimerList();
	
	void add(TimerNode &tm);

	uint32 handleTimeouts(const uint32);
	/**
		@return the number of remaining timers
	 */
	int remove(const TimerNode & tm);
	/**
		@return the number of remaining timers
	 */
	int remove(const uint32);
	
	//TimerList_t::iterator erase(TimerList_t::iterator it);

	TimerNode *find(const uint32);

	bool empty();

	uint32 getMinTimeout();


#ifdef _DEBUG_TIMER_
	void ShowTimers();
#endif
private:	
	
	TimerList_t tmlist_;

//	typedef TimerList_t::iterator TimerIterator_t;
//	std::set< TimerNode, std::greater< TimerNode > > tmlist2_;
};


#include "TimerList_T.cpp"


#endif//TIMERLIST_H
