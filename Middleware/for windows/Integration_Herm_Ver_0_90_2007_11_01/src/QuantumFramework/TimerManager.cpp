#include "TimerManager.h"

#if !defined (__MW_INLINE__)
#include "TimerManager.inl"
#endif//!__MW_INLINE__

#ifndef OSAL_WIN32
#include <stdio.h>
#include <errno.h>
#include <string.h> //strerror()
#include <unistd.h> // sleep()
#endif

TimerManager *
TimerManager::instance_=NULL;

TimerManager *
TimerManager::instance(os_timer_key queueKey)
{
	if(!instance_)
		instance_ = new TimerManager(queueKey);
	return instance_;
}

os_timer_t 
TimerManager::setTimer(os_timer_sec timeout, EventHandler * pInterceptor)
{
#ifdef OSAL_WIN32

	uint32 newto = timeout + getTimeofDay();

	if( timerList_.empty()			|| 
		newto <= timerList_.getMinTimeout()
		)
		setTimerEvent(timeout);
	
	Timer< int > tm(uuid_, pInterceptor, newto);
//	Timer< uint32 > *tm = new Timer< uint32 >(uuid_++, pInterceptor, newto);
	//insert a timer into timer list.
	timerList_.add(tm);
	
	return uuid_++;

#else

	timer_t timerid;
	struct itimerspec value;
	struct sigevent sevent;

	timerid = NULL;

	sevent.sigev_notify = SIGEV_SIGNAL;
	sevent.sigev_signo = SIGALRM;
	sevent.sigev_value.sival_ptr = (void *)listener;

	if(timer_create(CLOCK_REALTIME, &sevent, &timerid) == -1)
		return timerid;

	value.it_interval.tv_sec = (long)sec;
	value.it_interval.tv_nsec = (sec - value.it_interval.tv_sec) * BILLION;

	if(value.it_interval.tv_nsec >= BILLION){
		value.it_interval.tv_sec++;
		value.it_interval.tv_nsec -= BILLION;
	}

	value.it_value = value.it_interval;

	if(timer_settime(timerid, 0, &value, NULL)== -1)
		return timerid;

	return timerid;

#endif

}

void 
TimerManager::removeTimer(os_timer_t UID)
{
#ifdef OSAL_WIN32
	if(timerList_.remove(UID) == 0)
		OSAL::cancelSystemTimer(phTimer_);
#else
	timer_delete(timerid);
#endif

}

#ifndef OSAL_WIN32

int 
TimerManager::setDefaultSignal()
{
	struct sigaction act;
	act.sa_flags = SA_SIGINFO;
	act.sa_sigaction = TmrTimer::sigHandler;
	if((sigemptyset(&act.sa_mask) == -1) || (sigaction(SIGALRM, &act, NULL) == -1))
		return -1;
	return 0;
}


void 
TimerManager::sigHandler(int signo, siginfo_t *info, void *context) // signal callback fucntion
{
	printf("Enter : sigHandler \n");
	struct eventMsg sendmsg;

	sendmsg.msgType = EXPIRED_TIME_EVENT;
	sendmsg.listener = (TimerSignalListener * )(info->si_value.sival_ptr);
	//sendmsg.cnt = 1;

	if(_hMsgQueue < 0)
	{
		printf("Error msgQueue Handler \n");
		return ;
	}

	int ret;	
	ret = msgsnd(_hMsgQueue, (void *)&sendmsg, sizeof(struct eventMsg), IPC_NOWAIT);
	if(ret < 0){

		printf("Error : msgsnd : ret is %d\n",ret);
		printf("Error Number : %d, %s\n" ,errno, strerror(errno));
	}
	printf("Leave : sigHandler \n");

}
void* 
TimerManager::sigDispatcherThread(void *) // pthread function
{
	//sleep(5);
	printf("Enter : sigDispatcherThread \n");

	struct eventMsg recvmsg;

	/*sigset_t newmask;
	sigemptyset(&newmask);
	sigaddset(&newmask, SIGALRM);
	pthread_sigmask(SIG_BLOCK, &newmask, NULL);*/

	if(_hMsgQueue < 0)
	{
		printf("Error msgQueue Handler \n");
		pthread_exit(NULL);
	}

	while(1)
	{
		printf("Start : msgrcv \n");
		if(msgrcv(_hMsgQueue, (void *)&recvmsg, sizeof(struct eventMsg), EXPIRED_TIME_EVENT, 0) == -1)
		{

			printf("MSG RCV -->Error Number : %d, %s\n" ,errno, strerror(errno));
			printf("msg recv Error \n");
		}


		recvmsg.listener->handleEvent();


		printf("End : msgrcv \n");
	}

}

#endif
//}//namespace OSAL