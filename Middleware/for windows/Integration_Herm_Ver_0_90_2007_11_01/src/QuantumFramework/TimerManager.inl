// =========================================================================
/**
 *  @filename	TimerManager.inl
 *	@Revised
 *  @Revision
 *  @Author		SeongHoon Kim
 *  
 *  @Description

 *	@NOTE
 * 
 */
// =========================================================================

/*********************************************************************
 * TimerManager 
 */
//namespace OSAL{


MW_INLINE
TimerManager::TimerManager(os_timer_key queueKey)
{
#ifdef OSAL_WIN32
	uuid_ = 0;
#else
	_hMsgQueue = msgget((key_t)queueKey, IPC_CREAT|IPC_EXCL|0666);

	printf("Msg Queue ID : %d\n", _hMsgQueue);
	if(_hMsgQueue < 0){
		printf("Error : Create MsgQueue \n");
		printf("Error Number : %d, %s\n" ,errno, strerror(errno));
	}



	setDefaultSignal();

	pthread_create(&_hThread, NULL, sigDispatcherThread, NULL);
#endif
}

MW_INLINE
TimerManager::~TimerManager()
{
#ifdef OSAL_WIN32
	
#else
	printf("Enter : ~TmrTimer\n");
	msgctl(_hMsgQueue, IPC_RMID, 0);

	pthread_cancel(_hThread);
#endif
}

MW_INLINE void 
TimerManager::initialize()
{
	phTimer_ = OSAL::createSystemTimer();
	if(phTimer_ != OSAL_INVALID_HANDLE)
		OSAL::registerSystemTimerHandler(timeoutEventHandler);
}

MW_INLINE int
TimerManager::finalize()
{
	if((OSAL::cancelSystemTimer(phTimer_) != MW_SUCCESS) || 
		(OSAL::closeSystemTimer(phTimer_) != MW_SUCCESS))
		return MW_FAILED;
	else
		return MW_SUCCESS;
}

MW_INLINE uint32 
TimerManager::getTimeofDay()
{
	return OSAL::getTimeofDay();
}

MW_INLINE OSAL_HANDLE 
TimerManager::getHandle()
{
	return phTimer_;
}

MW_INLINE void 
TimerManager::dispatch()
{
//	timerList_->find()->handleTimeout();
	DEBUGTRACE("dispatch");
	
//	instance_->timerList_.find()->handleTimeout();
}

//FIXME: is this routine efficient?
//		It is recommended employing a dispatcher class 
MW_INLINE void 
TimerManager::timeoutEventHandler()
{
	MW_DEBUG((LM_DEBUG,"TimerManager::timeoutEventHandler\n"));

	int nextTimeout = instance_->timerList_.handleTimeouts(instance_->getTimeofDay());
	
	if(!instance_->timerList_.empty())
		instance_->setTimerEvent(nextTimeout);
}

MW_INLINE int 
TimerManager::setTimerEvent(int timeout)
{
	return OSAL::setSystemTimer(phTimer_, timeout);
}

//}//namespace OSAL