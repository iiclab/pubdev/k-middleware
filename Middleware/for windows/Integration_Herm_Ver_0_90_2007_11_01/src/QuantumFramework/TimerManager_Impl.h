// =========================================================================
/**
 *  @filename	Win32_TimerManager_Impl.h
 *	@Revised
 *  @Revision
 *  @Author		SeongHoon Kim
 *  
 *  @Description

 *	@NOTE
 * 
 */
// =========================================================================

#ifndef WIN32_TIMERMANAGER_IMPL_H
#define WIN32_TIMERMANAGER_IMPL_H

#include "ConfigAll.h"

/*********************************************************************
 * TimerManager 
 */
typedef void (*pfDispatch_t)();


class TimerManager_Impl
{
public:
	TimerManager_Impl(void *);
	~TimerManager_Impl();
	
	//Start Timer
	virtual int setTimerEvent(OSAL_HANDLE, int) = 0;
	
	virtual OSAL_HANDLE initialize() = 0;
	
	virtual int finalize(OSAL_HANDLE) = 0;

	virtual uint32 getTimeofDay() = 0;

protected:
	static pfDispatch_t pfDispatcher_;

private:
	
	
};

#if defined (__MW_INLINE__)
#include "TimerManager_Impl.inl"
#endif//__OSB_INLINE__



#endif//WIN32_TIMERMANAGER_IMPL_H