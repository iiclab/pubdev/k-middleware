// =========================================================================
/**
 *  @filename	EventManager.cpp
 *	@Revised
 *  @Revision
 *  @Author		SeongHoon Kim
 *  
 *  @Description
 *		
 *	@NOTE
 *	
 */	
// =========================================================================



#include "PriorityEventHandler.h"

#include "EventManager.h"

#include "osal/SysEventProcessor.h"
#include "osal/AsyncOperation.h"

#include "Errorno.h"

#include "Debug.h"

class EventManager_AsyncOperation : public OSAL::AsyncOperation
{
public:
	EventManager_AsyncOperation(
		EventManager *pEM,OSAL::AOP_Type_t AOPType) : pEventManager_(pEM)
	{
		setHandle(pEM->SysYieldhandle_);
		setAOPType(AOPType);
	}
	
	~EventManager_AsyncOperation(){}
	
	void handleUserDefinedSystemEvent()
	{
		pEventManager_->dispatch();
	}
    
	virtual void asyncOperation(){}
private:

    /**
     * @undirected 
     */
	EventManager *pEventManager_;
};

EventManager *
EventManager::pInstance_ = NULL;


EventManager *
EventManager::instance()
{
	if(pInstance_ == NULL)
		pInstance_ = new EventManager;
	return pInstance_;
}

EventManager::EventManager()
{
	apEventQueue_ = NULL;
	pAsyncOperation_ = NULL;
	priorityBitMap_ = 0;
}

EventManager::EventManager(const EventManager & X)
{
	
}

EventManager::~EventManager()
{
	delete pAsyncOperation_;

	delete [] apEventQueue_;

	pSysEventProcessor_->closeHandleRequest(SysYieldhandle_);
}

int 
EventManager::initialize()
{
	pSysEventProcessor_ = OSAL::SysEventProcessor::instance();
	
	SysYieldhandle_ = pSysEventProcessor_->createHandleRequest();

	apEventQueue_ = new EventQueue_t[NUM_PRIORITY_LEVEL];

	pAsyncOperation_ = new EventManager_AsyncOperation(this,OSAL::AOP_TYPE_USER_CALLBACK);
	
	pSysEventProcessor_->registerAsyncOperation(pAsyncOperation_);

	return 0;
}

int 
EventManager::setEvent(PriorityEventHandler *pPEH)
{
	int priorityBit = 0x0001;
	MW_ASSERT(pPEH != NULL);
	
	priorityBit <<= pPEH->getPriority();

//	MW_ASSERT((PRIORITY_MIN & priority) && (priority < PRIORITY_MAX));
	MW_ASSERT((0x02ff & priorityBit) != 0);

	priorityBitMap_ |= priorityBit;
	//Seperating events from handler is necessary for preventing missing nested event.
	apEventQueue_[pPEH->getPriority()].push(pPEH);
	//This routine should be removed for non-interuptible events
	pSysEventProcessor_->setSysEvent(SysYieldhandle_);

	return MW_SUCCESS;
}
//Dispatching time complexity is O(1).
void 
EventManager::dispatch()
{
	register int prioBitMapIndex;
	register int prioIndex = PRIORITY_MAX;
	DEBUGTRACE("EventManager::dispatch\n");	
	for( ; priorityBitMap_ != 0; prioBitMapIndex >>= 1, prioIndex--)
	{
		
		//shift 
		prioBitMapIndex = PRIORITY_BITMAP_MIN << prioIndex;
		//decrease by 1 
		
		if(!(priorityBitMap_ & prioBitMapIndex))
			continue;
		
		for(int i = 0; !apEventQueue_[prioIndex].empty(); i++)
		{
			/*
			PriorityEventHandler *pPEH = apEventQueue_[prioIndex].front();
			pPEH->handleEvent();
			*/
			apEventQueue_[prioIndex].front()->handleEvent();
			
			apEventQueue_[prioIndex].pop();
		}

		priorityBitMap_ ^= prioBitMapIndex;
	}
}


