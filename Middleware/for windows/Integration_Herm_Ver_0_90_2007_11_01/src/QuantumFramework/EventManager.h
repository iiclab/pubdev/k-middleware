/* Generated by Together */

#ifndef EVENTMANAGER_H
#define EVENTMANAGER_H

#include "osal/SysEventProcessor.h"

#include "Utility/Queue_T.h"
#include "PriorityEventHandler.h"



#define NUM_PRIORITY_LEVEL 10

#define PRIORITY_MAX		10
#define PRIORITY_BITMAP_MAX 0x00000200

#define PRIORITY_MIN		1
#define PRIORITY_BITMAP_MIN 0x00000001

#define PRIORITY_DEFUALT	1

//forward declaration
class EventManager_AsyncOperation;
class CallbackHandler;
class SysEventProcessor;
//Type definition
typedef Queue< PriorityEventHandler * > EventQueue_t;

/** class EventManager
 */
class EventManager{
	friend class EventManager_AsyncOperation;
public:

	
	
    static EventManager *instance();

    EventManager(const EventManager & X);

    virtual ~EventManager();

    int initialize();
	/**
     * @preconditions Priority of event handler shall be set.
     *                Priority shall range from 0 to 10
     * @input pPEH - which is a event handler containing the priority of that events. 
     */
    int setEvent(PriorityEventHandler *pPEH);
protected:
	EventManager();

private:
	static EventManager *pInstance_;

	void dispatch();
	//OSAL::SysEventProcessor *pSysEventProcessor_;
	//to implement O(1) scheduler
	unsigned long priorityBitMap_;
    
	/** @supplierCardinality 1..**/
	EventQueue_t * apEventQueue_;
	
	OSAL_HANDLE SysYieldhandle_;

	EventManager_AsyncOperation * pAsyncOperation_;
    /**
     * @link aggregation
     * @supplierCardinality 1
     * @undirected
     * @supplierRole pPQ*/
    /*# int lnkPriorityQueue; */
	OSAL::SysEventProcessor *pSysEventProcessor_;

    /** @link dependency */
    /*# PriorityEventHandler lnkPriorityEventHandler; */
};

#endif //EVENTMANAGER_H
