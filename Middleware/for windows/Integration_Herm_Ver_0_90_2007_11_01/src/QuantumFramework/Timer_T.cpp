// =========================================================================
/**
 *  @filename	TimerNode.h
 *	@Revised
 *  @Revision
 *  @Author		SeongHoon Kim
 *  
 *  @Description

 *	@NOTE
 * 
 */
// =========================================================================

#ifndef TIMER_T_CPP
#define TIMER_T_CPP

#include "Timer_T.h"

template < typename timeout_t >
OSAL_INLINE
Timer< timeout_t >::Timer()
{
	setAttr(0, NULL, 0);
}

template < typename timeout_t >
OSAL_INLINE
Timer< timeout_t >::~Timer()
{
	
}

template < typename timeout_t >
OSAL_INLINE
Timer< timeout_t >::Timer(const Timer &init)
{
	setAttr(init.UID_, init.handler_, init.timeout_);
}

template < typename timeout_t >
OSAL_INLINE
Timer< timeout_t >::Timer(const uint32 UID, EventHandler *h, const timeout_t & to)
{
	setAttr(UID, h, to);
}

template < typename timeout_t >
OSAL_INLINE const Timer< timeout_t > &
Timer< timeout_t >::operator=(const Timer< timeout_t > &right)
{
	setAttr(right.UID_, &right.handler_, right.timeout_);
	return *this;
}

template < typename timeout_t >
OSAL_INLINE bool 
Timer< timeout_t >::operator < (const Timer< timeout_t > &right) const
{
	return timeout_ < right.timeout_;
}

template < typename timeout_t >
OSAL_INLINE bool 
Timer< timeout_t >::operator == (const Timer< timeout_t > &right) const
{
	return (UID_ == right.UID_);
}

template < typename timeout_t >
OSAL_INLINE void
Timer< timeout_t >::setAttr(const uint32 UID, EventHandler *h, const timeout_t & to)
{
	timeout_ = to;
	handler_ = h;
	UID_ = UID;
}

template < typename timeout_t >
OSAL_INLINE uint32 
Timer< timeout_t >::getUID()
{
	return UID_;
}


template < typename timeout_t >
OSAL_INLINE timeout_t 
Timer< timeout_t >::getTimeout()
{
	return timeout_;
}


template < typename timeout_t >
OSAL_INLINE void 
Timer< timeout_t >::handleTimeout()
{
	handler_->handleEvent();
}


#ifdef _DEBUG_TIMER_
template < typename timeout_t >
OSAL_INLINE std::ostream &
operator<<(std::ostream &output, const Timer< timeout_t > &tmr){
	
	output<<"Task ID : "<< tmr.UID_<<"\n";
	output<<"Event Flag  : "<< tmr.handler_<<"\n";
	output<<"Timeout : "<< tmr.timeout_;
	output<<std::endl;
	return output;
}
#endif //_TIMER_DEBUG_

#ifdef _DEBUG_TIMER_
template < typename timeout_t >
std::ostream &
operator<<(std::ostream &output, const Timer< timeout_t > &tmr){
	
	output<<"Task ID : "<< tmr.UID_<<"\n";
	output<<"Event Flag  : "<< tmr.handler_<<"\n";
	output<<"Timeout : "<< tmr.timeout_;
	output<<std::endl;
	return output;
}
#endif //_TIMER_DEBUG_


#endif //TIMER_T_CPP