// =========================================================================
/**
 *  @filename	PriorityEventHandler.cpp
 *	@Revised
 *  @Revision
 *  @Author		SeongHoon Kim
 *  
 *  @Description

 *	@NOTE
 */
// =========================================================================

#include "PriorityEventHandler.h"

int 
PriorityEventHandler::getPriority()
{
	return priority_;
}

void 
PriorityEventHandler::setPriority(int priority)
{ 
	this->priority_ = priority; 
}

