/************************************************************************
* @ Filename : SL_ServantProxy.h                                                   
* @ Author : BumHyeon Baek (bhbaek@control.kangwon.ac.kr)               
* @ Version : 1.1
* @ Description

* @ NOTE
/************************************************************************/

#ifndef SL_SERVANTPROXY_H
#define SL_SERVANTPROXY_H

#pragma once
#include "SL_Servant.h"
#include "SL_LogicalConnection.h"

namespace MWSL
{
	class MWSL_API ServantPrx
	{
	public:

		~ServantPrx();

	protected:

		ServantPrx(LogicalConnection, const Identity&);

	protected:

		SLCommunicator *__conn;
		Identity __id;
	};
}

#endif
