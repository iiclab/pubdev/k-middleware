/************************************************************************
* @ Filename : SL_Stream.h                                                   
* @ Author : BumHyeon Baek (bhbaek@control.kangwon.ac.kr)               
* @ Version : 1.1
* @ Description

* @ NOTE
/************************************************************************/

#ifndef SL_STREAM_H
#define SL_STREAM_H

#pragma once
#include "SL_Define.h"
#include "SL_ProxyClass.h"
#include "SL_Servant.h"
#include <stdio.h>
#include <vector>
#include <map>

namespace MWSL
{
	class MWSL_API SLStream	// Read From Begin, Write at End
	{
	public:

		SLStream();
		SLStream(char* src, Int len);
		~SLStream();

		void resize(Int moreSize)
		{
			if ((getSize() + moreSize) > capability)
			{
				reallocate(getSize() + moreSize);
			}
		}

		void write(const Byte &b)
		{
			resize(sizeof(Byte));
			*(end_++) = b;
		}

		void read(Byte &b) { b = *(begin++); }

		void write(const Int &i)
		{
			resize( sizeof(Int) );
			Int *des = reinterpret_cast<Int*>(end_);
			*(des++) = i;
			end_ = reinterpret_cast<Byte*>(des);
		}

		void read(Int &i)
		{
			Int *src = reinterpret_cast<Int*>(begin);
			i = *(src++);
			begin = reinterpret_cast<Byte*>(src);
		}

		void write(const String &s)
		{
			write( Int(s.size()) );

			resize( (Int)(sizeof(char) * s.size()) );
			for( size_t i=0; i<s.size(); i++ )
			{
				*(end_++) = s[i];
			}
		}

		void read(String &s)
		{
			int sz = 0;
			read( sz );
			s.resize( sz );
			for( int i=0; i<sz; i++ )
			{
				read( s[i] );
			}
		}

		void write(const Char &c)
		{
			write( reinterpret_cast<const Byte&>(c) );
		}

		void read(Char &c)
		{
			read( reinterpret_cast<Byte&>(c) );
		}

		void write(const Double &d)
		{
			resize( sizeof( Double ) );
			Double *des = reinterpret_cast<Double*>(end_);
			*(des++) = d;
			end_ = reinterpret_cast<Byte*>(des);
		}

		void read(Double &d)
		{
			Double *src = reinterpret_cast<Double*>(begin);
			d = *(src++);
			begin = reinterpret_cast<Byte*>(src);
		}

		void write(const Float &f)
		{
			resize( sizeof( Float ) );
			Float *des = reinterpret_cast<Float*>(end_);
			*(des++) = f;
			end_ = reinterpret_cast<Byte*>(des);
		}

		void read(Float &f)
		{
			Float *src = reinterpret_cast<Float*>(begin);
			f = *(src++);
			begin = reinterpret_cast<Byte*>(src);
		}

		void write(const Bool &b)
		{
			resize(sizeof( Bool));
			Bool *des = reinterpret_cast<Bool*>(end_);
			*(des++) = b;
			end_ = reinterpret_cast<Byte*>(des);
		}

		void read(Bool &b)
		{
			Bool *src = reinterpret_cast<Bool*>(begin);
			b = *(src++);
			begin = reinterpret_cast<Byte*>(src);
		}

		void write(const UInt &ui)
		{
			resize( sizeof(UInt) );
			UInt *des = reinterpret_cast<UInt*>(end_);
			*(des++) = ui;
			end_ = reinterpret_cast<Byte*>(des);
		}

		void read(UInt &ui)
		{
			UInt *src = reinterpret_cast<UInt*>(begin);
			ui = *(src++);
			begin = reinterpret_cast<Byte*>(src);
		}

		// FOR X64

		// Only Encode all 64bit as 32bit
		void write(const Int64 &v)
		{
			write((const Int&)v);
		}

		void read(Int64 &v)
		{
			int v_shadow;
			read( v_shadow );
			v = v_shadow;
		}

		void write(const Identity &i)
		{
			write(i.category);
			write(i.name);
		}

		void read(Identity &i)
		{
			read(i.category);
			read(i.name);
		}

		// Portal Object
		void write(const ProxyClass &pc)
		{
			pc.Encode( *this );
		}
		void read(ProxyClass &pc)
		{
			pc.Decode( *this );
		}

		// Collection Support
		template <typename T>
		void write(const std::vector<T> &v)
		{
			this->write( (Int)(v.size()) );
			for( typename std::vector<T>::const_iterator i = v.begin();  i!=v.end(); ++i )
			{
				this->write( *i );
			}
		}

		template <typename T>
		void read(std::vector<T> &v)
		{
			Int length;
			this->read( length );
			v.reserve( length );
			for( int i=0; i<length; ++i )
			{
				T inst;
				this->read(inst);
				v.push_back(inst);
			}
		}

		template <typename TK, typename TV>
		void write(const std::map<TK, TV> &v)
		{
			this->write( (Int)(v.size()) );
			for( typename std::map<TK, TV>::const_iterator i=v.begin(); i!=v.end(); ++i )
			{
				this->write( i->first );
				this->write( i->second );
			}
		}

		template <typename TK, typename TV>
		void read(std::map<TK, TV> &v)
		{
			Int length;
			this->read( length );

			for( int i=0; i<length; ++i )
			{
				TK key;
				this->read( key );
				TV value;
				this->read( value );
				v[ key ] = value;
			}
		}

		void writeAt(const Int &v, Byte *addr)
		{
			Int *des = reinterpret_cast<Int*>(addr);
			*(des) = v;
		}

		friend SLStream& operator <<(SLStream&, const Int64 &);
		friend SLStream& operator >>(SLStream&, Int64 &);

		friend SLStream& operator <<(SLStream&, const ProxyClass&);
		friend SLStream& operator >>(SLStream&, ProxyClass&);

		friend SLStream& operator <<(SLStream&, const Int&);
		friend SLStream& operator >>(SLStream&, Int&);
		friend SLStream& operator <<(SLStream&, const Byte&);
		friend SLStream& operator >>(SLStream&, Byte&);
		friend SLStream& operator <<(SLStream&, const Char&);
		friend SLStream& operator >>(SLStream&, Char& );
		friend SLStream& operator <<(SLStream&, const String&);
		friend SLStream& operator >>(SLStream&, String&);
		friend SLStream& operator <<(SLStream&, const Float&);
		friend SLStream& operator >>(SLStream&, Float&) ;
		friend SLStream& operator <<(SLStream&, const Double&);
		friend SLStream& operator >>(SLStream&, Double&);
		friend SLStream& operator <<(SLStream&, const Bool&);
		friend SLStream& operator >>(SLStream&, Bool&);
		friend SLStream& operator <<(SLStream&, const UInt&);
		friend SLStream& operator >>(SLStream&, UInt&);

		template <typename T>
		friend SLStream& operator <<(SLStream& left, const std::vector<T> &v);
		template <typename T>
		friend SLStream& operator >>(SLStream& left, std::vector<T> & v);
		template <typename TK, typename TV>
		friend SLStream& operator <<(SLStream& left, const std::map<TK, TV> &v);
		template <typename TK, typename TV>
		friend SLStream& operator >>(SLStream& left, std::map<TK, TV> &v);

		Int getSize() const { return (Int)(end_ - buffer); }
		Byte* getBuffer() const { return buffer; }

		Byte* getEnd() const { return end_; }

		void setEnd(Byte *newEnd) { this->end_ = newEnd; }
		void reallocate(Int);

		void append(const char*, Int);
		void reset();

	private:

		Byte *buffer, *begin, *end_;
		Int capability;
	};

	template <typename T>
	SLStream& operator <<(SLStream& left, const std::vector<T> &v)
	{
		left.write( v );
		return left;
	}

	template <typename T>
	SLStream& operator >>(SLStream& left, std::vector<T> &v)
	{
		left.read( v );
		return left;
	}

	template <typename TK, typename TV>
	SLStream& operator <<(SLStream& left, const std::map<TK, TV> &v)
	{
		left.write( v );
		return left;
	}

	template <typename TK, typename TV>
	SLStream& operator >>(SLStream& left, std::vector<TK, TV> &v)
	{
		left.read( v );
		return left;
	}
}

#endif
