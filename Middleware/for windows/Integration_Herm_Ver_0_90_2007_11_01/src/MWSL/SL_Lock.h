/************************************************************************
* @ Filename : SL_Lock.h
* @ Author : BumHyeon Baek (bhbaek@control.kangwon.ac.kr)               
* @ Version : 1.1
* @ Description

* @ NOTE
/************************************************************************/

#pragma once

#include "../../Include/boost/thread.hpp"

namespace MWSL
{
	typedef boost::mutex SL_Mutex;
	typedef boost::mutex::scoped_lock SL_Lock;
	typedef boost::condition SL_Condition;
}
