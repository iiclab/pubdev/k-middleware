/************************************************************************
* @ Filename : SL_ServantPrx.cpp
* @ Author : BumHyeon Baek (bhbaek@control.kangwon.ac.kr)               
* @ Version : 1.1
* @ Description

* @ NOTE
/************************************************************************/

#include "SL_ServantProxy.h"

using namespace MWSL;

ServantPrx::ServantPrx(LogicalConnection conn, const Identity& id) : __conn(conn.Get()), __id(id)
{

}

ServantPrx::~ServantPrx()
{

}
