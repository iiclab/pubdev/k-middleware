/************************************************************************
* @ Filename : SL_ConfigManager.h                                                   
* @ Author : BumHyeon Baek (bhbaek@control.kangwon.ac.kr)               
* @ Version : 1.1
* @ Description

* @ NOTE
/************************************************************************/

#ifndef SL_CONFIGMANAGER_H
#define SL_CONFIGMANAGER_H

#pragma once

#include "SL_Config.h"

namespace MWSL
{
	class MWSL_API ConfigManager
	{
	public:

		static Config& Configure()
		{	
			static Config config;
			return config;
		}
	};
}

#endif

