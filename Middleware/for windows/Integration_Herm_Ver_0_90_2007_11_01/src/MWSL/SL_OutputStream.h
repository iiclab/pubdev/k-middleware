/************************************************************************
* @ Filename : SL_OutputStream.h                                                   
* @ Author : BumHyeon Baek (bhbaek@control.kangwon.ac.kr)               
* @ Version : 1.1
* @ Description

* @ NOTE
/************************************************************************/

#ifndef SL_OUTPUTSTREAM_H
#define SL_OUTPUTSTREAM_H

#pragma once
#include "SL_Define.h"
#include "SL_Stream.h"
#include "SL_Servant.h"
#include "SL_Lock.h"

namespace MWSL
{
	class SLCommunicator; 

	class MWSL_API MWSLOutput
	{
	public:

		MWSLOutput(SLCommunicator*, const Identity&, String, RequestType);
		virtual ~MWSLOutput();

		virtual bool Invoke();		
		virtual void OnReply();

		SLStream* getOS() { return &os; }
		SLStream* getIS() { return is; }

		Int getRequestId() { return reqId; }

		void Finish();		
		void WakeUpReply();
		void setIncomingStream(SLStream*);

	protected:

		void startWriteParam();
		void finishWriteParam();
		SLCommunicator *conn;
		RequestType mode;
		SLStream *is;
		SLStream os;

	private:

		void WaitForReply();

		Identity servantId;
		Int paramSizeBegin;
		Int reqId;
		SL_Mutex waitMutex;
		SL_Condition replyCondition;
		bool isReplied;
	};
}

#endif
