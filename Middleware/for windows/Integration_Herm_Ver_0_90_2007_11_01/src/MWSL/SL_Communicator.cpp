/************************************************************************
* @ Filename : SL_Communicator.cpp                                                   
* @ Author : BumHyeon Baek (bhbaek@control.kangwon.ac.kr)               
* @ Version : 1.1
* @ Description

* @ NOTE
/************************************************************************/

#include "SL_Communicator.h"
#include "SL_InputStream.h"
#include "SL_OutputStream.h"
#include "SL_Exception.h"

#include <iostream>
#include <malloc.h>

using namespace std;
using namespace MWSL;

class ServerDiscovery : public EventHandler
{
public:

	void handleEvent()
	{	
		MW_DEBUG((LM_DEBUG,"[SL] This module starts to discovery!!\n"));
		//request to discovery a server	
		//pSLComm_->requestFindServer4Obj();

		if ( !(pSLComm_->isServerDiscovered()) )
		{
			pSLComm_->setState(SL_INACTIVE);
			pSLComm_->ServerTimerRequest(_SERVER_DISCOVERY_PERIOD_);
		}
	}

	ServerDiscovery(SLCommunicator *pSLComm): pSLComm_(pSLComm)
	{
	}

private:
	SLCommunicator *pSLComm_;
};

class SLDataConfirm : public OutputCallbackHandler 
{
public:    
	SLDataConfirm(SLCommunicator *pSL): pLayer_(pSL)
	{
	}
	virtual ~SLDataConfirm(){}

	void handleOutput(SentStatus_t *pSentStatus)
	{
		//MW_DEBUG((LM_DEBUG,"SLDataConfirm::handleOutput status: %04x BytesTransfered %d\n", pSentStatus->status_, pSentStatus->MsgTransfered_.len_));
		pLayer_->dataConfirm(pSentStatus);
	}	
private:
	SLCommunicator *pLayer_;
};

class SLDataIndication : public InputCallbackHandler 
{
public:    
	SLDataIndication(SLCommunicator *pSL): pLayer_(pSL)
	{
	}
	virtual ~SLDataIndication(){}

	void handleInput(void *arg, Msg_t *msg)
	{	
		pLayer_->dataIndication(arg, msg);	
	}
private:
	SLCommunicator *pLayer_;
};

//** Static Member variables*/
SLCommunicator* 
SLCommunicator::pInstance_ = NULL;

SLCommunicator* 
SLCommunicator::instance(NIL *p2ndLowerLayer, NAL *pLowerLayer)
{
	if(pInstance_ == NULL)
		pInstance_ = new SLCommunicator(p2ndLowerLayer, pLowerLayer);
	return pInstance_;
}

SLCommunicator::SLCommunicator(NIL *p2ndLowerLayer, NAL *pLowerLayer) 
: reqIdGen(0), _adapter(NULL), pLowerLayer_(pLowerLayer), p2ndLowerLayer_(p2ndLowerLayer)
{
	pSLDataIndicationCB_ = new SLDataIndication(this);
	pSLDataConfirmCB_ = new SLDataConfirm(this);

	pUpperLayerDataConfirm_ = NULL;
	pUpperLayerDataIndication_ = NULL;

	serverFound = false;
	pServerCh_ = NULL;

	//usedport_ = 0;

	//Init Timer
	pTimerManager_ = TimerManager::instance(_DEFAULT_TIMER_KEY_);

	serivce_layer_communicator_state_ = SL_INACTIVE;
}

SLCommunicator::~SLCommunicator()
{
	OSAL_DELETE(pSLDataIndicationCB_);
	OSAL_DELETE(pSLDataConfirmCB_);
}

void 
SLCommunicator::initialize(ObjectAdapter* pOA, Byte retMwAddr, void* configure)
{
	int status;
	DEBUGTRACE("ServiceLayer::initialize");

	pConAddr = (configureAddr*)configure;
	if (pConAddr->_THIS_IS_SERVER_)
		serverFound = TRUE;

	_adapter = pOA;
	_REMOTE_MOUDLE_ADDR_ = retMwAddr;

	//Set Callback (for NAL)
	//pLowerLayer_->setDataConfirmCB(pSLDataConfirmCB_);
	//pLowerLayer_->setDataIndicationCB(pSLDataIndicationCB_);

	//Set Callback (for NIL)
	p2ndLowerLayer_->setDataConfirmCB(pSLDataConfirmCB_);
	p2ndLowerLayer_->setDataIndicationCB(pSLDataIndicationCB_);

	//start to discovery a server
	//if (THIS_IS_CLIENT)
	//	this->ServerTimerRequest(5000);
}

void 
SLCommunicator::OnMessage(void *arg, Msg_t* msg)
{
	char msgHeader[ MWSL::MWSL_Header_Size ];
	//SLStream is;
	is.reset();
	bool isFailed = false;

	//copy header of message to stream class
	memcpy(msgHeader, msg->pData_, MWSL_Header_Size);
	is.append( msgHeader, MWSL_Header_Size );
	msg->pData_ += MWSL_Header_Size;

	Int totalSize = 0;
	MessageType type = CLOSE;
	if(CheckHeader(is, totalSize, type))
	{
		//Int bodySize = totalSize - MWSL_Header_Size;
		Int bodySize = msg->len_ - MWSL_Header_Size;
		is.resize(bodySize);

		// copy body of message to stream class        
		memcpy((char*)(is.getEnd()), msg->pData_ ,bodySize);			

		is.setEnd(is.getEnd() + bodySize);

		// Begin parsing
		assert(is.getSize() == msg->len_);
#ifdef SLDEBUG
		cout << "MWSLInput : ";
		for(int i=0 ; i<14 ; i++)
			cout << msgHeader[i];
		cout << "\n" << totalSize;
#endif
		ParseMessage( arg, &is, type );  
	}		
}

void
SLCommunicator::GenerateHeader(SLStream &stream, MessageType type)
{
	assert(stream.getSize() == 0);

	stream.write(MWSL::MWSLMagic[0]);
	stream.write(MWSL::MWSLMagic[1]);
	stream.write(MWSL::MWSLMagic[2]);
	stream.write(MWSL::MWSLMagic[3]);
	stream.write(MWSL::MWSLMagic[4]);

	stream.write(MWSL::protocolMajor);
	stream.write(MWSL::protocolMinor);
	stream.write(MWSL::encodingMajor);
	stream.write(MWSL::encodingMinor);

	stream.write((Byte)type);

	stream.write((Int)0);
}	

bool
SLCommunicator::CheckHeader(SLStream &stream, Int &totalSize, MessageType &type)
{
	if( stream.getSize() < MWSL::MWSL_Header_Size )
		return false;

	// Magic Number
	char magicNum[6];
	stream.read(magicNum[0]);
	stream.read(magicNum[1]);
	stream.read(magicNum[2]);
	stream.read(magicNum[3]);
	stream.read(magicNum[4]);
	magicNum[5] = '\0';

	//Protocal
	Byte msgProtocalMajor, msgProtocalMinor, msgEncodingMajor, msgEncodingMinor;
	stream.read(msgProtocalMajor);
	stream.read(msgProtocalMinor);
	stream.read(msgEncodingMajor);
	stream.read(msgEncodingMinor);

	//Type
	Byte t;
	stream.read(t);

	//Size
	Int s;
	stream.read(s);

	if(strcmp(magicNum, MWSL::MWSLMagic) != 0)
		return false;   // Magic Number Invalid

	if(!( msgProtocalMajor == protocolMajor 
		&& msgProtocalMinor == protocolMinor
		&& msgEncodingMajor == encodingMajor
		&& msgEncodingMinor == encodingMinor))
		return false;  // Protocal Invalid

	// Validated
	type = (MessageType) t;
	totalSize = s;
	return true;
}

void
SLCommunicator::InsertHeaderSize(SLStream &stream)
{
	assert(stream.getSize() >= MWSL::MWSL_Header_Size);

	Int size = stream.getSize();

	stream.writeAt(size, stream.getBuffer() + 10);
}

#define MaxBuffer 1024
void
SLCommunicator::SendRequest(SLStream &os, int rServerConnectionId)
{
	InsertHeaderSize(os);
	// 	trans->Write((char*)(os.getBuffer()), os.getSize(), 0);

	byte Buf[MaxBuffer];
	int dataLen = os.getSize();

	byte *pData = os.getBuffer();
	memcpy(Buf, pData, dataLen);
	/*
	printf("[SL] output message byte :");
	for (int i = 0 ; i<dataLen ;i++)
	printf("%02x",pData[i]);
	printf("\n");
	*/
	Msg_t outMsg;
	outMsg.len_ = dataLen;
	outMsg.pData_ = Buf;

	DataRequestParam ReqParam;
	ReqParam.msgPriority_ = 0x03;
	ReqParam.nalMsg_ = FALSE;  

	ReqParam.dstAddrMode_ = 0x04;   //direct-send mode
	ReqParam.reqChID_ = rServerConnectionId;        

	//pLowerLayer_->dataRequest(&ReqParam ,&outMsg);
	p2ndLowerLayer_->dataRequest(rServerConnectionId ,&outMsg);

#ifdef SLDEBUG
	cout << "SendRequest : " << os.getBuffer() << endl;
	cout << "Transceiver's buffer : " << trans->buffer << endl;
#endif
}

void
SLCommunicator::SendResponse(int chID, SLStream &os)
{	
	InsertHeaderSize(os);
	// 	trans->Write((char*)(os.getBuffer()), os.getSize(), 0);

	byte Buf[MaxBuffer];
	int dataLen = os.getSize();

	byte *pData = os.getBuffer();
	memcpy(Buf, pData, dataLen);
	//printf("[SL] output message byte : %x\n",pData);

	Msg_t outMsg;
	outMsg.len_ = dataLen;
	outMsg.pData_ = Buf;

	DataRequestParam ReqParam;
	ReqParam.msgPriority_ = 0x03;
	ReqParam.nalMsg_ = FALSE; 

	ReqParam.dstAddrMode_ = 0x04;   //direct-send mode
	ReqParam.reqChID_ = chID;        
	ReqParam.dstAddr_ = _REMOTE_MOUDLE_ADDR_;        

	//pLowerLayer_->dataRequest(&ReqParam ,&outMsg);
	p2ndLowerLayer_->dataRequest(chID ,&outMsg);

#ifdef SLDEBUG
	cout << "SendResponse : " << os.getBuffer() << endl;
#endif	
}

void
SLCommunicator::CloseConnection()
{
	SLStream os;
	GenerateHeader(os, CLOSE);
	InsertHeaderSize(os);

	// 	trans->Write((char*)(os.getBuffer()), os.getSize(), 0);    
	byte Buf[MaxBuffer];
	int dataLen = os.getSize();

	byte *pData = os.getBuffer();
	memcpy(Buf, pData, dataLen);

	Msg_t outMsg;
	outMsg.len_ = dataLen;
	outMsg.pData_ = Buf;

	DataRequestParam ReqParam;
	ReqParam.msgPriority_ = 0x03;
	ReqParam.nalMsg_ = FALSE;

	sct_it pos; 
	for (pos = ServerConnectionTable_.begin(); pos != ServerConnectionTable_.end(); ++pos)
	{
		ReqParam.dstAddrMode_ = 0x04;   //direct-send mode
		//ReqParam.dstAddr_ = _REMOTE_MOUDLE_ADDR_;
		ReqParam.reqChID_ = pos->second;        

		//pLowerLayer_->dataRequest(&ReqParam ,&outMsg);
		p2ndLowerLayer_->dataRequest(pos->second ,&outMsg);
	}
}
void
SLCommunicator::ParseMessage(void* arg, SLStream* stream, MessageType type)
{
	Byte srcModuleAddr;
	int chID;

	NALIncomingData_t *pIncoming = (NALIncomingData_t *)arg;
	chID = pIncoming->srcIOChannelID_;
	srcModuleAddr = pIncoming->srcAddr;

	switch(type)
	{
	case REQUEST_MSG:
		{
			MWSLInput in(this, stream);
			stream = NULL;
			assert(_adapter != NULL);
			in.Invoke(chID, _adapter);
		}
		break;

	case REPLY:
		{					
			SL_Lock lock( requestsMutex );

			Int requestId;
			stream->read(requestId);

			//cout << "[Response] request id: " << requestId << endl;
			assert(requestId != 0);
			std::map<Int, MWSLOutput*>::iterator it = requests.find(requestId);

			assert(it != requests.end());

			MWSLOutput *out= it->second;
			assert(out);

			out->setIncomingStream(stream);
			out->OnReply();

			requests.erase(it);
		}
		break;
	case REQUEST_SERVER_ADDR:
		this->responseServerAddr(chID, srcModuleAddr, stream);
		break;
	case REPLY_SERVER_ADDR:
		this->processReplyServerAddr(srcModuleAddr, stream);
		break;       
	}
}

Int
SLCommunicator::GenerateRequestId()
{
	assert(reqIdGen+1 > 0);
	return ++reqIdGen;
}

void
SLCommunicator::RegisterRequest(MWSLOutput *out)
{
	SL_Lock lock( requestsMutex );   

	Int requestId = out->getRequestId();
	requests[requestId] = out;

//	cout << "[SLCOmmunicator] " << "request id:" << requestId << endl;
}

void
SLCommunicator::setAdapter(ObjectAdapter* adapter)
{
	this->_adapter = adapter;
}

void 
SLCommunicator::setDataIndicationCB(InputCallbackHandler *pDataIndication)
{
	pUpperLayerDataIndication_ = pDataIndication;
}

void 
SLCommunicator::setDataConfirmCB(OutputCallbackHandler *pDataConfirm)
{
	pUpperLayerDataConfirm_ = pDataConfirm;
}


int 
SLCommunicator::dataRequest(void* arg, Msg_t* inMsg)
{
	DEBUGTRACE("SLCommunicator::dataRequest");
	return 0;
}

void
SLCommunicator::dataConfirm(SentStatus_t* pSendStatus)
{
	DEBUGTRACE("SLCommunicator::dataConfirm");
}

void 
SLCommunicator::dataIndication(void *arg, Msg_t* msg)
{
	DEBUGTRACE("SLCommunicator::dataIndication");

	NILDEIncomingData_t *pIncoming = (NILDEIncomingData_t *)arg;
	int chID = pIncoming->channelID_;

	//p2ndLowerLayer_->dataRequest(pIncoming->channelID_ ,msg);
	OnMessage(arg, msg);
}

void
SLCommunicator::requestFindServer4Obj(string rObj)
{
	SLStream os;
	GenerateHeader(os, REQUEST_SERVER_ADDR);
	InsertHeaderSize(os);

	//write object name's length and name in message
	os.write(rObj);

	byte Buf[MaxBuffer];
	int dataLen = os.getSize();

	byte *pData = os.getBuffer();
	memcpy(Buf, pData, dataLen);

	Msg_t outMsg;
	outMsg.len_ = dataLen;
	outMsg.pData_ = Buf;

	DataRequestParam ReqParam;
	ReqParam.dstAddr_ = 0x00; //use default udp channel 
	ReqParam.dstAddrMode_ = 0x00;
	ReqParam.msgPriority_ = 0x03;
	ReqParam.nalMsg_ = FALSE;

	//pLowerLayer_->dataRequest(&ReqParam ,&outMsg);    
	p2ndLowerLayer_->dataRequest(_CONfIGURED_UDP_CHANNEL_,&outMsg);  
}

void
SLCommunicator::responseServerAddr(int chID, Byte srcModuleAddr, SLStream *is)
{
	SLStream os;
	char rObjNameLength;

	GenerateHeader(os, REPLY_SERVER_ADDR);
	InsertHeaderSize(os);
	byte* pBuf = is->getBuffer();

	//bring remote object name's length
	int retIntValue = byte2int(pBuf,14);

	//bring remote object name
	string rObjName(&pBuf[18], &pBuf[18+retIntValue]);

	sct_it pos;
	pos = ServerConnectionTable_.find(rObjName);

	if (pos == ServerConnectionTable_.end())
		return; // do nothing

	//write server address in message
	unsigned int ServerAddr = inet_addr(pConAddr->_IP_ADDR_);
	os.write(ServerAddr);

	//write corresponding object name and length in message
	os.write(rObjName);

	byte Buf[MaxBuffer];
	int dataLen = os.getSize();

	byte *pData = os.getBuffer();
	memcpy(Buf, pData, dataLen);

	Msg_t outMsg;
	outMsg.len_ = dataLen;
	outMsg.pData_ = Buf;

	DataRequestParam ReqParam;
	ReqParam.dstAddr_ = srcModuleAddr;  //broadcast
	ReqParam.dstAddrMode_ = 0x00;
	ReqParam.msgPriority_ = 0x03;
	ReqParam.nalMsg_ = FALSE;

	//pLowerLayer_->dataRequest(&ReqParam ,&outMsg);      
	p2ndLowerLayer_->dataRequest(chID ,&outMsg);
}

void
SLCommunicator::processReplyServerAddr(Byte srcModuleAddr, SLStream *is)
{
	char serverAddr[4];
	byte *pBuf = is->getBuffer();
	
	//bring object information
	int retIntValue = byte2int(pBuf, 18);
	string rObj(&pBuf[22], &pBuf[22+retIntValue]);

	//bring server address
	string rServerAddr(&pBuf[14], &pBuf[18]);

	//bring server information
	pBuf += MWSL_Header_Size;
	memcpy(serverAddr, pBuf, 4);

	sat_it pos = ServerAddressTable_.find(rServerAddr);
	if (pos != ServerAddressTable_.end()) {
		sct_it it = ServerConnectionTable_.find(rObj);
		it->second = pos->second;

		cout << "***************************************************" << endl;
		cout << "* the client is already connected to the server!! *" << endl;
		cout << "***************************************************" << endl;
		return;
	} else
		ServerAddressTable_[rServerAddr] = _INITIAL_SERVER_CONNECTION_ID_;

	//create tcp channel
	int retConnectionID = p2ndLowerLayer_->createChannelRequest(pConAddr->_SERVER_PORT_TCP_NO_, serverAddr);

	//set server address 
	pos = ServerAddressTable_.find(rServerAddr);
	pos->second = retConnectionID;

	//find corresponding object and set
	sct_it pos2 = ServerConnectionTable_.find(rObj);
	if (pos2 != ServerConnectionTable_.end())
		pos2->second = retConnectionID;

	cout << "****************************************" << endl;
	cout << "* the client connected to the server!! *" << endl;
	cout << "****************************************" << endl;
}

void
SLCommunicator::ServerTimerRequest(int period)
{
	if (serivce_layer_communicator_state_ == SL_INACTIVE)
	{
		serivce_layer_communicator_state_ = SL_DISCOVERY;
		ServerDiscovery *serverDiscoveryHandler = new ServerDiscovery(this);
		pTimerManager_->setTimer(period,serverDiscoveryHandler);
	}	
}

bool
SLCommunicator::isServerDiscovered()
{
	return serverFound;
}

SLState_t
SLCommunicator::getState()
{
	return serivce_layer_communicator_state_;
}

void
SLCommunicator::setState(SLState_t curState)
{
	serivce_layer_communicator_state_ = curState;
}

void 
SLCommunicator::registerRemoteObject(string rObj)
{
	ServerConnectionTable_[rObj] = _INITIAL_SERVER_CONNECTION_ID_;
}

void
SLCommunicator::connectRemoteObject()
{
	sct_it pos;
	for (pos = ServerConnectionTable_.begin(); pos != ServerConnectionTable_.end(); ++pos)
	{
		if (pos->second == _INITIAL_SERVER_CONNECTION_ID_)
		{
			//find a server which has the corresponding object 
			requestFindServer4Obj(pos->first);
		}
	}
}

int
SLCommunicator::lookupServer4Obj(string rObjName)
{
	sct_it pos;
	for (pos = ServerConnectionTable_.begin(); pos != ServerConnectionTable_.end(); ++pos)
	{
		if ( (pos->first == rObjName) && (pos->second != _INITIAL_SERVER_CONNECTION_ID_) )
		{
			//find a server which has the corresponding object 
			return pos->second;
			break;
		}
	}
	return _INITIAL_SERVER_CONNECTION_ID_;
}