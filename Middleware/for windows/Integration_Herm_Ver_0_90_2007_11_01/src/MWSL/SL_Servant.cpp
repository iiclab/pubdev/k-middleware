/************************************************************************
* @ Filename : SL_Servant.cpp
* @ Author : BumHyeon Baek (bhbaek@control.kangwon.ac.kr)               
* @ Version : 1.1
* @ Description

* @ NOTE
/************************************************************************/

#include "SL_Servant.h"

using namespace std;
using namespace MWSL;

MWSL::Identity::Identity()
{

}

MWSL::Identity::Identity(const char *_name, const char* _category) : name(_name), category(_category)
{

}

MWSL::Identity::Identity(const String& _name, const String &_category) : name(_name), category(_category)
{

}

bool Identity::operator < ( const Identity &v2 ) const
{
	std::string s1 = category;
	s1.append( name );
	std::string s2 = v2.category; 
	s2.append( v2.name );

	return (s1<s2);
}

MWSL::Servant::Servant()
{

}

MWSL::Servant::Servant(const String &_name, const String &_category) : _id(_name, _category)
{

}


MWSL::Servant::~Servant()
{

}

int
Servant::_Dispatch(MWSLInput &in, const Identity &id, const String &operation)
{
	return 0;
}
