/************************************************************************
* @ Filename : SL_Exception.h                                                   
* @ Author : BumHyeon Baek (bhbaek@control.kangwon.ac.kr)               
* @ Version : 1.1
* @ Description

* @ NOTE
/************************************************************************/

#ifndef SL_EXCEPTION_H
#define SL_EXCEPTION_H

#pragma once
#include "SL_Servant.h"
#include "SL_Stream.h"

#include <iostream>

namespace MWSL
{
	class MWSL_API SLException : public ProxyClass
	{
	public:

		SLException();
		SLException(const char*, int);
		virtual ~SLException();

		virtual std::string ToString();

		virtual void Encode(SLStream &) const;
		virtual void Decode(SLStream &);

		String _file;
		int _line;
		int error;
		String msg;
	};

	class MWSL_API ObjectNotExistException : public SLException
	{
	public:

		ObjectNotExistException(Identity, String);

		virtual std::string ToString();

		Identity objectId;
		String operation;
	};

	class MWSL_API OperationNotExistException : public SLException
	{
	public:

		OperationNotExistException(Identity, String);

		virtual std::string ToString();

		Identity objectId;
		String operation;
	};
	
	class MWSL_API ThreadSyscallException : public SLException
	{
	public:

		ThreadSyscallException(const char*, int, int);
		virtual std::string ToString();
		
		int error() const;	

		int _error;
	};

	class MWSL_API ThreadLockedException : public SLException
	{
	public:

		ThreadLockedException(const char*, int);
		virtual std::string ToString();		
	};

}

#endif
