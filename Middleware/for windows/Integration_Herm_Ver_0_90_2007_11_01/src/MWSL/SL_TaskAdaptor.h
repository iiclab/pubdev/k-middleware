/************************************************************************
* @ Filename : SL_TaskAdaptor.h                                                   
* @ Author : BumHyeon Baek (bhbaek@control.kangwon.ac.kr)               
* @ Version : 1.1
* @ Description

* @ NOTE
/************************************************************************/

#ifndef SL_TASKADAPTOR_H
#define SL_TASKADAPTOR_H

#include <boost/smart_ptr.hpp>

#include "SL_ThreadPool.h"

namespace MWSL
{
	/* Standard task function object. */
	typedef boost::function0<void> thread_func;	

	/* Prioritized task function object. */
	class prio_thread_func
	{
	public:
		/* Constructor.	*/
		explicit prio_thread_func(unsigned int priority, thread_func function) : priority_(priority), function_(function)
		{
		}

		/* Executes the task function. */
		void operator() (void) const
		{
			if(function_)
			{
				function_();
			}
		}
		
		bool operator< (const prio_thread_func& right) const
		{
			return priority_ < right.priority_; 
		}

	private:
		unsigned int priority_;  /* The priority of the task's function. */
		thread_func function_;   /* The task's function. */
	};	
}

#endif