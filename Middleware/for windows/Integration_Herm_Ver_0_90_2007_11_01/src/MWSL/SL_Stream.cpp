/************************************************************************
* @ Filename : SL_Stream.cpp
* @ Author : BumHyeon Baek (bhbaek@control.kangwon.ac.kr)               
* @ Version : 1.1
* @ Description

* @ NOTE
/************************************************************************/

#include "SL_Stream.h"

using namespace MWSL;

SLStream::SLStream() : buffer(NULL), begin(NULL), end_(NULL), capability(2)
{
	reallocate(1);
}

SLStream::SLStream(char *src, MWSL::Int len) : buffer(NULL), begin(NULL), end_(NULL), capability(2)
{
	reallocate(len);
	memcpy(this->buffer, src, len);
	begin = this->buffer;
	end_ = this->buffer + len;
}

SLStream::~SLStream()
{
	if (buffer)
	{
		delete[] buffer;
		buffer = NULL;
	}
}

void
SLStream::reallocate(Int newSize)
{
	Int beginPos = (Int)(begin - buffer), endPos = (Int)( end_ - buffer );
	Int newCapability = capability * 2;

	while (newCapability < newSize)
		newCapability *= 2;

	Byte* newBuffer = new Byte[newCapability];
	if (newBuffer)
	{
		memcpy(newBuffer, buffer, getSize());
		delete[] buffer;
		buffer = newBuffer;
		capability = newCapability;
		begin = buffer + beginPos;
		end_ = buffer + endPos;
	}
}

void
SLStream::reset()
{
	delete[] buffer;
	buffer = new Byte[4];
	capability = 4;

	begin = buffer;
	end_ = buffer;
}

void
SLStream::append(const char* src, Int size)
{
	resize(size);
	memcpy(end_, src, size);
	end_ += size;
}

namespace MWSL
{
	SLStream& operator <<(SLStream& left, const Int &i)
	{
		left.write(i);
		return left;
	}

	SLStream& operator >>(SLStream& left, Int &i)
	{
		left.read(i);
		return left;
	}
	SLStream& operator <<(SLStream& left, const Byte &b) 
	{
		left.write(b);
		return left;
	}
	SLStream& operator >>(SLStream& left, Byte &b)
	{
		left.read(b);
		return left;
	}
	SLStream& operator <<(SLStream& left, const Char &c) 
	{
		left.write(c);
		return left;
	}
	SLStream& operator >>(SLStream& left, Char &c)
	{
		left.read(c);
		return left;
	}
	SLStream& operator <<(SLStream& left, const String &s) 
	{
		left.write(s);
		return left;
	}
	SLStream& operator >>(SLStream& left, String &s)
	{
		left.read(s);
		return left;
	}
	SLStream& operator <<(SLStream& left, const Float &f) 
	{
		left.write(f);
		return left;
	}
	SLStream& operator >>(SLStream& left, Float &f)
	{
		left.read(f);
		return left;
	}
	SLStream& operator <<(SLStream& left, const Double &d) 
	{
		left.write(d);
		return left;
	}
	SLStream& operator >>(SLStream& left, Double &d)
	{
		left.read(d);
		return left;
	}
	SLStream& operator <<(SLStream& left, const Bool &b) 
	{
		left.write(b);
		return left;
	}
	SLStream& operator >>(SLStream& left, Bool &b)
	{
		left.read(b);
		return left;
	}
	SLStream& operator <<(SLStream& left, const UInt &ui) 
	{
		left.write(ui);
		return left;
	}
	SLStream& operator >>(SLStream& left, UInt &ui)
	{
		left.read(ui);
		return left;
	}

	SLStream& operator <<(SLStream& left, const Int64 &v) 
	{
		left.write(v);
		return left;
	}
	SLStream& operator >>(SLStream& left, Int64 &v)
	{
		left.read(v);
		return left;
	}

	SLStream& operator <<(SLStream& left, const ProxyClass &pc) 
	{
		left.write(pc);
		return left;
	}
	SLStream& operator >>(SLStream& left, ProxyClass &pc)
	{
		left.read(pc);
		return left;
	}
}

