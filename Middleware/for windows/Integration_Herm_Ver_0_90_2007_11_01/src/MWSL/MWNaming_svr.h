#ifndef __MWNaming_svr.h__
#define __MWNaming_svr.h__

#pragma once

#include "MWSL.h"

using namespace MWSL;

class NamingService: public ::MWSL::Servant
{
public:
	virtual int _Dispatch(::MWSL::MWSLInput &in, const ::MWSL::Identity &id, const ::MWSL::String &operation)
	{
		if (operation == "Register")
		{
			::MWSL::SLStream* _is = in.getIS();
			::MWSL::SLStream* _os = in.getOS();
			in.readParamHead();
			::MWSL::Identity _id;
			_is->read(_id);
			::MWSL::SLEndpoint _ep;
			_is->read(_ep);
			Register(_id,_ep);
			_os->write((Byte)0);/* Reply state success */
			return 1;
		}
		if (operation == "Remove")
		{
			::MWSL::SLStream* _is = in.getIS();
			::MWSL::SLStream* _os = in.getOS();
			in.readParamHead();
			::MWSL::Identity _id;
			_is->read(_id);
			Remove(_id);
			_os->write((Byte)0);/* Reply state success */
			return 1;
		}
		if (operation == "Lookup")
		{
			::MWSL::SLStream* _is = in.getIS();
			::MWSL::SLStream* _os = in.getOS();
			in.readParamHead();
			::MWSL::Identity _id;
			_is->read(_id);
			::MWSL::SLEndpoint retv = Lookup(_id);
			_os->write((Byte)0);/* Reply state success */
			_os->write(retv);
			return 1;
		}
		return 0;//failed
	}
	virtual void Register(::MWSL::Identity id,::MWSL::SLEndpoint ep) = 0;
	virtual void Remove(::MWSL::Identity id) = 0;
	virtual ::MWSL::SLEndpoint Lookup(::MWSL::Identity id) = 0;
};

#endif /* End of File */
