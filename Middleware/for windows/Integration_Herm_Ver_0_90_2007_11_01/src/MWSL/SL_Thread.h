/************************************************************************
* @ Filename : SL_Thread.h                                                   
* @ Author : BumHyeon Baek (bhbaek@control.kangwon.ac.kr)               
* @ Version : 1.1
* @ Description

* @ NOTE
/************************************************************************/

#ifndef SL_THREAD_H
#define SL_THREAD_H

#pragma once

#include "SL_Define.h"
#include "../../Include/boost/thread.hpp"
#include "SL_Lock.h"

namespace MWSL
{
	class SLThread
	{
	public:
		SLThread(void);	
		~SLThread(void);

		void Start();
		virtual void Run() = 0;
		void Join();
		void YieldThread();
		void Sleep( int nsec );

	private:
		boost::thread *pThread;
		SL_Mutex mutexThread;	
	};

}

#endif
