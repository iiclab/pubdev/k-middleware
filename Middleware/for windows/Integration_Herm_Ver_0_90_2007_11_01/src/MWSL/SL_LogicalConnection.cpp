/************************************************************************
* @ Filename : SL_LogicalConnection.cpp                                                   
* @ Author : BumHyeon Baek (bhbaek@control.kangwon.ac.kr)               
* @ Version : 1.1
* @ Description

* @ NOTE
/************************************************************************/

#include "SL_LogicalConnection.h"

using namespace MWSL;

LogicalConnection::LogicalConnection() : conn(NULL)
{
}

LogicalConnection::LogicalConnection(SLCommunicator* _conn) : conn(_conn)
{

}

LogicalConnection::~LogicalConnection()
{

}

void
LogicalConnection::CloseConnection()
{
	assert(conn);
	conn->CloseConnection();
}

