#ifndef SL_TYPE_H
#define SL_TYPE_H

namespace MWSL
{
	class Instance;
	typedef Instance* InstancePtr;

	class ServiceLayer;
	typedef ServiceLayer* ServiceLayerPtr;

	class ObjectAdapter;
	typedef ObjectAdapter* ObjectAdapterPtr;

	class ObjectAdapterFactory;
	typedef ObjectAdapterFactory* ObjectAdapterFactoryPtr;

	class Servant;
	typedef Servant* ServantPtr;

	class ServantManager;
	typedef ServantManager* ServantManagerPtr;

	class SLCommunicator;
	typedef SLCommunicator* SLCommunicatorPtr;

	class SLStream;
	typedef SLStream* SLStreamPtr;

	class Thread;
	typedef Thread* ThreadPtr;
}

#endif