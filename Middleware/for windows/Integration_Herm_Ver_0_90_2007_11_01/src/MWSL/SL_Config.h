/************************************************************************
* @ Filename : SL_Config.h                                                   
* @ Author : BumHyeon Baek (bhbaek@control.kangwon.ac.kr)               
* @ Version : 1.1
* @ Description

* @ NOTE
/************************************************************************/

#ifndef SL_CONFIG_H
#define SL_CONFIG_H

#pragma once
#include "SL_Logger.h"
#include "SL_Define.h"
#include "SL_Endpoint.h"
#include <map>

using namespace std;

namespace MWSL
{
	class Config
	{
	public:

		Config();
		~Config();
		
		Logger& getLogger() { return *pLog; }
		void setLogFile(const char*);

		string getContext(string);
		void setContext(string, string);

		SLEndpoint GetNamingServiceEndpoint()
		{
			return namingServiceEndpoint;
		}

		void SetNamingServiceEndpoint(const SLEndpoint &val)
		{
			namingServiceEndpoint = val;
		}

	private:
		
		SLEndpoint namingServiceEndpoint;
		Logger *pLog;
		map<string, string> context;
	};
}

#endif

