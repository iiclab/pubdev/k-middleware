/************************************************************************
* @ Filename : SL_Exception.cpp                                                
* @ Author : BumHyeon Baek (bhbaek@control.kangwon.ac.kr)               
* @ Version : 1.1
* @ Description

* @ NOTE
/************************************************************************/

#include "SL_Exception.h"
#include <sstream>

#ifdef _WIN32
    #include <Windows.h>
#endif

using namespace MWSL;

SLException::SLException() : _file(""), _line(0), error(0), msg("")
{

}

SLException::SLException(const char *file, int line) : _file(file), _line(line), error(0), msg("")
{

}

SLException::~SLException()
{

}

std::string
SLException::ToString()
{
	std::stringstream ss;

	ss << msg;
	
	if (_file != "")
		ss << " FILE : " << _file;
	if (_line != 0)
		ss << ", LINE : " << _line;
	ss << ";" ;
	return ss.str();
}

void
SLException::Encode(SLStream &stream) const
{
	stream.write(_file);
	stream.write(_line);
	stream.write(error);
	stream.write(msg);
}

void
SLException::Decode(SLStream &stream)
{
	stream.read(_file);
	stream.read(_line);
	stream.read(error);
	stream.read(msg);
}

ObjectNotExistException::ObjectNotExistException(Identity id, String op) : objectId(id), operation(op)
{
	msg = "Object Not Exist!";
}

std::string
ObjectNotExistException::ToString()
{
	std::stringstream ss;
	ss << msg << ",NAME : " << objectId.name << ",OPERATION : " <<operation<< ";" ;
	return ss.str();
}

OperationNotExistException::OperationNotExistException(Identity id, String op) : objectId(id), operation(op)
{
	msg = "Operation Not Exist!";
}

std::string
OperationNotExistException::ToString()
{
	std::stringstream ss;
	ss << msg << ",NAME : " << objectId.name << ",OPERATION : " <<operation << ";" ;
	return ss.str();
}

ThreadSyscallException::ThreadSyscallException(const char* file, int line, int err) : SLException(file, line), _error(err)
{	
	msg = "Thread Syscall Exception!";
}

std::string
ThreadSyscallException::ToString()
{
	std::string out = SLException::ToString();
	if (_error != 0)
	{
		out += ":\nthread syscall exception: ";
#ifdef _WIN32
		LPVOID lpMsgBuf = 0;
		DWORD ok = FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER |
			FORMAT_MESSAGE_FROM_SYSTEM |
			FORMAT_MESSAGE_IGNORE_INSERTS,
			NULL,
			_error,
			MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), 
			(LPTSTR)&lpMsgBuf,
			0,
			NULL);

		if (ok)
		{
			LPCTSTR msg = (LPCTSTR)lpMsgBuf;
			assert(msg && strlen((char*)msg) > 0);
			out += reinterpret_cast<const char*>(msg);
			LocalFree(lpMsgBuf);
		}
		else
		{
			out += "unknown thread error: ";			
		}
#endif
	}
	return out;
}

ThreadLockedException::ThreadLockedException(const char* file, int line) : SLException(file, line)
{
	msg = "Thread is already locked!";
}

std::string
ThreadLockedException::ToString()
{
	std::stringstream ss;
	ss << msg << ";" ;
	return ss.str();
}
