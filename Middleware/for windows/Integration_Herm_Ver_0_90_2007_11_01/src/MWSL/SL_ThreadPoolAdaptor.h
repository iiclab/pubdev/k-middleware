/************************************************************************
* @ Filename : SL_TheadPoolAdaptor.h                                                   
* @ Author : BumHyeon Baek (bhbaek@control.kangwon.ac.kr)               
* @ Version : 1.1
* @ Description

* @ NOTE
/************************************************************************/

#ifndef SL_THREADPOOLADAPTOR_H
#define SL_THREADPOOLADAPTOR_H

#include <boost/smart_ptr.hpp>
#include "SL_ThreadPool.h"
#include "SL_Scheduler.h"
#include "SL_TaskAdaptor.h"

namespace MWSL
{
	/* Pool smart pointer. */ 
	template <class Pool = ThreadPool<thread_func, Fifo_Scheduler<thread_func>>>
	class pool_smartPtr
	{
	public:
		typedef Pool pool_type;	

		/* Constructs a new thread pool. */
		pool_smartPtr(size_t size = 1)
		{
			m_pool = pool_type::createThreadPool(size);
		}

		/* Uses an existing thread pool. */
		pool_smartPtr(boost::shared_ptr<pool_type> pool) : m_pool(pool)
		{
		}

		/* Destructor. */
		~pool_smartPtr()
		{
			m_pool->resize(0);		
			m_pool->join();			
		}

		/* Returns a reference to the stored pool.	*/
		pool_type& operator*() const
		{
			return *m_pool.get();
		}

		/* Returns stored pool pointer. */
		pool_type* operator->() const
		{
			return m_pool.get();
		}

	private:	
		boost::shared_ptr<pool_type> m_pool;		
	};
}

#endif