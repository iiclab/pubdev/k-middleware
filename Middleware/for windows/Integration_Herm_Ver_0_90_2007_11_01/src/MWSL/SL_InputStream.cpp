/************************************************************************
* @ Filename : SL_InputStream.cpp
* @ Author : BumHyeon Baek (bhbaek@control.kangwon.ac.kr)               
* @ Version : 1.1
* @ Description

* @ NOTE
/************************************************************************/

#include "SL_InputStream.h"
#include "SL_Exception.h"
#include "Debug.h"

using namespace MWSL;

MWSLInput::MWSLInput(SLCommunicator *connection, SLStream *incomingStream) : conn(connection), os(NULL), is(incomingStream), reqId(0)
{
	os = new SLStream();
}

MWSLInput::~MWSLInput()
{
	if (os)
	{
		delete os;
		os = NULL;
	}
}

void
MWSLInput::Invoke(int chID, ObjectAdapter *adapter)
{
	bool response = true;	// need response?

	is->read(reqId);

	Identity id;
	is->read(id.name);
	is->read(id.category);

	String operation;
	is->read(operation);	

	Byte opMode;
	is->read(opMode);

	//TEST CONSOLE:
	MW_DEBUG((LM_DEBUG,"[SL] Request Message ID: %d\n", reqId));
	MW_DEBUG((LM_DEBUG,"[SL] Request Channel ID: %d\n", chID));

	if (response)
	{
		conn->GenerateHeader(*os, REPLY);
		assert(reqId != 0);
		os->write(reqId);
	}

	Byte replyState;
	Servant *object = adapter->findServant(id);
	int dispatchRet = 0;	// Success = 1, Fail = 0
	if (object)
	{		
		try 
		{		
			dispatchRet = object->_Dispatch(*this, id, operation);
			if (dispatchRet == 1)
				replyState = 0;
			else
			{
				replyState = 3;
				os->write(replyState);
				os->write(id.name);
				os->write( id.category );
				os->write(operation);
			}	
		}
		catch(SLException &ex)
		{
			replyState = 1;
			os->write(replyState);
			os->write(ex);
		}
	}
	else
	{
		replyState = 2;
		os->write(replyState);
		os->write(id.name);
		os->write( id.category );
		os->write(operation);
	}

	if (response)
	{
		finishWriteParam();
		conn->SendResponse(chID, *os);
	}	
	/*else
	{
		conn->SendNoResponse( );
	}*/
}

void
MWSLInput::finishWriteParam()
{

}

void
MWSLInput::readParamHead()
{
	Int paramSize;
	Byte encodingMajor, encodingMinor;
	is->read(paramSize);
	is->read((Byte&)(encodingMajor));
	is->read((Byte&)(encodingMinor));
}
