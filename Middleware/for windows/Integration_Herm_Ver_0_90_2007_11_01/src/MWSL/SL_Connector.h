/************************************************************************
* @ Filename : SL_Connector.h                                                   
* @ Author : BumHyeon Baek (bhbaek@control.kangwon.ac.kr)               
* @ Version : 1.1
* @ Description

* @ NOTE
/************************************************************************/

#ifndef SL_CONNECTOR_H
#define SL_CONNECTOR_H

#pragma once
#include "SL_Define.h"
#include "SL_Communicator.h"

namespace MWSL
{
	class SLConnector 
	{
	public:

		SLConnector(Byte mID);
		virtual ~SLConnector();

		virtual	SLCommunicator* Connect(int timeout);

	};
}

#endif
