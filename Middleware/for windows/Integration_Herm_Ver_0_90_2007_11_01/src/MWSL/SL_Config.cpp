/************************************************************************
* @ Filename : SL_Config.cpp                                                   
* @ Author : BumHyeon Baek (bhbaek@control.kangwon.ac.kr)               
* @ Version : 1.1
* @ Description

* @ NOTE
/************************************************************************/

#include "SL_Config.h"

using namespace MWSL;

Config::Config()
{
	pLog = new Logger();
}

Config::~Config()
{
	if (pLog)
	{
		delete pLog;
		pLog = NULL;
	}
}

string
Config::getContext(string key)
{
	map<string, string>::iterator it = context.find(key);
	if (it != context.end())
	{
		return it->second;
	}
	else
		return string("");
}

void
Config::setContext(string key, string value)
{
	context[key] = value;
}

void
Config::setLogFile(const char* fileName)
{
	if (pLog)
	{
		delete pLog;
		pLog = NULL;
	}
	assert(pLog == NULL);
	pLog = new Logger(fileName);
}
