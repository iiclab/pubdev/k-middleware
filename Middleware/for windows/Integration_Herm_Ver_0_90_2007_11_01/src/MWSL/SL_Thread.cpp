/************************************************************************
* @ Filename : SL_Thread.cpp                                                   
* @ Author : BumHyeon Baek (bhbaek@control.kangwon.ac.kr)               
* @ Version : 1.1
* @ Description

* @ NOTE
/************************************************************************/

#include "SL_Thread.h"
#include "SL_Exception.h"
#include "../Include/boost/bind.hpp"

using namespace MWSL;

SLThread::SLThread(void) : pThread(NULL), mutexThread()
{
}

SLThread::~SLThread(void)
{
	if( pThread )
	{
		delete pThread;
		pThread = NULL;
	}
}

void SLThread::Start()
{
	SL_Lock lock( mutexThread );   

	if( pThread == NULL )
	{		
		pThread = new boost::thread( boost::bind( &SLThread::Run, boost::ref(*this) ) );
	}
}

void SLThread::Join()
{
	if( pThread )
	{
		pThread->join();
	}
}

void SLThread::Sleep( int nsec )
{
	if( pThread )
	{
		boost::xtime xt;
		xt.nsec = nsec;
		pThread->sleep( xt ) ;
	}	
}
void SLThread::YieldThread()
{
	if( pThread )
	{
		pThread->yield();
	}
}
