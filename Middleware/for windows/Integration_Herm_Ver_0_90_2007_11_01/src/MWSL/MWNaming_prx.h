#ifndef __MWNaming_prx_h__
#define __MWNaming_prx_h__

#pragma once

#include "MWSL.h"

using namespace MWSL;

class NamingServiceProx : public ServantPrx
{
private:
	NamingServiceProx(LogicalConnection conn, const Identity id) : ServantPrx(conn, id)
	{
	}
public:
	static NamingServiceProx* CreateInstance(LogicalConnection conn , const char* name, const char* category)
	{
		return new NamingServiceProx(conn, Identity(MWSL::String(name), MWSL::String(category)));
	}
	void Register(Identity id, SLEndpoint ep)
	{
		MWSLOutput out(__conn, __id, MWSL::String("Register") , SYNC);
		SLStream *_is = out.getOS();
		/* Write Parameters */
		_is->write(id);
		//_is->write(ep);
		/* Invoke the operation */
		out.Invoke();
		/* Request Finish */
		out.Finish();
	}
	void Remove(Identity id)
	{
		MWSLOutput out(__conn, __id, MWSL::String("Remove") , SYNC);
		SLStream *_is = out.getOS();
		/* Write Parameters */
		_is->write(id);
		/* Invoke the operation */
		out.Invoke();
		/* Request Finish */
		out.Finish();
	}
	SLEndpoint Lookup(Identity id)
	{
		MWSLOutput out(__conn, __id, MWSL::String("Lookup") , SYNC);
		SLStream *_is = out.getOS();
		/* Write Parameters */
		_is->write(id);
		/* Invoke the operation */
		out.Invoke();
		/* Request Finish */
		out.Finish();
		SLStream *_os = out.getIS();
		SLEndpoint retval;
		_os->read(retval);
		return retval;
	}
};

#endif /* End of File */
