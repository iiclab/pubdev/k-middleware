/************************************************************************
* @ Filename : SL_InputStream.h                                                   
* @ Author : BumHyeon Baek (bhbaek@control.kangwon.ac.kr)               
* @ Version : 1.1
* @ Description

* @ NOTE
/************************************************************************/

#ifndef SL_INPUTSTREAM_H
#define SL_INPUTSTREAM_H

#pragma once

#include "SL_Communicator.h"
#include "SL_Servant.h"
#include "SL_ObjectAdapter.h"

using namespace std;

namespace MWSL
{
	class MWSL_API MWSLInput
	{
	public:

		MWSLInput(SLCommunicator*, SLStream*);
		~MWSLInput();

		void Invoke(int chID, ObjectAdapter*);

		SLStream* getIS() { return is; }
		SLStream* getOS() { return os; }

		void readParamHead();

	private:

		SLStream *os, *is;
		SLCommunicator *conn;
		Int reqId;

		void finishWriteParam();
	};
}

#endif
