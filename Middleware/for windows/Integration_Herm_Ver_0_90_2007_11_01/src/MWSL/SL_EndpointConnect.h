/************************************************************************
* @ Filename : SL_EndpointConnect.h                                                   
* @ Author : BumHyeon Baek (bhbaek@control.kangwon.ac.kr)               
* @ Version : 1.1
* @ Description

* @ NOTE
/************************************************************************/

#ifndef SL_ENDPOINTCONNECT_H
#define SL_ENDPOINTCONNECT_H

#pragma once
#include "MWSL.h"

namespace MWSL
{
	template<typename T>
	class EndpointConnect
	{
	public:

		EndpointConnect(SLEndpoint endp, ServiceLayer &sl) : isValid(false), endp_(endp)
		{
			if(endp.IsValid())
			{
				conn = sl.connectAdapter(endp.mID_);
				isValid = true;
			}
		}

		~EndpointConnect()
		{
			if(isValid)
			{
				//assert(conn.IsValid());
				conn.CloseConnection();
			}
		}

		T* CreateInstance()
		{
			if( isValid )
				return T::CreateInstance(conn, endp_.servantId_.name.c_str() , endp_.servantId_.category.c_str());
			else
				return NULL;
		}

		LogicalConnection GetProxyConnection()
		{
			return conn;
		}

		bool IsVaild()
		{
			return isValid;
		}

	private:
		
		EndpointConnect(const EndpointConnect<T> &rhs)
		{
			// Will not reach here
			assert(0);
		}

		LogicalConnection conn;
		SLEndpoint endp_;
		bool isValid;
	};
}

#endif
