/************************************************************************
* @ Filename : SL_ProxyClass.h                                                   
* @ Author : BumHyeon Baek (bhbaek@control.kangwon.ac.kr)               
* @ Version : 1.1
* @ Description

* @ NOTE
/************************************************************************/

#ifndef SL_PROXYCLASS_H
#define SL_PROXYCLASS_H

#pragma once
#include "SL_Define.h"

namespace MWSL
{
	class SLStream;

	class MWSL_API ProxyClass
	{
	public:

		ProxyClass();
		virtual ~ProxyClass();

		virtual void Encode(SLStream&) const = 0;
		virtual void Decode(SLStream&) = 0;
	};
}

#endif
