/************************************************************************
* @ Filename : SL_Logger.h                                                   
* @ Author : BumHyeon Baek (bhbaek@control.kangwon.ac.kr)               
* @ Version : 1.1
* @ Description

* @ NOTE
/************************************************************************/

#ifndef SL_LOGGER_H 
#define SL_LOGGER_H

#pragma once
#include <time.h>
#include <fstream>
#include <sstream>
#include <iostream>

using namespace std;

namespace MWSL
{
	class Logger
	{
	public:

		Logger(const char*);
		Logger();
		~Logger();

		void Write(string);

	private:

		ofstream* file;

		string getTime();		
	};
}

#endif

