/************************************************************************
* @ Filename : SL_Logger.cpp
* @ Author : BumHyeon Baek (bhbaek@control.kangwon.ac.kr)               
* @ Version : 1.1
* @ Description

* @ NOTE
/************************************************************************/

#include "SL_Logger.h"

using namespace MWSL;

Logger::Logger() : file(NULL)
{

}

Logger::Logger(const char *fileName)
{
	file = new ofstream(fileName, ios::app);
	if (file->bad())
	{
		delete file;
		file = NULL;
	}
}

Logger::~Logger()
{
	if (file)
	{
		file->close();
		delete file;
		file = NULL;
	}
}

string
Logger::getTime()
{
	time_t t = time(0);
	tm* lt = localtime(&t);
	stringstream ss;

	ss << lt->tm_year + 1900 << '/' << lt->tm_mon + 1 << '/' << lt->tm_mday << ' ' << lt->tm_hour << ':' << lt->tm_min << ':' << lt->tm_sec;

	return ss.str();
}

void
Logger::Write(string str)
{
	string tstr = getTime();

	if (file)
	{
		(*file) << '[' << tstr.c_str() << ']' << str.c_str() << endl;
	}
	else
		cout << '[' << tstr.c_str() << ']' << str.c_str() << endl;
}

