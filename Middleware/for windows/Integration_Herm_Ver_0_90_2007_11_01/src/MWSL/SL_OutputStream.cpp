/************************************************************************
* @ Filename : SL_OutputStream.cpp
* @ Author : BumHyeon Baek (bhbaek@control.kangwon.ac.kr)               
* @ Version : 1.1
* @ Description

* @ NOTE
/************************************************************************/

#include "SL_OutputStream.h"
#include "SL_Exception.h"
#include "SL_Communicator.h"
#include <iostream>

using namespace std;
using namespace MWSL;

MWSLOutput::MWSLOutput(SLCommunicator *connection, const Identity &id, MWSL::String operation, RequestType operationMode) :
conn(connection),
servantId(id),
mode(operationMode),
is(NULL),
paramSizeBegin(0),
isReplied(false)
{
	//os = new SLStream();

	conn->GenerateHeader(os, REQUEST_MSG);

	reqId = conn->GenerateRequestId();

	os.write(reqId);
	os.write(servantId.name);
	os.write(servantId.category);

	os.write(operation);
	os.write(static_cast<Byte>(mode));
	
	startWriteParam();
}

MWSLOutput::~MWSLOutput()
{
}

bool
MWSLOutput::Invoke()
{
	finishWriteParam();

	switch(mode)
	{
	case SYNC:
		byte Buf[1024];
		int dataLen = os.getSize();

		byte *pData = os.getBuffer();
		memcpy(Buf, pData, dataLen);

		//bring remote object name's length and name
		int retIntValue = byte2int(pData, 18);
		string rObj(&pData[22], &pData[22+retIntValue]);
		
		int foundObjServerId = conn->lookupServer4Obj(rObj);
		if (foundObjServerId == _INITIAL_SERVER_CONNECTION_ID_)
		{
			//conn->requestFindServer4Obj(rObj);
			break;
		}
		/*
		while (foundObjServerId == _INITIAL_SERVER_CONNECTION_ID_)	
		{		
			conn->requestFindServer4Obj(rObj);
			foundObjServerId = conn->lookupServer4Obj(rObj);
			Sleep(10000);
		}
		*/		
		conn->RegisterRequest(this);
		isReplied = false;

		conn->SendRequest(os, foundObjServerId);
		WaitForReply();
		break;
	}

	return true;
}

void
MWSLOutput::Finish()
{
	if (is == NULL)
		return;

	assert(is);

	Byte replyState = 0x00;
	is->read(replyState);

	if (replyState == 1)	// Object not exist
	{
		SLException ex;
		is->read(ex);
		throw ex;
	}

	if (replyState == 2)	// Operation not exist
	{
		Identity id;
		String operation;
		is->read(id.name);
		is->read(id.category);
		is->read(operation);
		OperationNotExistException ex(id, operation);
		throw ex;
	}

	if( replyState == 3 )  // Operation not exist , Should be Internal Error
	{
		Identity id;
		String operation;
		is->read( id.name );
		is->read( id.category );
		is->read( operation );
		OperationNotExistException ex( id, operation );
		throw ex;
	} 
}

void
MWSLOutput::setIncomingStream(SLStream *is)
{
	this->is = is;
}

void
MWSLOutput::startWriteParam()
{
	paramSizeBegin = os.getSize();
	os.write(Int(0));   
	os.write(MWSL::encodingMajor);  
	os.write(MWSL::encodingMinor); 
}

void 
MWSLOutput::finishWriteParam()
{
	assert( paramSizeBegin != 0 );
	// write msg size;
	Int paramSizeEnd = os.getSize();
	Int size = paramSizeEnd - paramSizeBegin;

	assert( size >= 0 );

	os.writeAt( size, os.getBuffer() + paramSizeBegin );
	paramSizeBegin = 0;
}

void
MWSLOutput::WaitForReply()
{
	SL_Lock sync(waitMutex);
	if (!isReplied)
	{
		replyCondition.wait(sync);
	}
}

void
MWSLOutput::WakeUpReply()
{
	SL_Lock sync(waitMutex);
	replyCondition.notify_one();
	isReplied = true;
}

void
MWSLOutput::OnReply()
{
	WakeUpReply();
}
