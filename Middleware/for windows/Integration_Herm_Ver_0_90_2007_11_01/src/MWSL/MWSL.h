/************************************************************************
* @ Filename : MWSL.h                                                   
* @ Author : BumHyeon Baek (bhbaek@control.kangwon.ac.kr)               
* @ Version : 1.1
* @ Description

* @ NOTE
/************************************************************************/

#ifndef MWSL_H
#define MWSL_H

#pragma once

#include "SL_ObjectAdapter.h"
#include "SL_LogicalConnection.h"
#include "SL_Exception.h"
#include "SL_ServantProxy.h"
#include "SL_InputStream.h"
#include "SL_OutputStream.h"
#include "SL_Endpoint.h"
#include "SL_ConfigManager.h"

using namespace std;

namespace MWSL
{
	class MWSL_API ServiceLayer
	{
	public:

		ServiceLayer();
		~ServiceLayer();

		ObjectAdapter* getAdapter();

		void Shutdown();
		ObjectAdapter* createObjectAdapter(Byte);

		LogicalConnection connectAdapter(Byte);

		SLEndpoint GetNamingServiceEndpoint()
		{
			return namingServiceEndpoint;
		}

		void SetNamingServiceEndpoint(const SLEndpoint &val)
		{
			namingServiceEndpoint = val;
		}

		SLEndpoint QueryEndpoint(const Identity &id);

	private:		

		SLEndpoint namingServiceEndpoint;

		ObjectAdapter* adapter;

		bool isShutdowned;
	};
}

#endif

