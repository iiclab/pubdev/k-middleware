/************************************************************************
* @ Filename : SL_Endpoint.cpp                                                   
* @ Author : BumHyeon Baek (bhbaek@control.kangwon.ac.kr)               
* @ Version : 1.1
* @ Description

* @ NOTE
/************************************************************************/

#include "SL_Endpoint.h"

using namespace MWSL;

SLEndpoint::SLEndpoint(void) : servantId_()
{
}

SLEndpoint::SLEndpoint(Identity servantId): servantId_(servantId)
{
}

SLEndpoint::~SLEndpoint(void)
{
}

void 
SLEndpoint::Encode(SLStream &stream) const
{
	//stream.write(mID_);	
	stream.write(servantId_);
}

void
SLEndpoint::Decode(SLStream &stream)
{
	//stream.read(mID_);	
	stream.read(servantId_);
}

bool 
SLEndpoint::IsValid()
{
	if(mID_ == 0x00)
		return false;
	else return true;
}

