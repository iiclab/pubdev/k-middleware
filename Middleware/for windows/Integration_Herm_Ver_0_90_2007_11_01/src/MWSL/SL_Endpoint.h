/************************************************************************
* @ Filename : SL_Endpoint.h                                                   
* @ Author : BumHyeon Baek (bhbaek@control.kangwon.ac.kr)               
* @ Version : 1.1
* @ Description

* @ NOTE
/************************************************************************/

#ifndef SL_ENDPOINT_H
#define SL_ENDPOINT_H

#pragma once
#include "SL_ProxyClass.h"
#include "SL_Servant.h"
#include "SL_Stream.h"

namespace MWSL
{
	class MWSL_API SLEndpoint : public ProxyClass
	{
	public:
		SLEndpoint(void);
		SLEndpoint(Identity servantId);
		virtual ~SLEndpoint(void);

		virtual void Encode(SLStream &stream) const;
		virtual void Decode(SLStream &stream);

		bool IsValid();

	public:
		Byte mID_;		
		Identity servantId_;
	};
}

#endif
