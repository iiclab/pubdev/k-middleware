/************************************************************************
* @ Filename : SL_Define.h                                                   
* @ Author : BumHyeon Baek (bhbaek@control.kangwon.ac.kr)               
* @ Version : 1.1
* @ Description

* @ NOTE
/************************************************************************/

#ifndef SL_DEFINE_H
#define SL_DEFINE_H

#pragma once

#include <iostream>
#include <string>
#include <algorithm>
#include <cassert>
#include <vector>
/*
#include "../Include/boost/bind.hpp"

#ifdef _WIN32
#define BOOST_HAS_WINTHREADS
#else 
#ifdef _WIN64
#define BOOST_HAS_WINTHREADS
#else
#define BOOST_HAS_PTHREADS
#endif
#endif
*/
//#define MWSL_DECLSPEC_IMPORT __declspec(dllimport)
//#define MWSL_DECLSPEC_EXPORT __declspec(dllexport)

#ifdef _WINDLL
#define MWSL_API MWSL_DECLSPEC_EXPORT
#else
#	ifdef _WIN32
#	define MWSL_API // MWSL_DECLSPEC_IMPORT
#	else
#	define MWSL_API
#	endif
#endif

#pragma warning (disable : 4251)
#pragma warning (disable : 4244)
#pragma warning (disable : 4819)
#pragma warning (disable : 4996)

namespace MWSL
{
	typedef unsigned char Byte;
	typedef char Char;
	typedef short Short;
	typedef int Int;
	typedef float Float;
	typedef double Double;
	typedef std::string String;
	typedef bool Bool;
	typedef unsigned int UInt;

#ifdef _WIN64
	typedef __int64 Int64;
#else
	typedef long Int64;
#endif

	const char MWSLMagic[] = { "KOMoR" };
	const Byte protocolMajor = 1;
	const Byte protocolMinor = 0;
	const Byte encodingMajor = 1;
	const Byte encodingMinor = 0;
	const int MWSL_Header_Size = 14;

	enum MessageType
	{
		REQUEST_MSG,
		REQUEST_BATCH,
		REPLY,
		VALIDATE,
		CLOSE,
		DISCOVERY,
		REQUEST_SERVER_ADDR,
		REPLY_SERVER_ADDR    
	};

	enum RequestType
	{
		SYNC,
		ASYNC
	};	

	const int THREAD_NUM = 2;
	const int THREAD_MAX_NUM = 20;

	class noncopyable
	{
	protected:

		noncopyable() { }
		~noncopyable() { } 

	private:

		noncopyable(const noncopyable&);
		const noncopyable& operator=(const noncopyable&);
	};
}

#endif