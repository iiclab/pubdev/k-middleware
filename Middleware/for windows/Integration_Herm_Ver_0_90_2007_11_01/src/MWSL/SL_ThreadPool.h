/************************************************************************
* @ Filename : SL_ThreadPool.h                                                   
* @ Author : BumHyeon Baek (bhbaek@control.kangwon.ac.kr)               
* @ Version : 1.1
* @ Description

* @ NOTE
/************************************************************************/

#ifndef SL_THREADPOOL_H
#define SL_THREADPOOL_H

#include <boost/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/condition.hpp>
#include <boost/smart_ptr.hpp>
#include <boost/bind.hpp>

#include "SL_Scheduler.h"
#include "SL_TaskAdaptor.h"

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable:4127)
#endif

namespace MWSL
{
	/* Thread pool. */ 
	template <class Task = thread_func, class Scheduler = Fifo_Scheduler<Task> > 
	class ThreadPool : private boost::noncopyable 
	{
	public:

		typedef ThreadPool<Task, Scheduler> pool_type;   /* Provides the thread pool's type. */
		typedef Task task_type;							 /* Provides the tasks' type. */
		typedef Scheduler scheduler_type;				 /* Provides the scheduler's type. */

	public:

		/* Constructs a new thread pool. */
		static boost::shared_ptr<pool_type> createThreadPool(size_t threads)
		{				
			boost::shared_ptr<pool_type> sp(new pool_type, taskDeleter<pool_type>());
			sp->storeSelfPtr(sp);
			sp->resize(threads);
			return sp;
		}

		/* Gets the number of threads in the pool. */
		size_t size() const
		{
			return thread_count;
		}

		/* Changes the number of threads in the pool. */
		bool resize(size_t threads)
		{
			boost::mutex::scoped_lock lock(monitor);

			bool result = true;

			boost::shared_ptr<pool_type> self = self_ptr.lock();

			target_thread_count = threads;

			while(thread_count < target_thread_count)
			{
				try
				{
					boost::shared_ptr<ThreadExecuter> executer(new ThreadExecuter(self));
					boost::thread* thread_ptr = _threads.create_thread(boost::bind(&ThreadExecuter::run, executer));
					executer->setThreadPtr(thread_ptr);

					thread_count++;
					running_thread_count++;	
				}
				catch(boost::thread_resource_error)
				{
					result = false;
					break;
				}
			}

			thread_is_idle.notify_all();

			return result;
		}

		/* Schedules a task for asynchronous execution. */  
		void schedule(const task_type &task)
		{	
			boost::mutex::scoped_lock lock(monitor);
			scheduler.push(task);
			task_is_available.notify_one();
		}	

		/* Returns the number of tasks which are ready for execution. */  
		size_t pending() const
		{
			return scheduler.size();
		}

		/* Indicates that there are no tasks pending. */   
		bool empty() const
		{
			return scheduler.empty();
		}	

		/* The current thread of execution blocks until all running and pending tasks are finished. */     
		void join()
		{
			boost::mutex::scoped_lock lock(monitor);
			while(0 != running_thread_count || !scheduler.empty())
			{ 
				thread_is_idle.wait(lock);
			}
		}	

		/*! The current thread of execution blocks until all running and pending tasks are finished or the timestamp is met. */       
		bool join(const boost::xtime& timestamp)
		{
			boost::mutex::scoped_lock lock(monitor);

			while(0 != running_thread_count || !scheduler.empty())
			{ 
				if(!thread_idle.timed_wait(lock, xt)) return false;
			}
			return true;
		}

	private:

		/* Constructor */
		ThreadPool() : thread_count(0), target_thread_count(0), running_thread_count(0)
		{
		}

		/* Destructor */
		~ThreadPool()
		{
		}

		template<class T>
		struct taskDeleter
		{
			void operator()(T* p)
			{
				delete p;
			}
		};    

	private:	

		bool executeTask()
		{
			boost::function0<void> task;
			{	
				boost::mutex::scoped_lock lock(monitor);
				
				if(thread_count > target_thread_count)
				{	
					thread_count--;
					running_thread_count--;
					thread_is_idle.notify_all();		
					return false;	
				}

				// wait for tasks
				while(scheduler.empty())
				{	
					thread_is_idle.notify_all();		

					if(thread_count > target_thread_count)
					{
						thread_count--;
						running_thread_count--;
						return false;
					}
					else
					{
						running_thread_count--;
						task_is_available.wait(lock);
						running_thread_count++;
					}
				}

				task = scheduler.top();
				scheduler.pop();
			}

			try
			{
				if(task)
				{
					task();
				}
			}
			catch(...)
			{
			}
			return true;
		}

		void storeSelfPtr(boost::shared_ptr<pool_type> self)
		{
			self_ptr = boost::weak_ptr<pool_type>(self);
		}	

		boost::weak_ptr<pool_type> self_ptr;

		size_t thread_count;	
		size_t target_thread_count;	
		size_t running_thread_count;

		boost::thread_group _threads;		
		scheduler_type scheduler;

		boost::mutex monitor;
		boost::condition thread_is_idle;	
		boost::condition task_is_available;

		/// Worker executes tasks.
		class ThreadExecuter
		{
		public:

			ThreadExecuter(boost::shared_ptr<pool_type> pool) : _pool(pool), _thread_ptr(0)
			{
			}

			/* Executes pool's tasks sequentially.	*/
			void run() const 
			{ 
				while(true)
				{
					boost::shared_ptr<pool_type> pool = _pool.lock();
					if(pool)
					{
						if(!pool->executeTask())
						{
							break;
						}
					}
					else
					{
						break;
					}
				}
	
				boost::shared_ptr<pool_type> pool = _pool.lock();
				if(pool && _thread_ptr)
				{
					pool->_threads.remove_thread(_thread_ptr);
				}
			}

			/* Stores the pointer to the thread whichs executes the run loop. */
			void setThreadPtr(boost::thread* thread_ptr)
			{
				thread_ptr = thread_ptr;
			}

		private:
			boost::weak_ptr<pool_type> _pool; 
			boost::thread* _thread_ptr;       
		};	
	};

	/* Fifo pool. */ 
	typedef ThreadPool<thread_func, Fifo_Scheduler<thread_func>> fifo_pool;

	/* Pool for prioritized task.*/ 
	typedef ThreadPool<prio_thread_func, Prio_Scheduler<prio_thread_func>> prio_pool;

#ifdef _MSC_VER
#pragma warning(pop)
#endif

}

#endif 
