/************************************************************************
* @ Filename : SL_Communicator.h                                                   
* @ Author : BumHyeon Baek (bhbaek@control.kangwon.ac.kr)               
* @ Version : 1.1
* @ Description

* @ NOTE
/************************************************************************/

//#ifndef SL_COMMUNICATOR_H
//#define SL_COMMUNICATOR_H

#pragma once
#include "SL_Stream.h"
#include "SL_Lock.h"
#include "Utility/Msg_t.h"
#include "nil/nil.h"
#include "nal/nal.h"
#include "ConfigAll.h"
#include "QuantumFramework/TimerManager.h"

#include <map>
#include <set>
#include <string>

class SLDataIndication;
class SLDataConfirm;

using namespace std;

#define byte2int(byteArray, begin) \
	(byteArray[begin+3]<<24)+(byteArray[begin+2]<<16)+(byteArray[begin+1]<<8)+(byteArray[begin]<<0)

#define _SERVER_DISCOVERY_PERIOD_ 50000
#define _INITIAL_SERVER_CONNECTION_ID_ 0
#define _CONfIGURED_UDP_CHANNEL_ 0 
#define _MAX_OBJECT_NAME_LENGTH_ 30

namespace MWSL
{
	struct serverTcpChannel
	{
		Int     tcp_channel_id_;
		Byte    server_module_id_;
	};

	typedef enum SLState_t
	{
		SL_DISCOVERY= 0x00,
		SL_INITIAL	= 0x01,
		SL_ACTIVE	= 0x02,
		SL_INACTIVE	= 0x04,
		SL_FAIL		= 0x08
	};

	class MWSLInput;
	class MWSLOutput;
	class ObjectAdapter;

	class MWSL_API SLCommunicator
	{
	public:
		//Enumeration of NAL state

		static SLCommunicator *instance(NIL *, NAL *);		
		~SLCommunicator();

		virtual void OnMessage(void *arg, Msg_t* msg);

		void SendRequest(SLStream&, int);				
		void SendResponse(int, SLStream&);
		void GenerateHeader(SLStream&, MessageType);
		Int GenerateRequestId();
		void RegisterRequest(MWSLOutput*);
		void setAdapter(ObjectAdapter*);		
		void CloseConnection();

		//*************************** Data Entity ******************************
		int dataRequest(void * , Msg_t* );
		void dataConfirm(SentStatus_t*);
		void dataIndication(void *, Msg_t *);        

		//************************ Management Entity ***************************
		void initialize(ObjectAdapter*, byte, void*);
		void setDataIndicationCB(InputCallbackHandler * pDataIndication);
		void setDataConfirmCB(OutputCallbackHandler * pDataConfirm);   
		void ServerTimerRequest(int period);
		bool isServerDiscovered();
		void requestFindServer4Obj(string rObj);
		SLState_t getState();
		void setState(SLState_t);
		void registerRemoteObject(string rObj);
		void connectRemoteObject();
		int lookupServer4Obj(string);

	protected:
		SLCommunicator(NIL *, NAL *);        

	private:
		static SLCommunicator * pInstance_;

		bool CheckHeader(SLStream&, Int&, MessageType&);
		void InsertHeaderSize(SLStream&);
		void ParseMessage(void *arg, SLStream* stream, MessageType type);

		//************************ Management Entity ***************************
		void responseServerAddr(int chID, Byte srcModuleAddr, SLStream *is);
		void processReplyServerAddr(Byte srcModuleAddr, SLStream *is);    

		Int reqIdGen;
		ObjectAdapter *_adapter;
		SL_Mutex requestsMutex;
		
		std::map<Int, MWSLOutput*> requests;
		std::map<string, int> ServerConnectionTable_;
		std::map<string, int> ServerAddressTable_;
		typedef std::map<string, int>::iterator sct_it;
		typedef std::map<string, int>::iterator sat_it;

		SLDataIndication * pSLDataIndicationCB_;
		SLDataConfirm * pSLDataConfirmCB_;

		InputCallbackHandler *pUpperLayerDataIndication_;
		OutputCallbackHandler *pUpperLayerDataConfirm_;

		// Lower Layer
		NAL * pLowerLayer_;
		NIL * p2ndLowerLayer_;     

		//connection for server
		struct serverTcpChannel* pServerCh_;
		bool serverFound;
		int usedport_;
		Byte _REMOTE_MOUDLE_ADDR_;      //test server module addr
		TimerManager * pTimerManager_;
		SLState_t serivce_layer_communicator_state_;
		//SL_Mutex m_;
		SLStream is;

		struct configureAddr* pConAddr;
	};

}

//#endif