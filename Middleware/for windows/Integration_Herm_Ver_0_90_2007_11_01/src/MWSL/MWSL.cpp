/************************************************************************
* @ Filename : MWSL.cpp                                                   
* @ Author : BumHyeon Baek (bhbaek@control.kangwon.ac.kr)               
* @ Version : 1.1
* @ Description

* @ NOTE
/************************************************************************/

#include "MWSL.h"
#include "SL_Connector.h"
#include "SL_EndpointConnect.h"
#include "SL_ConfigManager.h"
#include "MWNaming_prx.h"

using namespace MWSL;

ServiceLayer::ServiceLayer() : adapter(NULL), isShutdowned(false)
{

}

ServiceLayer::~ServiceLayer()
{
	if(!isShutdowned)
		this->Shutdown();

	if (adapter)
	{
		adapter->removeServants();
		delete adapter;
	}	
}

ObjectAdapter*
ServiceLayer::getAdapter()
{
	return adapter;
}


ObjectAdapter*
ServiceLayer::createObjectAdapter(Byte mID)
{
	assert( !isShutdowned );

	if (adapter)
	{
		adapter->removeServants();
		delete adapter;
	}


	adapter = new ObjectAdapter(this);	
	return adapter;
}

LogicalConnection
ServiceLayer::connectAdapter(Byte mID)
{
	SLConnector connector(mID);
	SLCommunicator *conn = connector.Connect(0);

	return LogicalConnection(conn);
}

SLEndpoint
ServiceLayer::QueryEndpoint(const MWSL::Identity &id)
{
	SLEndpoint endpoint = ConfigManager::Configure().GetNamingServiceEndpoint();
	EndpointConnect<NamingServiceProx> ec(endpoint, *this);

	SLEndpoint result;

	NamingServiceProx *prx = ec.CreateInstance();

	if(prx)
		result = prx->Lookup(id);
	return result;
}

void
ServiceLayer::Shutdown()
{

}
