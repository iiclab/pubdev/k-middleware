/************************************************************************
* @ Filename : SL_ObjectAdapter.h                                                   
* @ Author : BumHyeon Baek (bhbaek@control.kangwon.ac.kr)               
* @ Version : 1.1
* @ Description

* @ NOTE
/************************************************************************/

#ifndef SL_OBJECTADAPTER_H
#define SL_OBJECTADAPTER_H

#pragma once
#include "SL_Communicator.h"
#include "SL_Servant.h"
#include "SL_Lock.h"
#include <map>

using namespace std;

namespace MWSL
{
    class ServiceLayer;
    
	class MWSL_API ObjectAdapter
	{
	public:
		ObjectAdapter(ServiceLayer *sl_);
		~ObjectAdapter();

		void addServant(Servant *servant, const Identity& id);
        void Register(const Identity &namingId, Servant *servant, const Identity &localId);
		void removeServants();
		Servant* findServant(const Identity& id);
		void printServants();

	private:

		map<Identity, Servant*> servantMap;
        map<Identity, Identity> localnamingId;

		map<Identity, Servant*>::iterator _servantIterator;

		SL_Mutex mapMutex;
        ServiceLayer *sl;
	};
}

#endif
