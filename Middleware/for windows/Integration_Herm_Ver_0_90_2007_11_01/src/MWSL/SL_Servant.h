/************************************************************************
* @ Filename : SL_Servant.h                                                   
* @ Author : BumHyeon Baek (bhbaek@control.kangwon.ac.kr)               
* @ Version : 1.1
* @ Description

* @ NOTE
/************************************************************************/

#ifndef SL_SERVANT_H
#define SL_SERVANT_H

#pragma once
#include "SL_Define.h"

namespace MWSL
{
	class MWSLInput;

	struct MWSL_API Identity 
	{
	public:
		String name;
		String category;

		Identity();
		Identity(const char*, const char*);
		Identity(const String&, const String&);		

		bool operator < (const Identity&) const;
	};

	class MWSL_API Servant
	{
	public:

		Servant();
		Servant(const String& , const String&);
		virtual ~Servant(void);

		Identity getId() { return _id; }
		void setId(const Identity& id) { this->_id.name = id.name; this->_id.category = id.category; }

		virtual int _Dispatch(MWSLInput&, const Identity&, const String&);

	private:

		Identity _id;
	};
}

#endif
