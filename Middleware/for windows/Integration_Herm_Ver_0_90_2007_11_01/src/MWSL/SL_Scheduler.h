/************************************************************************
* @ Filename : SL_Scheduler.h                                                   
* @ Author : BumHyeon Baek (bhbaek@control.kangwon.ac.kr)               
* @ Version : 1.1
* @ Description

* @ NOTE
/************************************************************************/

#ifndef SL_SCHEDULER_H
#define SL_SCHEDULER_H

#pragma once
#include <queue>
#include <stack>
#include "SL_TaskAdaptor.h"

namespace MWSL 
{
	/* Fifo task scheduler. */ 
	template <class Task = thread_func>  
	class Fifo_Scheduler
	{
	public:

		/* Provides the scheduler's task type. */
		typedef Task task_type; 

		/* Adds a new task to the scheduler. */
		void push(const task_type& task)
		{
			queue.push(task);
		}

		/* Removes the task which should be executed next. */
		void pop()
		{
			queue.pop();
		}

		/* Gets the task which should be executed next. */
		const task_type& top() const
		{
			return queue.front();
		}

		/* Gets the current number of tasks in the scheduler. */
		size_t size() const
		{
			return queue.size();
		}

		/*! Checks if the scheduler is empty. */
		bool empty() const
		{
			return queue.empty();
		}

	private:

		/* Internal task container.	*/
		std::queue<task_type> queue;  
	};

	/* Scheduler for prioritized tasks. */ 
	template <class Task = prio_thread_func>  
	class Prio_Scheduler
	{
	public:

		typedef Task task_type; 

		void push(const task_type& task)
		{
			queue.push(task);
		}

		void pop()
		{
			queue.pop();
		}

		const task_type& top() const
		{
			return queue.top();
		}

		size_t size() const
		{
			return queue.size();
		}

		bool empty() const
		{
			return queue.empty();
		}

	private:
		std::priority_queue<task_type> queue;  
	};	
}

#endif