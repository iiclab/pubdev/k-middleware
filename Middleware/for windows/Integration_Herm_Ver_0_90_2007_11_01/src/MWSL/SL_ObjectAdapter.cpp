/************************************************************************
* @ Filename : SL_ObjectAdapter.cpp
* @ Author : BumHyeon Baek (bhbaek@control.kangwon.ac.kr)               
* @ Version : 1.1
* @ Description

* @ NOTE
/************************************************************************/

#include "SL_ObjectAdapter.h"
#include "MWNaming_prx.h"
#include "SL_EndpointConnect.h"

using namespace MWSL;

ObjectAdapter::ObjectAdapter(ServiceLayer* sl_) : sl(sl_), _servantIterator(servantMap.end())
{

}

ObjectAdapter::~ObjectAdapter()
{
	removeServants();
}

void
ObjectAdapter::addServant(Servant *servant, const Identity& id)
{
	SL_Lock sync(mapMutex);
	servant->setId(id);
	servantMap[id] = servant;	
}

Servant* ObjectAdapter::findServant(const Identity &id)
{	
	SL_Lock sync(mapMutex);
	map<Identity, Servant*>::iterator it = servantMap.find(id);
	if(it != servantMap.end())
	{
		return it->second;
	}
	else
		return NULL;
}

void
ObjectAdapter::Register(const Identity &namingId, Servant *servant, const Identity &localId)
{
    addServant(servant, localId);
    SLEndpoint endp = sl->GetNamingServiceEndpoint();
    EndpointConnect<NamingServiceProx> ec(endp, *sl);
    NamingServiceProx *prx = ec.CreateInstance();
    
    prx->Register(namingId, SLEndpoint(servant->getId()));
    
    localnamingId[servant->getId()] = namingId;
}

void
ObjectAdapter::removeServants()
{
	SL_Lock sync(mapMutex);
	for(map<Identity, Servant*>::iterator it = servantMap.begin(); it!=servantMap.end(); it++)
	{
		Servant *s = it->second;
		it->second = NULL;
		if(s)
		{
			delete s;
		}
	}
	servantMap.clear();
}

void
ObjectAdapter::printServants()
{
	map<Identity, Servant*>::iterator it = _servantIterator;

	for (it = servantMap.begin() ; it != servantMap.end() ; it++)
	{
		cout << it->first.name << endl;
	}
}
