/************************************************************************
* @ Filename : SL_LogicalConnection.h                                                   
* @ Author : BumHyeon Baek (bhbaek@control.kangwon.ac.kr)               
* @ Version : 1.1
* @ Description

* @ NOTE
/************************************************************************/

#ifndef SL_LOGICALCONNECTION_H
#define SL_LOGICALCONNECTION_H

#pragma once
#include "SL_Communicator.h"

namespace MWSL
{
	class MWSL_API LogicalConnection
	{
	friend class ServantPrx;

	public:

		LogicalConnection();
		LogicalConnection(SLCommunicator*);
		~LogicalConnection();

		void CloseConnection();

	private:

		SLCommunicator *conn;
		SLCommunicator *Get() { return conn; }
	};

}

#endif
