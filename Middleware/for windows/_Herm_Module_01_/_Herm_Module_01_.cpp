// _Herm_Module_01_.cpp : 콘솔 응용 프로그램에 대한 진입점을 정의합니다.
//

#include "stdafx.h"
#include <iostream>
#include <cstdio>
#include <TIME.H>

//operating system adaptation layer
#include "osal/SysEventProcessor.h"
#include "osal/Thread.h"
#include "osal/IRunnable.h"
#include "osal/OSAL_Socket.h"
#include "QuantumFramework/TimerManager.h"

//network interface layer
#include "nil/IODeviceManager.h"
#include "nil/nil.h"

//network adaptation layer
#include "nal/nal.h"

//service layer
#include "mwsl/MWSL.h"
#include "mwsl/SL_Communicator.h"
#include "mwsl/SL_ObjectAdapter.h"
#include "mwsl/SL_ThreadPool.h"
#include "mwsl/SL_Scheduler.h"
#include "mwsl/SL_ThreadPoolAdaptor.h"
#include "mwsl/SL_TaskAdaptor.h"

#include "Utility/Msg_t.h"
#include "test_svr.h"
#include "test_prx.h"
#include "Debug.h"

#include "../Include/boost/thread.hpp"
#include "../Include/boost/bind.hpp"

using namespace MWSL;
using namespace std;
//using namespace boost;

unsigned char _NONE_	= 0x00;
unsigned char _JENIBO_ = 0x24;			// a ROBOT
unsigned char _REMOTE_EASE_ = 0x32;     // SERVER

class TestImpl : public Test
{
public:
	virtual void HelloWorld(MWSL::String str)
	{
		std::cout<<"********************************************"<<endl;
		std::cout<<"From Client: "+str<<endl;
		std::cout<<"********************************************"<<endl;
	}	
};

/*
1. 서버/클라이언트 유무 정의
2. 현재 미들웨어의 TCP 포트
3. 현재 미들웨어의 UDP 포트
4. 서버 미들웨어의 UDP 포트(서버-클라이언트간 약속된 UDP 포트 사용)
5. 서버 미들웨어의 TCP 포트(서버-클라이언트간 약속된 UDP 포트 사용)
6. 현재 미들웨어의 IP주소 입력
7. 서버 미들웨어의 IP주소 입력(입력필요없음)
*/
#define _PROMISE_UDP_PORT_	9001
#define _PROMISE_TCP_PORT_	5002

struct configureAddr ca = {	FALSE, 
							_PROMISE_TCP_PORT_, 
							_PROMISE_UDP_PORT_, 
							_PROMISE_UDP_PORT_, 
							_PROMISE_TCP_PORT_, 
							"203.252.90.71", 
							"NONE"};
struct configureAddr *pCa = &ca;

int _tmain(int argc, _TCHAR* argv[])
{
	std::cout << "*****************************************" << endl;
	std::cout << "Communication Middleware is started..." << endl;
	std::cout << "*****************************************" << endl; 

	OSAL::SysEventProcessor * sysEventProcessor = OSAL::SysEventProcessor::instance();	
	sysEventProcessor->setOpMode(OSAL::OPMODE_ASYNC);	
	sysEventProcessor->initialize();

	TimerManager *pTimerManager = TimerManager::instance(_DEFAULT_TIMER_KEY_);
	pTimerManager->initialize();

	NIL NetworkInterfaceLayer(pCa);
	NetworkInterfaceLayer.initialize();

//	NAL NetworkAdaptationLayer(&NetworkInterfaceLayer);
//	NetworkAdaptationLayer.initialize(_REMOTE_EASE_);

	ServiceLayer _ServiceLayer;
	ObjectAdapter *ObjAdapter = _ServiceLayer.createObjectAdapter(_NONE_);

	SLCommunicator *pSLCommunicator 
		= SLCommunicator::instance(&NetworkInterfaceLayer, NULL);
	pSLCommunicator->initialize(ObjAdapter, _NONE_, pCa); 
	LogicalConnection conn(pSLCommunicator);

#define _REGISTER_OBJECT_NUMBERS_ 3
	std::string registerObj[_REGISTER_OBJECT_NUMBERS_] =
	{
		std::string("TestObj01")
		,std::string("TestObj02")
		,std::string("TestObj03")
	};

	for (int i=0; i<_REGISTER_OBJECT_NUMBERS_; i++) {
		Test *testObject = new TestImpl();
		Identity *id = new Identity(registerObj[i], string(""));
		ObjAdapter->addServant(testObject, *id);
		pSLCommunicator->registerRemoteObject(registerObj[i]);
	}

	//system event loop
	sysEventProcessor->handle_event();

	return 0;
}

