#pragma once

#include "mwsl/MWSL.h"

using namespace MWSL;

class Test: public ::MWSL::Servant
{
public:
	virtual int _Dispatch(::MWSL::MWSLInput &in, const ::MWSL::Identity &id, const ::MWSL::String &operation)
	{
		if (operation == "HelloWorld")
		{
			::MWSL::SLStream* _is = in.getIS();
			::MWSL::SLStream* _os = in.getOS();
			in.readParamHead();
			::MWSL::String _str;
			_is->read(_str);
			HelloWorld(_str);
			_os->write((Byte)0);/* Reply state success */
			return 1;
		}
		return 0;//failed
	}
	virtual void HelloWorld(::MWSL::String str) = 0;
};


