// _Herm_Module_01_.cpp : 콘솔 응용 프로그램에 대한 진입점을 정의합니다.
//

#include "stdafx.h"
#include <iostream>
#include <cstdio>
#include <TIME.H>
#include <string>

//operating system adaptation layer
#include "osal/SysEventProcessor.h"
#include "osal/Thread.h"
#include "osal/IRunnable.h"
#include "osal/OSAL_Socket.h"
#include "QuantumFramework/TimerManager.h"

//network interface layer
#include "nil/IODeviceManager.h"
#include "nil/nil.h"

//network adaptation layer
#include "nal/nal.h"

//service layer
#include "mwsl/MWSL.h"
#include "mwsl/SL_Communicator.h"
#include "mwsl/SL_ObjectAdapter.h"
#include "mwsl/SL_ThreadPool.h"
#include "mwsl/SL_Scheduler.h"
#include "mwsl/SL_ThreadPoolAdaptor.h"
#include "mwsl/SL_TaskAdaptor.h"

#include "Utility/Msg_t.h"
#include "test_prx.h"
#include "Debug.h"

#include "../Include/boost/thread.hpp"
#include "../Include/boost/bind.hpp"

using namespace MWSL;
using namespace std;

unsigned char _NONE_	= 0x00;
unsigned char _JENIBO_ = 0x24;			// a ROBOT
unsigned char _REMOTE_EASE_ = 0x32;     // SERVER

typedef boost::mutex ThreadMutex;
typedef boost::mutex::scoped_lock ThreadLock;

ThreadMutex syncThread;

void 
ThreadProc(TestProx *prx, char c)
{	
	Sleep(5000);

	while(1) 
	{
		ThreadLock lock_(syncThread);
		prx->HelloWorld(MWSL::String("Hello World!"));
		Sleep(1);		
	}
}

/*
1. 서버/클라이언트 유무 정의
2. 현재 미들웨어의 TCP 포트
3. 현재 미들웨어의 UDP 포트
4. 서버 미들웨어의 UDP 포트(서버-클라이언트간 약속된 UDP 포트 사용)
5. 서버 미들웨어의 TCP 포트(서버-클라이언트간 약속된 UDP 포트 사용)
6. 현재 미들웨어의 IP주소 입력
7. 서버 미들웨어의 IP주소 입력(입력필요없음)
*/

#define _PROMISE_UDP_PORT_	9001
#define _PROMISE_TCP_PORT_	5002

struct configureAddr ca = {	FALSE, 
							5100, 
							9002, 
							_PROMISE_UDP_PORT_, 
							_PROMISE_TCP_PORT_, 
							"203.252.90.71", 
							"NONE"};
struct configureAddr *pCa = &ca;


int _tmain(int argc, _TCHAR* argv[])
{
	std::cout << "*****************************************" << endl;
	std::cout << "Communication Middleware is started..." << endl;
	std::cout << "*****************************************" << endl; 

	OSAL::SysEventProcessor * sysEventProcessor = OSAL::SysEventProcessor::instance();	
	sysEventProcessor->setOpMode(OSAL::OPMODE_ASYNC);	
	sysEventProcessor->initialize();

	TimerManager *pTimerManager = TimerManager::instance(_DEFAULT_TIMER_KEY_);
	pTimerManager->initialize();

	NIL NetworkInterfaceLayer(pCa);
	NetworkInterfaceLayer.initialize();

	//NAL NetworkAdaptationLayer(&NetworkInterfaceLayer);
	//NetworkAdaptationLayer.initialize(_JENIBO_);

	ServiceLayer _ServiceLayer;
	ObjectAdapter *ObjAdapter = _ServiceLayer.createObjectAdapter(_NONE_);

	SLCommunicator *pSLCommunicator 
		= SLCommunicator::instance(&NetworkInterfaceLayer, NULL);
	pSLCommunicator->initialize(ObjAdapter, _REMOTE_EASE_, pCa); 
	LogicalConnection conn(pSLCommunicator);

	//QoS scheduler
	pool_smartPtr<prio_pool> tp(0);

#define _REGISTER_OBJECT_NUMBERS_ 3
	//proxy objects
	std::string registerObj[_REGISTER_OBJECT_NUMBERS_] =
	{
		std::string("TestObj01")
		,std::string("TestObj02")
		,std::string("TestObj03")
	};

	//set priority for each thread
	int priorityThread[_REGISTER_OBJECT_NUMBERS_] = {10, 15, 5};

	//boost::thread* prxThreads[_REGISTER_OBJECT_NUMBERS_];
	TestProx* prx[_REGISTER_OBJECT_NUMBERS_];

	for (int i=0; i<_REGISTER_OBJECT_NUMBERS_; i++) {
		prx[i] = TestProx::CreateInstance(pSLCommunicator, registerObj[i].data(), "");  
		pSLCommunicator->registerRemoteObject(registerObj[i]);
		tp->schedule(prio_thread_func(priorityThread[i], boost::bind(ThreadProc, prx[i], 'a'+i)));
	}
	tp->resize(10);

	//find servers which the corresponding objects
	pSLCommunicator->connectRemoteObject();

	//system event loop
	sysEventProcessor->handle_event();
	return 0;
}

