#pragma once

#include "mwsl/MWSL.h"

using namespace MWSL;

class TestProx : public ::MWSL::ServantPrx
{
private:
	TestProx(::MWSL::SLCommunicator* conn, const ::MWSL::Identity id) : ::MWSL::ServantPrx(conn, id)
	{
	}
public:
	static TestProx* CreateInstance(::MWSL::SLCommunicator* conn , const char* name, const char* category)
	{
		return new TestProx(conn, ::MWSL::Identity( ::MWSL::String(name), ::MWSL::String(category)));
	}
	void HelloWorld(::MWSL::String str)
	{
		::MWSL::MWSLOutput out(__conn, __id, ::MWSL::String("HelloWorld") , SYNC);
		::MWSL::SLStream *_is = out.getOS();
		/* Write Parameters */
		_is->write(str);
		/* Invoke the operation */
		out.Invoke();
		/* Request Finish */
		out.Finish();
	}
};


