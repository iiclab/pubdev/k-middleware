/* A Bison parser, made by GNU Bison 1.875b.  */

/* Skeleton parser for Yacc-like parsing with Bison,
   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

/* As a special exception, when this file is copied by Bison into a
   Bison output file, you may use that output file without restriction.
   This special exception was added by the Free Software Foundation
   in version 1.24 of Bison.  */

/* Written by Richard Stallman by simplifying the original so called
   ``semantic'' parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

#include "stdafx.h"
#include "MWIDL_bis.h"
/* Identify Bison output.  */
#define YYBISON 1

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Using locations.  */
#define YYLSP_NEEDED 0

/* Copy the first part of user declarations.  */
#line 1 "MWIDL_bis"

/*--------------------------------------------------------------------
 * 
 * Includes
 * 
 *------------------------------------------------------------------*/
#include "MWIDL.h"
#include "assert.h"
#include <string>
#include <iostream>
#include <stack>
using namespace std;
extern FILE *yyin, *yyout;

static std::stack< MIDLSemantics* > exps;

int yylex(void);
int yyerror(char * s);

MIDLSemantics* PopExp()
{
	cout<<"exps.pop()"<<endl;
	assert(!exps.empty());
	MIDLSemantics* exp = exps.top();
	exps.pop();
	
	return exp;
}


/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

#if ! defined (YYSTYPE) && ! defined (YYSTYPE_IS_DECLARED)
#line 42 "MWIDL_bis"
typedef union YYSTYPE {
	char strval[50];
	int intval;
	double dblval;
} YYSTYPE;
/* Line 191 of yacc.c.  */
#line 152 "MWIDL_bis.cpp"
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
# define YYSTYPE_IS_TRIVIAL 1
#endif



/* Copy the second part of user declarations.  */


/* Line 214 of yacc.c.  */
#line 164 "MWIDL_bis.cpp"

#if ! defined (yyoverflow) || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# if YYSTACK_USE_ALLOCA
#  define YYSTACK_ALLOC alloca
# else
#  ifndef YYSTACK_USE_ALLOCA
#   if defined (alloca) || defined (_ALLOCA_H)
#    define YYSTACK_ALLOC alloca
#   else
#    ifdef __GNUC__
#     define YYSTACK_ALLOC __builtin_alloca
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's `empty if-body' warning. */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
# else
#  if defined (__STDC__) || defined (__cplusplus)
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   define YYSIZE_T size_t
#  endif
#  define YYSTACK_ALLOC malloc
#  define YYSTACK_FREE free
# endif
#endif /* ! defined (yyoverflow) || YYERROR_VERBOSE */


#if (! defined (yyoverflow) \
     && (! defined (__cplusplus) \
	 || (YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  short yyss;
  YYSTYPE yyvs;
  };

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (short) + sizeof (YYSTYPE))				\
      + YYSTACK_GAP_MAXIMUM)

/* Copy COUNT objects from FROM to TO.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if 1 < __GNUC__
#   define YYCOPY(To, From, Count) \
      __builtin_memcpy (To, From, (Count) * sizeof (*(From)))
#  else
#   define YYCOPY(To, From, Count)		\
      do					\
	{					\
	  register YYSIZE_T yyi;		\
	  for (yyi = 0; yyi < (Count); yyi++)	\
	    (To)[yyi] = (From)[yyi];		\
	}					\
      while (0)
#  endif
# endif

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack)					\
    do									\
      {									\
	YYSIZE_T yynewbytes;						\
	YYCOPY (&yyptr->Stack, Stack, yysize);				\
	Stack = &yyptr->Stack;						\
	yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
	yyptr += yynewbytes / sizeof (*yyptr);				\
      }									\
    while (0)

#endif

#if defined (__STDC__) || defined (__cplusplus)
   typedef signed char yysigned_char;
#else
   typedef short yysigned_char;
#endif

/* YYFINAL -- State number of the termination state. */
#define YYFINAL  8
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   66

/* YYNTOKENS -- Number of terminals. */
#define YYNTOKENS  21
/* YYNNTS -- Number of nonterminals. */
#define YYNNTS  10
/* YYNRULES -- Number of rules. */
#define YYNRULES  31
/* YYNRULES -- Number of states. */
#define YYNSTATES  61

/* YYTRANSLATE(YYLEX) -- Bison symbol number corresponding to YYLEX.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   275

#define YYTRANSLATE(YYX) 						\
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[YYLEX] -- Bison symbol number corresponding to YYLEX.  */
static const unsigned char yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20
};

#if YYDEBUG
/* YYPRHS[YYN] -- Index of the first RHS symbol of rule number YYN in
   YYRHS.  */
static const unsigned char yyprhs[] =
{
       0,     0,     3,     5,     8,    10,    13,    19,    26,    32,
      39,    46,    54,    60,    67,    73,    80,    87,    95,    98,
     102,   104,   106,   109,   113,   116,   120,   122,   127,   130,
     133,   138
};

/* YYRHS -- A `-1'-separated list of the rules' RHS. */
static const yysigned_char yyrhs[] =
{
      22,     0,    -1,    23,    -1,    23,    22,    -1,    24,    -1,
      24,    22,    -1,     3,    20,     9,    27,    10,    -1,     3,
      20,     9,    27,    10,    14,    -1,     4,    20,     9,    25,
      10,    -1,     4,    20,     9,    25,    10,    14,    -1,    17,
      20,     5,    26,     6,    14,    -1,    17,    20,     5,    26,
       6,    14,    25,    -1,    29,    20,     5,     6,    14,    -1,
      29,    20,     5,     6,    14,    25,    -1,    17,    20,     5,
       6,    14,    -1,    17,    20,     5,     6,    14,    25,    -1,
      29,    20,     5,    26,     6,    14,    -1,    29,    20,     5,
      26,     6,    14,    25,    -1,    18,    25,    -1,    28,    13,
      26,    -1,    28,    -1,    17,    -1,    28,    14,    -1,    28,
      14,    27,    -1,    29,    20,    -1,    29,    20,    30,    -1,
      20,    -1,    20,    11,    29,    12,    -1,    29,    16,    -1,
      29,    15,    -1,     7,    19,     8,    30,    -1,     7,    19,
       8,    -1
};

/* YYRLINE[YYN] -- source line where rule number YYN was defined.  */
static const unsigned short yyrline[] =
{
       0,    96,    96,   103,   112,   118,   129,   135,   144,   150,
     157,   163,   170,   176,   183,   188,   194,   202,   210,   219,
     227,   233,   241,   248,   258,   264,   275,   280,   288,   295,
     302,   309
};
#endif

#if YYDEBUG || YYERROR_VERBOSE
/* YYTNME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals. */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "classDeclT", "interfaceDeclT", "lParentT", 
  "rParentT", "lBracketT", "rBracketT", "lBraceT", "rBraceT", 
  "lCheveronT", "rCheveronT", "commaT", "semicolonT", "andT", "starT", 
  "voidT", "atT", "digitT", "idT", "$accept", "program", "classdecl", 
  "interfacedecl", "interfacestmt", "params", "classstmt", "vardecl", 
  "typename", "dimension", 0
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[YYLEX-NUM] -- Internal token number corresponding to
   token YYLEX-NUM.  */
static const unsigned short yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275
};
# endif

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const unsigned char yyr1[] =
{
       0,    21,    22,    22,    22,    22,    23,    23,    24,    24,
      25,    25,    25,    25,    25,    25,    25,    25,    25,    26,
      26,    26,    27,    27,    28,    28,    29,    29,    29,    29,
      30,    30
};

/* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN.  */
static const unsigned char yyr2[] =
{
       0,     2,     1,     2,     1,     2,     5,     6,     5,     6,
       6,     7,     5,     6,     5,     6,     6,     7,     2,     3,
       1,     1,     2,     3,     2,     3,     1,     4,     2,     2,
       4,     3
};

/* YYDEFACT[STATE-NAME] -- Default rule to reduce with in state
   STATE-NUM when YYTABLE doesn't specify something else to do.  Zero
   means the default is an error.  */
static const unsigned char yydefact[] =
{
       0,     0,     0,     0,     2,     4,     0,     0,     1,     3,
       5,     0,     0,    26,     0,     0,     0,     0,     0,     0,
       0,     0,     6,    22,    29,    28,    24,     0,    18,     8,
       0,     0,     7,    23,     0,    25,     0,     9,     0,    27,
       0,     0,    21,     0,    20,     0,     0,    31,    14,     0,
       0,    12,     0,    30,    15,    10,    19,    13,    16,    11,
      17
};

/* YYDEFGOTO[NTERM-NUM]. */
static const yysigned_char yydefgoto[] =
{
      -1,     3,     4,     5,    19,    43,    14,    44,    20,    35
};

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
#define YYPACT_NINF -38
static const yysigned_char yypact[] =
{
      39,   -17,   -13,    11,    39,    39,    16,    27,   -38,   -38,
     -38,    19,    14,    35,     7,    33,     6,    28,    14,    41,
       8,    19,    36,    19,   -38,   -38,    42,    47,   -38,    43,
      48,    -6,   -38,   -38,    37,   -38,    -2,   -38,    -1,   -38,
      46,    44,   -38,    49,    50,    45,    54,    42,    14,    51,
      18,    14,    52,   -38,   -38,    14,   -38,   -38,    14,   -38,
     -38
};

/* YYPGOTO[NTERM-NUM].  */
static const yysigned_char yypgoto[] =
{
     -38,    40,   -38,   -38,   -18,   -37,    38,    -3,    -9,    15
};

/* YYTABLE[YYPACT[STATE-NUM]].  What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule which
   number is the opposite.  If zero, do what YYDEFACT says.
   If YYTABLE_NINF, syntax error.  */
#define YYTABLE_NINF -1
static const unsigned char yytable[] =
{
      28,    46,    16,     6,    41,    45,    39,     7,    15,    24,
      25,     8,    31,    56,    16,    42,    42,    22,    13,    13,
      15,    24,    25,    24,    25,    11,    26,    16,    30,    16,
      54,    17,    18,    57,    13,    42,    12,    59,    13,    13,
      60,    16,     1,     2,     9,    10,    21,    23,    27,    34,
      32,    29,    36,    38,    47,    49,    40,    37,    48,    51,
      52,    33,    53,    50,     0,    55,    58
};

static const yysigned_char yycheck[] =
{
      18,    38,    11,    20,     6,     6,    12,    20,    11,    15,
      16,     0,    21,    50,    23,    17,    17,    10,    20,    20,
      23,    15,    16,    15,    16,     9,    20,    36,    20,    38,
      48,    17,    18,    51,    20,    17,     9,    55,    20,    20,
      58,    50,     3,     4,     4,     5,    11,    14,    20,     7,
      14,    10,     5,     5,     8,     6,    19,    14,    14,    14,
       6,    23,    47,    13,    -1,    14,    14
};

/* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
   symbol of state STATE-NUM.  */
static const unsigned char yystos[] =
{
       0,     3,     4,    22,    23,    24,    20,    20,     0,    22,
      22,     9,     9,    20,    27,    28,    29,    17,    18,    25,
      29,    11,    10,    14,    15,    16,    20,    20,    25,    10,
      20,    29,    14,    27,     7,    30,     5,    14,     5,    12,
      19,     6,    17,    26,    28,     6,    26,     8,    14,     6,
      13,    14,     6,    30,    25,    14,    26,    25,    14,    25,
      25
};

#if ! defined (YYSIZE_T) && defined (__SIZE_TYPE__)
# define YYSIZE_T __SIZE_TYPE__
#endif
#if ! defined (YYSIZE_T) && defined (size_t)
# define YYSIZE_T size_t
#endif
#if ! defined (YYSIZE_T)
# if defined (__STDC__) || defined (__cplusplus)
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# endif
#endif
#if ! defined (YYSIZE_T)
# define YYSIZE_T unsigned int
#endif

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		(-2)
#define YYEOF		0

#define YYACCEPT	goto yyacceptlab
#define YYABORT		goto yyabortlab
#define YYERROR		goto yyerrlab1


/* Like YYERROR except do call yyerror.  This remains here temporarily
   to ease the transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */

#define YYFAIL		goto yyerrlab

#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)					\
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    {								\
      yychar = (Token);						\
      yylval = (Value);						\
      yytoken = YYTRANSLATE (yychar);				\
      YYPOPSTACK;						\
      goto yybackup;						\
    }								\
  else								\
    { 								\
      yyerror ("syntax error: cannot back up");\
      YYERROR;							\
    }								\
while (0)

#define YYTERROR	1
#define YYERRCODE	256

/* YYLLOC_DEFAULT -- Compute the default location (before the actions
   are run).  */

#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)         \
  Current.first_line   = Rhs[1].first_line;      \
  Current.first_column = Rhs[1].first_column;    \
  Current.last_line    = Rhs[N].last_line;       \
  Current.last_column  = Rhs[N].last_column;
#endif

/* YYLEX -- calling `yylex' with the right arguments.  */

#ifdef YYLEX_PARAM
# define YYLEX yylex (YYLEX_PARAM)
#else
# define YYLEX yylex ()
#endif

/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)			\
do {						\
  if (yydebug)					\
    YYFPRINTF Args;				\
} while (0)

# define YYDSYMPRINT(Args)			\
do {						\
  if (yydebug)					\
    yysymprint Args;				\
} while (0)

# define YYDSYMPRINTF(Title, Token, Value, Location)		\
do {								\
  if (yydebug)							\
    {								\
      YYFPRINTF (stderr, "%s ", Title);				\
      yysymprint (stderr, 					\
                  Token, Value);	\
      YYFPRINTF (stderr, "\n");					\
    }								\
} while (0)

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (cinluded).                                                   |
`------------------------------------------------------------------*/

#if defined (__STDC__) || defined (__cplusplus)
static void
yy_stack_print (short *bottom, short *top)
#else
static void
yy_stack_print (bottom, top)
    short *bottom;
    short *top;
#endif
{
  YYFPRINTF (stderr, "Stack now");
  for (/* Nothing. */; bottom <= top; ++bottom)
    YYFPRINTF (stderr, " %d", *bottom);
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)				\
do {								\
  if (yydebug)							\
    yy_stack_print ((Bottom), (Top));				\
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

#if defined (__STDC__) || defined (__cplusplus)
static void
yy_reduce_print (int yyrule)
#else
static void
yy_reduce_print (yyrule)
    int yyrule;
#endif
{
  int yyi;
  unsigned int yylno = yyrline[yyrule];
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %u), ",
             yyrule - 1, yylno);
  /* Print the symbols being reduced, and their result.  */
  for (yyi = yyprhs[yyrule]; 0 <= yyrhs[yyi]; yyi++)
    YYFPRINTF (stderr, "%s ", yytname [yyrhs[yyi]]);
  YYFPRINTF (stderr, "-> %s\n", yytname [yyr1[yyrule]]);
}

# define YY_REDUCE_PRINT(Rule)		\
do {					\
  if (yydebug)				\
    yy_reduce_print (Rule);		\
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YYDSYMPRINT(Args)
# define YYDSYMPRINTF(Title, Token, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef	YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   SIZE_MAX < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#if YYMAXDEPTH == 0
# undef YYMAXDEPTH
#endif

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif



#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined (__GLIBC__) && defined (_STRING_H)
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
#   if defined (__STDC__) || defined (__cplusplus)
yystrlen (const char *yystr)
#   else
yystrlen (yystr)
     const char *yystr;
#   endif
{
  register const char *yys = yystr;

  while (*yys++ != '\0')
    continue;

  return yys - yystr - 1;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined (__GLIBC__) && defined (_STRING_H) && defined (_GNU_SOURCE)
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
#   if defined (__STDC__) || defined (__cplusplus)
yystpcpy (char *yydest, const char *yysrc)
#   else
yystpcpy (yydest, yysrc)
     char *yydest;
     const char *yysrc;
#   endif
{
  register char *yyd = yydest;
  register const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

#endif /* !YYERROR_VERBOSE */



#if YYDEBUG
/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

#if defined (__STDC__) || defined (__cplusplus)
static void
yysymprint (FILE *yyoutput, int yytype, YYSTYPE *yyvaluep)
#else
static void
yysymprint (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE *yyvaluep;
#endif
{
  /* Pacify ``unused variable'' warnings.  */
  (void) yyvaluep;

  if (yytype < YYNTOKENS)
    {
      YYFPRINTF (yyoutput, "token %s (", yytname[yytype]);
# ifdef YYPRINT
      YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# endif
    }
  else
    YYFPRINTF (yyoutput, "nterm %s (", yytname[yytype]);

  switch (yytype)
    {
      default:
        break;
    }
  YYFPRINTF (yyoutput, ")");
}

#endif /* ! YYDEBUG */
/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

#if defined (__STDC__) || defined (__cplusplus)
static void
yydestruct (int yytype, YYSTYPE *yyvaluep)
#else
static void
yydestruct (yytype, yyvaluep)
    int yytype;
    YYSTYPE *yyvaluep;
#endif
{
  /* Pacify ``unused variable'' warnings.  */
  (void) yyvaluep;

  switch (yytype)
    {

      default:
        break;
    }
}


/* Prevent warnings from -Wmissing-prototypes.  */

#ifdef YYPARSE_PARAM
# if defined (__STDC__) || defined (__cplusplus)
int yyparse (void *YYPARSE_PARAM);
# else
int yyparse ();
# endif
#else /* ! YYPARSE_PARAM */
#if defined (__STDC__) || defined (__cplusplus)
int yyparse (void);
#else
int yyparse ();
#endif
#endif /* ! YYPARSE_PARAM */



/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;

/* Number of syntax errors so far.  */
int yynerrs;



/*----------.
| yyparse.  |
`----------*/

#ifdef YYPARSE_PARAM
# if defined (__STDC__) || defined (__cplusplus)
int yyparse (void *YYPARSE_PARAM)
# else
int yyparse (YYPARSE_PARAM)
  void *YYPARSE_PARAM;
# endif
#else /* ! YYPARSE_PARAM */
#if defined (__STDC__) || defined (__cplusplus)
int
yyparse (void)
#else
int
yyparse ()

#endif
#endif
{
  
  register int yystate;
  register int yyn;
  int yyresult;
  /* Number of tokens to shift before error messages enabled.  */
  int yyerrstatus;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;

  /* Three stacks and their tools:
     `yyss': related to states,
     `yyvs': related to semantic values,
     `yyls': related to locations.

     Refer to the stacks thru separate pointers, to allow yyoverflow
     to reallocate them elsewhere.  */

  /* The state stack.  */
  short	yyssa[YYINITDEPTH];
  short *yyss = yyssa;
  register short *yyssp;

  /* The semantic value stack.  */
  YYSTYPE yyvsa[YYINITDEPTH];
  YYSTYPE *yyvs = yyvsa;
  register YYSTYPE *yyvsp;



#define YYPOPSTACK   (yyvsp--, yyssp--)

  YYSIZE_T yystacksize = YYINITDEPTH;

  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;


  /* When reducing, the number of symbols on the RHS of the reduced
     rule.  */
  int yylen;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY;		/* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */

  yyssp = yyss;
  yyvsp = yyvs;

  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed. so pushing a state here evens the stacks.
     */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
	/* Give user a chance to reallocate the stack. Use copies of
	   these so that the &'s don't force the real ones into
	   memory.  */
	YYSTYPE *yyvs1 = yyvs;
	short *yyss1 = yyss;


	/* Each stack pointer address is followed by the size of the
	   data in use in that stack, in bytes.  This used to be a
	   conditional around just the two extra args, but that might
	   be undefined if yyoverflow is a macro.  */
	yyoverflow ("parser stack overflow",
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),

		    &yystacksize);

	yyss = yyss1;
	yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyoverflowlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
	goto yyoverflowlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
	yystacksize = YYMAXDEPTH;

      {
	short *yyss1 = yyss;
	union yyalloc *yyptr =
	  (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
	if (! yyptr)
	  goto yyoverflowlab;
	YYSTACK_RELOCATE (yyss);
	YYSTACK_RELOCATE (yyvs);

#  undef YYSTACK_RELOCATE
	if (yyss1 != yyssa)
	  YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;


      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
		  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
	YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

/* Do appropriate processing given the current state.  */
/* Read a lookahead token if we need one and don't already have one.  */
/* yyresume: */

  /* First try to decide what to do without reference to lookahead token.  */

  yyn = yypact[yystate];
  if (yyn == YYPACT_NINF)
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = YYLEX;
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YYDSYMPRINTF ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yyn == 0 || yyn == YYTABLE_NINF)
	goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  if (yyn == YYFINAL)
    YYACCEPT;

  /* Shift the lookahead token.  */
  YYDPRINTF ((stderr, "Shifting token %s, ", yytname[yytoken]));

  /* Discard the token being shifted unless it is eof.  */
  if (yychar != YYEOF)
    yychar = YYEMPTY;

  *++yyvsp = yylval;


  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  yystate = yyn;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     `$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:
#line 97 "MWIDL_bis"
    {
		MIDLSemantics* topexp = PopExp();
		exps.push(new MIDLProgram(NULL, topexp));
		cout<<"exps.push(new MIDLProgram(NULL, topexp));"<<endl;
		
    ;}
    break;

  case 3:
#line 104 "MWIDL_bis"
    {
		MIDLSemantics* exp2 = PopExp();
		MIDLSemantics* exp1 = PopExp();
		
		exps.push(new MIDLProgram((MIDLProgram *)exp1, exp2));
		cout<<"exps.push(new MIDLProgram((MIDLProgram *)exp1, exp2));"<<endl;
		
    ;}
    break;

  case 4:
#line 113 "MWIDL_bis"
    {
		MIDLSemantics* topexp = PopExp();
		exps.push(new MIDLProgram(NULL, topexp));
		cout<<"exps.push(new MIDLProgram(NULL, interfacedecl));"<<endl;
	;}
    break;

  case 5:
#line 119 "MWIDL_bis"
    {
		MIDLSemantics* exp2 = PopExp();
		MIDLSemantics* exp1 = PopExp();
		
		exps.push(new MIDLProgram((MIDLProgram *)exp1, exp2));
		cout<<"exps.push(new MIDLProgram((MIDLProgram *)exp1, interfacedecl));"<<endl;
    ;}
    break;

  case 6:
#line 130 "MWIDL_bis"
    {
		 MIDLSemantics* exp = PopExp();
		 exps.push(new MIDLClassDeclation(yyvsp[-3].strval, (MIDLClassStmt* )exp));
         cout<<"exps.push(new MIDLClassDeclation($2, (MIDLClassStmt* )exp));"<<endl;
       ;}
    break;

  case 7:
#line 136 "MWIDL_bis"
    {
		MIDLSemantics* exp = PopExp();
		exps.push(new MIDLClassDeclation(yyvsp[-4].strval, (MIDLClassStmt* )exp));
    	cout<<"exps.push(new MIDLClassDeclation($2, (MIDLClassStmt* )exp)); semicolon"<<endl;
    ;}
    break;

  case 8:
#line 145 "MWIDL_bis"
    {
		MIDLSemantics* exp = PopExp();
		exps.push(new MIDLInterfaceDeclation(yyvsp[-3].strval, (MIDLInterfaceStmt* )exp));
		cout<<"exps.push(new MIDLInterfaceDeclation($2, (MIDLInterfaceStmt* )exp));"<<endl;
	;}
    break;

  case 9:
#line 151 "MWIDL_bis"
    {
	;}
    break;

  case 10:
#line 158 "MWIDL_bis"
    {
		MIDLSemantics* exp = PopExp();
		exps.push(new MIDLInterfaceStmt(yyvsp[-4].strval, NULL, (MIDLParameters* )exp));
		cout<<"exps.push(new MIDLInterfaceStmtExp($2, NULL, (MIDLParameters* )exp));"<<endl;
	;}
    break;

  case 11:
#line 164 "MWIDL_bis"
    {
		MIDLSemantics* exp2 = PopExp();
		MIDLSemantics* exp1 = PopExp();
		exps.push(new MIDLInterfaceStmt(yyvsp[-5].strval, NULL, (MIDLParameters* )exp1,(MIDLInterfaceStmt*)exp2));
		cout<<"exps.push(new MIDLInterfaceStmt($2, NULL, (MIDLParameters* )exp1, (MIDLInterfaceStmt*)exp2));"<<endl;
	;}
    break;

  case 12:
#line 171 "MWIDL_bis"
    {
		MIDLSemantics* exp = PopExp();
		exps.push(new MIDLInterfaceStmt(yyvsp[-3].strval, (MIDLTypeName* )exp, NULL));
		cout<<"exps.push(new MIDLInterfaceStmt($2, (MIDLTypeName* )exp1, NULL));"<<endl;
	;}
    break;

  case 13:
#line 177 "MWIDL_bis"
    {
		MIDLSemantics* exp2 = PopExp();
		MIDLSemantics* exp1 = PopExp();
		exps.push(new MIDLInterfaceStmt(yyvsp[-4].strval, (MIDLTypeName* )exp1, NULL,(MIDLInterfaceStmt*)exp2));
		cout<<"exps.push(new MIDLInterfaceStmt($2, (MIDLTypeName* )exp1, (MIDLInterfaceStmt*)exp2));"<<endl;
	;}
    break;

  case 14:
#line 184 "MWIDL_bis"
    {
		exps.push(new MIDLInterfaceStmt(yyvsp[-3].strval, NULL, NULL));
		cout<<"exps.push(new MIDLInterfaceStmt($2, NULL, NULL));"<<endl;
	;}
    break;

  case 15:
#line 189 "MWIDL_bis"
    {
		MIDLSemantics* exp = PopExp();
		exps.push(new MIDLInterfaceStmt(yyvsp[-4].strval, NULL, NULL,(MIDLInterfaceStmt*)exp));
		cout<<"exps.push(new MIDLInterfaceStmt($2, NULL, NULL, (MIDLInterfaceStmt*)exp)));"<<endl;
	;}
    break;

  case 16:
#line 195 "MWIDL_.bis"
    {
		MIDLSemantics* exp2 = PopExp();
		MIDLSemantics* exp1 = PopExp();
		exps.push(new MIDLInterfaceStmt(yyvsp[-4].strval, (MIDLTypeName* )exp1, (MIDLParameters* )exp2));
		cout<<"exps.push(new MIDLInterfaceStmt($2, (MIDLTypeName* )exp1, (MIDLParameters* )exp2));"<<endl;
	;}
    break;

  case 17:
#line 203 "MWIDL_bis"
    {
		MIDLSemantics* exp3 = PopExp();
		MIDLSemantics* exp2 = PopExp();
		MIDLSemantics* exp1 = PopExp();
		exps.push(new MIDLInterfaceStmt(yyvsp[-5].strval, (MIDLTypeName* )exp1, (MIDLParameters* )exp2, (MIDLInterfaceStmt*)exp3));
		cout<<"exps.push(new MIDLInterfaceStmt($2, (MIDLTypeName* )exp1, (MIDLParameters* )exp2, (MIDLInterfaceStmt*)exp3));"<<endl;
	;}
    break;

  case 19:
#line 220 "MWIDL_bis"
    {
		MIDLSemantics* exp2 = PopExp();
		MIDLSemantics* exp1 = PopExp();
		exps.push(new MIDLParameters((MIDLVariableDeclation* )exp1, (MIDLParameters* )exp2));
		cout<<"exps.push(new MIDLParameters(MIDLVarDeclExp* )exp1, (MIDLParameters* )exp2));"<<endl;
		
	;}
    break;

  case 20:
#line 228 "MWIDL_bis"
    {
		MIDLSemantics* exp = PopExp();
		exps.push(new MIDLParameters((MIDLVariableDeclation* )exp, NULL));
		cout<<"exps.push(new MIDLParameters((MIDLVariableDeclation* )exp, NULL));"<<endl;
	;}
    break;

  case 21:
#line 234 "MWIDL_bis"
    {
		exps.push(NULL);//void
		cout<<"exps.push(NULL);//void"<<endl;
	;}
    break;

  case 22:
#line 242 "MWIDL_bis"
    {
		MIDLSemantics* exp = PopExp();
		exps.push(new MIDLClassStmt((MIDLVariableDeclation* )exp, NULL));
		cout<<"exps.push(new MIDLClassStmt((MIDLVariableDeclation* )exp, NULL));"<<endl;
		
	;}
    break;

  case 23:
#line 249 "MWIDL_bis"
    {
		MIDLSemantics* exp2 = PopExp();
		MIDLSemantics* exp1 = PopExp();
		exps.push(new MIDLClassStmt((MIDLVariableDeclation* )exp1, (MIDLClassStmt* )exp2));
		cout<<"exps.push(new MIDLClassStmt((MIDLVariableDeclation* )exp, NULL));"<<endl;
		
	;}
    break;

  case 24:
#line 259 "MWIDL_bis"
    {
		MIDLSemantics* exp = PopExp();
		exps.push(new MIDLVariableDeclation((MIDLTypeName* )exp, yyvsp[0].strval, NULL));
		cout<<"exps.push(new MIDLVariableDeclation((MIDLTypeName* )exp, $2, NULL));"<<endl;
    ;}
    break;

  case 25:
#line 265 "MWIDL_bis"
    {
 	  	
		MIDLSemantics* exp2 = PopExp();
		MIDLSemantics* exp1 = PopExp();

		exps.push(new MIDLVariableDeclation((MIDLTypeName* )exp1, yyvsp[-1].strval, (MIDLDim* )exp2));
		cout<<"exps.push(new MIDLVariableDeclation((MIDLTypeName* )exp, $2, NULL));"<<endl;
   	;}
    break;

  case 26:
#line 276 "MWIDL_bis"
    {
		exps.push(new MIDLTypeName(yyvsp[0].strval));
		cout<<"exps.push(new MIDLTypeName($1));"<<endl;
	;}
    break;

  case 27:
#line 281 "MWIDL_bis"
    {
		MIDLSemantics * exp = PopExp();

		exps.push(new MIDLTypeName(yyvsp[-3].strval, (MIDLTypeName* )exp));
		cout<<"exps.push(new MIDLTypeName($1, (MIDLTypeName* )exp));"<<endl;
		
	;}
    break;

  case 28:
#line 289 "MWIDL_bis"
    {
		MIDLSemantics * exp = PopExp();
		exps.push(new MIDLTypeName((MIDLTypeName* )exp));
		cout<<"exps.push(new MIDLTypeName((MIDLTypeName* )exp));"<<endl;
		
	;}
    break;

  case 29:
#line 296 "MWIDL_bis"
    {
		cout<<"%not implemented^^&U&*"<<endl;		
	;}
    break;

  case 30:
#line 303 "MWIDL_bis"
    {
		MIDLSemantics* exp = PopExp();
		exps.push(new MIDLDim(yyvsp[-2].intval, (MIDLDim* )exp));
		cout<<"exps.push(new MIDLDim($2, (MIDLDim* )exp));"<<endl;
		
	;}
    break;

  case 31:
#line 310 "MWIDL_bis"
    {
		exps.push(new MIDLDim(yyvsp[-1].intval, NULL));
		cout<<"exps.push(new MIDLDim($2, NULL));"<<endl;
		
	;}
    break;


    }

/* Line 999 of yacc.c.  */
#line 1375 "MWIDL_bis.cpp"

  yyvsp -= yylen;
  yyssp -= yylen;


  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;


  /* Now `shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*------------------------------------.
| yyerrlab -- here on detecting error |
`------------------------------------*/
yyerrlab:
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if YYERROR_VERBOSE
      yyn = yypact[yystate];

      if (YYPACT_NINF < yyn && yyn < YYLAST)
	{
	  YYSIZE_T yysize = 0;
	  int yytype = YYTRANSLATE (yychar);
	  const char* yyprefix;
	  char *yymsg;
	  int yyx;

	  /* Start YYX at -YYN if negative to avoid negative indexes in
	     YYCHECK.  */
	  int yyxbegin = yyn < 0 ? -yyn : 0;

	  /* Stay within bounds of both yycheck and yytname.  */
	  int yychecklim = YYLAST - yyn;
	  int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
	  int yycount = 0;

	  yyprefix = ", expecting ";
	  for (yyx = yyxbegin; yyx < yyxend; ++yyx)
	    if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR)
	      {
		yysize += yystrlen (yyprefix) + yystrlen (yytname [yyx]);
		yycount += 1;
		if (yycount == 5)
		  {
		    yysize = 0;
		    break;
		  }
	      }
	  yysize += (sizeof ("syntax error, unexpected ")
		     + yystrlen (yytname[yytype]));
	  yymsg = (char *) YYSTACK_ALLOC (yysize);
	  if (yymsg != 0)
	    {
	      char *yyp = yystpcpy (yymsg, "syntax error, unexpected ");
	      yyp = yystpcpy (yyp, yytname[yytype]);

	      if (yycount < 5)
		{
		  yyprefix = ", expecting ";
		  for (yyx = yyxbegin; yyx < yyxend; ++yyx)
		    if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR)
		      {
			yyp = yystpcpy (yyp, yyprefix);
			yyp = yystpcpy (yyp, yytname[yyx]);
			yyprefix = " or ";
		      }
		}
	      yyerror (yymsg);
	      YYSTACK_FREE (yymsg);
	    }
	  else
	    yyerror ("syntax error; also virtual memory exhausted");
	}
      else
#endif /* YYERROR_VERBOSE */
	yyerror ("syntax error");
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
	 error, discard it.  */

      /* Return failure if at end of input.  */
      if (yychar == YYEOF)
        {
	  /* Pop the error token.  */
          YYPOPSTACK;
	  /* Pop the rest of the stack.  */
	  while (yyss < yyssp)
	    {
	      YYDSYMPRINTF ("Error: popping", yystos[*yyssp], yyvsp, yylsp);
	      yydestruct (yystos[*yyssp], yyvsp);
	      YYPOPSTACK;
	    }
	  YYABORT;
        }

      YYDSYMPRINTF ("Error: discarding", yytoken, &yylval, &yylloc);
      yydestruct (yytoken, &yylval);
      yychar = YYEMPTY;

    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*----------------------------------------------------.
| yyerrlab1 -- error raised explicitly by an action.  |
`----------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;	/* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (yyn != YYPACT_NINF)
	{
	  yyn += YYTERROR;
	  if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
	    {
	      yyn = yytable[yyn];
	      if (0 < yyn)
		break;
	    }
	}

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
	YYABORT;

      YYDSYMPRINTF ("Error: popping", yystos[*yyssp], yyvsp, yylsp);
      yydestruct (yystos[yystate], yyvsp);
      yyvsp--;
      yystate = *--yyssp;

      YY_STACK_PRINT (yyss, yyssp);
    }

  if (yyn == YYFINAL)
    YYACCEPT;

  YYDPRINTF ((stderr, "Shifting error token, "));

  *++yyvsp = yylval;


  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#ifndef yyoverflow
/*----------------------------------------------.
| yyoverflowlab -- parser overflow comes here.  |
`----------------------------------------------*/
yyoverflowlab:
  yyerror ("parser stack overflow");
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
  return yyresult;
}


#line 315 "MWSL_bis"



int main(int argc, char** argv)
{
	++argv, --argc;
	
	std::string target;
	
	if(argc > 0)
		target = argv[0];
	else
	{
		cout<<"Enter the IDL file name:"<<endl;
		cin>>target;
	}
	yyin = fopen( target.c_str(), "r" );
	int retv = yyparse();
	MIDLSemantics * root = exps.top();
	exps.pop();
	cout<<"Syntax Tree:"<<endl;
	root->PrintToFile(":");
		
	//action output
	std::string dest1 = target.substr(0,4) + "_prx.h";
	std::string dest2 = target.substr(0,4) + "_svr.h";
	//std::string dest3 = target.substr(0,4) + "_cls.h"; 
	
	GenerateStream strm1(dest1.c_str());
	strm1<<HEADER_FILE_PRAGMA<<"\nusing namespace MWSL;\n\n";
	GenerateStream strm2(dest2.c_str());
	strm2<<HEADER_FILE_PRAGMA<<"\nusing namespace MWSL;\n\n";
	//GenerateStream strm3(dest3.c_str());
	//strm3<<HEADER_FILE_PRAGMA<<"\n\nusing namespace MWSL;\n\n";
	root->GenerateProxy(strm1);
	root->GenerateStub(strm2);
	//root->GenerateE(strm3);
	cout<<"Code Generated to File: "<<dest1<<" & "<<dest2<<" & "<<endl;
	return retv;
}


int yyerror(char *s)
{
	fprintf(stderr,"%s\n",s);
	return 0;
}
