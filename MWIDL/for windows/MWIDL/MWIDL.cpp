#include "stdafx.h"
#include "MWIDL.h"
#include "assert.h"
#include <fstream>
#include <iostream>
using namespace std;
#define REPLACER "$$$"
#define REPLACERLEN (3) //strlen(REPLACER
#define PROX_ENCODE_HEADER( n ) "private:\n\t"\
	+n+"(::MWSL::SLCommunicator* conn, const ::MWSL::Identity id) \
: ::MWSL::ServantPrx(conn, id)\n\
\t{\n\
\t}\n\
public:\n\
\tstatic "+n+"* CreateInstance(::MWSL::SLCommunicator* conn , const char* name, const char* category)\n\
\t{\n\
\t\treturn new "+n+"(conn, ::MWSL::Identity( ::MWSL::String(name), ::MWSL::String(category)));\n\
\t}\n\
"

template<class T>
class ContextSaver
{
public:
	ContextSaver(T& globalVar, const T& newValue)
		: glo(globalVar)
	{
		tmp = globalVar;
		globalVar = newValue;
	}
	~ContextSaver()
	{
		glo = tmp;
	}
private:
	T& glo;
	T tmp;
};



static bool hasTemplate = false;
static std::string varhead = "_";
static std::string strmop = "->";
static std::string strmname = "_is";

GenerateStream& operator << (GenerateStream& strm, const std::string& str)
{
	std::string tmp = str;
	if(strm._indent.length() > 0)
	{
		int index = tmp.find('\n');
		while(index >= 0)
		{
			tmp.replace(index, 1, "\n"+strm._indent);
			index = tmp.find('\n', index+2);
		}
	}
	strm._implement<<tmp;
	return strm;
};

GenerateStream& operator << (GenerateStream& strm, char* str)
{	
	if(str[0] == '\n' && str[1] == '\0')
		strm._implement<<endl<<strm._indent;
	else
		strm<<std::string(str);
	return strm;
};

GenerateStream& operator << (GenerateStream& strm, int n)
{
	strm._implement<<n;
	return strm;
};

GenerateStream& operator << (GenerateStream& strm, double r)
{
	strm._implement<<r;
	return strm;
};

void RemoveReplacer(std::string& str)
{
	int repindex = str.find(REPLACER);
	while(repindex >= 0)
	{
		str.replace(repindex, REPLACERLEN, "");
		repindex = str.find(REPLACER,repindex);
	}
}

void MIDLProgram::PrintToFile(const std::string& str)
{
	if(_prog != NULL)
		_prog->PrintToFile(str+"  ");
	if(_clas != NULL)
		_clas->PrintToFile(str+"  ");
};

void MIDLClassDeclation::PrintToFile(const std::string& str)
{
	cout<<str<<"Class Decl: "<<_name<<endl;
	if(_stmt != NULL)
		_stmt->PrintToFile(str+"  ");
};


void MIDLClassStmt::PrintToFile(const std::string& str)
{
	cout<<str<<"Class Statement"<<endl;
	if(_decl != NULL)
		_decl->PrintToFile(str+"  ");
	if(_stmt != NULL)
		_stmt->PrintToFile(str+"  ");
};

void MIDLVariableDeclation::PrintToFile(const std::string& str)
{
	cout<<str<<"Var declaration:"<<_id<<endl;
	if(_type != NULL)
		_type->PrintToFile(str+"  ");
	if(_dim != NULL)
		_dim->PrintToFile(str+"  ");
};

void MIDLTypeName::PrintToFile(const std::string& str)
{
	cout<<str<<"TypeName: "<<_id<<"  ";
	if(_isPtr)
		cout<<"Ptr";
	if(_isTemplate)
		cout<<"Template";
	cout<<endl;
	if(_type != NULL)
		_type->PrintToFile(str+"  ");
};

void MIDLDim::PrintToFile(const std::string& str)
{
	cout<<str<<"Dimension: "<<_digi<<endl;
	if(_dim != NULL)
		_dim->PrintToFile(str+"  ");
};

void MIDLParameters::PrintToFile(const std::string& str)
{
	this->_var->PrintToFile(str);	
	if(this->_params)
		_params->PrintToFile(str);
};

void MIDLInterfaceDeclation::PrintToFile(const std::string& str)
{
	cout<<str<<"interface:"<<this->_name<<endl;
	this->_stmt->PrintToFile(str+"\t");
};

void MIDLInterfaceStmt::PrintToFile(const std::string& str)
{
	cout<<str<<"Method name:"<<this->_id<<endl;
	cout<<str<<"Return Type:"<<endl;
	if(_rettype == NULL)
		cout<<str<<"void"<<endl;
	else
		this->_rettype->PrintToFile(str+"\t");
	cout<<str<<"Params List:"<<endl;
	if(_params == NULL)
		cout<<str<<"void"<<endl;
	else
		this->_params->PrintToFile(str+"\t");
	if(_stmt)
		this->_stmt->PrintToFile(str);
};


////////////////////////encode/////////////////////////

void MIDLProgram::GenerateE(GenerateStream& stream)
{
	if(_prog)
		_prog->GenerateE(stream);
	if(_clas)
		_clas->GenerateE(stream);
}

void MIDLProgram::GenerateProxy(GenerateStream& stream)
{
	if(_prog)
		_prog->GenerateProxy(stream);
	if(_clas)
		_clas->GenerateProxy(stream);
}

void MIDLClassDeclation::GenerateE(GenerateStream& stream)
{	
	ContextSaver<string> setstrmname(strmname, "stream");
	ContextSaver<string> setvarhead(varhead, "");
	ContextSaver<string> setstrmop(strmop, ".");
	stream<<"class "<<this->_name<<" : public ::MWSL::ProxyClass\n"<<"{\n";

	//data members
	stream<<"public:\n";
	MIDLClassStmt* tm = this->_stmt;
	while(tm != NULL)
	{
		if(tm->_decl != NULL)
			stream<<"\t" << tm->_decl->ToString()<<";\n";
		tm = tm->_stmt;
	}
	stream<<"\n";

	//overriden methods
	stream<<"public:\n\n\tvirtual void "<<"Encode(MWSL::SLStream& stream) const"<<"\n"<<"\t{";
	stream<<"\n";
	if(_stmt)
		_stmt->GenerateE(stream);
	stream<<"\t}"<<"\n"<<"\n";

	hasTemplate = false;
	stream<<"\tvirtual void "<<"Decode(MWSL::SLStream& stream)"<<"\n"<<"\t{";
	stream<<"\n";
	if(_stmt)
		_stmt->GenerateD(stream);
	stream<<"\t}"<<"\n";
	stream<<"};\n\n";	
}

void MIDLClassStmt::GenerateE(GenerateStream& stream)
{
	_decl->GenerateE(stream);
	if(_stmt)
		_stmt->GenerateE(stream);
};

void MIDLVariableDeclation::GenerateProxy(GenerateStream& stream)
{
	std::string buf, indent;
	assert(_type != NULL);
	_type->GenerateConcreteE(stream, buf, this->_id);
	if(_dim != NULL)
	{
		_dim->GenerateConcreteE(stream, buf, buf,indent);
		stream<<"\n";		
	}
	else
	{
		RemoveReplacer(buf);
		stream<<buf;
		stream<<"\n";
	}
};

void MIDLTypeName::GenerateConcreteE(GenerateStream& stream, std::string & output, const std::string & variableName, int n)
{
	std::string varnew = variableName+REPLACER;
	char tmp[512];
	if(!this->_isTemplate && !this->_isPtr)//int b;
	{
		output = "\t\t" + strmname + strmop +"write("+varnew+");";
	}
	else if(this->_isPtr)
	{
		std::string stars = "*";
		MIDLTypeName* chdtp = _type;
		while(chdtp && chdtp->_isPtr)
		{
			stars += "*";
			chdtp = chdtp->_type;
		}
		output = "\t\t" + strmname + strmop + "write("+stars+varnew+");";
	}
	else if(this->_isTemplate && this->_id == "vector")//vector<int>
	{
		sprintf(tmp, "%s%swrite(%s.size());\nfor (int __v%d = 0; __v%d < %s.size(); __v%d++)\n{\n\t%s%swrite(%s[__v%d]);\n}", 
			strmname.c_str(), strmop.c_str(), varnew.c_str(), n+1, n+1, varnew.c_str(), n+1, strmname.c_str(), 
			strmop.c_str(), varnew.c_str(), n+1);
		output = tmp;
	}	
}


void MIDLDim::GenerateConcreteE(GenerateStream& stream, std::string & output, std::string & str, std::string& indent, int i)
{
	char arr[10];
	sprintf(arr,"__i%d", i+1);
	std::string var = arr;
	int repindex = str.find(REPLACER);
	assert(repindex >= 0);
	while(repindex >= 0)
	{
		str.replace(repindex, 3, std::string("[")+var+"]"+REPLACER);
		repindex = str.find(REPLACER,repindex+8);
	}

	char tmp[256];
	sprintf(tmp, "%sfor(int %s = 0; %s < %d; %s++)\n",indent.c_str(), var.c_str(), var.c_str(), _digi, var.c_str());

	std::string curidt = indent;

	stream<<tmp<<indent<<"{"<<"\n";
	if(_dim != NULL)
	{
		indent += "\t";
		_dim->GenerateConcreteE(stream, output, str, indent, i+1);
		stream<<"\n"<<curidt<<"}";
	}
	else//the deepest nested one
	{
		indent += "\t";
		output = indent + str;	
		//find other lines inside this section , add indents to them
		repindex = output.find("\n");
		while(repindex >= 0)
		{

			output.replace(repindex, 1, "\n\t\t"+indent);
			repindex = output.find("\n",repindex+2);
		}
		RemoveReplacer(output);
		stream<<output<<"\n"<<curidt<<"}";		
	}
};


//////////////////////////////////////decode////////////////////////////////////


void MIDLProgram::GenerateStub(GenerateStream& stream)
{
	if(_prog)
		_prog->GenerateStub(stream);
	if(_clas)
		_clas->GenerateStub(stream);
}

void MIDLProgram::GenerateD(GenerateStream& stream)
{
	assert(false);
	if(_prog)
		_prog->GenerateD(stream);
	if(_clas)
		_clas->GenerateD(stream);
}

void MIDLClassDeclation::GenerateD(GenerateStream& stream)
{
	hasTemplate = false;
	stream<<"//decoding code for class "<<_name<<"\n";
	stream<<"void "<<this->_name<<"::Decode(MWSL::SLStream& stream)"<<"\n"<<"{"<<"\n";
	if(_stmt)
		_stmt->GenerateD(stream);
	stream<<"}"<<"\n"<<"\n";
}

void MIDLClassStmt::GenerateD(GenerateStream& stream)
	{
	_decl->GenerateD(stream);
	if(_stmt)
		_stmt->GenerateD(stream);
	};

void MIDLVariableDeclation::GenerateStub(GenerateStream& stream)
{
	std::string buf, indent;
	assert(_type != NULL);
	_type->GenerateConcreteD(stream, buf, this->_id);
	if(_dim != NULL)
	{
		_dim->GenerateConcreteD(stream, buf, buf,indent);
		stream<<"\t\t\t\n";
	}
	else
	{
		RemoveReplacer(buf);
		stream<<buf<<"\n";
	}
};

void MIDLTypeName::GenerateConcreteD(GenerateStream& stream, std::string & output, const std::string & varname, int n)
{
	std::string varnew = varhead+varname+REPLACER;
	char tmp[512];
	if(!this->_isTemplate && !this->_isPtr)//int b;
	{
		output = "\t\t\t" + strmname + strmop + "read("+varnew+");";
	}
	else if(this->_isPtr)
	{
		std::string stars = "*";
		MIDLTypeName* chdtp = _type;
		while(chdtp && chdtp->_isPtr)
		{
			stars += "*";
			chdtp = chdtp->_type;
		}
		output = "\t\t\t" + strmname + strmop +"read("+stars+varnew+");";
	}
	else if(this->_isTemplate && this->_id == "vector")//vector<int>
	{
		std::string varlen;
		if(!hasTemplate)
			varlen = "int __length;\n";
		sprintf(tmp, "%s%s%sread(__length);\nfor (int __v%d = 0; __v%d < __length; __v%d++)\n{\n\t%s%sread(%s[v%d]);\n}", 
			varlen.c_str(), strmname.c_str(), strmop.c_str(), n+1, n+1, n+1, strmname.c_str(), 
			strmop.c_str(), varnew.c_str(), n+1);
		output = tmp;
		hasTemplate = true;
	}	
}


void MIDLDim::GenerateConcreteD(GenerateStream& stream, std::string & output, std::string & str, std::string& indent, int i)
{
	char arr[10];
	sprintf(arr,"__i%d", i+1);
	std::string var = arr;
	int repindex = str.find(REPLACER);
	assert(repindex >= 0);
	while(repindex >= 0)
	{
		str.replace(repindex, 3, std::string("[")+var+"]"+REPLACER);
		repindex = str.find(REPLACER,repindex+8);
	}

	char tmp[256];
	sprintf(tmp, "%sfor(int %s = 0; %s < %d; %s++)\n",indent.c_str(), var.c_str(), var.c_str(), _digi, var.c_str());


	std::string curidt = indent;

	stream<<tmp<<indent<<"{"<<"\n";
	if(_dim != NULL)
	{
		indent += "\t";
		_dim->GenerateConcreteD(stream, output, str, indent, i+1);
		stream<<"\n"<<curidt<<"}";
	}
	else//the deepest nested one
	{
		indent += "\t";
		output = indent + str;	
		//find other lines inside this section , add indents to them
		repindex = output.find("\n");
		while(repindex >= 0)
		{
			//indent += '\t';
			output.replace(repindex, 1, "\n"+indent);
			repindex = output.find("\n",repindex+2);
		}
		RemoveReplacer(output);
		stream<<output<<"\n"<<curidt<<"}";
		//output = str;
	}
};

//////////////helper methods//////////////////////////
std::string MIDLTypeName::ToString()const
{
	if(this->_isPtr && _type != NULL)
		return _type->ToString()+"*";
	if(this->_isTemplate && _type != NULL)
		return this->_id+"<"+_type->ToString()+">";
	return "::MWSL::"+this->_id;
};

std::string MIDLParameters::ToString()const
{
	if(this->_params == NULL)
		return this->_var->ToString();
	return this->_var->ToString()+std::string(",")+_params->ToString();
};

std::string MIDLInterfaceStmt::ToString()const
{
	std::string retv = (_rettype == NULL) ? "void" : _rettype->ToString();
	std::string params = (_params == NULL) ? "void" : _params->ToString();
	std::string prototp = retv+" "+this->_id+"("+params+")";
	return prototp;
};

std::string MIDLParameters::GetParameterList()const
{
	string sglvar = varhead+this->_var->_id;
	if(this->_params == NULL)
		return sglvar;
	return sglvar+std::string(",")+_params->GetParameterList();
};

std::string MIDLVariableDeclation::ToString()const
{
	assert(_type != NULL);
	if(_dim == NULL)
		return _type->ToString() + " "+this->_id;
	else
		return _type->ToString() + " "+this->_id + this->_dim->ToString();
};

std::string MIDLDim::ToString()const
{
	char buf[256];
	sprintf(buf, "[%d]", _digi);
	std::string str = buf;
	if(_dim != NULL)
		return str + _dim->ToString();
	else
		return str;
};

/////////////////interface Proxy/Stub///////////////////////
void MIDLInterfaceDeclation::GenerateProxy(GenerateStream& stream)
{
	if(_stmt)
	{
		stream<<"class "+_name<<"Prox : public ::MWSL::ServantPrx"<<"\n"<<"{\n";
		stream<<PROX_ENCODE_HEADER(_name+"Prox");
		_stmt->GenerateProxy(stream);
		stream<<"};\n\n\n";
	}
};

void MIDLInterfaceDeclation::GenerateStub(GenerateStream& stream)
{
	stream<<"class "+_name<<": public ::MWSL::Servant"<<"\n"<<"{\n";
	stream<<"public:\n"<<"\tvirtual int _Dispatch(::MWSL::MWSLIn &in, const ::MWSL::Identity &id, const ::MWSL::String &operation)\n\t{\n";
	_stmt->GenerateStub(stream);
	stream<<"\t\treturn 0;//failed\n\t}\n";

	MIDLInterfaceStmt* itfstmt = _stmt;
	while(itfstmt != NULL)
	{
		stream<<"\tvirtual "<<itfstmt->ToString()<<" = 0;\n";
		itfstmt = itfstmt->_stmt;
	}
	stream<<"};\n\n\n";
};

void MIDLInterfaceStmt::GenerateProxy(GenerateStream& stream)
{
	std::string retv = (_rettype == NULL) ? "void" : _rettype->ToString();
	std::string params = (_params == NULL) ? "void" : _params->ToString();
	std::string prototp = "\t" + retv+" "+this->_id+"("+params+")";

	stream<<prototp<<"\n";
	stream<<"\t{\n";
	stream<<"\t\t::MWSL::MWSLOut out(_conn, _id, ::MWSL::String(\""<<this->_id<<"\") , SYNC)"<<";\n";
	stream<<"\t\t::MWSL::SLStream *_is = out.getOS();\n";
	stream<<"\t\t/* Write Parameters */\n";
	if(_params != NULL)
		_params->GenerateProxy(stream);
	stream<<"\t\t/* Invoke the operation */\n";
	stream<<"\t\tout.Invoke();\n";
	stream<<"\t\t/* Request Finish */\n";
	stream<<"\t\tout.Finish();\n";
	if(_rettype != NULL)
	{
		stream<<"\t\t::MWSL::SLStream *_os = out.getIS();\n";
		stream<<"\t\t" << retv<<" retval;\n";
		stream<<"\t\t_os->read(retval);\n\t\treturn retval;\n";
	}
	stream<<"\t}\n";

	
	//the next statement
	if(_stmt)
		_stmt->GenerateProxy(stream);
};

void MIDLInterfaceStmt::GenerateStub(GenerateStream& stream)
{
	std::string prototp = "\t\tif (operation == \""+this->_id+"\")";
	stream<<prototp<<"\n";
	stream<<"\t\t{\n";

	stream<<"\t\t\t::MWSL::SLStream* _is = in.getIS();\n\t\t\t::MWSL::SLStream* _os = in.getOS();\n\t\t\tin.readParamHead();\n";
	if(_params != NULL)//has more than 0 parameters
		_params->GenerateStub(stream);
	std::string paramlist = _params == NULL ? "" : _params->GetParameterList();

	if(_rettype != NULL)
	{
		stream<<"\t\t\t"<<_rettype->ToString()<<" retv = "<<this->_id<<"("<<paramlist<<");\n";
		stream<<"\t\t\t_os->write((Byte)0);/* Reply state success */\n";
		stream<<"\t\t\t_os->write(retv);\n";
	}
	else
	{
		stream<<"\t\t\t" << this->_id<<"("<<paramlist<<");\n";
		stream<<"\t\t\t_os->write((Byte)0);/* Reply state success */\n";
	}


	stream<<"\t\t\treturn 1;"<<"\n";
	stream<<"\t\t}\n";
	//the next statement
	if(_stmt)
		_stmt->GenerateStub(stream);

};

void MIDLParameters::GenerateProxy(GenerateStream& stream)
{
	if(_var != NULL)
		_var->GenerateProxy(stream);
	if(this->_params != NULL)
		_params->GenerateProxy(stream);
};

void MIDLParameters::GenerateStub(GenerateStream& stream)
{
	if(_var != NULL)
	{	
		if(_var->_dim == NULL)
			stream<< "\t\t\t" <<  _var->_type->ToString()<<" _"<<_var->_id<<";\n";
		else
			stream<< "\t\t\t" <<  _var->_type->ToString()<<" _"<<_var->_id<<_var->_dim->ToString()<<";\n";
		_var->GenerateStub(stream);
	}
	if(this->_params != NULL)
		_params->GenerateStub(stream);
};

void MIDLParameters::GenerateE(GenerateStream& stream)
{
	if(_var != NULL)
		_var->GenerateE(stream);
	if(this->_params != NULL)
		_params->GenerateE(stream);
};

void MIDLParameters::GenerateD(GenerateStream& stream)
{
	if(_var != NULL)
		_var->GenerateD(stream);
	if(this->_params != NULL)
		_params->GenerateD(stream);
};

void MIDLVariableDeclation::GenerateE(GenerateStream& stream)
{
	std::string buf, indent;// = "stream<<" + _id + REPLACER +";";
	assert(_type != NULL);
	_type->GenerateConcreteE(stream, buf, this->_id);
	if(_dim != NULL)
	{
		_dim->GenerateConcreteE(stream, buf, buf,indent);
		stream<<"\n";		
	}
	else
	{
		RemoveReplacer(buf);
		stream<<buf;
		stream<<"\n";
	}
};

void MIDLVariableDeclation::GenerateD(GenerateStream& stream)
{
	std::string buf, indent;
	assert(_type != NULL);
	_type->GenerateConcreteD(stream, buf, this->_id);
	if(_dim != NULL)
	{
		_dim->GenerateConcreteD(stream, buf, buf,indent);
		stream<<"\n";
	}
	else
	{
		RemoveReplacer(buf);
		stream<<buf<<"\n";
	}
};
