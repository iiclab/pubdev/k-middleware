/* A Bison parser, made by GNU Bison 1.875b.  */

/* Skeleton parser for Yacc-like parsing with Bison,
Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003 Free Software Foundation, Inc.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2, or (at your option)
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330,
Boston, MA 02111-1307, USA.  */

/* As a special exception, when this file is copied by Bison into a
Bison output file, you may use that output file without restriction.
This special exception was added by the Free Software Foundation
in version 1.24 of Bison.  */

/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
/* Put the tokens into the symbol table, so that GDB and other debuggers
know about them.  */
enum yytokentype {
	classDeclT = 258,
	interfaceDeclT = 259,
	lParentT = 260,
	rParentT = 261,
	lBracketT = 262,
	rBracketT = 263,
	lBraceT = 264,
	rBraceT = 265,
	lCheveronT = 266,
	rCheveronT = 267,
	commaT = 268,
	semicolonT = 269,
	andT = 270,
	starT = 271,
	voidT = 272,
	atT = 273,
	digitT = 274,
	idT = 275
	};
#endif
#define classDeclT 258
#define interfaceDeclT 259
#define lParentT 260
#define rParentT 261
#define lBracketT 262
#define rBracketT 263
#define lBraceT 264
#define rBraceT 265
#define lCheveronT 266
#define rCheveronT 267
#define commaT 268
#define semicolonT 269
#define andT 270
#define starT 271
#define voidT 272
#define atT 273
#define digitT 274
#define idT 275




#if ! defined (YYSTYPE) && ! defined (YYSTYPE_IS_DECLARED)
#line 42 "MWDIL_bis"
typedef union YYSTYPE {
	char strval[50];
	int intval;
	double dblval;
	} YYSTYPE;
/* Line 1252 of yacc.c.  */
#line 83 "MWDIL_bis.hpp"
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
# define YYSTYPE_IS_TRIVIAL 1
#endif

extern YYSTYPE yylval;



