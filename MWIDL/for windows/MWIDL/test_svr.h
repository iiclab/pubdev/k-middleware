#pragma once

#include "MWSL.h"
#include "test_cls.h"

using namespace MWSL;

class CTest: public Servant
{
public:
	virtual int _Dispatch( MWSLIn &in, const Identity &id, const String &operation )
	{
		if ( operation == "HelloWorld" )
		{
			BasicStream* _is = in.getIS();
			BasicStream* _os = in.getOS();
			in.ReadParamHead();
			String __str;
			_is->read(__str);
			HelloWorld(__str);
			_os->write( (Byte)0 );/* Reply state success */
			return 1;
		}
		if ( operation == "GetInverse" )
		{
			BasicStream* _is = in.getIS();
			BasicStream* _os = in.getOS();
			in.ReadParamHead();
			int __i;
			_is->read(__i);
			int retv = GetInverse(__i);
			_os->write( (Byte)0 );/* Reply state success */
			_os->write(retv);
			return 1;
		}
		return 0;//failed
	}
	virtual void HelloWorld(String str) = 0;
	virtual int GetInverse(int i) = 0;
};


