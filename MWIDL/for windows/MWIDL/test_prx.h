#pragma once

#include "MWSL.h"
#include "test_cls.h"

using namespace MWSL;

class CTestProx : public ServantPrx
{
private:
	CTestProx(SLConnection* conn, const Identity id) : ServantPrx(conn, id)
	{
	}
public:
	static CTestProx* CreateInstance(SLConnection* conn , const char* name, const char* category)
	{
		return new CTestProx(conn, Identity( String(name), String(category)));
	}
	void HelloWorld(String str)
	{
		MWSLOut out( __conn, __id, String("HelloWorld") , SYNC );
		BasicStream *_is = out.getOS();
		/* Write Parameters */
		_is->write(str);
		/* Invoke the operation */
		out.Invoke();
		/* Request Finish */
		out.Finish();
	}
	int GetInverse(int i)
	{
		MWSLOut out( __conn, __id, String("GetInverse") , SYNC );
		BasicStream *_is = out.getOS();
		/* Write Parameters */
		_is->write(i);
		/* Invoke the operation */
		out.Invoke();
		/* Request Finish */
		out.Finish();
		BasicStream *_os = out.getIS();
		int retval;
		_os->read(retval);
		return retval;
	}
};


