#include "MWIDL_bis.h"
#include <string>
#include <fstream>
#include <vector>
#include <iostream>

class MIDLSemantics;
class MIDLProgram;
class MIDLClassDeclation;
class MIDLInterfaceDeclation;
class MIDLClassStmt;
class MIDLInterfaceStmt;
class MIDLParameters;
class MIDLVariableDeclation;
class MIDLTypeName;
class MIDLDim;
class GenerateStream;

using namespace std;

#pragma warning (disable:4996)
#pragma warning (disable:4267)

#define HEADER_FILE_PRAGMA "#pragma once\n\n\
#include \"MWSL.h\"\n"

//fstream wrapper
class GenerateStream
{
public:
	GenerateStream(const char* filepath)
		: _implement(filepath)
	{}
public:
	void SetIndent(const std::string& str)
	{
		_indent = str;
	};
	void WriteHeaderfile(const char* str = HEADER_FILE_PRAGMA)
	{
		_implement<<str;
	};
protected:
	std::string _indent;
	std::ofstream _implement;
	friend GenerateStream& operator << (GenerateStream&, const std::string&);
	friend GenerateStream& operator << (GenerateStream&, char*);
	friend GenerateStream& operator << (GenerateStream&, int);
	friend GenerateStream& operator << (GenerateStream&, double);
};

GenerateStream& operator << (GenerateStream&, const std::string&);
GenerateStream& operator << (GenerateStream&, char*);
GenerateStream& operator << (GenerateStream&, int);
GenerateStream& operator << (GenerateStream&, double);

//semantics
class MIDLSemantics
{
public:
	MIDLSemantics()	{};
	virtual ~MIDLSemantics(){};
	virtual void GenerateE(GenerateStream&){};
	virtual void GenerateD(GenerateStream&){};
	virtual void GenerateProxy(GenerateStream&){};
	virtual void GenerateStub(GenerateStream&){};
	virtual void PrintToFile(const std::string&) = 0;
public:
	int pos;
};

class MIDLProgram : public MIDLSemantics
{
public:
	MIDLProgram(MIDLProgram* prog, MIDLSemantics* clas)
		: _prog(prog)
		, _clas(clas)
	{}
	virtual void PrintToFile(const std::string&);
	virtual void GenerateE(GenerateStream&);
	virtual void GenerateD(GenerateStream&);
	virtual void GenerateProxy(GenerateStream&);
	virtual void GenerateStub(GenerateStream&);

public:
	MIDLProgram* _prog;
	MIDLSemantics* _clas;
};

class MIDLClassDeclation : public MIDLSemantics
{
public:
	MIDLClassDeclation(const std::string& name, MIDLClassStmt* stmt)
		: _name (name)
		, _stmt (stmt)
	{}
	virtual void PrintToFile(const std::string&);
	virtual void GenerateE(GenerateStream&);
	virtual void GenerateD(GenerateStream&);
public:
	std::string _name;
	MIDLClassStmt * _stmt;
};


class MIDLInterfaceDeclation : public MIDLSemantics
{
public:
	MIDLInterfaceDeclation(const std::string& name, MIDLInterfaceStmt* stmt)
		: _name (name)
		, _stmt (stmt)
	{}
	virtual void PrintToFile(const std::string&);
	virtual void GenerateProxy(GenerateStream&);
	virtual void GenerateStub(GenerateStream&);
public:
	std::string _name;
	MIDLInterfaceStmt* _stmt;
};


class MIDLInterfaceStmt : public MIDLSemantics
{
public:
	MIDLInterfaceStmt(const std::string& id, MIDLTypeName* rettype, MIDLParameters* params, MIDLInterfaceStmt* stmt = NULL)
		: _id(id)
		, _rettype(rettype)
		, _params(params)
		, _stmt(stmt)
		
		{}
	virtual void PrintToFile(const std::string&);
	virtual void GenerateProxy(GenerateStream&);
	virtual void GenerateStub(GenerateStream&);
	
	std::string ToString()const;
public:
	
	std::string _id;
	MIDLTypeName* _rettype;
	MIDLParameters* _params;
	MIDLInterfaceStmt* _stmt;
};


class MIDLParameters : public MIDLSemantics
{
public:
	MIDLParameters(MIDLVariableDeclation* var, MIDLParameters* params)
		: _var(var)
		, _params(params)
	{
	}
	virtual void PrintToFile(const std::string&);
	virtual void GenerateE(GenerateStream&);
	virtual void GenerateD(GenerateStream&);
	virtual void GenerateProxy(GenerateStream&);
	virtual void GenerateStub(GenerateStream&);
	std::string ToString()const;
	std::string GetParameterList()const;
public:
	MIDLVariableDeclation* _var;
	MIDLParameters* _params;
};

class MIDLClassStmt : public MIDLSemantics
{
public:
	MIDLClassStmt(MIDLVariableDeclation* vdecl, MIDLClassStmt* cstmt)
		: _decl(vdecl)
		, _stmt(cstmt)
	{}
	virtual void PrintToFile(const std::string&);
	virtual void GenerateE(GenerateStream&);
	virtual void GenerateD(GenerateStream&);
public:
	MIDLVariableDeclation* _decl;
	MIDLClassStmt* _stmt;
};


class MIDLVariableDeclation : public MIDLSemantics
{
public:
	MIDLVariableDeclation(MIDLTypeName* typen, const std::string& id, MIDLDim* dim)
		: _type(typen)
		, _id(id)
		, _dim(dim)
		{}
public:
	virtual void PrintToFile(const std::string&);
	virtual void GenerateE(GenerateStream&);
	virtual void GenerateD(GenerateStream&);
	virtual void GenerateProxy(GenerateStream&);
	virtual void GenerateStub(GenerateStream&);
	std::string ToString()const;

public:
	MIDLTypeName* _type;
	std::string _id;
	MIDLDim* _dim; //if not null, var is an array
};

class MIDLTypeName : public MIDLSemantics
{
public:
	 MIDLTypeName( MIDLTypeName* type)//ptr
		: _isPtr(true)
		, _type(type)
		, _isTemplate(false)
		, _id("N/A")
	{}
	 MIDLTypeName(const std::string& id,  MIDLTypeName* type)//template
		: _isPtr(false)
		, _type(type)
		, _isTemplate(true)
		, _id(id)
	{}
	 MIDLTypeName(const std::string& id)//identifier
		: _isPtr(false)
		, _isTemplate(false)
		, _type(NULL)
		, _id(id)
		{}

public:
	virtual void PrintToFile(const std::string& str);
	void GenerateConcreteE(GenerateStream&, std::string &, const std::string &, int n = 0);
	void GenerateConcreteD(GenerateStream&, std::string &, const std::string &, int n = 0);
	std::string ToString()const;
public:
	bool _isPtr;
	bool _isTemplate;
	std::string _id;
	MIDLTypeName* _type;
};

class MIDLDim : public MIDLSemantics
{
public:
	MIDLDim(int digi, MIDLDim* dim)
		: _digi(digi)
		, _dim(dim)
	{}
public:
	virtual void PrintToFile(const std::string&);
	void GenerateConcreteE(GenerateStream&, std::string &, std::string &, std::string& indent, int i = 0);
	void GenerateConcreteD(GenerateStream&, std::string &, std::string &, std::string& indent, int i = 0);
	std::string ToString()const;
public:
	int _digi;
	MIDLDim* _dim;
};