#pragma once

#include "MWSL.h"


using namespace MWSL;

class B : public SLClass
{
public:
	int sum;
	String msg;

public:

	virtual void Encode( MWSL::BasicStream& stream ) const
	{
		stream.write(sum);
		stream.write(msg);
	}

	virtual void Decode( MWSL::BasicStream& stream )
	{
			stream.read(sum);
			stream.read(msg);
	}
};

